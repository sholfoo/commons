package com.miru.miruApps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.model.response.Payment;
import com.miru.miruApps.ui.activity.payment.MiruPaymentDetailActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentAdapter extends BaseListRecyclerAdapter<PaymentAdapter.PaymentHolder, Payment.PaymentEntity> {

    List<Payment.PaymentEntity> listFiltered = new ArrayList<>();

    public PaymentAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public PaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PaymentHolder(getViewFromId(R.layout.list_item_payment, parent));
    }

    @Override
    public void onBindViewHolder(PaymentHolder holder, Payment.PaymentEntity payment, int position) {
        holder.salesName.setText(payment.getCustomerStoreName());
        holder.paymentId.setText(payment.getExtPaymentId());
        holder.paymentInvoiceNo.setText(payment.getExtInvoiceId());
        holder.paymentDueDate.setText(DateTimeUtil.getConvertedFormat(
                payment.getInvoiceDueDate(),
                "dd-MM-yyyy",
                "dd MMM yyyy"));
        holder.paymentDate.setText(DateTimeUtil.getConvertedFormat(
                payment.getPaymentDate(),
                "dd-MM-yyyy",
                "dd MMM yyyy"));
        holder.paymentAmount.setText(FormatNumber.getFormatCurrencyIDR(payment.getPaymentAmount()));
        holder.listItemPayment.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, MiruPaymentDetailActivity.class);
            intent.putExtra("paymentId", payment.getPaymentId());
            intent.putExtra("extPaymentId", payment.getExtPaymentId());
            intent.putExtra("paymentDate", payment.getPaymentDate());
            intent.putExtra("userName", payment.getCustomerName());
            intent.putExtra("customerName", payment.getCustomerName());
            intent.putExtra("totalPayment", payment.getPaymentAmount());
            intent.putExtra("paymentMethod", payment.getPaymentMethod());
            intent.putExtra("notes", payment.getNotes());
            intent.putExtra("invoiceId", payment.getInvoiceId());
            intent.putExtra("extInvoiceId", payment.getExtInvoiceId());
            intent.putExtra("docPathUrl", payment.getDocUrlPath());
            mContext.startActivity(intent);
        });
    }

    public class PaymentHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sales_name) TextView salesName;
        @BindView(R.id.payment_id) TextView paymentId;
        @BindView(R.id.payment_invoice_no) TextView paymentInvoiceNo;
        @BindView(R.id.payment_due_date) TextView paymentDueDate;
        @BindView(R.id.payment_date) TextView paymentDate;
        @BindView(R.id.payment_amount) TextView paymentAmount;
        @BindView(R.id.list_item_payment) LinearLayout listItemPayment;

        public PaymentHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = getListData();
                } else {
                    List<Payment.PaymentEntity> filteredList = new ArrayList<>();
                    for (Payment.PaymentEntity row : getListData()) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCustomerName().toLowerCase().contains(charString.toLowerCase()) || row.getPaymentId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListData = (ArrayList<Payment.PaymentEntity>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
