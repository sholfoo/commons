package com.miru.miruApps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.ui.activity.invoice.MiruMainDetailInvoiceActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.miru.miruApps.config.Constants.DUE_DATE;
import static com.miru.miruApps.config.Constants.FORMAT_DATE;
import static com.miru.miruApps.config.Constants.OVERDUE;
import static com.miru.miruApps.config.Constants.PARTIAL_PAYMENT;
import static com.miru.miruApps.config.Constants.UNPAID;

public class InvoiceAdapter extends BaseListRecyclerAdapter<InvoiceAdapter.InvoiceHolder, Invoice.InvoiceContainerEntity> {

    List<Invoice.InvoiceContainerEntity> listFiltered = new ArrayList<>();

    public InvoiceAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public InvoiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InvoiceHolder(getViewFromId(R.layout.list_item_invoice, parent));
    }

    @Override
    public void onBindViewHolder(InvoiceHolder holder, Invoice.InvoiceContainerEntity invoiceContainerEntity, int position) {
        holder.itemInvoiceCustomerName.setText(invoiceContainerEntity.getInvoice().getCustomerName());
        holder.itemInvoiceNumber.setText(invoiceContainerEntity.getInvoice().getExtInvoiceId());
        holder.itemInvoiceTotal.setText(FormatNumber.getFormatCurrencyIDR(invoiceContainerEntity.getInvoice().getFinalAmount()));
        holder.itemInvoiceOwing.setText(FormatNumber.getFormatCurrencyIDR(invoiceContainerEntity.getInvoice().getRemainingAmount()));
        holder.itemInvoiceDate.setText(DateTimeUtil.getConvertedFormat(
                invoiceContainerEntity.getInvoice().getInvoiceDate(),
                FORMAT_DATE,
                "dd MMM yyyy"));
        holder.itemInvoiceDueDate.setText(DateTimeUtil.getConvertedFormat(
                invoiceContainerEntity.getInvoice().getDueDate(),
                FORMAT_DATE,
                "dd MMM yyyy"));
        holder.listItemInvoice.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, MiruMainDetailInvoiceActivity.class);
            intent.putExtra("invoiceId", invoiceContainerEntity.getInvoice().getInvoiceId());
            intent.putExtra("extInvoiceId", invoiceContainerEntity.getInvoice().getExtInvoiceId());
            intent.putExtra("docPathUrl", invoiceContainerEntity.getInvoice().getDocUrlPath());
            mContext.startActivity(intent);
        });

        if (invoiceContainerEntity.getInvoice().getPaymentStatus().equalsIgnoreCase(UNPAID)){
            holder.triangleShape.setBackgroundResource(
                    R.drawable.shape_triangle_corner_right_blue
            );
        } else if (invoiceContainerEntity.getInvoice().getPaymentStatus().equalsIgnoreCase(DUE_DATE)){
            holder.triangleShape.setBackgroundResource(
                    R.drawable.shape_triangle_corner_right_orange
            );
        } else if (invoiceContainerEntity.getInvoice().getPaymentStatus().equalsIgnoreCase(OVERDUE)){
            holder.triangleShape.setBackgroundResource(
                    R.drawable.shape_triangle_corner_right_red
            );
        } else if (invoiceContainerEntity.getInvoice().getPaymentStatus().equalsIgnoreCase(PARTIAL_PAYMENT)){
            holder.triangleShape.setBackgroundResource(
                    R.drawable.shape_triangle_corner_right_yellow
            );
        }
    }

    public class InvoiceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_invoice_customer_name) TextView itemInvoiceCustomerName;
        @BindView(R.id.item_invoice_number) TextView itemInvoiceNumber;
        @BindView(R.id.item_invoice_total) TextView itemInvoiceTotal;
        @BindView(R.id.item_invoice_owing) TextView itemInvoiceOwing;
        @BindView(R.id.item_invoice_date) TextView itemInvoiceDate;
        @BindView(R.id.item_invoice_due_date) TextView itemInvoiceDueDate;
        @BindView(R.id.list_item_invoice) LinearLayout listItemInvoice;
        @BindView(R.id.triangle_shape) View triangleShape;
        public InvoiceHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = getListData();
                } else {
                    List<Invoice.InvoiceContainerEntity> filteredList = new ArrayList<>();
                    for (Invoice.InvoiceContainerEntity row : mListData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getInvoice().getCustomerName().toLowerCase().contains(charString.toLowerCase()) || row.getInvoice().getExtInvoiceId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListData = (ArrayList<Invoice.InvoiceContainerEntity>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

}
