package com.miru.miruApps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.model.response.Product;
import com.miru.miruApps.ui.activity.product.MiruProductDetailActivity;
import com.miru.miruApps.utils.FormatNumber;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends BaseListRecyclerAdapter<ProductAdapter.ProductHolder, Product.ProductContainerEntity> {
    private List<Product.ProductContainerEntity> suggestions = new ArrayList<>();

    public ProductAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(getViewFromId(R.layout.list_item_product, parent));
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, Product.ProductContainerEntity productContainerEntity, int position) {
        int defaultPosition = 0;
        if (productContainerEntity.getProductPrices().size() == 2) {
            defaultPosition = 1;
        }

        String mPriceStr = FormatNumber.getFormatCurrencyIDR(
                productContainerEntity.getProductPrices().get(defaultPosition).getSellingPrice())
                + "/" + productContainerEntity.getProductPrices().get(defaultPosition).getIdentifierId().getMeasurementUnit();
        holder.productPrice.setText(mPriceStr);
        holder.productCode.setText(productContainerEntity.getItemSKU());
        holder.productName.setText(productContainerEntity.getProductName());

        Glide.with(mContext)
                .load(productContainerEntity.getImageProfile())
                .into(holder.productImage);

        holder.listItemProduct.setOnClickListener(view -> {
            Intent intentDetail = new Intent(mContext, MiruProductDetailActivity.class);
            intentDetail.putExtra("itemSKU", productContainerEntity.getItemSKU());
            mContext.startActivity(intentDetail);
        });
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_image) ImageView productImage;
        @BindView(R.id.product_code) TextView productCode;
        @BindView(R.id.product_name) TextView productName;
        @BindView(R.id.product_price) TextView productPrice;
        @BindView(R.id.list_item_product) LinearLayout listItemProduct;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    suggestions = getListData();
                } else {
                    List<Product.ProductContainerEntity> filteredList = new ArrayList<>();
                    for (Product.ProductContainerEntity row : mListData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCategory().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getItemSKU().contains(charSequence) ||
                                row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    suggestions = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListData = (ArrayList<Product.ProductContainerEntity>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
