package com.miru.miruApps.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.model.SoProduct;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewSoProductAdapter extends BaseListRecyclerAdapter<ReviewSoProductAdapter.ReviewHolder, SoProduct> {

    public ReviewSoProductAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewHolder(getViewFromId(R.layout.list_item_so_product_review, parent));
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, SoProduct soProduct, int position) {
        holder.itemProductName.setText(soProduct.getProductName());
        holder.itemProductQty.setText(soProduct.getProductQty());
        holder.itemProductSku.setText(soProduct.getProductSku());
        holder.itemProductPricePerUnit.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(soProduct.getProductAfterDiscount())));
        holder.itemProductPrice.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(soProduct.getTotalAmount())));
    }

    public class ReviewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_product_name) TextView itemProductName;
        @BindView(R.id.item_product_sku) TextView itemProductSku;
        @BindView(R.id.item_product_qty) TextView itemProductQty;
        @BindView(R.id.item_product_price_per_unit) TextView itemProductPricePerUnit;
        @BindView(R.id.item_product_price) TextView itemProductPrice;
        public ReviewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
