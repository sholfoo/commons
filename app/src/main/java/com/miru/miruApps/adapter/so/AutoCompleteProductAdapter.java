package com.miru.miruApps.adapter.so;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.data.db.Product;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteProductAdapter extends ArrayAdapter<Product> {

    private List<Product> productList;
    private List<Product> suggestions;
    public AutoCompleteProductAdapter(@NonNull Context context, List<Product> productList) {
        super(context, 0, productList);
        this.productList = productList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.product_autocomplete_row, parent, false
            );
        }

        TextView itemSkuView = convertView.findViewById(R.id.item_sku);
        TextView itemNameView = convertView.findViewById(R.id.item_name);

        Product product = getItem(position);

        if (product != null) {
            itemSkuView.setText(product.getProductSku());
            itemNameView.setText(product.getProductName());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return productList.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return productFilter;
    }

    @SuppressWarnings("unchecked")
    private Filter productFilter = new Filter() {
        private Object lock = new Object();
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (suggestions == null) {
                synchronized (lock) {
                    suggestions = new ArrayList<>(productList);
                }
            }

            if (constraint == null || constraint.length() == 0) {
                synchronized (lock) {
                    results.values = suggestions;
                    results.count = suggestions.size();
                }
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                List<Product> productListFiltered = new ArrayList<>();
                for (Product product : suggestions) {
                    if (product.getProductSku().toLowerCase().contains(filterPattern) ||
                            product.getProductName().toLowerCase().contains(filterPattern)) {
                        productListFiltered.add(product);
                    }
                }

                results.values = productListFiltered;
                results.count = productListFiltered.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Product) resultValue).getProductName();
        }
    };

}
