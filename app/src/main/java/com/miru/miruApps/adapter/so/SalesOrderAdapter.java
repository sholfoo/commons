package com.miru.miruApps.adapter.so;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.BaseListRecyclerAdapter;
import com.miru.miruApps.model.response.SalesOrder;
import com.miru.miruApps.ui.activity.salesorder.MiruSODetailActivity;
import com.miru.miruApps.utils.FormatNumber;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesOrderAdapter extends BaseListRecyclerAdapter<SalesOrderAdapter.SalesOrderHolder, SalesOrder.OrdersEntity> {

    List<SalesOrder.OrdersEntity> listFiltered = new ArrayList<>();
    public SalesOrderAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public SalesOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SalesOrderHolder(getViewFromId(R.layout.list_item_sales_order, parent));
    }

    @Override
    public void onBindViewHolder(SalesOrderHolder holder, SalesOrder.OrdersEntity salesOrder, int position) {
        holder.itemSoCustomerName.setText(salesOrder.getSalesOrder().getCustomerStoreName());
        holder.itemSoId.setText(salesOrder.getSalesOrder().getExternalSoId());
        holder.itemSoTotal.setText(FormatNumber.getFormatCurrencyIDR(salesOrder.getSalesOrder().getFulfilledTotalAmount()));
        holder.itemSoTerpenuhi.setText(FormatNumber.getFormatCurrencyIDR(salesOrder.getSalesOrder().getFulfilledFinalAmount()));
        holder.listItemSalesOrder.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, MiruSODetailActivity.class);
            intent.putExtra("salesOrderId", salesOrder.getSalesOrder().getSalesOrderId());
            intent.putExtra("extSalesOrderId", salesOrder.getSalesOrder().getExternalSoId());
            intent.putExtra("docPathUrl", salesOrder.getSalesOrder().getDocUrlPath());
            mContext.startActivity(intent);
        });
    }

    public class SalesOrderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_so_customer_name) TextView itemSoCustomerName;
        @BindView(R.id.item_so_id) TextView itemSoId;
        @BindView(R.id.item_so_total) TextView itemSoTotal;
        @BindView(R.id.item_so_terpenuhi) TextView itemSoTerpenuhi;
        @BindView(R.id.list_item_sales_order) LinearLayout listItemSalesOrder;
        public SalesOrderHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = getListData();
                } else {
                    List<SalesOrder.OrdersEntity> filteredList = new ArrayList<>();
                    for (SalesOrder.OrdersEntity row : mListData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getSalesOrder().getCustomerStoreName().toLowerCase().contains(charString.toLowerCase()) || row.getSalesOrder().getExternalSoId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    listFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListData = (ArrayList<SalesOrder.OrdersEntity>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
