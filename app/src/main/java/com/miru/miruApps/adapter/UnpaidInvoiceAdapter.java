package com.miru.miruApps.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.miru.miruApps.config.Constants.DUE_DATE;
import static com.miru.miruApps.config.Constants.OVERDUE;
import static com.miru.miruApps.config.Constants.PARTIAL_PAYMENT;
import static com.miru.miruApps.config.Constants.UNPAID;

public class UnpaidInvoiceAdapter extends BaseListRecyclerAdapter<UnpaidInvoiceAdapter.UnpaidInvoiceHolder, Invoice.SummaryEntity.InvoiceDetailsEntity> {

    public UnpaidInvoiceAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public UnpaidInvoiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UnpaidInvoiceHolder(getViewFromId(R.layout.list_item_invoice_unpaid_dialog, parent));
    }

    @Override
    public void onBindViewHolder(UnpaidInvoiceHolder holder, Invoice.SummaryEntity.InvoiceDetailsEntity invoiceDetailsEntity, int position) {
        if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(UNPAID)){
            holder.lyShapeCircleSolid.setBackgroundResource(
                    R.drawable.shape_circle_blue
            );
            holder.paidStatus.setText("Belum Jatuh Tempo");
        } else if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(DUE_DATE)){
            holder.lyShapeCircleSolid.setBackgroundResource(
                    R.drawable.shape_circle_orange
            );
            holder.paidStatus.setText("Jatuh Tempo");
        } else if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(OVERDUE)){
            holder.lyShapeCircleSolid.setBackgroundResource(
                    R.drawable.shape_circle_red
            );
            holder.paidStatus.setText("Terlambat");
        } else if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(PARTIAL_PAYMENT)){
            holder.lyShapeCircleSolid.setBackgroundResource(
                    R.drawable.shape_circle_yellow
            );
            holder.paidStatus.setText("Bayar Sebagian");
        }

        holder.paidQuantity.setText(invoiceDetailsEntity.getNumOfInvoices());
        holder.paidAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(invoiceDetailsEntity.getSumOfInvoices())));
    }

    public class UnpaidInvoiceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ly_shape_circle_solid) View lyShapeCircleSolid;
        @BindView(R.id.paid_status) TextView paidStatus;
        @BindView(R.id.paid_quantity) TextView paidQuantity;
        @BindView(R.id.paid_amount) TextView paidAmount;
        public UnpaidInvoiceHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
