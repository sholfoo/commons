package com.miru.miruApps.adapter.customer;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.miru.miruApps.R;
import com.miru.miruApps.model.section.SectionCustomer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SectionCustomerHolder extends ParentViewHolder {

    @BindView(R.id.list_section_customer) TextView listSectionCustomer;

    private Context mContext;
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public SectionCustomerHolder(Context mContext, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mContext = mContext;
    }

    public void bind(SectionCustomer sectionCustomer){
        listSectionCustomer.setText(sectionCustomer.getSection());
    }
}
