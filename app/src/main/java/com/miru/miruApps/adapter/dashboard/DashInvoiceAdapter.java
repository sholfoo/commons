package com.miru.miruApps.adapter.dashboard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.BaseListRecyclerAdapter;
import com.miru.miruApps.model.response.SummaryDashboard;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DashInvoiceAdapter extends BaseListRecyclerAdapter<DashInvoiceAdapter.DashInvoiceHolder, SummaryDashboard.CustomerInvoiceSummaryEntity> {

    public DashInvoiceAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public DashInvoiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DashInvoiceHolder(getViewFromId(R.layout.list_item_dash_invoice, parent));
    }

    @Override
    public void onBindViewHolder(DashInvoiceHolder holder, SummaryDashboard.CustomerInvoiceSummaryEntity customerInvoiceSummaryEntity, int position) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(mContext)
                .load(customerInvoiceSummaryEntity.getCustomerProfile())
                .apply(options)
                .into(holder.customerImage);

        holder.invoiceTotalAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(customerInvoiceSummaryEntity.getSumOfFinalAmount())));
        holder.invoiceTotalQuantity.setText(
                String.format("%s Invoice", String.valueOf(
                        customerInvoiceSummaryEntity.getNumOfInvoices())));
        holder.customerName.setText(customerInvoiceSummaryEntity.getCustomerStoreName());
    }

    public class DashInvoiceHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.customer_image) CircleImageView customerImage;
        @BindView(R.id.invoice_total_amount) TextView invoiceTotalAmount;
        @BindView(R.id.invoice_total_quantity) TextView invoiceTotalQuantity;
        @BindView(R.id.ly_invoice_summary) LinearLayout lyInvoiceSummary;
        @BindView(R.id.customer_name) TextView customerName;

        public DashInvoiceHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
