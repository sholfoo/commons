package com.miru.miruApps.adapter.customer;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.model.section.CustomerContent;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContentCustomerHolder extends ChildViewHolder {

    @BindView(R.id.customer_store_image) CircleImageView customerStoreImage;
    @BindView(R.id.customer_store_name) TextView customerStoreName;
    @BindView(R.id.list_item_customer_content) LinearLayout listItemCustomerContent;

    private Context mContext;

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public ContentCustomerHolder(Context mContext, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mContext = mContext;
    }

    public void bind(CustomerContent customerContent){
        customerStoreName.setText(customerContent.getCustomerStoreName());

        Glide.with(mContext)
                .load(customerContent.getCustomerImage())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(customerStoreImage);
    }
}
