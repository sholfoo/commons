package com.miru.miruApps.adapter.so;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.data.db.Customer;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteCustomerAdapter extends ArrayAdapter<Customer> {

    private List<Customer> customerList;
    private List<Customer> suggestions;

    public AutoCompleteCustomerAdapter(@NonNull Context context, List<Customer> customerList) {
        super(context, 0, customerList);
        this.customerList = customerList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.product_autocomplete_row, parent, false
            );
        }

        TextView itemSkuView = convertView.findViewById(R.id.item_sku);
        TextView itemNameView = convertView.findViewById(R.id.item_name);

        Customer customer = getItem(position);

        if (customer != null) {
            itemSkuView.setText(customer.getCustomerId());
            itemNameView.setText(customer.getFullName());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return customerList.size();
    }

    @Nullable
    @Override
    public Customer getItem(int position) {
        return customerList.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return productFilter;
    }

    @SuppressWarnings("unchecked")
    private Filter productFilter = new Filter() {
        private Object lock = new Object();
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (suggestions == null) {
                synchronized (lock) {
                    suggestions = new ArrayList<>(customerList);
                }
            }

            if (constraint == null || constraint.length() == 0) {
                synchronized (lock) {
                    results.values = suggestions;
                    results.count = suggestions.size();
                }
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                List<Customer> customerListFiltered = new ArrayList<>();
                for (Customer customer : suggestions) {
                    if (customer.getFullName().toLowerCase().contains(filterPattern) || customer.getStoreName().toLowerCase().contains(filterPattern)) {
                        customerListFiltered.add(customer);
                    }
                }

                results.values = customerListFiltered;
                results.count = customerListFiltered.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Customer) resultValue).getFullName();
        }
    };
}
