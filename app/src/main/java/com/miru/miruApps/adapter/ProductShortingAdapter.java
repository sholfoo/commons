package com.miru.miruApps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miru.miruApps.R;
import com.miru.miruApps.data.db.Product;
import com.miru.miruApps.model.ProductShort;
import com.miru.miruApps.ui.activity.product.MiruProductDetailActivity;
import com.miru.miruApps.utils.FormatNumber;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductShortingAdapter extends BaseListRecyclerAdapter<ProductShortingAdapter.ProductHolder, ProductShort> {
    private List<ProductShort> suggestions = new ArrayList<>();

    public ProductShortingAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(getViewFromId(R.layout.list_item_product, parent));
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, ProductShort productContainerEntity, int position) {
        String mPriceStr = FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(productContainerEntity.getProductBasicPrice()))
                + "/PCS";
        holder.productPrice.setText(mPriceStr);
        holder.productCode.setText(productContainerEntity.getItemSKU());
        holder.productName.setText(productContainerEntity.getProductName());

        String productImage = productContainerEntity.getImageLinks();
        JSONArray temp = null;
        try {
            temp = new JSONArray(productImage);
            Glide.with(mContext)
                    .load(temp.get(0))
                    .into(holder.productImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.listItemProduct.setOnClickListener(view -> {
            Intent intentDetail = new Intent(mContext, MiruProductDetailActivity.class);
            intentDetail.putExtra("itemSKU", productContainerEntity.getItemSKU());
            mContext.startActivity(intentDetail);
        });
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_image) ImageView productImage;
        @BindView(R.id.product_code) TextView productCode;
        @BindView(R.id.product_name) TextView productName;
        @BindView(R.id.product_price) TextView productPrice;
        @BindView(R.id.list_item_product) LinearLayout listItemProduct;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    suggestions = getListData();
                } else {
                    List<ProductShort> filteredList = new ArrayList<>();
                    for (ProductShort row : mListData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCategory().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getItemSKU().contains(charSequence) ||
                                row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    suggestions = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListData = (ArrayList<ProductShort>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}