package com.miru.miruApps.adapter.dashboard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.BaseListRecyclerAdapter;
import com.miru.miruApps.config.Constants;
import com.miru.miruApps.model.response.SummaryDashboard;
import com.miru.miruApps.utils.FormatNumber;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DashProductAdapter extends BaseListRecyclerAdapter<DashProductAdapter.DashProductHolder, SummaryDashboard.ProductSummaryEntity> {

    public DashProductAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public DashProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DashProductHolder(getViewFromId(R.layout.list_item_dash_product, parent));
    }

    @Override
    public void onBindViewHolder(DashProductHolder holder, SummaryDashboard.ProductSummaryEntity productSummaryEntity, int position) {

        Glide.with(mContext)
                .load(productSummaryEntity.getImages().get(Constants.DEFAULT_SIZE))
                .into(holder.productImage);

        holder.productTotalAmount.setText(FormatNumber.getFormatCurrencyIDR(
                productSummaryEntity.getTotalAmount()));
        holder.productTotalQuantity.setText(productSummaryEntity.getTotalQty() + " " + StringUtils.capitalize(productSummaryEntity.getUnit().toLowerCase()));
        holder.productSku.setText(productSummaryEntity.getItemSKU());
        holder.productName.setText(productSummaryEntity.getItemName());
    }

    public class DashProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_image) CircleImageView productImage;
        @BindView(R.id.product_total_amount) TextView productTotalAmount;
        @BindView(R.id.product_total_quantity) TextView productTotalQuantity;
        @BindView(R.id.product_sku) TextView productSku;
        @BindView(R.id.product_name) TextView productName;

        public DashProductHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
