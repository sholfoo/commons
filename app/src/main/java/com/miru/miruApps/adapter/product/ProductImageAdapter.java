package com.miru.miruApps.adapter.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.BaseListRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductImageAdapter extends BaseListRecyclerAdapter<ProductImageAdapter.ProductImageHolder, String> {

    public ProductImageAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public ProductImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductImageHolder(getViewFromId(R.layout.list_item_image, parent));
    }

    @Override
    public void onBindViewHolder(ProductImageHolder holder, String s, int position) {
        Log.e("ImageAdapter", s);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();

        Glide.with(mContext)
                .load(s)
                .apply(requestOptions)
                .into(holder.itemImage);
    }

    public class ProductImageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_image) ImageView itemImage;
        public ProductImageHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
