package com.miru.miruApps.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;

import java.util.ArrayList;
import java.util.List;

public class AddImageAdapter extends RecyclerView.Adapter<AddImageAdapter.ViewHolder> {

    protected List<String> mListData = new ArrayList<>();

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected OnInsertListener mOnInsertListener;

    public AddImageAdapter(Context mContext) {
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public void setOnInsertListener(OnInsertListener mOnInsertListener) {
        this.mOnInsertListener = mOnInsertListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return (viewType == R.layout.list_item_image) ? new ViewHolder(getViewFromId(R.layout.list_item_image, parent)) : new ViewHolder(getViewFromId(R.layout.list_item_button, parent));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == mListData.size()) {
            holder.actionAddItem.setOnClickListener(view -> mOnInsertListener.onAddImage());
        } else {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerCrop();
            Glide.with(mContext)
                    .load(mListData.get(position))
                    .apply(requestOptions)
                    .into(holder.itemImage);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mListData.size()) ? R.layout.list_item_button : R.layout.list_item_image;
    }

    @Override
    public int getItemCount() {
        return mListData.size() + 1;
    }

    protected View getViewFromId(int layout, ViewGroup viewGroup) {
        return mInflater.inflate(layout, viewGroup, false);
    }

    public List<String> getListData() {
        return mListData;
    }


    public interface OnInsertListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onAddImage();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout actionAddItem;
        ImageView itemImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            actionAddItem = itemView.findViewById(R.id.action_add_item);
            itemImage = itemView.findViewById(R.id.item_image);
        }
    }

    public void pushData(List<String> listData) {
        pushData(listData, null);
    }

    public void pushData(List<String> listData, Bundle bundle) {
        int page = bundle != null && bundle.containsKey(BaseListRecyclerAdapter.Property.PAGE) ? bundle.getInt(BaseListRecyclerAdapter.Property.PAGE) : 0;

        int insertIndex = 0;
        boolean isDataChanged = false;

        if (page == 0) {
            if (!mListData.isEmpty())
                isDataChanged = true;
            mListData.clear();
        } else
            insertIndex = mListData.size();

        mListData.addAll(listData);

        if (!mListData.isEmpty()) {
            if (isDataChanged)
                notifyItemRangeChanged(insertIndex, Math.max(mListData.size(), 0));
            else
                notifyItemRangeInserted(insertIndex, Math.max(mListData.size(), 0));
        }
    }
}
