package com.miru.miruApps.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @sholfoo on 22/09/2015, with awesomeness
 */
public abstract class BaseListRecyclerAdapter<VH extends RecyclerView.ViewHolder, T> extends
		RecyclerView.Adapter<VH> {

	protected List<T> mListData = new ArrayList<>();

	protected Context mContext;
	protected LayoutInflater mInflater;

	protected final int VIEW_TYPE_ITEM = 0;
	protected final int VIEW_TYPE_LOADING = 1;

	public class Property {
		public static final String PAGE = "Adapter.Page";
	}

	public BaseListRecyclerAdapter(Context mContext) {
		this.mContext = mContext;
		this.mInflater = LayoutInflater.from(mContext);
	}

	@Override
	public VH onCreateViewHolder(ViewGroup parent, int viewType) {
		return null;
	}

	@Override
	public void onBindViewHolder(VH holder, int position) {
		onBindViewHolder(holder, mListData.get(position), position);
	}

	protected View getViewFromId(int layout, ViewGroup viewGroup) {
		return mInflater.inflate(layout, viewGroup, false);
	}

	public abstract void onBindViewHolder(VH holder, T t, int position);

	@Override
	public int getItemCount() {
		return mListData == null ? 0 : mListData.size();
	}

	@Override
	public int getItemViewType(int position) {
		return mListData.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
	}

	public List<T> getListData() {
		return mListData;
	}

	public void pushData(List<T> listData) {
		pushData(listData, null);
	}

	public void pushData(List<T> listData, Bundle bundle) {
		int page = bundle != null && bundle.containsKey(Property.PAGE) ? bundle.getInt(Property.PAGE) : 0;

		int insertIndex = 0;
		boolean isDataChanged = false;

		if (page == 0) {
			if (!mListData.isEmpty())
				isDataChanged = true;
			mListData.clear();
		} else
			insertIndex = mListData.size();

		mListData.addAll(listData);

		if (!mListData.isEmpty()) {
			if (isDataChanged)
				notifyItemRangeChanged(insertIndex, Math.max(mListData.size(), 0));
			else
				notifyItemRangeInserted(insertIndex, Math.max(mListData.size(), 0));
		}
	}
}
