package com.miru.miruApps.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.product.ProductImageAdapter;
import com.miru.miruApps.adapter.product.ProductPriceAdapter;
import com.miru.miruApps.adapter.product.ProductPriceDialogAdapter;
import com.miru.miruApps.model.ProductPrice;
import com.miru.miruApps.model.SoProduct;
import com.miru.miruApps.model.response.DetailProduct;
import com.miru.miruApps.model.response.Product;
import com.miru.miruApps.utils.FormatNumber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;

public class ProductDialogAdapter extends BaseListRecyclerAdapter<ProductDialogAdapter.ProductHolder, Product.ProductContainerEntity> {

    private List<Product.ProductContainerEntity> suggestions = new ArrayList<>();
    private List<SoProduct> soProductList = new ArrayList<>();
    public ProductDialogAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(getViewFromId(R.layout.list_item_dialog_product, parent));
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, Product.ProductContainerEntity productContainerEntity, int position) {
        int defaultPosition = 0;
        if (productContainerEntity.getProductPrices().size() == 2) {
            defaultPosition = 1;
        }

        String mPriceStr = FormatNumber.getFormatCurrencyIDR(
                productContainerEntity.getProductPrices().get(defaultPosition).getPricePerUnit())
                + "/" + productContainerEntity.getProductPrices().get(defaultPosition).getIdentifierId().getMeasurementUnit();
        holder.productPrice.setText(mPriceStr);
        holder.productCode.setText(productContainerEntity.getItemSKU());
        holder.productName.setText(productContainerEntity.getProductName());

        if (productContainerEntity.getImageLinks().size() != 0) {
            Glide.with(mContext)
                    .load(productContainerEntity.getImageProfile())
                    .into(holder.productImage);
        }

//        holder.listItemProduct.setOnClickListener(view -> {
//            if (!holder.actionSelect.isChecked()){
//                holder.actionSelect.setChecked(true);
//                showDialogDetailProductPrice(mContext, productContainerEntity);
//            } else {
//                for (SoProduct soProduct : soProductList) {
//                    if (soProduct.getProductSku().equalsIgnoreCase(productContainerEntity.getItemSKU())) {
//                        soProductList.remove(soProduct);
//                        break;
//                    }
//                }
//                holder.actionSelect.setChecked(false);
//            }
//        });

        holder.actionSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    showDialogDetailProductPrice(mContext, productContainerEntity);
                } else {
                    for (SoProduct soProduct : soProductList) {
                        if (soProduct.getProductSku().equalsIgnoreCase(productContainerEntity.getItemSKU())) {
                            soProductList.remove(soProduct);
                            break;
                        }
                    }
                }
            }
        });
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_image) ImageView productImage;
        @BindView(R.id.action_select) CheckBox actionSelect;
        @BindView(R.id.product_code) TextView productCode;
        @BindView(R.id.product_name) TextView productName;
        @BindView(R.id.product_price) TextView productPrice;
        @BindView(R.id.list_item_product) RelativeLayout listItemProduct;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    suggestions = getListData();
                } else {
                    List<Product.ProductContainerEntity> filteredList = new ArrayList<>();
                    for (Product.ProductContainerEntity row : mListData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCategory().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getItemSKU().contains(charSequence) ||
                                row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    suggestions = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListData = (ArrayList<Product.ProductContainerEntity>) filterResults.values;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    private void showDialogDetailProductPrice(Context mContext, Product.ProductContainerEntity productContainerEntity) {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_detail_product_price);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView productItemSku = dialog.findViewById(R.id.product_item_sku);
        TextView productName = dialog.findViewById(R.id.product_name);
        TextView productCategory = dialog.findViewById(R.id.product_category);
        TextView productBrand = dialog.findViewById(R.id.product_brand);
        TextView productSize = dialog.findViewById(R.id.product_size);
        TextView productDescription = dialog.findViewById(R.id.product_description);
        RecyclerView lvContentProduct = dialog.findViewById(R.id.lv_content_product);
        RecyclerView lvContentProductPrice = dialog.findViewById(R.id.lv_content_product_price);
        TextView lastUpdate = dialog.findViewById(R.id.last_update);
        ImageButton actionDelete = dialog.findViewById(R.id.action_delete);

        ProductImageAdapter mProductImageAdapter = new ProductImageAdapter(mContext);
        lvContentProduct.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecoratorImage = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
        itemDecoratorImage.setDrawable(mContext.getResources().getDrawable(R.drawable.line_divider_transparent));
        lvContentProduct.addItemDecoration(itemDecoratorImage);
        lvContentProduct.setAdapter(mProductImageAdapter);

        ProductPriceDialogAdapter mProductPriceAdapter = new ProductPriceDialogAdapter(mContext);
        lvContentProductPrice.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecoratorPrice = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecoratorPrice.setDrawable(mContext.getResources().getDrawable(R.drawable.line_divider_solid));
        lvContentProductPrice.addItemDecoration(itemDecoratorPrice);
        lvContentProductPrice.setAdapter(mProductPriceAdapter);
        mProductPriceAdapter.setOnPriceListener(produkPrice -> {
            dialog.dismiss();
            soProductList.add(new SoProduct(
                    productContainerEntity.getItemSKU(),
                    productContainerEntity.getProductName(),
                    String.valueOf(1),
                    produkPrice.getProductUnit(),
                    produkPrice.getProductBasicPrice(),
                    produkPrice.getProductDiscount(),
                    produkPrice.getProductSellingPrice(),
                    produkPrice.getProductSellingPrice()
            ));
        });

        List<String> stringList = new ArrayList<>();
        List<ProductPrice> productPriceList = new ArrayList<>();
        for (int i = 0; i < productContainerEntity.getImageLinks().size(); i++) {
            stringList.add(productContainerEntity
                    .getImageLinks()
                    .get(i));
            Log.e("ImageResponse", productContainerEntity
                    .getImageLinks().get(i));
        }

        for (int i = 0; i < productContainerEntity.getProductPrices().size(); i++) {
            productPriceList.add(new ProductPrice(
                    productContainerEntity.getProductPrices().get(i).getIdentifierId().getMeasurementUnit(),
                    String.valueOf(productContainerEntity.getProductPrices().get(i).getPricePerUnit()),
                    String.valueOf(productContainerEntity.getProductPrices().get(i).getSellingPrice()),
                    String.valueOf(productContainerEntity.getProductPrices().get(i).getDiscount())
            ));
        }

        Collections.reverse(productPriceList);
        mProductImageAdapter.pushData(stringList);
        mProductPriceAdapter.pushData(productPriceList);
        productItemSku.setText(productContainerEntity.getItemSKU());
        productName.setText(productContainerEntity.getProductName());
        productCategory.setText(productContainerEntity.getCategory());
        productBrand.setText(productContainerEntity.getBrand());
        productSize.setText(productContainerEntity.getSize());
        productDescription.setText(productContainerEntity.getDescription());
        lastUpdate.setText("Last Update : " + productContainerEntity.getLastUpdatedTime());

        actionDelete.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    public List<SoProduct> getPickedSoProduct(){
        return soProductList;
    }
}
