package com.miru.miruApps.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.model.ProductPerformance;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductPerformanceAdapter extends BaseListRecyclerAdapter<ProductPerformanceAdapter.PerformanceHolder, ProductPerformance> {

    public ProductPerformanceAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public PerformanceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PerformanceHolder(getViewFromId(R.layout.list_item_product_performance, parent));
    }

    @Override
    public void onBindViewHolder(PerformanceHolder holder, ProductPerformance productPerformance, int position) {
        holder.itemProductName.setText(productPerformance.getProductName());
        holder.itemProductAmount.setText(FormatNumber.getFormatCurrencyIDR(productPerformance.getProductAmount()));

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(mContext)
                .load(productPerformance.getProductImageUrl())
                .apply(options)
                .into(holder.itemProductImage);
    }

    public class PerformanceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_product_image) ImageView itemProductImage;
        @BindView(R.id.item_product_amount) TextView itemProductAmount;
        @BindView(R.id.item_product_name) TextView itemProductName;
        public PerformanceHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
