package com.miru.miruApps.adapter.notification;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.miru.miruApps.R;
import com.miru.miruApps.model.response.Notification;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContentNotificationHolder extends ChildViewHolder {
    @BindView(R.id.item_message) TextView itemMessage;
    @BindView(R.id.list_item_content) LinearLayout listItemContent;
    private Context mContext;

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public ContentNotificationHolder(Context mContext, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mContext = mContext;
    }

    public void bind(Notification.NotificationEntity.DataEntity notificationContent){
        itemMessage.setText(notificationContent.getMessage());
    }
}
