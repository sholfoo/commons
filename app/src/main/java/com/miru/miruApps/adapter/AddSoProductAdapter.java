package com.miru.miruApps.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.so.AutoCompleteProductAdapter;
import com.miru.miruApps.data.db.Product;
import com.miru.miruApps.data.db.ProductDao;
import com.miru.miruApps.model.SoProduct;
import com.miru.miruApps.ui.activity.product.MiruProductListActivity;
import com.miru.miruApps.ui.activity.product.MiruProductListDialogActivity;
import com.miru.miruApps.utils.FormatNumber;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class AddSoProductAdapter extends RecyclerView.Adapter<AddSoProductAdapter.ViewHolder> {

    protected List<SoProduct> mListData = new ArrayList<>();

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected OnActionProductListener mOnActionProductListener;
    private boolean isVisible = true;

    private ProductDao productDao;
    private Query<Product> productQuery;
    private List<Product> productListFilter;
    private List<Product> productList;
    private AutoCompleteProductAdapter adapter;

    public AddSoProductAdapter(Context mContext) {
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);

        productDao = MiruApp.getDaoSession().getProductDao();
        productQuery = productDao
                .queryBuilder()
                .orderAsc(ProductDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        productList = productQuery.list();
        productListFilter = new ArrayList<>();
        for (Product product : productList) {
            if (product.getIsCheapPrice()) {
                productListFilter.add(product);
            }
        }
         adapter = new AutoCompleteProductAdapter(mContext, productListFilter);
    }

    public void setOnActionProductListener(OnActionProductListener mOnActionProductListener) {
        this.mOnActionProductListener = mOnActionProductListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return (viewType == R.layout.list_item_so_product) ? new ViewHolder(getViewFromId(R.layout.list_item_so_product, parent)) : new ViewHolder(getViewFromId(R.layout.list_item_so_product_button, parent));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == mListData.size()) {
            holder.inputProductNameOrSku.setThreshold(1);
            holder.inputProductNameOrSku.setAdapter(adapter);
            holder.inputProductNameOrSku.setOnItemClickListener((adapterView, view, i, l) -> {
                mOnActionProductListener.onAddProduct(adapter.getItem(i));
                holder.inputProductNameOrSku.setText("");
                /* this for logging */
                Log.e("ITEM SKU", adapter.getItem(i).getProductSku());
                Log.e("ITEM NAME", adapter.getItem(i).getProductName());
                Log.e("ITEM PRICE", adapter.getItem(i).getProductSellingPrice());
                Log.e("ITEM DISCOUNT", adapter.getItem(i).getProductDiscount());
            });
            holder.actionSearchCatalogProduct.setOnClickListener(view -> {
                mOnActionProductListener.onSearchCatlogue();
            });
        } else {
            holder.itemProductName.setText(mListData.get(position).getProductName());
            holder.itemProductSku.setText(mListData.get(position).getProductSku());
            holder.inputQuantity.setText(mListData.get(position).getProductQty());
            holder.inputQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().isEmpty()){
                        Toast.makeText(mContext,
                                "Minimum pembelian produk ini adalah 1 barang, harap tambahkan jumlah",
                                Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        mListData.get(position).setProductQty(holder.inputQuantity.getText().toString());
                        int totalAmountProduct = Integer.parseInt(mListData.get(position).getProductQty()) * Integer.parseInt(mListData.get(position).getProductAfterDiscount());
                        mListData.get(position).setTotalAmount(String.valueOf(totalAmountProduct));
                        holder.totalAmountProduct.setText(FormatNumber.getFormatCurrencyIDR(totalAmountProduct));
                        mOnActionProductListener.onQuantityChanged(totalAmountProduct);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            holder.inputUnit.setText(mListData.get(position).getProductUnit());
            holder.inputPricePerUnit.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(mListData.get(position).getProductSellingPrice())));
            holder.inputProductDiscount.setText(mListData.get(position).getProductDiscount());
            holder.inputPercent.setText("%");
            holder.inputPriceAfterDiscount.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(mListData.get(position).getProductAfterDiscount())));

            int totalAmountProduct = Integer.parseInt(mListData.get(position).getProductQty()) * Integer.parseInt(mListData.get(position).getProductAfterDiscount());
            holder.totalAmountProduct.setText(FormatNumber.getFormatCurrencyIDR(totalAmountProduct));
            holder.actionDeleteSoProduct.setOnClickListener(view -> {
                mOnActionProductListener.onDeleteSoProduct(position);
                mListData.remove(position);
                notifyDataSetChanged();
            });
            holder.actionExpandCollapse.setOnClickListener(view -> {
                if (isVisible) {
                    expand(holder.layoutDiscount);
                    isVisible = false;
                } else if (!isVisible) {
                    collapse(holder.layoutDiscount);
                    isVisible = true;
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mListData.size()) ? R.layout.list_item_so_product_button : R.layout.list_item_so_product;
    }

    @Override
    public int getItemCount() {
        return mListData.size() + 1;
    }

    protected View getViewFromId(int layout, ViewGroup viewGroup) {
        return mInflater.inflate(layout, viewGroup, false);
    }

    public List<SoProduct> getListData() {
        return mListData;
    }

    public interface OnActionProductListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onAddProduct(Product product);
        void onQuantityChanged(int subtotal);
        void onDeleteSoProduct(int position);
        void onSearchCatlogue();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AutoCompleteTextView inputProductNameOrSku;
        Button actionSearchCatalogProduct;
        LinearLayout layoutDiscount;
        ImageButton actionExpandCollapse;
        ImageButton actionDeleteSoProduct;

        TextView itemProductName;
        TextView itemProductSku;
        EditText inputQuantity;
        EditText inputUnit;
        EditText inputPricePerUnit;
        EditText inputProductDiscount;
        EditText inputPercent;
        EditText inputPriceAfterDiscount;
        TextView totalAmountProduct;
        LinearLayout lyDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            inputProductNameOrSku = itemView.findViewById(R.id.input_product_name_or_sku);
            actionSearchCatalogProduct = itemView.findViewById(R.id.action_search_catalog_product);

            itemProductName = itemView.findViewById(R.id.item_product_name);
            itemProductSku = itemView.findViewById(R.id.item_product_sku);
            inputQuantity = itemView.findViewById(R.id.input_quantity);
            inputUnit = itemView.findViewById(R.id.input_unit);
            inputPricePerUnit = itemView.findViewById(R.id.input_price_per_unit);
            inputProductDiscount = itemView.findViewById(R.id.input_product_discount);
            inputPercent = itemView.findViewById(R.id.input_percent);
            inputPriceAfterDiscount = itemView.findViewById(R.id.input_price_after_discount);
            itemProductName = itemView.findViewById(R.id.item_product_name);
            itemProductName = itemView.findViewById(R.id.item_product_name);
            totalAmountProduct = itemView.findViewById(R.id.total_amount_product);

            layoutDiscount = itemView.findViewById(R.id.ly_discount);
            actionDeleteSoProduct = itemView.findViewById(R.id.action_delete_list);
            actionExpandCollapse = itemView.findViewById(R.id.action_expand_so);
        }
    }

    public void pushData(List<SoProduct> listData) {
        pushData(listData, null);
    }

    public void pushData(List<SoProduct> listData, Bundle bundle) {
        int page = bundle != null && bundle.containsKey(BaseListRecyclerAdapter.Property.PAGE) ? bundle.getInt(BaseListRecyclerAdapter.Property.PAGE) : 0;

        int insertIndex = 0;
        boolean isDataChanged = false;

        if (page == 0) {
            if (!mListData.isEmpty())
                isDataChanged = true;
            mListData.clear();
        } else
            insertIndex = mListData.size();

        mListData.addAll(listData);

        if (!mListData.isEmpty()) {
            if (isDataChanged)
                notifyItemRangeChanged(insertIndex, Math.max(mListData.size(), 0));
            else
                notifyItemRangeInserted(insertIndex, Math.max(mListData.size(), 0));
        }
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
