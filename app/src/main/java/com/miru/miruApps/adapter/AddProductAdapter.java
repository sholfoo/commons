package com.miru.miruApps.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.model.ProductPrice;
import com.miru.miruApps.utils.FormatNumber;

import java.util.ArrayList;
import java.util.List;

public class AddProductAdapter extends RecyclerView.Adapter<AddProductAdapter.ViewHolder> {

    protected List<ProductPrice> mListData = new ArrayList<>();

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected OnAddProductListener mOnAddProductListener;

    public AddProductAdapter(Context mContext) {
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public void setOnAddProductListener(OnAddProductListener mOnAddProductListener) {
        this.mOnAddProductListener = mOnAddProductListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return (viewType == R.layout.list_item_product_price) ? new ViewHolder(getViewFromId(R.layout.list_item_product_price, parent)) : new ViewHolder(getViewFromId(R.layout.list_item_product_button, parent));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == mListData.size()) {
            holder.listItemActionAdd.setOnClickListener(view -> mOnAddProductListener.onAddProduct());
        } else {
            holder.productUnit.setText(mListData.get(position).getProductUnit());
            holder.productBasicPrice.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(mListData.get(position).getProductBasicPrice())));
            holder.productSellingPrice.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(mListData.get(position).getProductSellingPrice())));
            holder.productBasicDiscount.setText(FormatNumber.getFormatPercent(mListData.get(position).getProductDiscount()));
            holder.actionDelete.setOnClickListener(view -> {
                mOnAddProductListener.onDeleteProduct(position);
                mListData.remove(position);
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mListData.size()) ? R.layout.list_item_product_button : R.layout.list_item_product_price;
    }

    @Override
    public int getItemCount() {
        return mListData.size() + 1;
    }

    protected View getViewFromId(int layout, ViewGroup viewGroup) {
        return mInflater.inflate(layout, viewGroup, false);
    }

    public List<ProductPrice> getListData() {
        return mListData;
    }

    public interface OnAddProductListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onAddProduct();
        void onDeleteProduct(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout listItemActionAdd;
        TextView productUnit;
        TextView productBasicPrice;
        TextView productSellingPrice;
        TextView productBasicDiscount;
        ImageButton actionDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            listItemActionAdd = itemView.findViewById(R.id.list_item_action_add);
            productUnit = itemView.findViewById(R.id.product_unit);
            productBasicPrice = itemView.findViewById(R.id.product_basic_price);
            productSellingPrice = itemView.findViewById(R.id.product_selling_price);
            productBasicDiscount = itemView.findViewById(R.id.product_basic_discount);
            actionDelete = itemView.findViewById(R.id.action_delete);
        }
    }

    public void pushData(List<ProductPrice> listData) {
        pushData(listData, null);
    }

    public void pushData(List<ProductPrice> listData, Bundle bundle) {
        int page = bundle != null && bundle.containsKey(BaseListRecyclerAdapter.Property.PAGE) ? bundle.getInt(BaseListRecyclerAdapter.Property.PAGE) : 0;

        int insertIndex = 0;
        boolean isDataChanged = false;

        if (page == 0) {
            if (!mListData.isEmpty())
                isDataChanged = true;
            mListData.clear();
        } else
            insertIndex = mListData.size();

        mListData.addAll(listData);

        if (!mListData.isEmpty()) {
            if (isDataChanged)
                notifyItemRangeChanged(insertIndex, Math.max(mListData.size(), 0));
            else
                notifyItemRangeInserted(insertIndex, Math.max(mListData.size(), 0));
        }
    }
}
