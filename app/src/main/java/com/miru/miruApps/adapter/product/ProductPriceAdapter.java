package com.miru.miruApps.adapter.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.BaseListRecyclerAdapter;
import com.miru.miruApps.model.ProductPrice;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductPriceAdapter extends BaseListRecyclerAdapter<ProductPriceAdapter.ProductPriceHolder, ProductPrice> {

    public ProductPriceAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public ProductPriceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductPriceHolder(getViewFromId(R.layout.list_item_price, parent));
    }

    @Override
    public void onBindViewHolder(ProductPriceHolder holder, ProductPrice productPrice, int position) {
        holder.productBasicPrice.setText(
                String.format("%s / %s", FormatNumber.getFormatCurrencyIDR(
                        Integer.parseInt(productPrice.getProductBasicPrice())),
                        productPrice.getProductUnit())
        );

        holder.productSellingPrice.setText(
                String.format("%s / %s", FormatNumber.getFormatCurrencyIDR(
                        Integer.parseInt(productPrice.getProductSellingPrice())),
                        productPrice.getProductUnit())
        );

        holder.productDiscount.setText(FormatNumber.getFormatPercent(
                productPrice.getProductDiscount()));
    }

    public class ProductPriceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_basic_price) TextView productBasicPrice;
        @BindView(R.id.product_selling_price) TextView productSellingPrice;
        @BindView(R.id.product_discount) TextView productDiscount;
        public ProductPriceHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
