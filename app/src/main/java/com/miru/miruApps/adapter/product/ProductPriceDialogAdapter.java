package com.miru.miruApps.adapter.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.BaseListRecyclerAdapter;
import com.miru.miruApps.model.ProductPrice;
import com.miru.miruApps.model.retrieve.RetrieveSoProductData;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductPriceDialogAdapter extends BaseListRecyclerAdapter<ProductPriceDialogAdapter.ProductPriceHolder, ProductPrice> {

    public OnPriceListener onPriceListener;

    public ProductPriceDialogAdapter(Context mContext) {
        super(mContext);
    }

    public void setOnPriceListener(OnPriceListener onPriceListener) {
        this.onPriceListener = onPriceListener;
    }

    @Override
    public ProductPriceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductPriceHolder(getViewFromId(R.layout.list_item_price, parent));
    }

    @Override
    public void onBindViewHolder(ProductPriceHolder holder, ProductPrice productPrice, int position) {
        holder.productBasicPrice.setText(
                String.format("%s / %s", FormatNumber.getFormatCurrencyIDR(
                        Integer.parseInt(productPrice.getProductBasicPrice())),
                        productPrice.getProductUnit())
        );

        holder.productSellingPrice.setText(
                String.format("%s / %s", FormatNumber.getFormatCurrencyIDR(
                        Integer.parseInt(productPrice.getProductSellingPrice())),
                        productPrice.getProductUnit())
        );

        holder.productDiscount.setText(FormatNumber.getFormatPercent(
                productPrice.getProductDiscount()));

        holder.listItem.setOnClickListener(view -> onPriceListener.onSelectPrice(productPrice));
    }

    public class ProductPriceHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_basic_price) TextView productBasicPrice;
        @BindView(R.id.product_selling_price) TextView productSellingPrice;
        @BindView(R.id.product_discount) TextView productDiscount;
        @BindView(R.id.list_item) LinearLayout listItem;

        public ProductPriceHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnPriceListener {

        void onSelectPrice(ProductPrice produkPrice);
    }
}
