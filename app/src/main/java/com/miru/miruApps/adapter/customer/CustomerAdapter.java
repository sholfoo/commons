package com.miru.miruApps.adapter.customer;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.miru.miruApps.R;
import com.miru.miruApps.model.section.CustomerContent;
import com.miru.miruApps.model.section.SectionCustomer;
import com.miru.miruApps.ui.activity.customer.MiruCustomerDetailActivity;

import java.util.List;

public class CustomerAdapter extends ExpandableRecyclerAdapter<SectionCustomerHolder, ContentCustomerHolder> {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private List<? extends ParentListItem> parentListItems;

    /**
     * Primary constructor. Sets up {@link #mParentItemList} and {@link #mItemList}.
     * <p>
     * Changes to {@link #mParentItemList} should be made through add/remove methods in
     * {@link ExpandableRecyclerAdapter}
     *
     * @param parentItemList List of all {@link ParentListItem} objects to be
     *                       displayed in the RecyclerView that this
     *                       adapter is linked to
     */
    public CustomerAdapter(Context mContext, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        this.mContext = mContext;
        this.parentListItems = parentItemList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public SectionCustomerHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View menuItemView = mLayoutInflater.inflate(R.layout.list_item_customer_section, parentViewGroup, false);
        return new SectionCustomerHolder(mContext, menuItemView);
    }

    @Override
    public ContentCustomerHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View subMenuItemView = mLayoutInflater.inflate(R.layout.list_item_customer_content, childViewGroup, false);
        return new ContentCustomerHolder(mContext, subMenuItemView);
    }

    @Override
    public void onBindParentViewHolder(SectionCustomerHolder parentViewHolder, int position, ParentListItem parentListItem) {
        final SectionCustomer sectionCustomer = (SectionCustomer) parentListItem;
        parentViewHolder.bind(sectionCustomer);
    }

    @Override
    public void onBindChildViewHolder(ContentCustomerHolder childViewHolder, int position, Object childListItem) {
        final CustomerContent customerContent = (CustomerContent) childListItem;
        childViewHolder.bind(customerContent);
        childViewHolder.listItemCustomerContent.setOnClickListener(v -> {
            Log.e("ON SELECTED", customerContent.getCustomerId());
            Intent intent = new Intent(mContext, MiruCustomerDetailActivity.class);
            intent.putExtra("customerStoreName", customerContent.getCustomerStoreName());
            intent.putExtra("customerType", customerContent.getCustomerType());
            intent.putExtra("customerStoreImage", customerContent.getCustomerImage());
            intent.putExtra("customerId", customerContent.getCustomerId());
            intent.putExtra("customerStatus", customerContent.getCustomerStatus());
            mContext.startActivity(intent);
        });

    }

    @Override
    public List<? extends ParentListItem> getParentItemList() {
        return super.getParentItemList();
    }
}
