package com.miru.miruApps.adapter.notification;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.miru.miruApps.R;
import com.miru.miruApps.model.section.SectionNotification;
import com.miru.miruApps.utils.DateTimeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SectionNotificationHolder extends ParentViewHolder {

    @BindView(R.id.list_section_notification)
    TextView listSectionNotification;

    private Context mContext;
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public SectionNotificationHolder(Context mContext, View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mContext = mContext;
    }

    public void bind(SectionNotification sectionNotification){
        listSectionNotification.setText(DateTimeUtil.getConvertedFormat(
                sectionNotification.getSection(),
                "dd-MM-yyyy",
                "dd MMM yyyy"));
    }
}
