package com.miru.miruApps.adapter.notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.miru.miruApps.R;
import com.miru.miruApps.model.response.Notification;
import com.miru.miruApps.model.section.SectionNotification;

import java.util.List;

public class NotificationAdapter extends ExpandableRecyclerAdapter<SectionNotificationHolder, ContentNotificationHolder> {
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private List<? extends ParentListItem> parentListItems;

    /**
     * Primary constructor. Sets up {@link #mParentItemList} and {@link #mItemList}.
     * <p>
     * Changes to {@link #mParentItemList} should be made through add/remove methods in
     * {@link ExpandableRecyclerAdapter}
     *
     * @param parentItemList List of all {@link ParentListItem} objects to be
     *                       displayed in the RecyclerView that this
     *                       adapter is linked to
     */
    public NotificationAdapter(Context mContext, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        this.mContext = mContext;
        this.parentListItems = parentItemList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public SectionNotificationHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View menuItemView = mLayoutInflater.inflate(R.layout.list_item_notificatin_section, parentViewGroup, false);
        return new SectionNotificationHolder(mContext, menuItemView);
    }

    @Override
    public ContentNotificationHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View subMenuItemView = mLayoutInflater.inflate(R.layout.list_item_notification_content, childViewGroup, false);
        return new ContentNotificationHolder(mContext, subMenuItemView);
    }

    @Override
    public void onBindParentViewHolder(SectionNotificationHolder parentViewHolder, int position, ParentListItem parentListItem) {
        final SectionNotification sectionNotification = (SectionNotification) parentListItem;
        parentViewHolder.bind(sectionNotification);
    }

    @Override
    public void onBindChildViewHolder(ContentNotificationHolder childViewHolder, int position, Object childListItem) {
        final Notification.NotificationEntity.DataEntity notificationContent = (Notification.NotificationEntity.DataEntity) childListItem;
        childViewHolder.bind(notificationContent);
        childViewHolder.listItemContent.setOnClickListener(v -> {
            Log.e("ON SELECTED", notificationContent.getMessage());
//            Intent intent = new Intent(mContext, MiruCustomerDetailActivity.class);
//            intent.putExtra("customerStoreName", customerContent.getCustomerStoreName());
//            intent.putExtra("customerStoreImage", customerContent.getCustomerImage());
//            intent.putExtra("customerId", customerContent.getCustomerId());
//            mContext.startActivity(intent);
        });
    }
}
