package com.miru.miruApps.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.miru.miruApps.R;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;

public class GetAddressIntentService extends IntentService {
    private static final String TAG = GetAddressIntentService.class.getCanonicalName();
    private static final String IDENTIFIER = "GetAddressIntentService";
    protected ResultReceiver addressResultReceiver;

    static OkHttpClient client = new OkHttpClient();

    public GetAddressIntentService() {
        super(IDENTIFIER);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String errorMessage = "";
        String mFullAddress = null;
        String mProvince = null;
        String mPostalCode = null;
        String mCity = null;
        String mDistrict = null;
        List<Address> addresses = null;

        //get result receiver from intent
        addressResultReceiver = intent.getParcelableExtra("add_receiver");
        if (addressResultReceiver == null) {
            Log.e("GetAddressIntentService",
                    "No receiver, not processing the request further");
            return;
        }

        Location location = intent.getParcelableExtra("add_location");
        //send no location error to results receiver
        if (location == null) {
            errorMessage = "No location, can't go further without location";
            deliverResultToReceiver(0, null, null, null,null, errorMessage);
            return;
        }

        Geocoder geocoder = new Geocoder(this);
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            mFullAddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()s

            String[] mAddressData = mFullAddress.split(",");
            mProvince = addresses.get(0).getAdminArea();
            mPostalCode = addresses.get(0).getPostalCode();
            mCity = mAddressData[3].trim();
            mDistrict = mAddressData[2].trim();

            Log.e("Full Address", mFullAddress + "");
            Log.e("Province", mProvince + "");
            Log.e("Kode Pos", mPostalCode + "");
            Log.e("kecamatan",mDistrict + "");
            Log.e("Kota", mCity + "");

        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.alert_service_not_available);
            ioException.printStackTrace();
            Log.e("IOException", ioException.toString());
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.alert_invalid_lat_long_used);
            illegalArgumentException.printStackTrace();
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " +
                    location.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.alert_no_address_found);
                Log.e(TAG, errorMessage);
            }

            deliverResultToReceiver(1, null, null, null, null, errorMessage);

        } else {
            deliverResultToReceiver(2, mCity, mDistrict, mProvince, mPostalCode, mFullAddress);
        }

    }

    private void deliverResultToReceiver(int resultCode, String mCity, String mDistrict, String mProvince, String mPostalCode, String mFullAddress) {
        Bundle bundle = new Bundle();
        bundle.putString("address_city", mCity);
        bundle.putString("address_district", mDistrict);
        bundle.putString("address_province", mProvince);
        bundle.putString("address_postal_code", mPostalCode);
        bundle.putString("address_result", mFullAddress);
        addressResultReceiver.send(resultCode, bundle);
    }
}
