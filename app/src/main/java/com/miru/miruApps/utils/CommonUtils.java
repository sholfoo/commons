package com.miru.miruApps.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by esa on 14/08/15, with awesomeness
 */
public class CommonUtils {

	public static void toast(Context context, int message){
		toast(context, context.getString(message));
	}

	public static void toast(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
