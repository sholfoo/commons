package com.miru.miruApps.utils;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by sholfoo on 6/14/17.
 */

public class ImageUtilities {

    public static File scaleImage(String path, String image) {
        String strMyImagePath = null;
        Bitmap scaledBitmap;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, 240, 320, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= 240 && unscaledBitmap.getHeight() <= 320)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 240, 320, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return new File(path);
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            File f = new File(mFolder.getAbsolutePath(), System.currentTimeMillis() + image);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {}

        if (strMyImagePath == null) {
            return new File(path);
        }

        return new File(strMyImagePath);

    }

    public static File signatureImage(Bitmap bitmap) {
        String strMyImagePath;

        // Store to tmp file
        String extr = Environment.getExternalStorageDirectory().toString();
        File mFolder = new File(extr + "/TMMFOLDER");
        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = "tmpSignature.png";
        File f = new File(mFolder.getAbsolutePath(), s);
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream os;
        try {
            os = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(ImageUtilities.class.getSimpleName(), "Error writing bitmap", e);
        }

        return new File(strMyImagePath);
    }
}
