package com.miru.miruApps.utils;

public enum TimeMode {
    MORNING("Selamat Pagi, "),
    NOON("Selamat Siang, "),
    AFTERNOON("Selamat Sore, "),
    NIGHT("Selamat Malam, ");

    private String greeting;

    TimeMode(String say) {
        this.greeting = say;
    }

    public String greeting(){
        return greeting;
    }
}
