package com.miru.miruApps.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sholfoo on 3/14/17.
 */

public class DateTimeUtil {

    /**
     *
     * @param currentFormat
     * for example : "yyyy-MM-dd HH:mm:ss"/"dd-MM-yyyy"/"HH:mm:ss"
     * @return
     */
    public static String getCurrentFormat(String currentFormat){
        SimpleDateFormat sdf = new SimpleDateFormat(currentFormat);
        return sdf.format(new Date());
    }

    /**
     *
     * @param mDate (your input date.)
     * @param currentFormat (ex : "yyyy-MM-dd HH:mm:ss")
     * @param desireFormat (convert format ex : "HH:mm:ss"/"EEEE, dd MMM yyyy")
     * @return
     */
    public static String getConvertedFormat(String mDate, String currentFormat, String desireFormat){
        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat(currentFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(desireFormat);
        try {
            date = df.parse(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return sdf.format(date);
    }

    public static String getFormatDateWithDaysAdded(String mDate, String currentFormat, String desireFormat, int days){
        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat(currentFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(desireFormat);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(mDate));
            cal.add(Calendar.DATE, days);
            date = new Date(cal.getTimeInMillis());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return sdf.format(date);
    }

    public static String getFormatDateWithDay2(String dateStr){
        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return sdf.format(date);
    }

    public static String getFormatDate(String dateStr){
        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return sdf.format(date);
    }

    public static long getCurrentMilliseconds() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        long timeInMilliseconds = 0L;
        try {
            Date mDate = sdf.parse(currentDateandTime);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public static long formatToMillisecond(String checkInServer) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long timeInMilliseconds = 0L;
        try {
            Date mDate = sdf.parse(checkInServer);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }
}
