package com.miru.miruApps.utils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by esa on 12/08/15, with awesomeness
 */
public class EBus {

	public static void post(Object o) {
		EventBus.getDefault().post(o);
	}

	public static void register(Object o) {
		if (!EventBus.getDefault().isRegistered(o))
			EventBus.getDefault().register(o);
	}

	public static void unregister(Object o) {
		EventBus.getDefault().unregister(o);
	}
}
