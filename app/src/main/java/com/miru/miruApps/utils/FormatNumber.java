package com.miru.miruApps.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by sholfoo on 1/4/16 with awesomeness.
 */
public class FormatNumber {

    public static String getFormatNumber(String pointsStr){
        String points = null;
        try {
            NumberFormat num = NumberFormat.getInstance();
            long pointLong   = Long.parseLong(pointsStr);
            num.setMaximumFractionDigits(3);
            points = num.format(pointLong);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return points;
    }

    public static String getFormatCurrencyIDR(int currency) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
        String mPriceCurrency  = format.format(currency);
        return mPriceCurrency.replace("Rp", "Rp ");
    }

    public static String removeFromatCurrencyIDR(String currency){
        String clearRp = currency.replace("Rp ", "");
        String clearComa = clearRp.replace(".00", "");
        String clearString = clearComa.replace(". ", "");
        return clearString.replaceAll(",", "");
    }

    public static String getFormatPercent(String discount){
        return discount += " %";
    }

    public static String getFormatHashTag(String quantity) {
        return "# " + quantity + " Invoice";
    }
}
