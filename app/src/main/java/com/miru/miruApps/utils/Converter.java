package com.miru.miruApps.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by sholfoo on 5/1/17.
 */

public class Converter {

    /**
     * Converting dp to pixel
     */
    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static String[] stringToArray(String string) throws JSONException {
        JSONArray jsonArray = new JSONArray(string);
        return new String[jsonArray.length()];
    }
}
