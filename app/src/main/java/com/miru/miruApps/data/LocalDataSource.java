package com.miru.miruApps.data;

import android.content.Context;

import com.miru.miruApps.data.preference.SPLogin;

public class LocalDataSource {

    private static String TAG = SPLogin.class.getSimpleName();
    private static final String PREF_KEY   = "bulb_local_data_source";
    private static final String KEY_TARGET_AMOUNT = "target_amount";
    private static final String KEY_IS_STORED = "isStored";

    private static void setSharedPreference(Context context, String mKey, String mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, boolean mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, int mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putInt(mKey, mValue)
                .apply();
    }

    private static String getSharedPreferenceString(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getString(mKey, null);
    }

    private static boolean getSharedPreferenceBoolean(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(mKey, false);
    }

    private static int getSharedPreferenceInt(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getInt(mKey, 0);
    }

    /* GETTER SETTER */

    public String getKeyTargetAmount(Context context) {
        return getSharedPreferenceString(context, KEY_TARGET_AMOUNT);
    }

    public boolean isStored(Context context) {
        return getSharedPreferenceBoolean(context, KEY_IS_STORED);
    }

    public void setKeyTargetAmount(Context context, String mValue) {
        setSharedPreference(context, KEY_TARGET_AMOUNT, mValue);
    }

    public void setKeyIsStored(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_STORED, mValue);
    }

    public void clearSession(Context context) {
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }

}
