package com.miru.miruApps.data.preference;

import android.content.Context;

public class SPCreateCustomer {
    // LogCat tag
    private static String TAG = SPCreateCustomer.class.getSimpleName();
    private static final String PREF_KEY   = "bulb_create_customer_session";
    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_LAST_NAME = "last_name";
    private static final String KEY_STORE_NAME = "store_name";
    private static final String KEY_PHONE_NO = "phone_no";
    private static final String KEY_MOBILE_NO = "mobile_no";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_LIMIT = "credit_limit";
    private static final String KEY_DOCUMENTATION = "documentation";
    private static final String KEY_TAG = "tags";
    private static final String KEY_DESC = "description";
    private static final String KEY_STATUS = "status";


    private static final String KEY_IS_STORED = "isStored";

    private static void setSharedPreference(Context context, String mKey, String mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, boolean mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, int mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putInt(mKey, mValue)
                .apply();
    }

    private static String getSharedPreferenceString(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getString(mKey, null);
    }

    private static boolean getSharedPreferenceBoolean(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(mKey, false);
    }

    private static int getSharedPreferenceInt(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getInt(mKey, 0);
    }

    /* GETTER SETTER */

    public void setKeyFirstName(Context context, String mValue) {
        setSharedPreference(context, KEY_FIRST_NAME, mValue);
    }

    public void setKeyLastName(Context context, String mValue) {
        setSharedPreference(context, KEY_LAST_NAME, mValue);
    }

    public void setKeyStoreName(Context context, String mValue) {
        setSharedPreference(context, KEY_STORE_NAME, mValue);
    }

    public void setKeyPhoneNo(Context context, String mValue) {
        setSharedPreference(context, KEY_PHONE_NO, mValue);
    }

    public void setKeyMobileNo(Context context, String mValue) {
        setSharedPreference(context, KEY_MOBILE_NO, mValue);
    }

    public void setKeyEmail(Context context, String mValue) {
        setSharedPreference(context, KEY_EMAIL, mValue);
    }

    public void setKeyCategory(Context context, String mValue) {
        setSharedPreference(context, KEY_CATEGORY, mValue);
    }

    public void setKeyLimit(Context context, String mValue) {
        setSharedPreference(context, KEY_LIMIT, mValue);
    }

    public void setKeyDocumentation(Context context, String mValue) {
        setSharedPreference(context, KEY_DOCUMENTATION, mValue);
    }

    public void setKeyTag(Context context, String mValue) {
        setSharedPreference(context, KEY_TAG, mValue);
    }

    public void setKeyStatus(Context context, String mValue) {
        setSharedPreference(context, KEY_STATUS, mValue);
    }

    public int getKeyFirstName(Context context) {
        return getSharedPreferenceInt(context, KEY_FIRST_NAME);
    }

    public String getKeyLastName(Context context) {
        return getSharedPreferenceString(context, KEY_LAST_NAME);
    }

    public int isStored(Context context) {
        return getSharedPreferenceInt(context, KEY_IS_STORED);
    }

    public void setKeyIsStored(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_STORED, mValue);
    }

    public void clearSession(Context context) {
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }
}
