package com.miru.miruApps.data.preference;

import android.content.Context;
import android.util.Log;

public class SPUser {

    // LogCat tag
    private static String TAG = SPLogin.class.getSimpleName();
    private static final String PREF_KEY   = "bulb_user_session";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_USER_CLIENT_ID = "userClientId";
    private static final String KEY_USER_SESSION_ID = "sessionId";
    private static final String KEY_USER_NAME = "userName";
    private static final String KEY_USER_FULL_NAME = "userFullName";
    private static final String KEY_USER_STORE = "userStore";
    private static final String KEY_USER_PROFILE = "userProfile";
    private static final String KEY_USER_STORE_LOGO = "userStoreLogo";
    private static final String KEY_USER_PHONE = "userPhone";
    private static final String KEY_USER_TOTAL_INVOICE = "totalInvoice";
    private static final String KEY_USER_TOTAL_SO = "totalSo";
    private static final String KEY_USER_TOTAL_AMOUNT = "totalAmount";

    private static final String KEY_IS_STORED = "isStored";

    private static void setSharedPreference(Context context, String mKey, String mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, boolean mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, int mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putInt(mKey, mValue)
                .apply();
    }

    private static String getSharedPreferenceString(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getString(mKey, null);
    }

    private static boolean getSharedPreferenceBoolean(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(mKey, false);
    }

    private static int getSharedPreferenceInt(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getInt(mKey, 0);
    }

    /* GETTER SETTER */

    public String getKeyUserId(Context context) {
        return getSharedPreferenceString(context, KEY_USER_ID);
    }

    public String getKeyUserClientId(Context context) {
        return getSharedPreferenceString(context, KEY_USER_CLIENT_ID);
    }

    public String getKeyUserSessionId(Context context) {
        return getSharedPreferenceString(context, KEY_USER_SESSION_ID);
    }

    public String getKeyUserName(Context context) {
        return getSharedPreferenceString(context, KEY_USER_NAME);
    }

    public String getKeyUserFullName(Context context) {
        return getSharedPreferenceString(context, KEY_USER_FULL_NAME);
    }

    public String getKeyUserStore(Context context) {
        return getSharedPreferenceString(context, KEY_USER_STORE);
    }

    public String getKeyUserProfile(Context context) {
        return getSharedPreferenceString(context, KEY_USER_PROFILE);
    }

    public String getKeyUserStoreLogo(Context context) {
        return getSharedPreferenceString(context, KEY_USER_STORE_LOGO);
    }

    public String getKeyUserPhone(Context context) {
        return getSharedPreferenceString(context, KEY_USER_PHONE);
    }

    public String getKeyUserTotalInvoice(Context context) {
        return getSharedPreferenceString(context, KEY_USER_TOTAL_INVOICE);
    }

    public String getKeyUserTotalSo(Context context) {
        return getSharedPreferenceString(context, KEY_USER_TOTAL_SO);
    }

    public String getKeyUserTotalAmount(Context context) {
        return getSharedPreferenceString(context, KEY_USER_TOTAL_AMOUNT);
    }

    public boolean isStored(Context context) {
        return getSharedPreferenceBoolean(context, KEY_IS_STORED);
    }

    public void setKeyUserId(Context context, String mValue) {
        Log.e("User Id", "Saved!" + " UserId : " + mValue);
        setSharedPreference(context, KEY_USER_ID, mValue);
    }

    public void setKeyUserClientId(Context context, String mValue) {
        Log.e("User ClientId", "Saved!" + " ClientId : " + mValue);
        setSharedPreference(context, KEY_USER_CLIENT_ID, mValue);
    }

    public void setKeyUserSessionId(Context context, String mValue) {
        Log.e("User SessionId", "Saved!" + " SessionId : " + mValue);
        setSharedPreference(context, KEY_USER_SESSION_ID, mValue);
    }

    public void setKeyUserName(Context context, String mValue) {
        Log.e("UserName", "Saved!" + " Username : " + mValue);
        setSharedPreference(context, KEY_USER_NAME, mValue);
    }

    public void setKeyUserFullName(Context context, String mValue) {
        Log.e("User FullName", "Saved!" + " FullName : " + mValue);
        setSharedPreference(context, KEY_USER_FULL_NAME, mValue);
    }

    public void setKeyUserStore(Context context, String mValue) {
        Log.e("User StoreName", "Saved!" + " StoreName : " + mValue);
        setSharedPreference(context, KEY_USER_STORE, mValue);
    }

    public void setKeyUserProfile(Context context, String mValue) {
        Log.e("User ProfileImage", "Saved!" + " ProfileImage : " + mValue);
        setSharedPreference(context, KEY_USER_PROFILE, mValue);
    }

    public void setKeyUserStoreLogo(Context context, String mValue) {
        Log.e("User StoreLogo", "Saved!" + " StoreLogo : " + mValue);
        setSharedPreference(context, KEY_USER_STORE_LOGO, mValue);
    }

    public void setKeyUserPhone(Context context, String mValue) {
        Log.e("User Phone", "Saved!" + " Phone : " + mValue);
        setSharedPreference(context, KEY_USER_PHONE, mValue);
    }

    public void setKeyUserTotalInvoice(Context context, String mValue) {
        setSharedPreference(context, KEY_USER_TOTAL_INVOICE, mValue);
    }

    public void setKeyUserTotalSo(Context context, String mValue) {
        setSharedPreference(context, KEY_USER_TOTAL_SO, mValue);
    }

    public void setKeyUserTotalAmount(Context context, String mValue) {
        setSharedPreference(context, KEY_USER_TOTAL_AMOUNT, mValue);
    }

    public void setKeyIsStored(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_STORED, mValue);
    }

    public void clearSession(Context context) {
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }
}
