package com.miru.miruApps.data.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class Invoice {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "invoice_id")
    private String invoiceId;

    @Property(nameInDb = "ext_invoice_id")
    private String extInvoiceId;

    @Property(nameInDb = "sales_name")
    private String salesName;

    @Property(nameInDb = "invoice_date")
    private String invoiceDate;

    @Property(nameInDb = "final_amount")
    private int finalAmount;

    @Property(nameInDb = "remaining_amount")
    private int remainingAmount;

    @Generated(hash = 20242601)
    public Invoice(Long id, String invoiceId, String extInvoiceId, String salesName,
            String invoiceDate, int finalAmount, int remainingAmount) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.extInvoiceId = extInvoiceId;
        this.salesName = salesName;
        this.invoiceDate = invoiceDate;
        this.finalAmount = finalAmount;
        this.remainingAmount = remainingAmount;
    }

    @Generated(hash = 1296330302)
    public Invoice() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceId() {
        return this.invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getExtInvoiceId() {
        return this.extInvoiceId;
    }

    public void setExtInvoiceId(String extInvoiceId) {
        this.extInvoiceId = extInvoiceId;
    }

    public String getSalesName() {
        return this.salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getInvoiceDate() {
        return this.invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public int getFinalAmount() {
        return this.finalAmount;
    }

    public void setFinalAmount(int finalAmount) {
        this.finalAmount = finalAmount;
    }

    public int getRemainingAmount() {
        return this.remainingAmount;
    }

    public void setRemainingAmount(int remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

}
