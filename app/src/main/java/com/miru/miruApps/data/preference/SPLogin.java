package com.miru.miruApps.data.preference;

import android.content.Context;

/**
 * Created by sholfoo on 7/26/17.
 */

public class SPLogin {

    // LogCat tag
    private static String TAG = SPLogin.class.getSimpleName();
    private static final String PREF_KEY   = "bulb_login_session";
    private static final String KEY_USER_NAME = "userName";
    private static final String KEY_USER_PASSWORD = "userPassword";

    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_IS_REMEMBER = "isRemember";

    private static void setSharedPreference(Context context, String mKey, String mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, boolean mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, int mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putInt(mKey, mValue)
                .apply();
    }

    private static String getSharedPreferenceString(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getString(mKey, null);
    }

    private static boolean getSharedPreferenceBoolean(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(mKey, false);
    }

    private static int getSharedPreferenceInt(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getInt(mKey, 0);
    }

    /* GETTER SETTER */

    public String getKeyUserName(Context context) {
        return getSharedPreferenceString(context, KEY_USER_NAME);
    }

    public String getKeyUserPassword(Context context) {
        return getSharedPreferenceString(context, KEY_USER_PASSWORD);
    }

    public boolean isLoggedIn(Context context) {
        return getSharedPreferenceBoolean(context, KEY_IS_LOGGED_IN);
    }

    public boolean isRemember(Context context) {
        return getSharedPreferenceBoolean(context, KEY_IS_REMEMBER);
    }

    public void setKeyUserName(Context context, String mValue) {
        setSharedPreference(context, KEY_USER_NAME, mValue);
    }

    public void setKeyUserPassword(Context context, String mValue) {
        setSharedPreference(context, KEY_USER_PASSWORD, mValue);
    }

    public void setKeyIsLoggedIn(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_LOGGED_IN, mValue);
    }

    public void setKeyIsRemember(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_REMEMBER, mValue);
    }

    public void clearSession(Context context) {
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }
}
