package com.miru.miruApps.data.preference;

import android.content.Context;

public class SPExtraCustomer {

    // LogCat tag
    private static String TAG = SPLogin.class.getSimpleName();
    private static final String PREF_KEY   = "bulb_extra_session";
    private static final String KEY_CUST_ID = "customer_id";
    private static final String KEY_CUST_NAME = "customer_name";
    private static final String KEY_CUST_STORE_NAME = "customer_store_name";
    private static final String KEY_CUST_PHONE = "customer_phone";
    private static final String KEY_CUST_MOBILE = "customer_mobile";
    private static final String KEY_CUST_EMAIL = "customer_email";
    private static final String KEY_CUST_ADDRESS = "customer_address";
    private static final String KEY_CUST_CITY = "customer_city";
    private static final String KEY_CUST_POSTAL_CODE = "customer_postal_code";

    private static final String KEY_IS_STORED = "isStored";

    private static void setSharedPreference(Context context, String mKey, String mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, boolean mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, int mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putInt(mKey, mValue)
                .apply();
    }

    private static String getSharedPreferenceString(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getString(mKey, null);
    }

    private static boolean getSharedPreferenceBoolean(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(mKey, false);
    }

    private static int getSharedPreferenceInt(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getInt(mKey, 0);
    }

    /* GETTER SETTER */

    public String getKeyCustId(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_ID);
    }

    public String getKeyCustName(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_NAME);
    }

    public String getKeyCustStoreName(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_STORE_NAME);
    }

    public String getKeyCustPhone(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_PHONE);
    }

    public String getKeyCustMobile(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_MOBILE);
    }

    public String getKeyCustEmail(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_EMAIL);
    }

    public String getKeyCustAddress(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_ADDRESS);
    }

    public String getKeyCustCity(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_CITY);
    }

    public String getKeyCustPostalCode(Context context) {
        return getSharedPreferenceString(context, KEY_CUST_POSTAL_CODE);
    }

    public boolean isStored(Context context) {
        return getSharedPreferenceBoolean(context, KEY_IS_STORED);
    }

    public void setKeyCustId(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_ID, mValue);
    }

    public void setKeyCustName(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_NAME, mValue);
    }

    public void setKeyCustStoreName(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_STORE_NAME, mValue);
    }

    public void setKeyCustPhone(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_PHONE, mValue);
    }

    public void setKeyCustMobile(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_MOBILE, mValue);
    }

    public void setKeyCustEmail(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_EMAIL, mValue);
    }

    public void setKeyCustAddress(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_ADDRESS, mValue);
    }

    public void setKeyCustCity(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_CITY, mValue);
    }

    public void setKeyCustPostalCode(Context context, String mValue) {
        setSharedPreference(context, KEY_CUST_POSTAL_CODE, mValue);
    }

    public void setKeyIsStored(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_STORED, mValue);
    }

    public void clearSession(Context context) {
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }

}
