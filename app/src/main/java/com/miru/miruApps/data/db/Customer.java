package com.miru.miruApps.data.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class Customer {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "customer_id")
    private String customerId;

    @Property(nameInDb = "full_name")
    private String fullName;

    @Property(nameInDb = "store_name")
    private String storeName;

    @Property(nameInDb = "phone_number")
    private String phoneNumber;

    @Property(nameInDb = "mobile_number")
    private String mobileNumber;

    @Property(nameInDb = "email")
    private String email;

    @Property(nameInDb = "category")
    private String category;

    @Property(nameInDb = "status")
    private String status;

    @Property(nameInDb = "description")
    private String description;

    @Property(nameInDb = "tags")
    private String tags;

    @Property(nameInDb = "city")
    private String city;

    @Property(nameInDb = "zip_code")
    private String zipCode;

    @Property(nameInDb = "full_address")
    private String fullAddress;

    @Property(nameInDb = "created_at")
    private String createdAt;

    @Property(nameInDb = "updated_at")
    private String updatedAt;

    @Generated(hash = 296230872)
    public Customer(Long id, String customerId, String fullName, String storeName,
            String phoneNumber, String mobileNumber, String email, String category,
            String status, String description, String tags, String city,
            String zipCode, String fullAddress, String createdAt,
            String updatedAt) {
        this.id = id;
        this.customerId = customerId;
        this.fullName = fullName;
        this.storeName = storeName;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.category = category;
        this.status = status;
        this.description = description;
        this.tags = tags;
        this.city = city;
        this.zipCode = zipCode;
        this.fullAddress = fullAddress;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    @Generated(hash = 60841032)
    public Customer() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStoreName() {
        return this.storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return this.tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getFullAddress() {
        return this.fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
