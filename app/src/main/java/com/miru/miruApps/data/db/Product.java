package com.miru.miruApps.data.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class Product {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "product_sku")
    private String productSku;

    @Property(nameInDb = "product_brand")
    private String productBrand;

    @Property(nameInDb = "product_name")
    private String productName;

    @Property(nameInDb = "product_category")
    private String productKategory;

    @Property(nameInDb = "product_description")
    private String productDescription;

    @Property(nameInDb = "product_unit")
    private String productUnit;

    @Property(nameInDb = "product_size")
    private String productSize;

    @Property(nameInDb = "product_images")
    private String productImages;

    @Property(nameInDb = "product_basic_price")
    private String productBasicPrice;

    @Property(nameInDb = "product_selling_price")
    private String productSellingPrice;

    @Property(nameInDb = "product_discount")
    private String productDiscount;

    @Property(nameInDb = "created_at")
    private String createdAt;

    @Property(nameInDb = "updated_at")
    private String updatedAt;

    @Property(nameInDb = "is_cheap_price")
    private boolean isCheapPrice;

    @Generated(hash = 1964631707)
    public Product(Long id, String productSku, String productBrand,
            String productName, String productKategory, String productDescription,
            String productUnit, String productSize, String productImages,
            String productBasicPrice, String productSellingPrice,
            String productDiscount, String createdAt, String updatedAt,
            boolean isCheapPrice) {
        this.id = id;
        this.productSku = productSku;
        this.productBrand = productBrand;
        this.productName = productName;
        this.productKategory = productKategory;
        this.productDescription = productDescription;
        this.productUnit = productUnit;
        this.productSize = productSize;
        this.productImages = productImages;
        this.productBasicPrice = productBasicPrice;
        this.productSellingPrice = productSellingPrice;
        this.productDiscount = productDiscount;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.isCheapPrice = isCheapPrice;
    }

    @Generated(hash = 1890278724)
    public Product() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductSku() {
        return this.productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getProductBrand() {
        return this.productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductKategory() {
        return this.productKategory;
    }

    public void setProductKategory(String productKategory) {
        this.productKategory = productKategory;
    }

    public String getProductDescription() {
        return this.productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductUnit() {
        return this.productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public String getProductSize() {
        return this.productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductImages() {
        return this.productImages;
    }

    public void setProductImages(String productImages) {
        this.productImages = productImages;
    }

    public String getProductBasicPrice() {
        return this.productBasicPrice;
    }

    public void setProductBasicPrice(String productBasicPrice) {
        this.productBasicPrice = productBasicPrice;
    }

    public String getProductSellingPrice() {
        return this.productSellingPrice;
    }

    public void setProductSellingPrice(String productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getProductDiscount() {
        return this.productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean getIsCheapPrice() {
        return this.isCheapPrice;
    }

    public void setIsCheapPrice(boolean isCheapPrice) {
        this.isCheapPrice = isCheapPrice;
    }

}
