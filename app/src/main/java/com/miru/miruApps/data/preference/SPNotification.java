package com.miru.miruApps.data.preference;

import android.content.Context;

/**
 * Created by sholfoo on 8/2/17.
 */

public class SPNotification {

    // LogCat tag
    private static String TAG = SPNotification.class.getSimpleName();
    private static final String PREF_KEY   = "bulb_notification_counter_session";
    private static final String KEY_COUNTER = "counter";
    private static final String KEY_IS_STORED = "isStored";

    private static void setSharedPreference(Context context, String mKey, String mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, boolean mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(mKey, mValue)
                .apply();
    }

    private static void setSharedPreference(Context context, String mKey, int mValue){
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .putInt(mKey, mValue)
                .apply();
    }

    private static String getSharedPreferenceString(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getString(mKey, null);
    }

    private static boolean getSharedPreferenceBoolean(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getBoolean(mKey, false);
    }

    private static int getSharedPreferenceInt(Context context, String mKey){
        return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .getInt(mKey, 0);
    }

    /* GETTER SETTER */

    public void setKeyCounter(Context context, int mValue) {
        setSharedPreference(context, KEY_COUNTER, mValue);
    }

    public int getKeyCounter(Context context) {
        return getSharedPreferenceInt(context, KEY_COUNTER);
    }

    public int isStored(Context context) {
        return getSharedPreferenceInt(context, KEY_IS_STORED);
    }

    public void setKeyIsStored(Context context, boolean mValue) {
        setSharedPreference(context, KEY_IS_STORED, mValue);
    }

    public void clearSession(Context context) {
        context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }
}
