package com.miru.miruApps.eventbus.invoice;

import com.miru.miruApps.model.retrieve.RetrieveInvoiceProductData;

public class EventInvoiceProduct {
    RetrieveInvoiceProductData retrieveInvoiceProductData;

    public EventInvoiceProduct(RetrieveInvoiceProductData retrieveInvoiceProductData) {
        this.retrieveInvoiceProductData = retrieveInvoiceProductData;
    }

    public RetrieveInvoiceProductData getRetrieveInvoiceProductData() {
        return retrieveInvoiceProductData;
    }

    public void setRetrieveInvoiceProductData(RetrieveInvoiceProductData retrieveInvoiceProductData) {
        this.retrieveInvoiceProductData = retrieveInvoiceProductData;
    }
}
