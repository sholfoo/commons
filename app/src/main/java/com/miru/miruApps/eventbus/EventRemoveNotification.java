package com.miru.miruApps.eventbus;

/**
 * Created by sholfoo on 8/7/17.
 */

public class EventRemoveNotification {

    private boolean isRead;

    public EventRemoveNotification(boolean isRead) {
        this.isRead = isRead;
    }

    public boolean isRead() {
        return isRead;
    }
}
