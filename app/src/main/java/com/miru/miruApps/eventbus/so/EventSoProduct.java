package com.miru.miruApps.eventbus.so;

import com.miru.miruApps.model.retrieve.RetrieveSoProductData;

public class EventSoProduct {
    RetrieveSoProductData retrieveSoProductData;

    public EventSoProduct(RetrieveSoProductData retrieveSoProductData) {
        this.retrieveSoProductData = retrieveSoProductData;
    }

    public RetrieveSoProductData getRetrieveSoProductData() {
        return retrieveSoProductData;
    }

    public void setRetrieveSoProductData(RetrieveSoProductData retrieveSoProductData) {
        this.retrieveSoProductData = retrieveSoProductData;
    }
}
