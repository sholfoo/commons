package com.miru.miruApps.eventbus.invoice;

import com.miru.miruApps.model.retrieve.RetrieveInvoiceInfoData;

public class EventInvoiceCustomer {
    RetrieveInvoiceInfoData retrieveInvoiceInfoData;

    public EventInvoiceCustomer(RetrieveInvoiceInfoData retrieveInvoiceInfoData) {
        this.retrieveInvoiceInfoData = retrieveInvoiceInfoData;
    }

    public RetrieveInvoiceInfoData getRetrieveInvoiceInfoData() {
        return retrieveInvoiceInfoData;
    }

    public void setRetrieveInvoiceInfoData(RetrieveInvoiceInfoData retrieveInvoiceInfoData) {
        this.retrieveInvoiceInfoData = retrieveInvoiceInfoData;
    }
}
