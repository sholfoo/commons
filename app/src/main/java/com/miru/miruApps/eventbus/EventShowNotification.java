package com.miru.miruApps.eventbus;

/**
 * Created by sholfoo on 8/7/17.
 */

public class EventShowNotification {

    private int notif;

    public EventShowNotification(int notif) {
        this.notif = notif;
    }

    public int getNotif() {
        return notif;
    }
}
