package com.miru.miruApps.eventbus;

import com.miru.miruApps.model.retrieve.RetrieveCustomerData;

public class EventCustomer {

    private RetrieveCustomerData mRetrieveCustomerData;

    public EventCustomer(RetrieveCustomerData mRetrieveCustomerData) {
        this.mRetrieveCustomerData = mRetrieveCustomerData;
    }

    public RetrieveCustomerData getmRetrieveCustomerData() {
        return mRetrieveCustomerData;
    }

    public void setmRetrieveCustomerData(RetrieveCustomerData mRetrieveCustomerData) {
        this.mRetrieveCustomerData = mRetrieveCustomerData;
    }
}
