package com.miru.miruApps.eventbus;

import com.miru.miruApps.model.retrieve.RetrieveProductData;

public class EventProduct {

    private RetrieveProductData retrieveProductData;

    public EventProduct(RetrieveProductData retrieveProductData) {
        this.retrieveProductData = retrieveProductData;
    }

    public RetrieveProductData getRetrieveProductData() {
        return retrieveProductData;
    }

    public void setRetrieveProductData(RetrieveProductData retrieveProductData) {
        this.retrieveProductData = retrieveProductData;
    }
}
