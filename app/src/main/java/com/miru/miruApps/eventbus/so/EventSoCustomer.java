package com.miru.miruApps.eventbus.so;

import com.miru.miruApps.model.retrieve.RetrieveSoInfoData;

public class EventSoCustomer {

    RetrieveSoInfoData retrieveSoInfoData;

    public EventSoCustomer(RetrieveSoInfoData retrieveSoInfoData) {
        this.retrieveSoInfoData = retrieveSoInfoData;
    }

    public RetrieveSoInfoData getRetrieveSoInfoData() {
        return retrieveSoInfoData;
    }

    public void setRetrieveSoInfoData(RetrieveSoInfoData retrieveSoInfoData) {
        this.retrieveSoInfoData = retrieveSoInfoData;
    }
}
