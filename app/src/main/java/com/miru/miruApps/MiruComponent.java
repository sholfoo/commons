package com.miru.miruApps;

import com.miru.miruApps.dagger.PerApp;
import com.miru.miruApps.network.NetworkModule;
import com.miru.miruApps.ui.activity.MiruUserInfoActivity;
import com.miru.miruApps.ui.activity.customer.MiruCustomerEditActivity;
import com.miru.miruApps.ui.activity.customer.MiruCustomerListActivity;
import com.miru.miruApps.ui.activity.dashboard.MiruDashboardActivity;
import com.miru.miruApps.ui.activity.invoice.MiruInvoiceListActivity;
import com.miru.miruApps.ui.activity.invoice.MiruMainDetailInvoiceActivity;
import com.miru.miruApps.ui.activity.invoice.MiruMainEditInvoiceActivity;
import com.miru.miruApps.ui.activity.login.MiruRegisterActivity;
import com.miru.miruApps.ui.activity.payment.MiruPaymentCreateActivity;
import com.miru.miruApps.ui.activity.payment.MiruPaymentListActivity;
import com.miru.miruApps.ui.activity.product.MiruProductDetailActivity;
import com.miru.miruApps.ui.activity.product.MiruProductEditActivity;
import com.miru.miruApps.ui.activity.product.MiruProductListActivity;
import com.miru.miruApps.ui.activity.product.MiruProductListDialogActivity;
import com.miru.miruApps.ui.activity.salesorder.MiruSODetailActivity;
import com.miru.miruApps.ui.activity.salesorder.MiruSOEditActivity;
import com.miru.miruApps.ui.activity.salesorder.MiruSOListActivity;
import com.miru.miruApps.ui.base.BaseLoginActivity;
import com.miru.miruApps.ui.fragment.customer.CreateCustLocationFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustInvoiceFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustPaymentFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustPerformanceFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustProfileFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustSalesOrderFragment;
import com.miru.miruApps.ui.fragment.customer.EditCustLocationFragment;
import com.miru.miruApps.ui.fragment.invoice.CreateInvoiceReviewFragment;
import com.miru.miruApps.ui.fragment.invoice.DetailInvoiceFragment;
import com.miru.miruApps.ui.fragment.invoice.DetailPaymentFragment;
import com.miru.miruApps.ui.fragment.invoice.EditInvoiceReviewFragment;
import com.miru.miruApps.ui.fragment.main.HomeFragment;
import com.miru.miruApps.ui.fragment.main.NotificationFragment;
import com.miru.miruApps.ui.fragment.main.ProfileFragment;
import com.miru.miruApps.ui.fragment.product.CreateProductDescriptionFragment;
import com.miru.miruApps.ui.fragment.product.CreateProductPriceFragment;
import com.miru.miruApps.ui.fragment.product.EditProductDescFragment;
import com.miru.miruApps.ui.fragment.product.EditProductPriceFragment;
import com.miru.miruApps.ui.fragment.salesorder.CreateSOReviewFragment;
import com.miru.miruApps.ui.fragment.salesorder.DetailSOInvoiceFragment;
import com.miru.miruApps.ui.fragment.salesorder.DetailSOOrderFragment;
import com.miru.miruApps.ui.fragment.salesorder.EditSOReviewFragment;

import dagger.Component;

@PerApp
@Component(modules = {
        NetworkModule.class
})
public interface MiruComponent {

    // per app
    void inject(BaseLoginActivity activity);
    void inject(ProfileFragment fragment);
    void inject(MiruUserInfoActivity activity);
    void inject(MiruRegisterActivity activity);

    // for master data
    void inject(MiruCustomerListActivity activity);
    void inject(MiruProductListActivity activity);
    void inject(DetailCustProfileFragment fragment);
    void inject(CreateCustLocationFragment fragment);
    void inject(CreateProductPriceFragment fragment);
    void inject(CreateProductDescriptionFragment fragment);
    void inject(MiruProductDetailActivity activity);
    void inject(EditCustLocationFragment fragment);
    void inject(EditProductDescFragment fragment);
    void inject(EditProductPriceFragment fragment);
    void inject(MiruProductListDialogActivity activity);

    // for sales order
    void inject(CreateSOReviewFragment fragment);
    void inject(MiruSOListActivity activity);
    void inject(DetailSOOrderFragment fragment);
    void inject(DetailCustSalesOrderFragment fragment);
    void inject(NotificationFragment fragment);
    void inject(EditSOReviewFragment fragment);

    // for invoice payment
    void inject(MiruDashboardActivity activity);
    void inject(HomeFragment fragment);
    void inject(MiruInvoiceListActivity activity);
    void inject(CreateInvoiceReviewFragment fragment);
    void inject(EditInvoiceReviewFragment fragment);
    void inject(MiruPaymentListActivity activity);
    void inject(MiruPaymentCreateActivity activity);
    void inject(DetailInvoiceFragment fragment);
    void inject(DetailSOInvoiceFragment fragment);
    void inject(DetailPaymentFragment fragment);
    void inject(DetailCustInvoiceFragment fragment);
    void inject(DetailCustPaymentFragment fragment);
    void inject(DetailCustPerformanceFragment fragment);

    void inject(MiruMainEditInvoiceActivity activity);
    void inject(MiruSOEditActivity activity);
    void inject(MiruMainDetailInvoiceActivity activity);
    void inject(MiruSODetailActivity activity);
    void inject(MiruCustomerEditActivity activity);
    void inject(MiruProductEditActivity activity);


    final class initializer {
        public static MiruComponent init(MiruApp MiruApp) {
            return DaggerMiruComponent.builder().networkModule(new NetworkModule()).build();
        }
    }
}
