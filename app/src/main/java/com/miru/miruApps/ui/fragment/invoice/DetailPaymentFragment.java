package com.miru.miruApps.ui.fragment.invoice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.PaymentAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Payment;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.activity.payment.MiruPaymentCreateActivity;
import com.miru.miruApps.ui.base.BaseFragment;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_FROM_INVOICE;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class DetailPaymentFragment extends BaseFragment {

    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.txtNoDataTitle) TextView txtNoDataTitle;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.ly_error) LinearLayout lyError;

    @Inject InvoiceApiService mApiService;
    private PaymentAdapter mAdapter;

    @OnClick(R.id.action_create_payment)
    void onActionCreatePayment(){
        Intent intentCreatePayment = new Intent(getActivity(), MiruPaymentCreateActivity.class);
        intentCreatePayment.putExtra(   "extra_from_id", EXTRA_FROM_INVOICE);
        intentCreatePayment.putExtra(   "invoiceId", getArgumentExtraInvoiceId());
        intentCreatePayment.putExtra("extInvoiceId", getArgumentExtraExtInvoiceId());
        startActivity(intentCreatePayment);
    }

    public static DetailPaymentFragment newInstance(String invoiceId, String extInvoiceId) {
        Bundle bundle = new Bundle();
        bundle.putString("invoiceId", invoiceId);
        bundle.putString("extInvoiceId", extInvoiceId);
        DetailPaymentFragment detailPaymentFragment = new DetailPaymentFragment();
        detailPaymentFragment.setArguments(bundle);
        return detailPaymentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice_detail_payment, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new PaymentAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mAdapter);

        lyRefresh.setColorSchemeResources(R.color.colorAccent);
        lyRefresh.setOnRefreshListener(this::initializePayments);

        initializePayments();
    }

    private String getArgumentExtraInvoiceId() {
        return getArguments() != null ? getArguments().getString("invoiceId") : null;
    }

    private String getArgumentExtraExtInvoiceId() {
        return getArguments() != null ? getArguments().getString("extInvoiceId") : null;
    }

    private void initializePayments() {
        pbLoad.setVisibility(View.VISIBLE);
        Observable<Payment> paymentObservable = mApiService.getPaymentByInvoiceList(
                new SPUser().getKeyUserClientId(mContext),
                getArgumentExtraExtInvoiceId());

        paymentObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Payment>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Payment payment) {
                        if (payment.getStatusCode() == STATUS_SUCCESS) {
                            Log.e("Size", String.valueOf(payment.getPayment().size()));
                            mAdapter.pushData(payment.getPayment());
                            resetUILoading();
                        } else
                            showErrorView();
                    }
                });
    }


    protected void resetUILoading() {
        hideErrorView();
        if (isAdded()){
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
        }
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        if (isAdded()){
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
            lyError.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(v -> {
                hideErrorView();
                Intent intentCreatePayment = new Intent(getActivity(), MiruPaymentCreateActivity.class);
                intentCreatePayment.putExtra(   "extra_from_id", EXTRA_FROM_INVOICE);
                intentCreatePayment.putExtra(   "invoiceId", getArgumentExtraInvoiceId());
                intentCreatePayment.putExtra("extInvoiceId", getArgumentExtraExtInvoiceId());
                startActivity(intentCreatePayment);
            });

            if (!TextUtils.isEmpty(text))
                txtNoDataTitle.setText(getString(R.string.error_no_payment_data));
        }
    }

    protected void hideErrorView() {
        if (isAdded()){
            lyError.setVisibility(View.GONE);
        }
    }
}
