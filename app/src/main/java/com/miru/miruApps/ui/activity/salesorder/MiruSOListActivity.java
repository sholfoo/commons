package com.miru.miruApps.ui.activity.salesorder;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.so.SalesOrderAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.SalesOrder;
import com.miru.miruApps.network.SoApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.FORMAT_DATE;
import static com.miru.miruApps.config.Constants.NULL;
import static com.miru.miruApps.config.Constants.SO_PARAMETER_ALL;
import static com.miru.miruApps.config.Constants.SO_PARAMETER_DATE;
import static com.miru.miruApps.config.Constants.STATUS_ERROR;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruSOListActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    @Inject SoApiService mApiService;

    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.date_day) TextView dateDay;
    @BindView(R.id.date_days_of_month) TextView dateDaysOfMonth;
    @BindView(R.id.date_month_year) TextView dateMonthYear;
    @BindView(R.id.waiting_process_order_quantity) TextView waitingProcessOrderQuantity;
    @BindView(R.id.waiting_process_order_amount) TextView waitingProcessOrderAmount;
    @BindView(R.id.process_order_quantity) TextView processOrderQuantity;
    @BindView(R.id.process_order_amount) TextView processOrderAmount;
    @BindView(R.id.header) RecyclerViewHeader header;
    @BindView(R.id.ly_list) FrameLayout lyList;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;

    private SalesOrderAdapter mAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_change_date)
    void onActionChangeDate(){
        createDatePickerDialog();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_so_list);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViewsAndAdapter();
    }

    protected void createDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(
                this,
                R.style.MiruApp_DatePicker,
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.show();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_sales_order);
        Drawable img = getResources().getDrawable(R.drawable.ic_actionbar_so);
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);
    }

    private void setupViewsAndAdapter() {
//        dateDay.setText(DateTimeUtil.getCurrentFormat("EEE"));
//        dateDaysOfMonth.setText(DateTimeUtil.getCurrentFormat("dd"));
//        dateMonthYear.setText(
//                String.format("%s %s",
//                        DateTimeUtil.getCurrentFormat("MMM"),
//                        DateTimeUtil.getCurrentFormat("yyyy"))
//        );

        mAdapter = new SalesOrderAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);
        header.attachTo(lvContent);

        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> initializeSalesOrderList(SO_PARAMETER_ALL, NULL));

        initializeSalesOrderList(SO_PARAMETER_ALL, NULL);
    }

    private void initializeSalesOrderList(int parameter, String creationDate) {
        pbLoad.setVisibility(View.VISIBLE);
        lyList.setVisibility(View.GONE);
        Observable<SalesOrder> salesOrderObservable = mApiService.getSalesOrderList(new SPUser().getKeyUserClientId(this),
                parameter,
                creationDate);

        salesOrderObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SalesOrder>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(SalesOrder salesOrder) {
                        if (salesOrder.getStatusCode() == STATUS_SUCCESS) {
                            resetUILoading();
                            Collections.reverse(salesOrder.getOrders());
                            mAdapter.pushData(salesOrder.getOrders());
                            waitingProcessOrderQuantity.setText(
                                    String.valueOf(salesOrder.getInfo().getNumOfUnprocessedOrders()));
                            waitingProcessOrderAmount.setText(
                                    FormatNumber.getFormatCurrencyIDR(
                                            Integer.parseInt(salesOrder.getInfo()
                                                    .getSumOfUnprocessedOrders())));
                            processOrderQuantity.setText(
                                    String.valueOf(salesOrder.getInfo().getNumOfProcessedOrders()));
                            processOrderAmount.setText(
                                    FormatNumber.getFormatCurrencyIDR(
                                            Integer.parseInt(salesOrder.getInfo()
                                                    .getSumOfProcessedOrders())));
                        } else if (salesOrder.getStatusCode() == STATUS_ERROR){
                            showErrorView("Belum ada Pemesanan");
                        } else
                            showErrorView();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu_with_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setQueryHint("Cari");
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().isEmpty()){
                    initializeSalesOrderList(SO_PARAMETER_ALL, NULL);
                }

                mAdapter.getFilter().filter(query);
                searchViewAndroidActionBar.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().isEmpty()){
                    initializeSalesOrderList(SO_PARAMETER_ALL, NULL);
                }
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_new:
                startActivity(new Intent(this, MiruSOCreateActivity.class));
                return true;
            case R.id.action_search:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyList.setVisibility(View.VISIBLE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initializeSalesOrderList(SO_PARAMETER_ALL, NULL);
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthStr = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
        String yearStr = String.valueOf(year);

        String dateStr = dayStr + "-" + monthStr + "-" + yearStr;
        dateDay.setText(DateTimeUtil.getConvertedFormat(dateStr, FORMAT_DATE, "EEE"));
        dateDaysOfMonth.setText(DateTimeUtil.getConvertedFormat(dateStr, FORMAT_DATE, "dd"));
        dateMonthYear.setText(DateTimeUtil.getConvertedFormat(dateStr, FORMAT_DATE, "MMM yyyy"));
        initializeSalesOrderList(SO_PARAMETER_DATE, dateStr);
    }
}
