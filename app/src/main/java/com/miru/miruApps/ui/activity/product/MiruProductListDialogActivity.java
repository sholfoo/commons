package com.miru.miruApps.ui.activity.product;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ProductAdapter;
import com.miru.miruApps.adapter.ProductDialogAdapter;
import com.miru.miruApps.data.db.Category;
import com.miru.miruApps.data.db.CategoryDao;
import com.miru.miruApps.data.db.Product;
import com.miru.miruApps.data.db.ProductDao;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.CataloguePick;
import com.miru.miruApps.model.response.ProductCategory;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.activity.product.filtering.MiruProductFilterActivity;
import com.miru.miruApps.ui.activity.product.filtering.MiruProductShortingActivity;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_BRAND;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_DISCOUNT;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_MAX;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_MIN;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_PRICE;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_QTY;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_SHORT;
import static com.miru.miruApps.config.Constants.NULL;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruProductListDialogActivity extends BaseActivity {
    private static final int FILTER_REQ = 1001;
    private static final int SHORTING_REQ = 1002;

    @Inject MasterApiService mMasterApiService;

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.spn_product_category) MaterialBetterSpinner spnProductCategory;
    @BindView(R.id.ly_search) LinearLayout lySearch;
    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;

    private ProductDialogAdapter mProductAdapter;
    private ProductDao productDao;
    private Query<Product> productQuery;

    private CategoryDao categoryDao;
    private Query<com.miru.miruApps.data.db.Category> categoryQuery;

    private CharSequence filterStr;
    private CataloguePick cataloguePick;
    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_pick_product)
    void onActionPickProduct() {
        Log.e("Picked", String.valueOf(mProductAdapter.getPickedSoProduct().size()));
        cataloguePick = new CataloguePick(mProductAdapter.getPickedSoProduct());
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", cataloguePick);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        productDao.deleteAll();
        categoryDao.deleteAll();
        initListProduct();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_list_for_pick);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        productDao = MiruApp.getDaoSession().getProductDao();
        categoryDao = MiruApp.getDaoSession().getCategoryDao();

        productQuery = productDao
                .queryBuilder()
                .orderAsc(ProductDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FILTER_REQ) {

            // The resultCode is set by the AnotherActivity
            // By convention RESULT_OK means that what ever
            // AnotherActivity did was successful
            if(resultCode == Activity.RESULT_OK) {
                // Get the result from the returned Intent
                final String productBrand = data.getStringExtra(EXTRA_PRODUCT_BRAND);
                final String productDiscount = data.getStringExtra(EXTRA_PRODUCT_DISCOUNT);
                final String productMin = data.getStringExtra(EXTRA_PRODUCT_MIN);
                final String productMax = data.getStringExtra(EXTRA_PRODUCT_MAX);
                final String productQty = data.getStringExtra(EXTRA_PRODUCT_QTY);

                // Use the data - in this case, display it in a Toast.
                Toast.makeText(this, "Result: " + productBrand
                        + " | " + productDiscount
                        + " | " + productMin
                        + " | " + productMax
                        + " | " + productQty, Toast.LENGTH_LONG).show();
            } else {
                // AnotherActivity was not successful. No data to retrieve.
            }
        } else if (requestCode == SHORTING_REQ) {
            if (resultCode == Activity.RESULT_OK){
                final String productPrice = data.getStringExtra(EXTRA_PRODUCT_PRICE);
                final String productShort = data.getStringExtra(EXTRA_PRODUCT_SHORT);

                // Use the data - in this case, display it in a Toast.
                Toast.makeText(this, "Result: " + productPrice
                                + " | " + productShort
                        , Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setupActionBar(){
        toolbarLogo.setText(getResources().getString(R.string.action_bar_product));
        Drawable img = getResources().getDrawable( R.drawable.ic_actionbar_product );
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);

    }

    private void setupViews() {
        mProductAdapter = new ProductDialogAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mProductAdapter);
        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> {
            initListProduct();
        });
        initListProduct();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setQueryHint("Cari");
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().isEmpty()){
                    initListProduct();
                }

                mProductAdapter.getFilter().filter(query);
                searchViewAndroidActionBar.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().isEmpty()){
                    initListProduct();
                }
                mProductAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void initListProduct() {
        pbLoad.setVisibility(View.VISIBLE);
        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(this));
        Observable<com.miru.miruApps.model.response.Product> productObservable = mMasterApiService.getProducts(requestClientId);
        productObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<com.miru.miruApps.model.response.Product>() {
                    @Override
                    public void onCompleted() {
                        getProductCategories();
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(com.miru.miruApps.model.response.Product product) {
                        if (product.getStatusCode() == STATUS_SUCCESS){
                            mProductAdapter.pushData(product.getProductContainer());
                            saveToSQLiteDatabase(product);
                        } else
                            showErrorView();
                    }
                });
    }

    private void getProductCategories() {
        Observable<ProductCategory> productCategoryObservable = mMasterApiService.getProductCategory(
                new SPUser().getKeyUserClientId(this));
        productCategoryObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ProductCategory>() {
                    @Override
                    public void onCompleted() {
                        resetUILoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(ProductCategory productCategory) {
                        if (productCategory.getStatusCode() == STATUS_SUCCESS){
                            setupAdapterCategory(productCategory);
                        }
                    }
                });
    }

    private void setupAdapterCategory(ProductCategory productCategory) {
        if (!spnProductCategory.getText().toString().equalsIgnoreCase("semua produk")){
            mProductAdapter.getFilter().filter(spnProductCategory.getText());
        }

        List<String> categoryList = new ArrayList<>();
        for (int i = 0; i < productCategory.getCatalog().size(); i++) {
            Category category = new Category();
            category.setCategoryId(productCategory.getCatalog().get(i).getCategoryId());
            category.setCategoryName(StringUtils.capitalize(productCategory.getCatalog().get(i).getCategory().toLowerCase()));
            categoryDao.insert(category);
            categoryList.add(StringUtils.capitalize(productCategory.getCatalog().get(i).getCategory().toLowerCase()));
        }
        categoryList.add("Semua Produk");
        Collections.reverse(categoryList);
        String[] categories = categoryList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, categories);
        spnProductCategory.setAdapter(adapter);
        spnProductCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (spnProductCategory.getText().toString().equalsIgnoreCase("semua produk")){
                    initListProduct();
                } else {
                    mProductAdapter.getFilter().filter(spnProductCategory.getText());
                }
            }
        });
    }

    private void saveToSQLiteDatabase(com.miru.miruApps.model.response.Product product){

        for (int i = 0; i < product.getProductContainer().size(); i++){
            int hargaTermurah = 0;
            for (int j = 0; j < product.getProductContainer().get(i).getProductPrices().size(); j++){
                com.miru.miruApps.data.db.Product productDb = new com.miru.miruApps.data.db.Product();

                if (hargaTermurah == 0) {
                    hargaTermurah = product
                            .getProductContainer()
                            .get(i)
                            .getProductPrices()
                            .get(j)
                            .getPricePerUnit();
                    productDb.setIsCheapPrice(true);
                } else {
                    if (product.getProductContainer().get(i).getProductPrices().get(j).getPricePerUnit() < hargaTermurah) {
                        hargaTermurah = product
                                .getProductContainer()
                                .get(i)
                                .getProductPrices()
                                .get(j)
                                .getPricePerUnit();

                        List<com.miru.miruApps.data.db.Product> productList = productDao.queryBuilder()
                                .where(ProductDao.Properties.ProductSku.eq(
                                        product.getProductContainer().get(i).getItemSKU())).list();

                        for (com.miru.miruApps.data.db.Product mProduct : productList){
                            mProduct.setIsCheapPrice(false);
                            productDao.update(mProduct);
                        }
                        productDb.setIsCheapPrice(true);
                    } else {
                        productDb.setIsCheapPrice(false);
                    }
                }

                productDb.setProductSku(product.getProductContainer().get(i).getItemSKU());
                productDb.setProductName(product.getProductContainer().get(i).getProductName());
                productDb.setProductKategory(product.getProductContainer().get(i).getCategory());
                productDb.setProductDescription(isNull(product.getProductContainer().get(i).getDescription()));
                productDb.setProductSize(isNull(product.getProductContainer().get(i).getSize()));
                productDb.setProductBrand(isNull(product.getProductContainer().get(i).getBrand()));
                productDb.setProductImages(product.getProductContainer().get(i).getImageLinks().toString());
                productDb.setCreatedAt(DateTimeUtil.getCurrentFormat("dd-mm-yyyy"));
                productDb.setUpdatedAt(DateTimeUtil.getCurrentFormat("dd-mm-yyyy"));
                productDb.setProductUnit(product.getProductContainer().get(i).getProductPrices().get(j).getIdentifierId().getMeasurementUnit());
                productDb.setProductBasicPrice(String.valueOf(product.getProductContainer().get(i).getProductPrices().get(j).getPricePerUnit()));
                productDb.setProductSellingPrice(String.valueOf(product.getProductContainer().get(i).getProductPrices().get(j).getSellingPrice()));
                productDb.setProductDiscount(String.valueOf(product.getProductContainer().get(i).getProductPrices().get(j).getDiscount()));
                productDao.insert(productDb);
            }
        }

        List<com.miru.miruApps.data.db.Product> productList = productQuery.list();
        for (int i = 0; i < productList.size(); i++){
            Log.e("Product", productList.get(i).getProductSku());
            Log.e("Product", productList.get(i).getProductName());
            Log.e("Product PCS", productList.get(i).getProductUnit());
            Log.e("Product", productList.get(i).getProductBasicPrice());
            Log.e("Product", productList.get(i).getIsCheapPrice() + "");
            Log.e("Product","=======================================");

        }
    }

    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }


    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initListProduct();
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }

    private String isNull(String mValue){
        if (TextUtils.isEmpty(mValue)){
            return NULL;
        }
        return mValue;
    }
}
