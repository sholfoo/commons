package com.miru.miruApps.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseLoginActivity;
import com.miru.miruApps.view.SimpleGestureFilter;
import com.miru.miruApps.view.SimpleGestureFilter.SimpleGestureListener;

import butterknife.OnClick;

public class MiruLoginActivity extends BaseLoginActivity implements SimpleGestureListener {

    private SimpleGestureFilter detector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detector = new SimpleGestureFilter(MiruLoginActivity.this,this);
    }

    @OnClick(R.id.ly_footer)
    void onActionRegister() {
        startActivity(new Intent(MiruLoginActivity.this, MiruRegisterActivity.class));
        overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    @OnClick(R.id.action_forget_password)
    void onActionForgetPassword() {
        startActivity(new Intent(MiruLoginActivity.this, MiruForgetPasswordActivity.class));
    }

    @OnClick(R.id.action_btn_login)
    void onActionLogin() {
//        if (isPhoneAuth) {
//            startPhoneNumberVerification(inputId.getText().toString());
//        } else {
//
//        }
        attempLogin();
    }

    @OnClick(R.id.action_connect_gmail)
    void onActionGoogleSignIn(){
//        signInGoogle();
    }

    @OnClick(R.id.action_connect_facebook)
    void onActionFacebookSignIn(){
//        facebookLogin();
    }

    @OnClick(R.id.action_login_step)
    void onActionLoginStep(){
//        configureLoginAuthStep();
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {
            case SimpleGestureFilter.SWIPE_UP :
                startActivity(new Intent(MiruLoginActivity.this, MiruRegisterActivity.class));
                overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                break;

        }
    }

    @Override
    public void onDoubleTap() {

    }
}
