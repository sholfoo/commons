package com.miru.miruApps.ui.activity.invoice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.eventbus.invoice.EventInvoiceCustomer;
import com.miru.miruApps.eventbus.invoice.EventInvoiceProduct;
import com.miru.miruApps.model.retrieve.RetrieveInvoiceInfoData;
import com.miru.miruApps.model.retrieve.RetrieveInvoiceProductData;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.invoice.CreateInvoiceCustomerFragment;
import com.miru.miruApps.ui.fragment.invoice.CreateInvoiceProductFragment;
import com.miru.miruApps.ui.fragment.invoice.CreateInvoiceReviewFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiruMainCreateInvoiceActivity extends BaseActivity implements CreateInvoiceCustomerFragment.OnNextListener, CreateInvoiceProductFragment.OnReviewListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        showDiscardDialog();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_invoice_main_create);
        ButterKnife.bind(this);

        setupActionBar();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_invoice);
    }

    private void setupViewPager(NonSwipeableViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(CreateInvoiceCustomerFragment.newInstance(getExtraFromId()),
                getResources().getString(R.string.tab_title_so_customer));
        mViewPagerAdapter.addFragment(CreateInvoiceProductFragment.newInstance(),
                getResources().getString(R.string.tab_title_so_product));
        mViewPagerAdapter.addFragment(CreateInvoiceReviewFragment.newInstance(),
                getResources().getString(R.string.tab_title_so_review));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNextInfo(RetrieveInvoiceInfoData retrieveInvoiceInfoData) {
//        htabTabs.getTabAt(1).select();
        htabTabs.setScrollPosition(1, 0f, true);
        htabViewpager.setCurrentItem(1);
        EventBus.getDefault().postSticky(new EventInvoiceCustomer(retrieveInvoiceInfoData));
    }

    @Override
    public void onActionReviewproduct(RetrieveInvoiceProductData retrieveInvoiceProductData) {
//        htabTabs.getTabAt(2).select();
        htabTabs.setScrollPosition(2, 0f, true);
        htabViewpager.setCurrentItem(2);
        EventBus.getDefault().postSticky(new EventInvoiceProduct(retrieveInvoiceProductData));
    }

    private int getExtraFromId(){
        return getIntent().getIntExtra("extra_from_id", 0);
    }

    @Override
    public void onBackPressed() {
        showDiscardDialog();
    }
}
