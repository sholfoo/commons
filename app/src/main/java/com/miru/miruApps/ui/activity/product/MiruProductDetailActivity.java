package com.miru.miruApps.ui.activity.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.product.ProductImageAdapter;
import com.miru.miruApps.adapter.product.ProductPriceAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.ProductPrice;
import com.miru.miruApps.model.response.DetailProduct;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.activity.customer.MiruCustomerEditActivity;
import com.miru.miruApps.ui.base.BaseActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruProductDetailActivity extends BaseActivity {

    @BindView(R.id.action_home)
    ImageView actionHome;
    @BindView(R.id.ly_toolbar)
    Toolbar lyToolbar;
    @BindView(R.id.toolbar_logo)
    TextView toolbarLogo;
    @BindView(R.id.product_item_sku)
    TextView productItemSku;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.lv_content_product)
    RecyclerView lvContentProduct;
    @BindView(R.id.product_brand)
    TextView productBrand;
    @BindView(R.id.product_size)
    TextView productSize;
    @BindView(R.id.product_description)
    TextView productDescription;
    @BindView(R.id.lv_content_product_price)
    RecyclerView lvContentProductPrice;
    @BindView(R.id.last_update)
    TextView lastUpdate;
    @BindView(R.id.product_category)
    TextView productCategory;

    @Inject
    MasterApiService mMasterApiService;

    private ProductImageAdapter mProductImageAdapter;
    private ProductPriceAdapter mProductPriceAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_detail);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupAdapterImage();
        setupAdapterPrice();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_product_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                Intent intentEdit = new Intent(this, MiruProductEditActivity.class);
                intentEdit.putExtra("itemSKU", getExtraItemSKU());
                startActivity(intentEdit);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDetailProduct();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_detail_product);
    }

    private void initDetailProduct() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(this));
        RequestBody requestItemSku = createRequestBody(getExtraItemSKU());
        Observable<DetailProduct> detailProductObservable = mMasterApiService.getProductDetail(
                requestClientId,
                requestItemSku);
        detailProductObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DetailProduct>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(DetailProduct detailProduct) {
                        if (detailProduct.getStatusCode() == STATUS_SUCCESS) {
                            Log.e("Response", detailProduct.toString());
                            setupViews(detailProduct);
                        }
                    }
                });
    }

    private void setupAdapterImage() {
        mProductImageAdapter = new ProductImageAdapter(this);
        lvContentProduct.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_transparent));
        lvContentProduct.addItemDecoration(itemDecorator);
        lvContentProduct.setAdapter(mProductImageAdapter);
    }


    private void setupAdapterPrice() {
        mProductPriceAdapter = new ProductPriceAdapter(this);
        lvContentProductPrice.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_solid));
        lvContentProductPrice.addItemDecoration(itemDecorator);
        lvContentProductPrice.setAdapter(mProductPriceAdapter);
    }

    private void setupViews(DetailProduct detailProduct) {
        List<String> stringList = new ArrayList<>();
        List<ProductPrice> productPriceList = new ArrayList<>();
        for (int i = 0; i < detailProduct.getProductContainer().get(DEFAULT_SIZE).getImageLinks().size(); i++) {
            stringList.add(detailProduct
                    .getProductContainer()
                    .get(DEFAULT_SIZE)
                    .getImageLinks()
                    .get(i));
            Log.e("ImageResponse", detailProduct
                    .getProductContainer()
                    .get(DEFAULT_SIZE)
                    .getImageLinks().get(i));
        }

        for (int i = 0; i < detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().size(); i++) {
            productPriceList.add(new ProductPrice(
                    detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getIdentifierId().getMeasurementUnit(),
                    String.valueOf(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getPricePerUnit()),
                    String.valueOf(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getSellingPrice()),
                    String.valueOf(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getDiscount())
            ));
        }

        Collections.reverse(productPriceList);
        mProductImageAdapter.pushData(stringList);
        mProductPriceAdapter.pushData(productPriceList);
        productItemSku.setText(detailProduct.getProductContainer().get(DEFAULT_SIZE).getItemSKU());
        productName.setText(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductName());
        productCategory.setText(detailProduct.getProductContainer().get(DEFAULT_SIZE).getCategory());
        productBrand.setText(detailProduct.getProductContainer().get(DEFAULT_SIZE).getBrand());
        productSize.setText(detailProduct.getProductContainer().get(DEFAULT_SIZE).getSize());
        productDescription.setText(detailProduct.getProductContainer().get(DEFAULT_SIZE).getDescription());
        lastUpdate.setText("Diperbaharui : " + detailProduct.getProductContainer().get(DEFAULT_SIZE).getLastUpdatedTime());
    }

    private String getExtraItemSKU() {
        return getIntent().getStringExtra("itemSKU");
    }
}
