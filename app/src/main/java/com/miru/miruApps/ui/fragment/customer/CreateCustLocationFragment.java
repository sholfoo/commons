package com.miru.miruApps.ui.fragment.customer;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.data.db.CustomerDao;
import com.miru.miruApps.data.db.DaoSession;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.EventCustomer;
import com.miru.miruApps.model.response.CreateCustomer;
import com.miru.miruApps.model.retrieve.RetrieveCustomerData;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.service.GetAddressIntentService;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.yayandroid.locationmanager.base.LocationBaseFragment;
import com.yayandroid.locationmanager.configuration.Configurations;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.constants.FailType;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class CreateCustLocationFragment extends LocationBaseFragment implements OnMapReadyCallback {

    static final String TAG = CreateCustLocationFragment.class.getCanonicalName();
    protected Context mContext;
    protected LayoutInflater mInflater;

    @Inject
    MasterApiService mMasterApiService;

    @BindView(R.id.location_city)
    EditText locationCity;
    @BindView(R.id.input_layout_city)
    TextInputLayout inputLayoutCity;
    @BindView(R.id.location_postal_code)
    EditText locationPostalCode;
    @BindView(R.id.input_layout_postal_code)
    TextInputLayout inputLayoutPostalCode;
    @BindView(R.id.location_province)
    EditText locationProvince;
    @BindView(R.id.input_layout_provinces)
    TextInputLayout inputLayoutProvinces;
    @BindView(R.id.location_district)
    EditText locationDistrict;
    @BindView(R.id.input_layout_district)
    TextInputLayout inputLayoutDistrict;
    @BindView(R.id.location_address)
    EditText locationAddress;
    @BindView(R.id.input_layout_complete_address)
    TextInputLayout inputLayoutCompleteAddress;
    @BindView(R.id.action_save)
    Button actionSave;
    @BindView(R.id.places_autocomplete)
    PlacesAutocompleteTextView locationAddressOnMap;
    Fragment autoCompleteFragment;

    private ResultReceiver addressResultReceiver;
    private Geocoder geocoder;
    private Location mLocation;

    private String mAddress;
    private String mProvince;
    private String mPostalCode;
    private String mCity;
    private String mDistrict;

    private String photoPath;
    private File photoFile;
    private File compressedPhotoFile;
    protected double mLatitude;
    protected double mLongitude;

    private RetrieveCustomerData mRetrieveCustomerData;
    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;

    private CustomerDao customerDao;

    public static CreateCustLocationFragment newInstance() {
        return new CreateCustLocationFragment();
    }

    @OnClick(R.id.action_save)
    void onActionSave() {
//        if (!validateInput(inputLayoutAddressOnMap, locationAddressOnMap))
//            return;
//
//        if (!validateInput(inputLayoutCity, locationCity))
//            return;
//
//        if (!validateInput(inputLayoutPostalCode, locationPostalCode))
//            return;
//
//        if (!validateInput(inputLayoutProvinces, locationProvince))
//            return;
//
//        if (!validateInput(inputLayoutDistrict, locationDistrict))
//            return;
//
//        if (!validateInput(inputLayoutCompleteAddress, locationAddress))
//            return;

        createNewCustomer();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mInflater = LayoutInflater.from(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
        geocoder = new Geocoder(getContext(), Locale.getDefault());
        addressResultReceiver = new LocationAddressResultReceiver(new Handler());
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        DaoSession daoSession = MiruApp.getDaoSession();
        customerDao = daoSession.getCustomerDao();
    }

    @Override
    public void onResume() {
        super.onResume();
        getLocation();
        if (getLocationManager().isWaitingForLocation()
                && !getLocationManager().isAnyDialogShowing()) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_create_location, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        locationAddressOnMap.showClearButton(true);
        locationAddressOnMap.setOnPlaceSelectedListener(
                new OnPlaceSelectedListener() {
                    @Override
                    public void onPlaceSelected(final Place place) {
                        // do something awesome with the selected place
                        locationAddressOnMap.setText(place.description);
                    }
                }
        );

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(EventCustomer eventCustomer) {
        this.mRetrieveCustomerData = eventCustomer.getmRetrieveCustomerData();
        Log.e(TAG, "Implementation CreateCustLocationFragment Running...");
    }

    @Override
    public LocationConfiguration getLocationConfiguration() {
        return Configurations.defaultConfiguration("Permission Request!",
                "Would you mind to turn GPS on?");
    }

    private RequestBody createRequestBody(String mValue) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), mValue);
    }

    private MultipartBody.Part prepareFilePart(String partName, File documentFile) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(
                MediaType.parse("multipart/form-data"), documentFile);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, documentFile.getName(), requestFile);
    }

    private void createNewCustomer() {
        MaterialDialog submitDialog = new MaterialDialog.Builder(mContext)
                .content("Submitting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        MultipartBody.Part[] imagePart = null;
        if (mRetrieveCustomerData.getImages().size() != 0) {
            imagePart = new MultipartBody.Part[mRetrieveCustomerData.getImages().size()];
            for (int index = 0; index < mRetrieveCustomerData.getImages().size(); index++) {
                Log.d("Part", "requestUploadSurvey: survey image " + index + "  " + mRetrieveCustomerData.getImages().get(index).toString());
                imagePart[index] = prepareFilePart("customerImage", mRetrieveCustomerData.getImages().get(index));
            }
        }

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(mContext));
        RequestBody requestStoreName = createRequestBody(mRetrieveCustomerData.getStoreName());
        RequestBody requestOwnerName = createRequestBody(mRetrieveCustomerData.getFullName());
        RequestBody requestAddress = createRequestBody(mAddress);
        RequestBody requestCity = createRequestBody(mCity);
        RequestBody requestProvince = createRequestBody(mProvince);
        RequestBody requestPostalCode = createRequestBody(mPostalCode);
        RequestBody requestLatitude = createRequestBody(String.valueOf(mLatitude));
        RequestBody requestLongitude = createRequestBody(String.valueOf(mLongitude));
        RequestBody requestPhoneNumber = createRequestBody(mRetrieveCustomerData.getPhoneNumber());
        RequestBody requestMobileNumber = createRequestBody(mRetrieveCustomerData.getMobileNumber());
        RequestBody requestEmailAddress = createRequestBody(mRetrieveCustomerData.getEmail());
        RequestBody requestStatus = createRequestBody(mRetrieveCustomerData.getStatus());
        RequestBody requestCustomerType = createRequestBody(mRetrieveCustomerData.getCategory());
        RequestBody requestTags = createRequestBody(mRetrieveCustomerData.getTags());
        RequestBody requestDescription = createRequestBody(mRetrieveCustomerData.getDescription());
        RequestBody requestCreditLimit = createRequestBody(mRetrieveCustomerData.getCreditLimit());

        Observable<CreateCustomer> createCustomerObservable = mMasterApiService.createCustomer(
                requestClientId,
                requestStoreName,
                requestOwnerName,
                requestAddress,
                requestCity,
                requestProvince,
                requestPostalCode,
                requestLatitude,
                requestLongitude,
                requestPhoneNumber,
                requestMobileNumber,
                requestEmailAddress,
                requestStatus,
                requestCustomerType,
                requestTags,
                requestDescription,
                requestCreditLimit,
                imagePart
        );
        createCustomerObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CreateCustomer>() {
                    @Override
                    public void onCompleted() {
                        submitDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        submitDialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(CreateCustomer createCustomer) {
                        if (createCustomer.getStatusCode() == STATUS_SUCCESS) {
                            showDialogAlert();
                            Log.e("JSON", new Gson().toJson(createCustomer));
                        }
                    }
                });
    }

    private void getAddress(Location currentLocation) {

        if (!Geocoder.isPresent()) {
            Toast.makeText(mContext,
                    "Can't find current address, ",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(mContext, GetAddressIntentService.class);
        intent.putExtra("add_receiver", addressResultReceiver);
        intent.putExtra("add_location", currentLocation);
        mContext.startService(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        LatLng mLocation = new LatLng(mLatitude, mLongitude);
        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mLatitude, mLongitude))
                .zoom(15).build();
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mLocation));
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng midLatLng = mGoogleMap.getCameraPosition().target;
                Location temp = new Location(LocationManager.GPS_PROVIDER);
                temp.setLatitude(midLatLng.latitude);
                temp.setLongitude(midLatLng.longitude);
                Log.e("LatLong", String.valueOf(midLatLng.latitude) + ", " + String.valueOf(midLatLng.longitude));
                getAddress(temp);
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("LatLong", location.getLatitude() + ", " + location.getLongitude());
        mLocation = location;
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();

        mapFragment.getMapAsync(this);
        getAddress(location);
    }

    @Override
    public void onLocationFailed(int failType) {
        switch (failType) {
            case FailType.PERMISSION_DENIED: {
                Log.e("onLocationFailed", "Couldn't get location, because user didn't give permission!");
                Toast.makeText(getContext(), "Couldn't get location, because user didn't give permission!", Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE:
            case FailType.GOOGLE_PLAY_SERVICES_CONNECTION_FAIL: {
                Log.e("onLocationFailed", "Couldn't get location, because Google Play Services not available!");
                Toast.makeText(getContext(), "Couldn't get location, because Google Play Services not available!", Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.NETWORK_NOT_AVAILABLE: {
                Log.e("onLocationFailed", "Couldn't get location, because network is not accessible!");
                Toast.makeText(getContext(), "Couldn't get location, because network is not accessible!", Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.TIMEOUT: {
                Log.e("onLocationFailed", "Couldn't get location, because user didn't activate providers via settingsApi!");
                Toast.makeText(getContext(), "Couldn't get location, because user didn't activate providers via settingsApi!", Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_SETTINGS_DENIED: {
                Log.e("onLocationFailed", "Couldn't get location, because user didn't activate providers via settingsApi!");
                Toast.makeText(getContext(), "Couldn't get location, because user didn't activate providers via settingsApi!", Toast.LENGTH_SHORT).show();
                break;
            }
            case FailType.GOOGLE_PLAY_SERVICES_SETTINGS_DIALOG: {
                Log.e("onLocationFailed", "Couldn't display settingsApi dialog!");
                Toast.makeText(getContext(), "Couldn't display settingsApi dialog!", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }

    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == 1) {
                Toast.makeText(mContext,
                        "Address not found, ",
                        Toast.LENGTH_SHORT).show();
            }

            mAddress = resultData.getString("address_result");
            mCity = resultData.getString("address_city");
            mDistrict = resultData.getString("address_district");
            mProvince = resultData.getString("address_province");
            mPostalCode = resultData.getString("address_postal_code");

            Log.e(TAG, mAddress);
            Log.e(TAG, mProvince);
            Log.e(TAG, mPostalCode);
            Log.e(TAG, mDistrict);
            Log.e(TAG, mCity);

//            locationAddressOnMap.setText(mAddress);
            locationAddress.setText(mAddress);
            locationCity.setText(mCity);
            locationDistrict.setText(mDistrict);
            locationProvince.setText(mProvince);
            locationPostalCode.setText(mPostalCode);
        }
    }

    private boolean validateInput(TextInputLayout mTextInputLayout, EditText mInputEditText) {
        String input = mInputEditText.getText().toString().trim();
        if (input.isEmpty()) {
            mTextInputLayout.setError(getString(R.string.error_field_required));
            requestFocus(mInputEditText);
            return false;
        } else {
            mTextInputLayout.setError(null);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            );
        }
    }

    private void showDialogAlert() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText("Data Pelanggan Berhasil Disimpan");

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            getActivity().finish();
        });
        dialog.show();
    }
}
