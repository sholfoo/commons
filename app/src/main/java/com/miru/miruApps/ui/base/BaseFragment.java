package com.miru.miruApps.ui.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.miru.miruApps.R;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.ScalingUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

public class BaseFragment extends Fragment {

    protected Context mContext;
    protected LayoutInflater mInflater;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mInflater = LayoutInflater.from(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    public ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    public void finish() {
        if (isAdded())
            getActivity().finish();
    }

    protected File scaleImage(String path) {
        String strMyImagePath = null;
        Bitmap scaledBitmap;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, 240, 320, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= 240 && unscaledBitmap.getHeight() <= 320)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 240, 320, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return new File(path);
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/Miru");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = DateTimeUtil.getCurrentFormat("ddmmyyyyHHmmss") + ".png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {}

        if (strMyImagePath == null) {
            return new File(path);
        }

        return new File(strMyImagePath);

    }

    protected boolean validateInput(TextInputLayout mTextInputLayout, EditText mInputEditText) {
        String input = mInputEditText.getText().toString().trim();
        if (input.isEmpty()) {
            mTextInputLayout.setError(getString(R.string.error_field_required));
            requestFocus(mInputEditText);
            return false;
        } else {
            mTextInputLayout.setError(null);
        }

        return true;
    }

    protected void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            );
        }
    }

    public static String getJoinedString(List<String> stringList){
        final String SEPARATOR = ",";
        StringBuilder csvBuilder = new StringBuilder();

        for(String s : stringList){
            csvBuilder.append(s);
            csvBuilder.append(SEPARATOR);
        }

        String joinedString = csvBuilder.toString();
        return joinedString.substring(0, joinedString.length() - SEPARATOR.length());
    }

}
