package com.miru.miruApps.ui.activity.invoice.filtering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.EXTRA_INVOCE_TIME_CREATE;
import static com.miru.miruApps.config.Constants.EXTRA_INVOICE_DATE;
import static com.miru.miruApps.config.Constants.EXTRA_INVOICE_SHORT;

public class MiruInvoiceSortingActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.spn_created_time) MaterialBetterSpinner spnCreatedTime;
    @BindView(R.id.spn_invoice_date) MaterialBetterSpinner spnInvoiceDate;
    @BindView(R.id.spn_alphabet) MaterialBetterSpinner spnAlphabet;
    @BindView(R.id.action_apply) Button actionApply;

    private static String[] alphabetShort = {"Alfabet A-Z", "Alfabet Z-A"};
    private static String[] timeCreateShort = {"Terbaru", "Terlama"};
    private static String[] dateShort = {"Terdekat", "Terjauh"};
    private String alphabetShortStr = "";
    private String timeCreateShortStr = "";
    private String dateShortStr = "";

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_apply)
    void onActionApply(){
        if (spnAlphabet.getText().toString().equalsIgnoreCase(alphabetShort[0])){
            alphabetShortStr = "ASC";
        } else {
            alphabetShortStr = "DESC";
        }

        if (spnCreatedTime.getText().toString().equalsIgnoreCase(timeCreateShort[0])){
            timeCreateShortStr = "ASC";
        } else {
            timeCreateShortStr = "DESC";
        }

        if (spnInvoiceDate.getText().toString().equalsIgnoreCase(dateShort[0])){
            dateShortStr = "ASC";
        } else {
            dateShortStr = "DESC";
        }


        Intent data = new Intent();

        // Add the required data to be returned to the MainActivity
        data.putExtra(EXTRA_INVOCE_TIME_CREATE, timeCreateShortStr);
        data.putExtra(EXTRA_INVOICE_DATE, dateShortStr);
        data.putExtra(EXTRA_INVOICE_SHORT, alphabetShortStr);

        // Set the resultCode to Activity.RESULT_OK to
        // indicate a success and attach the Intent
        // which contains our result data
        setResult(Activity.RESULT_OK, data);
        finish();
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_invoice_shorting);
        ButterKnife.bind(this);

        setupActionBar();
        setupSpinner();
    }

    private void setupActionBar() {
        actionHome.setImageResource(R.drawable.ic_action_bar_close);
        toolbarLogo.setText(getResources().getString(R.string.action_bar_shorting));
    }

    private void setupSpinner(){
        ArrayAdapter<String> alphabetAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, alphabetShort);
        ArrayAdapter<String> timeCreateAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, timeCreateShort);
        ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, dateShort);
        spnAlphabet.setAdapter(alphabetAdapter);
        spnCreatedTime.setAdapter(timeCreateAdapter);
        spnInvoiceDate.setAdapter(dateAdapter);
    }

}
