package com.miru.miruApps.ui.fragment.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.PaymentAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Payment;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.activity.payment.MiruPaymentCreateActivity;
import com.miru.miruApps.ui.base.BaseFragment;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_FROM_CUSTOMER;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class DetailCustPaymentFragment extends BaseFragment {

    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.ly_error) LinearLayout lyError;
    @BindView(R.id.action_filter) TextView actionFilter;
    @BindView(R.id.action_shorting) TextView actionShorting;

    @Inject InvoiceApiService mApiService;
    private PaymentAdapter mAdapter;

    @OnClick(R.id.action_create_payment)
    void onActionCreatePayment(){
        startActivity(new Intent(getActivity(), MiruPaymentCreateActivity.class)
                .putExtra("extra_from_id", EXTRA_FROM_CUSTOMER));
    }

    public static DetailCustPaymentFragment newInstance(String customerId) {
        Bundle bundle = new Bundle();
        bundle.putString("customerId", customerId);
        DetailCustPaymentFragment detailCustPaymentFragment = new DetailCustPaymentFragment();
        detailCustPaymentFragment.setArguments(bundle);
        return detailCustPaymentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_detail_payment, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new PaymentAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mAdapter);
        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(this::initializeCustomerPayements);
        initializeCustomerPayements();
    }

    private String getArgumentExtraCustomerId() {
        return getArguments() != null ? getArguments().getString("customerId") : null;
    }

    private void initializeCustomerPayements() {
        pbLoad.setVisibility(View.VISIBLE);
        Observable<Payment> paymentObservable = mApiService.getCustomerPayment(
                new SPUser().getKeyUserClientId(mContext),
                getArgumentExtraCustomerId());

        paymentObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Payment>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Payment payment) {
                        if (payment.getStatusCode() == STATUS_SUCCESS) {
                            mAdapter.pushData(payment.getPayment());
                            resetUILoading();
                        } else
                            showErrorView();
                    }
                });
    }

    protected void resetUILoading() {
        hideErrorView();
        if (isAdded()){
            pbLoad.setVisibility(View.GONE);
            lyRefresh.setRefreshing(false);
        }
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        if (isAdded()){
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
            lyError.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(v -> {
                hideErrorView();
                initializeCustomerPayements();
            });

            if (!TextUtils.isEmpty(text))
                txtNoData.setText(getString(R.string.error_no_data));
        }
    }

    protected void hideErrorView() {
        if (isAdded()){
            lyError.setVisibility(View.GONE);
        }
    }
}
