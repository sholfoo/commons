package com.miru.miruApps.ui.fragment.salesorder;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.so.AutoCompleteCustomerAdapter;
import com.miru.miruApps.data.db.Customer;
import com.miru.miruApps.data.db.CustomerDao;
import com.miru.miruApps.model.response.SalesOrder;
import com.miru.miruApps.model.retrieve.RetrieveSoInfoData;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.FORMAT_DATE;

public class EditSOCustomerFragment extends BaseFragment implements android.app.DatePickerDialog.OnDateSetListener{
    @BindView(R.id.input_sales_name) AutoCompleteTextView inputSalesName;
    @BindView(R.id.input_layout_sales_name) TextInputLayout inputLayoutSalesName;
    @BindView(R.id.input_so_date) EditText inputSoDate;
    @BindView(R.id.input_layout_so_date) TextInputLayout inputLayoutSoDate;
    @BindView(R.id.input_description) EditText inputDescription;
    @BindView(R.id.input_layout_description) TextInputLayout inputLayoutDescription;

    protected OnNextListener mOnNextListener;
    private CustomerDao customerDao;
    private Query<Customer> customerQuery;
    private List<Customer> customerList;
    private AutoCompleteCustomerAdapter adapter;
    private String mCustomerId;

    public static EditSOCustomerFragment newInstance(SalesOrder salesOrder) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("sales_order", salesOrder);
        EditSOCustomerFragment editSOCustomerFragment = new EditSOCustomerFragment();
        editSOCustomerFragment.setArguments(bundle);
        return editSOCustomerFragment;
    }

    @OnClick(R.id.input_so_date)
    void onActionInputDate() {
        createDatePickerDialog();
    }

    @OnClick(R.id.action_next)
    void onActionNext() {
        if (!validateInput(inputLayoutSoDate, inputSoDate))
            return;

        if (!validateInput(inputLayoutSalesName, inputSalesName))
            return;

        attemptNext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_so_create_customer, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAutoCompleteCustomer();
        inputSoDate.setKeyListener(null);
        inputSoDate.setText(DateTimeUtil.getCurrentFormat(FORMAT_DATE));

        setupViews(getArguments().getParcelable("sales_order"));
    }

    protected void createDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(
                mContext,
                R.style.MiruApp_DatePicker,
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.show();
    }

    private void attemptNext() {
        String mSalesName = inputSalesName.getText().toString();
        String mSoDate = inputSoDate.getText().toString();
        String mDescription = inputDescription.getText().toString();

        mOnNextListener.onActionNextInfo(new RetrieveSoInfoData(
                mCustomerId,
                mSalesName,
                mSoDate,
                mDescription
        ));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnNextListener = (OnNextListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthStr = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
        String yearStr = String.valueOf(year);

        String dateStr = dayStr + "-" + monthStr + "-" + yearStr;
        inputSoDate.setText(dateStr);
    }

    public interface OnNextListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onActionNextInfo(RetrieveSoInfoData retrieveSoInfoData);
    }

    private void setupAutoCompleteCustomer(){
        customerDao = MiruApp.getDaoSession().getCustomerDao();
        customerQuery = customerDao
                .queryBuilder()
                .orderAsc(CustomerDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        customerList = customerQuery.list();
        adapter = new AutoCompleteCustomerAdapter(mContext, customerList);
        inputSalesName.setThreshold(1);
        inputSalesName.setAdapter(adapter);
        inputSalesName.setOnItemClickListener((adapterView, view, i, l) -> {
            mCustomerId = customerList.get(i).getCustomerId();
        });

    }

    private void setupViews(SalesOrder salesOrder){
        mCustomerId = salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getCustomerId();
        inputSalesName.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getSalesName());
        inputDescription.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getNotes());
        inputSoDate.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getSalesOrderDate());
    }
}
