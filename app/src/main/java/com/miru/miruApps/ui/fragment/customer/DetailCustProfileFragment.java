package com.miru.miruApps.ui.fragment.customer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.product.ProductImageAdapter;
import com.miru.miruApps.data.preference.SPExtraCustomer;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.DetailCustomer;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mabbas007.tagsedittext.TagsEditText;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;

public class DetailCustProfileFragment extends BaseFragment implements OnMapReadyCallback {

    @Inject MasterApiService mMasterApiService;

    @BindView(R.id.detail_customer_name) TextView detailCustomerName;
    @BindView(R.id.detail_customer_phone_number) TextView detailCustomerPhoneNumber;
    @BindView(R.id.detail_customer_mobile_number) TextView detailCustomerMobileNumber;
    @BindView(R.id.detail_customer_address) TextView detailCustomerAddress;
    @BindView(R.id.detail_customer_email) TextView detailCustomerEmail;
    @BindView(R.id.input_tags) TagsEditText inputTags;
    @BindView(R.id.detail_customer_description) TextView detailCustomerDescription;
    @BindView(R.id.lv_content_documentation) RecyclerView lvContentDocumentation;
    @BindView(R.id.last_update) TextView lastUpdate;

    private ProductImageAdapter mProductImageAdapter;
    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;
    private double mLatitude;
    private double mLongitude;

    public static DetailCustProfileFragment newInstance(String customerId) {
        Bundle bundle = new Bundle();
        bundle.putString("customerId", customerId);
        DetailCustProfileFragment detailCustProfileFragment = new DetailCustProfileFragment();
        detailCustProfileFragment.setArguments(bundle);
        return detailCustProfileFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_detail_info_profile, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        setupAdapterImage();
        initDetailCustomer(getArgumentExtraCustomerId());
    }

    private String getArgumentExtraCustomerId() {
        return getArguments() != null ? getArguments().getString("customerId") : null;
    }

    private void initDetailCustomer(String customerId) {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<DetailCustomer> detailCustomerObservable = mMasterApiService.getCustomerDetailInfo(
                new SPUser().getKeyUserClientId(mContext), customerId
        );

        detailCustomerObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DetailCustomer>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                        Log.e("CustomerDetail", "onComplete...");
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof Exception) {
                            Log.e("CustomerDetail", "onError..." + e.getMessage());
                            Log.e("CustomerDetail", "onError..." + e.getLocalizedMessage());
                            Log.e("CustomerDetail", "onError..." + e.getCause());
                            Log.e("CustomerDetail", "onError..." + e.getStackTrace().toString());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(DetailCustomer detailCustomer) {
                        dialog.dismiss();
                        try {
                            mLatitude = Double.parseDouble(detailCustomer
                                    .getCustomerContainer().get(DEFAULT_SIZE)
                                    .getLatitudes());
                            mLongitude = Double.parseDouble(detailCustomer
                                    .getCustomerContainer().get(DEFAULT_SIZE)
                                    .getLongitudes());
                        } catch (Exception e) {
                            mLatitude = 0.0;
                            mLongitude = 0.0;
                        }

                        setupViews(detailCustomer);
                        saveExtraCustomerPreference(detailCustomer);
                    }
                });
    }

    @SuppressLint("SetTextI18n")
    private void setupViews(DetailCustomer detailCustomer) {
        Log.e("CustomerDetail", "onNext...");
        String[] tags = detailCustomer
                .getCustomerContainer()
                .get(DEFAULT_SIZE)
                .getCustomerTag().toArray(new String[0]);

        List<String> documentations = new ArrayList<>();
        for (String s : detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getImageLink()) {
            documentations.add(s);
        }

        mProductImageAdapter.pushData(documentations);
        detailCustomerName.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getOwnerName());
        detailCustomerAddress.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getAddress().get(DEFAULT_SIZE).getAddress());
        detailCustomerPhoneNumber.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getAddress().get(DEFAULT_SIZE).getPhoneNumber());
        detailCustomerMobileNumber.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getMobilePhone());
        detailCustomerDescription.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getDescription());
        detailCustomerEmail.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getEmailAddress());
        lastUpdate.setText("Diperbaharui : " + DateTimeUtil.getConvertedFormat(
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getLastUpdatedTime(),
                "dd-MM-yyyy HH:mm:ss.SSS",
                "dd-MM-yyyy HH:mm:ss"
        ));
        inputTags.setTags(tags);
        mapFragment.getMapAsync(this);
    }

    private void saveExtraCustomerPreference(DetailCustomer detailCustomer){
        new SPExtraCustomer().setKeyCustId(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getCustomerId());
        new SPExtraCustomer().setKeyCustName(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getOwnerName());
        new SPExtraCustomer().setKeyCustStoreName(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getStoreName());
        new SPExtraCustomer().setKeyCustPhone(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getAddress().get(DEFAULT_SIZE).getPhoneNumber());
        new SPExtraCustomer().setKeyCustMobile(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getMobilePhone());
        new SPExtraCustomer().setKeyCustEmail(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getEmailAddress());
        new SPExtraCustomer().setKeyCustAddress(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getAddress().get(DEFAULT_SIZE).getAddress());
        new SPExtraCustomer().setKeyCustCity(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getAddress().get(DEFAULT_SIZE).getCity());
        new SPExtraCustomer().setKeyCustPostalCode(mContext,
                detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getAddress().get(DEFAULT_SIZE).getPostalCode());
        new SPExtraCustomer().setKeyIsStored(mContext,
                true);
    }

    private void setupAdapterImage() {
        mProductImageAdapter = new ProductImageAdapter(mContext);
        lvContentDocumentation.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_transparent));
        lvContentDocumentation.addItemDecoration(itemDecorator);
        lvContentDocumentation.setAdapter(mProductImageAdapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        LatLng mLocation = new LatLng(mLatitude, mLongitude);
        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mLatitude, mLongitude))
                .zoom(15).build();
        mGoogleMap.addMarker(new MarkerOptions().position(mLocation)
                .title("Lokasi anda saat ini."));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mLocation));
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

}
