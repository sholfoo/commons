package com.miru.miruApps.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.model.request.RegisterBody;
import com.miru.miruApps.model.response.Register;
import com.miru.miruApps.network.ApiService;
import com.miru.miruApps.ui.base.BaseActivity;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;
public class MiruRegisterActivity extends BaseActivity {

    @Inject ApiService mApiService;
    @BindView(R.id.action_register) LinearLayout actionRegister;
    @BindView(R.id.ly_header) RelativeLayout lyHeader;
    @BindView(R.id.input_first_name) EditText inputFirstName;
    @BindView(R.id.input_layout_first_name) TextInputLayout inputLayoutFirstName;
    @BindView(R.id.input_last_name) EditText inputLastName;
    @BindView(R.id.input_layout_last_name) TextInputLayout inputLayoutLastName;
    @BindView(R.id.input_store_name) EditText inputStoreName;
    @BindView(R.id.input_layout_store_name) TextInputLayout inputLayoutStoreName;
    @BindView(R.id.input_email) EditText inputEmail;
    @BindView(R.id.input_layout_email) TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_password) EditText inputPassword;
    @BindView(R.id.input_layout_password) TextInputLayout inputLayoutPassword;
    @BindView(R.id.input_password_confirmation) EditText inputPasswordConfirmation;
    @BindView(R.id.input_layout_password_confirmation) TextInputLayout inputLayoutPasswordConfirmation;
    @BindView(R.id.input_phone_number) EditText inputPhoneNumber;
    @BindView(R.id.action_btn_register) Button actionBtnRegister;

    @OnClick(R.id.action_btn_register)
    void onActionRegister() {
        attemptRegister();
    }

    @OnClick(R.id.ly_header)
    void onActionFinish() {
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_register);
        ButterKnife.bind(this);

        MiruApp.getmComponent().inject(this);
    }

    private void attemptRegister() {
        inputFirstName.setError(null);
        inputLastName.setError(null);
        inputStoreName.setError(null);
        inputEmail.setError(null);
        inputPassword.setError(null);
        inputPasswordConfirmation.setError(null);
        inputPhoneNumber.setError(null);

        // Store values at the time of the login attempt.
        String fullName = inputFirstName.getText().toString() + " " + inputLastName.getText().toString();
        String storeName = inputStoreName.getText().toString();
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        String passwordConfirmation = inputPasswordConfirmation.getText().toString();
        String phoneNumber = "0" + inputPhoneNumber.getText().toString();

        if (!validateInput(inputLayoutFirstName, inputFirstName))
            return;
        if (!validateInput(inputLayoutLastName, inputLastName))
            return;
        if (!validateInput(inputLayoutStoreName, inputStoreName))
            return;
        if (!validateInput(inputLayoutEmail, inputEmail))
            return;
        if (!validateInput(inputLayoutPassword, inputPassword))
            return;
        if (!validateInput(inputLayoutPasswordConfirmation, inputPasswordConfirmation))
            return;
        if (!validateInput(inputPhoneNumber))
            return;

        if (isNetworkAvailable()) {
            if (password.equalsIgnoreCase(passwordConfirmation)) {
                Log.e("CHANGE PASSWORD", "Password validate");
                initializeRegisterApiCall(new RegisterBody(
                        storeName,
                        fullName,
                        email,
                        email,
                        password,
                        phoneNumber
                ));
            } else {
                Log.e("PASSWORD", password);
                Log.e("RETYPE PASSWORD", passwordConfirmation);
                inputLayoutPasswordConfirmation.setError("Password tidak sesuai. Silahkan cek kembali.");
                requestFocus(inputPasswordConfirmation);
                Toast.makeText(this,
                        "Password tidak sesuai. Silahkan cek kembali.",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.error_no_internet_connection),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeRegisterApiCall(RegisterBody registerBody) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Registering, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestClientName = createRequestBody(registerBody.getClientName());
        RequestBody requestFullName = createRequestBody(registerBody.getFullName());
        RequestBody requestEmailAddress = createRequestBody(registerBody.getEmailAddress());
        RequestBody requestUserName = createRequestBody(registerBody.getUserName());
        RequestBody requestPassword = createRequestBody(registerBody.getPassword());
        RequestBody requestMobileNumber = createRequestBody(registerBody.getMobileNumber());

        Observable<Register> registerObservable = mApiService.register(
                requestClientName,
                requestFullName,
                requestEmailAddress,
                requestUserName,
                requestPassword,
                requestMobileNumber
        );

        registerObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Register>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(Register register) {
                        if (register.getStatusCode() == STATUS_SUCCESS){
                            finish();
                            startActivity(new Intent(MiruRegisterActivity.this,
                                    MiruOTPVerificationActivity.class));
                        } else {
                            Toast.makeText(MiruRegisterActivity.this,
                                    register.getStatusMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }
}
