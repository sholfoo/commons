package com.miru.miruApps.ui.activity.product;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ProductAdapter;
import com.miru.miruApps.adapter.ProductShortingAdapter;
import com.miru.miruApps.data.db.Category;
import com.miru.miruApps.data.db.CategoryDao;
import com.miru.miruApps.data.db.ProductDao;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.ProductShort;
import com.miru.miruApps.model.response.Product;
import com.miru.miruApps.model.response.ProductCategory;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.activity.product.filtering.MiruProductFilterActivity;
import com.miru.miruApps.ui.activity.product.filtering.MiruProductShortingActivity;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_BRAND;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_DISCOUNT;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_MAX;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_MIN;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_PRICE;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_QTY;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_SHORT;
import static com.miru.miruApps.config.Constants.NULL;
import static com.miru.miruApps.config.Constants.ORDER_ASC;
import static com.miru.miruApps.config.Constants.ORDER_DESC;
import static com.miru.miruApps.config.Constants.STATUS_ERROR;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruProductListActivity extends BaseActivity {
    private static final int FILTER_REQ = 1001;
    private static final int SHORTING_REQ = 1002;

    @Inject MasterApiService mMasterApiService;

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.spn_product_category) MaterialBetterSpinner spnProductCategory;
    @BindView(R.id.ly_search) LinearLayout lySearch;
    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.action_filter) TextView actionFilter;
    @BindView(R.id.action_shorting) TextView actionShorting;

    private ProductAdapter mProductAdapter;
    private ProductShortingAdapter mProductShortingAdapter;
    private ProductDao productDao;
    private Query<com.miru.miruApps.data.db.Product> productQuery;

    private CategoryDao categoryDao;
    private Query<com.miru.miruApps.data.db.Category> categoryQuery;

    private CharSequence filterStr;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_filter)
    void onActionFilter() {
        Intent intentFilter = new Intent(this, MiruProductFilterActivity.class);
        startActivityForResult(intentFilter, FILTER_REQ);
    }

    @OnClick(R.id.action_shorting)
    void onActionShorting() {
        Intent intentFilter = new Intent(this, MiruProductShortingActivity.class);
        startActivityForResult(intentFilter, SHORTING_REQ);
    }

    @Override
    protected void onResume() {
        super.onResume();
        productDao.deleteAll();
        categoryDao.deleteAll();
        initListProduct();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_list);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        productDao = MiruApp.getDaoSession().getProductDao();
        categoryDao = MiruApp.getDaoSession().getCategoryDao();

        productQuery = productDao
                .queryBuilder()
                .orderAsc(ProductDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViews();

        List<String> strings = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, strings);
        spnProductCategory.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FILTER_REQ) {

            // The resultCode is set by the AnotherActivity
            // By convention RESULT_OK means that what ever
            // AnotherActivity did was successful
            if(resultCode == Activity.RESULT_OK) {
                // Get the result from the returned Intent
                final String productBrand = data.getStringExtra(EXTRA_PRODUCT_BRAND);
                final String productDiscount = data.getStringExtra(EXTRA_PRODUCT_DISCOUNT);
                final String productMin = data.getStringExtra(EXTRA_PRODUCT_MIN);
                final String productMax = data.getStringExtra(EXTRA_PRODUCT_MAX);
                final String productQty = data.getStringExtra(EXTRA_PRODUCT_QTY);

                // Use the data - in this case, display it in a Toast.
                Toast.makeText(this, "Result: " + productBrand
                        + " | " + productDiscount
                        + " | " + productMin
                        + " | " + productMax
                        + " | " + productQty, Toast.LENGTH_LONG).show();
            } else {
                // AnotherActivity was not successful. No data to retrieve.
            }
        } else if (requestCode == SHORTING_REQ) {
            if (resultCode == Activity.RESULT_OK){
                final String productPrice = data.getStringExtra(EXTRA_PRODUCT_PRICE);
                final String productShort = data.getStringExtra(EXTRA_PRODUCT_SHORT);
                Log.e("Result", productPrice + "|" + productShort);

                setupShortingAdapter(productShort, productPrice);
            }
        }
    }

    private void setupActionBar(){
        toolbarLogo.setText(getResources().getString(R.string.action_bar_product));
        Drawable img = getResources().getDrawable( R.drawable.ic_actionbar_product );
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);

    }

    private void setupViews() {
        mProductAdapter = new ProductAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mProductAdapter);
        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> {
            mProductAdapter = new ProductAdapter(this);
            lvContent.setLayoutManager(new LinearLayoutManager(this,
                    LinearLayoutManager.VERTICAL, false));
            lvContent.setAdapter(mProductAdapter);
            initListProduct();
        });
        initListProduct();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu_with_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setQueryHint("Search");
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().isEmpty()){
                    initListProduct();
                }

                mProductAdapter.getFilter().filter(query);
                searchViewAndroidActionBar.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().isEmpty()){
                    initListProduct();
                }
                mProductAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_new:
                startActivity(new Intent(this, MiruProductCreateActivity.class));
                return true;
            case R.id.action_search:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void initListProduct() {
        pbLoad.setVisibility(View.VISIBLE);
        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(this));
        Observable<Product> productObservable = mMasterApiService.getProducts(requestClientId);
        productObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Product>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(Product product) {
                        if (product.getStatusCode() == STATUS_SUCCESS){
                            mProductAdapter.pushData(product.getProductContainer());
                            saveToSQLiteDatabase(product);
                            getProductCategories();
                        } else if(product.getStatusCode() == STATUS_ERROR){
                            showErrorView("Belum ada Produk");
                        } else
                            showErrorView();
                    }
                });
    }

    private void getProductCategories() {
        Observable<ProductCategory> productCategoryObservable = mMasterApiService.getProductCategory(
                new SPUser().getKeyUserClientId(this));
        productCategoryObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ProductCategory>() {
                    @Override
                    public void onCompleted() {
                        resetUILoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(ProductCategory productCategory) {
                        if (productCategory.getStatusCode() == STATUS_SUCCESS){
                            setupAdapterCategory(productCategory);
                        }
                    }
                });
    }

    private void setupAdapterCategory(ProductCategory productCategory) {
        if (!spnProductCategory.getText().toString().equalsIgnoreCase("semua produk")){
            mProductAdapter.getFilter().filter(spnProductCategory.getText());
        }

        List<String> categoryList = new ArrayList<>();
        for (int i = 0; i < productCategory.getCatalog().size(); i++) {
            Category category = new Category();
            category.setCategoryId(productCategory.getCatalog().get(i).getCategoryId());
            category.setCategoryName(StringUtils.capitalize(productCategory.getCatalog().get(i).getCategory().toLowerCase()));
            categoryDao.insert(category);
            categoryList.add(StringUtils.capitalize(productCategory.getCatalog().get(i).getCategory().toLowerCase()));
        }
        categoryList.add("Semua Produk");
        Collections.reverse(categoryList);
        String[] categories = categoryList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, categories);
        spnProductCategory.setAdapter(adapter);
        spnProductCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (spnProductCategory.getText().toString().equalsIgnoreCase("semua produk")){
                    initListProduct();
                } else {
                    mProductAdapter.getFilter().filter(spnProductCategory.getText());
                }
            }
        });
    }

    private void saveToSQLiteDatabase(Product product){

        for (int i = 0; i < product.getProductContainer().size(); i++){
            int hargaTermurah = 0;
            for (int j = 0; j < product.getProductContainer().get(i).getProductPrices().size(); j++){
                com.miru.miruApps.data.db.Product productDb = new com.miru.miruApps.data.db.Product();

                if (hargaTermurah == 0) {
                    hargaTermurah = product
                            .getProductContainer()
                            .get(i)
                            .getProductPrices()
                            .get(j)
                            .getPricePerUnit();
                    productDb.setIsCheapPrice(true);
                } else {
                    if (product.getProductContainer().get(i).getProductPrices().get(j).getPricePerUnit() < hargaTermurah) {
                        hargaTermurah = product
                                .getProductContainer()
                                .get(i)
                                .getProductPrices()
                                .get(j)
                                .getPricePerUnit();

                        List<com.miru.miruApps.data.db.Product> productList = productDao.queryBuilder()
                                .where(ProductDao.Properties.ProductSku.eq(
                                        product.getProductContainer().get(i).getItemSKU())).list();

                        for (com.miru.miruApps.data.db.Product mProduct : productList){
                            mProduct.setIsCheapPrice(false);
                            productDao.update(mProduct);
                        }
                        productDb.setIsCheapPrice(true);
                    } else {
                        productDb.setIsCheapPrice(false);
                    }
                }

                productDb.setProductSku(product.getProductContainer().get(i).getItemSKU());
                productDb.setProductName(product.getProductContainer().get(i).getProductName());
                productDb.setProductKategory(product.getProductContainer().get(i).getCategory());
                productDb.setProductDescription(isNull(product.getProductContainer().get(i).getDescription()));
                productDb.setProductSize(isNull(product.getProductContainer().get(i).getSize()));
                productDb.setProductBrand(isNull(product.getProductContainer().get(i).getBrand()));
                productDb.setProductImages(product.getProductContainer().get(i).getImageLinks().toString());
                productDb.setCreatedAt(DateTimeUtil.getCurrentFormat("dd-mm-yyyy"));
                productDb.setUpdatedAt(DateTimeUtil.getCurrentFormat("dd-mm-yyyy"));
                productDb.setProductUnit(product.getProductContainer().get(i).getProductPrices().get(j).getIdentifierId().getMeasurementUnit());
                productDb.setProductBasicPrice(String.valueOf(product.getProductContainer().get(i).getProductPrices().get(j).getPricePerUnit()));
                productDb.setProductSellingPrice(String.valueOf(product.getProductContainer().get(i).getProductPrices().get(j).getSellingPrice()));
                productDb.setProductDiscount(String.valueOf(product.getProductContainer().get(i).getProductPrices().get(j).getDiscount()));
                productDao.insert(productDb);
            }
        }

        List<com.miru.miruApps.data.db.Product> productList = productQuery.list();
        for (int i = 0; i < productList.size(); i++){
            Log.e("Product", productList.get(i).getProductSku());
            Log.e("Product", productList.get(i).getProductName());
            Log.e("Product PCS", productList.get(i).getProductUnit());
            Log.e("Product", productList.get(i).getProductBasicPrice());
            Log.e("Product", productList.get(i).getIsCheapPrice() + "");
            Log.e("Product","=======================================");

        }
    }

    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }


    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initListProduct();
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }

    private String isNull(String mValue){
        if (TextUtils.isEmpty(mValue)){
            return NULL;
        }
        return mValue;
    }

    private void setupShortingAdapter(String alphabetShort, String priceShort){
        mProductShortingAdapter = new ProductShortingAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mProductShortingAdapter);

        productDao = MiruApp.getDaoSession().getProductDao();
        if (alphabetShort.equalsIgnoreCase(ORDER_ASC) && priceShort.equalsIgnoreCase(ORDER_ASC)){
            productQuery = productDao
                    .queryBuilder()
                    .orderAsc(ProductDao.Properties.ProductName, ProductDao.Properties.ProductBasicPrice)
                    .build();
        }

        if (alphabetShort.equalsIgnoreCase(ORDER_DESC) && priceShort.equalsIgnoreCase(ORDER_DESC)){
            productQuery = productDao
                    .queryBuilder()
                    .orderDesc(ProductDao.Properties.ProductName, ProductDao.Properties.ProductBasicPrice)
                    .build();
        }

        if (alphabetShort.equalsIgnoreCase(ORDER_ASC) && priceShort.equalsIgnoreCase(ORDER_DESC)){
            productQuery = productDao
                    .queryBuilder()
                    .orderAsc(ProductDao.Properties.ProductName)
                    .orderDesc(ProductDao.Properties.ProductBasicPrice)
                    .build();
        }

        if (alphabetShort.equalsIgnoreCase(ORDER_DESC) && priceShort.equalsIgnoreCase(ORDER_ASC)){
            productQuery = productDao
                    .queryBuilder()
                    .orderDesc(ProductDao.Properties.ProductName)
                    .orderAsc(ProductDao.Properties.ProductBasicPrice)
                    .build();
        }

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        List<com.miru.miruApps.data.db.Product> productList = productQuery.list();
        List<ProductShort> productShortList = new ArrayList<>();
        for (com.miru.miruApps.data.db.Product product : productList){
            productShortList.add(new ProductShort(
                String.valueOf(product.getId()),
                    product.getProductSku(),
                    product.getProductName(),
                    product.getProductKategory(),
                    product.getProductDescription(),
                    product.getProductImages(),
                    product.getProductBrand(),
                    product.getProductSize(),
                    product.getProductBasicPrice(),
                    product.getProductSellingPrice(),
                    product.getProductDiscount()
            ));
        }

        mProductShortingAdapter.pushData(productShortList);
    }
}
