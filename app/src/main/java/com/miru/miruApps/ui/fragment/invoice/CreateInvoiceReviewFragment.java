package com.miru.miruApps.ui.fragment.invoice;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ReviewSoProductAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.invoice.EventInvoiceProduct;
import com.miru.miruApps.model.response.CreateInvoice;
import com.miru.miruApps.model.retrieve.RetrieveInvoiceProductData;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.utils.ImageUtilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class CreateInvoiceReviewFragment extends BaseFragment {
    private static final String TAG = CreateInvoiceReviewFragment.class.getCanonicalName();

    @BindView(R.id.invoice_date) TextView invoiceDate;
    @BindView(R.id.user_full_name) TextView userFullName;
    @BindView(R.id.user_store_logo) ImageView userStoreLogo;
    @BindView(R.id.user_store_name) TextView userStoreName;
    @BindView(R.id.sales_name) TextView salesName;
    @BindView(R.id.invoice_due_date) TextView invoiceDueDate;
    @BindView(R.id.lv_content_review) RecyclerView lvContentReview;
    @BindView(R.id.invoice_subtotal) TextView invoiceSubtotal;
    @BindView(R.id.invoice_subtotal_after_discount) TextView invoiceSubtotalAfterDiscount;
    @BindView(R.id.invoice_discount) TextView invoiceDiscount;
    @BindView(R.id.invoice_discount_price) TextView invoiceDiscountPrice;
    @BindView(R.id.input_invoice_tax) TextView inputInvoiceTax;
    @BindView(R.id.invoice_tax_price) TextView invoiceTaxPrice;
    @BindView(R.id.so_total_price) TextView soTotalPrice;
    @BindView(R.id.invoice_note) TextView invoiceNote;
    @BindView(R.id.current_date) TextView currentDate;
    @BindView(R.id.signature_pad_description) TextView signaturePadDescription;
    @BindView(R.id.signature_pad) SignaturePad signaturePad;
    @BindView(R.id.signature_pad_container) RelativeLayout signaturePadContainer;
    @BindView(R.id.customer_name) EditText customerName;
    @BindView(R.id.action_save_invoice_data) Button actionSaveInvoiceData;

    private RetrieveInvoiceProductData retrieveInvoiceProductData;
    private ReviewSoProductAdapter mAdapter;
    private File compressedSignatureFile;

    @Inject
    InvoiceApiService mApiService;

    @OnClick(R.id.action_save_invoice_data)
    void onActionSaveInvoiceData() {
        createNewInvoice(signaturePad.getSignatureBitmap());
    }

    public static CreateInvoiceReviewFragment newInstance() {
        return new CreateInvoiceReviewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(EventInvoiceProduct eventInvoiceProduct) {
        this.retrieveInvoiceProductData = eventInvoiceProduct.getRetrieveInvoiceProductData();
        Log.e(TAG, "Implementation Running...");
        Log.e(TAG, retrieveInvoiceProductData.toString());
        setupReview(eventInvoiceProduct.getRetrieveInvoiceProductData());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice_create_review, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ReviewSoProductAdapter(mContext);
        lvContentReview.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContentReview.setAdapter(mAdapter);
    }

    private void setupReview(RetrieveInvoiceProductData retrieveInvoiceProductData) {
        mAdapter.pushData(retrieveInvoiceProductData.getSoProductList());

        Glide.with(mContext)
                .load(new SPUser().getKeyUserStoreLogo(mContext))
                .into(userStoreLogo);
        userStoreName.setText(new SPUser().getKeyUserStore(mContext));
        invoiceDate.setText(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmInvoiceDate());
        userFullName.setText(new SPUser().getKeyUserFullName(mContext));
        salesName.setText(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmSalesName());
        invoiceSubtotal.setText(FormatNumber.getFormatCurrencyIDR(retrieveInvoiceProductData.getmSubtotal()));
        invoiceSubtotalAfterDiscount.setText(FormatNumber.getFormatCurrencyIDR(retrieveInvoiceProductData.getmSubtotalAftterDiscount()));
        invoiceDiscount.setText(FormatNumber.getFormatPercent(retrieveInvoiceProductData.getmDiscountPercent()));
        invoiceDiscountPrice.setText(FormatNumber.getFormatCurrencyIDR(retrieveInvoiceProductData.getmDiscountTotal()));
        inputInvoiceTax.setText(FormatNumber.getFormatPercent(retrieveInvoiceProductData.getmTaxPercent()));
        invoiceTaxPrice.setText(FormatNumber.getFormatCurrencyIDR(retrieveInvoiceProductData.getmTaxTotal()));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(retrieveInvoiceProductData.getmTotalAmount()));
        invoiceNote.setText(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmDescription());
        currentDate.setText(DateTimeUtil.getCurrentFormat("dd MMM yyyy"));
        customerName.setText(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmSalesName());
    }


    private void createNewInvoice(Bitmap bitmap) {
        MaterialDialog submitDialog = new MaterialDialog.Builder(mContext)
                .content("Submitting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        List<String> itemSKUList = new ArrayList<>();
        List<String> itemNameList = new ArrayList<>();
        List<String> quantityList = new ArrayList<>();
        List<String> measurementUnitList = new ArrayList<>();
        List<String> pricePerUnitList = new ArrayList<>();
        List<String> discountList = new ArrayList<>();

        String itemSKU = null;
        String itemName = null;
        String quantity = null;
        String measurementUnit = null;
        String pricePerUnit = null;
        String discount = null;

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(mContext));
        RequestBody requestUserId = createRequestBody(new SPUser().getKeyUserId(mContext));
        RequestBody requestUserName = createRequestBody(new SPUser().getKeyUserFullName(mContext));
        RequestBody requestStoreName = createRequestBody(new SPUser().getKeyUserStore(mContext));
        RequestBody requestAddress = createRequestBody("null");
        RequestBody requestCity = createRequestBody("null");
        RequestBody requestPostalCode = createRequestBody("null");
        RequestBody requestPhoneNumber = createRequestBody("null");

        RequestBody requestNotes = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmDescription());
        RequestBody requestCustomerId = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getCustomerId());
        RequestBody requestSalesName = createRequestBody(new SPUser().getKeyUserFullName(mContext));
        RequestBody requestCustomerName = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmSalesName());
        RequestBody requestCustomerAddress = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmAddress());
        RequestBody requestCustomerCity = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmCity());
        RequestBody requestCustomerPostalCode = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmZipCode());
        RequestBody requestCustomerPhoneNumber = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmPhoneNumber());

        RequestBody requestInvoiceDate = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmInvoiceDate());
        RequestBody requestTermOfPayment = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmTermOfPayment());
        RequestBody requestInvoiceDueDate = createRequestBody(retrieveInvoiceProductData.getRetrieveInvoiceInfoData().getmDueDate());

        RequestBody requestFinalDiscount = createRequestBody(String.valueOf(retrieveInvoiceProductData.getmDiscountPercent()));
        RequestBody requestTaxPercent = createRequestBody(String.valueOf(retrieveInvoiceProductData.getmTaxPercent()));
        RequestBody requestTotalAmount = createRequestBody(String.valueOf(retrieveInvoiceProductData.getmSubtotal()));
        RequestBody requestFinalAmount = createRequestBody(String.valueOf(retrieveInvoiceProductData.getmTotalAmount()));

        for (int i = 0; i < retrieveInvoiceProductData.getSoProductList().size(); i++) {
            itemNameList.add(retrieveInvoiceProductData.getSoProductList().get(i).getProductName());
            itemSKUList.add(retrieveInvoiceProductData.getSoProductList().get(i).getProductSku());
            quantityList.add(retrieveInvoiceProductData.getSoProductList().get(i).getProductQty());
            measurementUnitList.add(retrieveInvoiceProductData.getSoProductList().get(i).getProductUnit());
            pricePerUnitList.add(retrieveInvoiceProductData.getSoProductList().get(i).getProductAfterDiscount());
            discountList.add(retrieveInvoiceProductData.getSoProductList().get(i).getProductDiscount());
        }

        try {
            itemSKU = getJoinedString(itemSKUList);
            itemName = getJoinedString(itemNameList);
            quantity = getJoinedString(quantityList);
            measurementUnit = getJoinedString(measurementUnitList);
            pricePerUnit = getJoinedString(pricePerUnitList);
            discount = getJoinedString(discountList);
            Log.e("Item SKU", itemSKU);
            Log.e("Quantity", quantity);
            Log.e("Measurement Unit", measurementUnit);
            Log.e("Price Unit", pricePerUnit);
            Log.e("Discount", discount);
        } catch (Exception e) {

        }

        compressedSignatureFile = ImageUtilities.signatureImage(bitmap);
        MultipartBody.Part signaturePart = null;
        signaturePart = prepareFilePart("signature", compressedSignatureFile);

        RequestBody requestItemSKU = createRequestBody(itemSKU);
        RequestBody requestItemName = createRequestBody(itemName);
        RequestBody requestQuantity = createRequestBody(quantity);
        RequestBody requestMeasurementUnit = createRequestBody(measurementUnit);
        RequestBody requestPricePerUnit = createRequestBody(pricePerUnit);
        RequestBody requestDiscount = createRequestBody(discount);

        Observable<CreateInvoice> createInvoiceObservable = mApiService.createInvoice(
                requestClientId,
                requestUserId,
                requestUserName,
                requestStoreName,
                requestAddress,
                requestCity,
                requestPostalCode,
                requestPhoneNumber,
                requestNotes,
                requestCustomerId,
                requestSalesName,
                requestCustomerName,
                requestCustomerAddress,
                requestCustomerCity,
                requestCustomerPostalCode,
                requestCustomerPhoneNumber,
                requestInvoiceDate,
                requestTermOfPayment,
                requestInvoiceDueDate,
                requestItemSKU,
                requestItemName,
                requestPricePerUnit,
                requestMeasurementUnit,
                requestDiscount,
                requestQuantity,
                requestFinalDiscount,
                requestTaxPercent,
                requestTotalAmount,
                requestFinalAmount,
                signaturePart
        );
        createInvoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CreateInvoice>() {
                    @Override
                    public void onCompleted() {
                        submitDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        submitDialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CreateInvoice createInvoice) {
                        if (createInvoice.getStatusCode() == STATUS_SUCCESS) {
                            showDialogAlert();
                            Log.e("JSON", new Gson().toJson(createInvoice));
                        }
                    }
                });
    }

    private RequestBody createRequestBody(String mValue) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), mValue);
    }

    private MultipartBody.Part prepareFilePart(String partName, File documentFile) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(
                MediaType.parse("multipart/form-data"), documentFile);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, documentFile.getName(), requestFile);
    }


    private void showDialogAlert() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText("Data Invoice Berhasil Disimpan");

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            getActivity().finish();
        });
        dialog.show();
    }
}
