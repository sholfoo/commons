package com.miru.miruApps.ui.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.notification.NotificationAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Notification;
import com.miru.miruApps.model.section.SectionNotification;
import com.miru.miruApps.network.SoApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class NotificationFragment extends BaseFragment {


    @Inject
    SoApiService mApiService;
    @BindView(R.id.toolbar_logo)
    TextView toolbarLogo;
    @BindView(R.id.ly_toolbar)
    Toolbar lyToolbar;
    @BindView(R.id.lvContent)
    RecyclerView lvContent;
    @BindView(R.id.lyRefresh)
    SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad)
    ProgressBar pbLoad;
    @BindView(R.id.imgContent)
    ImageView imgContent;
    @BindView(R.id.txtNoData)
    TextView txtNoData;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private NotificationAdapter mNotificationAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_miru_notification, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupActionBar();
        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(this::initListNotification);

        initListNotification();
    }

    private void setupActionBar() {
        toolbarLogo.setText(getResources()
                .getString(R.string.action_bar_notification));

    }

    private void setupAdapter(Notification notification) {
        List<SectionNotification> sectionNotificationList = new ArrayList<>();
        for (int i = 0; i < notification.getNotification().size(); i++) {
            sectionNotificationList.add(new SectionNotification(
                    notification.getNotification().get(i).getDate(),
                    notification.getNotification().get(i).getData()
            ));
        }

        mNotificationAdapter = new NotificationAdapter(mContext, sectionNotificationList);
        mNotificationAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                SectionNotification sectionNotification = sectionNotificationList.get(position);
            }

            @Override
            public void onListItemCollapsed(int position) {
                SectionNotification sectionNotification = sectionNotificationList.get(position);
            }
        });
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mNotificationAdapter);
    }

    private void initListNotification() {
        pbLoad.setVisibility(View.VISIBLE);
        Observable<Notification> notificationsObservable = mApiService.getNotifications(
                new SPUser().getKeyUserClientId(mContext),
                DateTimeUtil.getCurrentFormat("dd-MM-yyyy"));
        notificationsObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Notification>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(Notification notification) {
                        if (notification.getStatusCode() == STATUS_SUCCESS) {
                            setupAdapter(notification);
                            resetUILoading();
                        } else
                            showErrorView();
                    }
                });
    }

    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initListNotification();
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }
}
