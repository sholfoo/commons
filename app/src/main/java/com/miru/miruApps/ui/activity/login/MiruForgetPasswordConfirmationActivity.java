package com.miru.miruApps.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;

import butterknife.OnClick;

public class MiruForgetPasswordConfirmationActivity extends BaseActivity {

    @OnClick(R.id.action_back_to_login)
    void onActionBackToLogin(){
        Intent finishIntent = new Intent(this, MiruLoginActivity.class);
        finishIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finishIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finishIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(finishIntent);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_forget_password_confirmation);
    }
}
