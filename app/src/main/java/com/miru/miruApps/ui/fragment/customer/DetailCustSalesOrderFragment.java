package com.miru.miruApps.ui.fragment.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.so.SalesOrderAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.SalesOrder;
import com.miru.miruApps.network.SoApiService;
import com.miru.miruApps.ui.activity.salesorder.MiruSOCreateActivity;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.FormatNumber;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_FROM_CUSTOMER;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class DetailCustSalesOrderFragment extends BaseFragment {

    @Inject SoApiService mApiService;

    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.action_filter) TextView actionFilter;
    @BindView(R.id.action_shorting) TextView actionShorting;
    @BindView(R.id.ly_error) LinearLayout lyError;
    @BindView(R.id.ly_shape_circle_blue) View lyShapeCircleBlue;
    @BindView(R.id.unprocess_quantity) TextView unprocessQuantity;
    @BindView(R.id.ly_shape_circle_unprocess_transparent) View lyShapeCircleUnprocessTransparent;
    @BindView(R.id.unprocess_amount) TextView unprocessAmount;
    @BindView(R.id.ly_action_unpaid) LinearLayout lyActionUnpaid;
    @BindView(R.id.ly_shape_circle_green) View lyShapeCircleGreen;
    @BindView(R.id.process_quantity) TextView processQuantity;
    @BindView(R.id.ly_shape_circle_process_transparent) View lyShapeCircleProcessTransparent;
    @BindView(R.id.process_amount) TextView processAmount;
    @BindView(R.id.ly_action_paid) LinearLayout lyActionPaid;
    @BindView(R.id.header) RecyclerViewHeader header;
    @BindView(R.id.ly_list) FrameLayout lyList;

    private SalesOrderAdapter mAdapter;

    @OnClick(R.id.action_create_so)
    void onActionCreateSo(){
        startActivity(new Intent(getActivity(), MiruSOCreateActivity.class)
                .putExtra("extra_from_id", EXTRA_FROM_CUSTOMER));
    }

    public static DetailCustSalesOrderFragment newInstance(String customerId) {
        Bundle bundle = new Bundle();
        bundle.putString("customerId", customerId);
        DetailCustSalesOrderFragment detailCustSalesOrderFragment = new DetailCustSalesOrderFragment();
        detailCustSalesOrderFragment.setArguments(bundle);
        return detailCustSalesOrderFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_detail_so, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new SalesOrderAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);
        header.attachTo(lvContent);

        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(this::initializeCustomerSalesOrders);

        initializeCustomerSalesOrders();
    }

    private void setupViews(SalesOrder salesOrder){
        unprocessQuantity.setText(String.valueOf(salesOrder.getInfo().getNumOfUnprocessedOrders()));
        unprocessAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(salesOrder.getInfo().getSumOfUnprocessedOrders())));
        processQuantity.setText(String.valueOf(salesOrder.getInfo().getNumOfProcessedOrders()));
        processAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(salesOrder.getInfo().getSumOfProcessedOrders())));

    }


    private String getArgumentExtraCustomerId() {
        return getArguments() != null ? getArguments().getString("customerId") : null;
    }

    private void initializeCustomerSalesOrders() {
        pbLoad.setVisibility(View.VISIBLE);
        lyList.setVisibility(View.GONE);
        Observable<SalesOrder> salesOrderObservable = mApiService.getCustomerSo(
                new SPUser().getKeyUserClientId(mContext),
                8,
                getArgumentExtraCustomerId());

        salesOrderObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SalesOrder>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(SalesOrder salesOrder) {
                        if (salesOrder.getStatusCode() == STATUS_SUCCESS) {
                            setupViews(salesOrder);
                            mAdapter.pushData(salesOrder.getOrders());
                            resetUILoading();
                        } else
                            showErrorView();
                    }
                });
    }

    protected void resetUILoading() {
        hideErrorView();
        if (isAdded()) {
            pbLoad.setVisibility(View.GONE);
            lyList.setVisibility(View.VISIBLE);
            lyRefresh.setRefreshing(false);
        }
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        if (isAdded()) {
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
            lyList.setVisibility(View.GONE);
            lyError.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(v -> {
                hideErrorView();
                initializeCustomerSalesOrders();
            });

            if (!TextUtils.isEmpty(text))
                txtNoData.setText(getString(R.string.error_no_data));
        }
    }

    protected void hideErrorView() {
        if (isAdded()) {
            lyError.setVisibility(View.GONE);
        }
    }
}
