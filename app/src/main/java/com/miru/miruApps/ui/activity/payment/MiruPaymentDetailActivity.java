package com.miru.miruApps.ui.activity.payment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.FORMAT_CASH;
import static com.miru.miruApps.config.Constants.FORMAT_CC;
import static com.miru.miruApps.config.Constants.FORMAT_CEK_OR_GIRO;
import static com.miru.miruApps.config.Constants.FORMAT_ETC;
import static com.miru.miruApps.config.Constants.FORMAT_TRANSFER;

public class MiruPaymentDetailActivity extends BaseActivity {

    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.payment_id) TextView paymentId;
    @BindView(R.id.payment_date) TextView paymentDate;
    @BindView(R.id.user_name) TextView userName;
    @BindView(R.id.user_store_image) ImageView userStoreLogo;
    @BindView(R.id.user_store_name) TextView userStoreName;
    @BindView(R.id.customer_name) TextView customerName;
    @BindView(R.id.invoice_number) TextView invoiceNumber;
    @BindView(R.id.total_payment) TextView totalPayment;
    @BindView(R.id.payment_method) TextView paymentMethod;
    @BindView(R.id.notes) TextView notes;
    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_payment_detail);
        ButterKnife.bind(this);

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViews();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_detail_payment);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_payment_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_download:
                Intent intentDownload = new Intent(Intent.ACTION_VIEW);
                intentDownload.setData(Uri.parse(getExtraDocPathUrl()));
                startActivity(intentDownload);
                return true;
            case R.id.action_share:
                if (checkPermission()){
                    downloadDocumentFile(getExtraPaymentId(), getExtraDocPathUrl());
                } else {
                    requestPermission();
                }
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private String getExtraPaymentId() {
        return getIntent().getStringExtra("paymentId");
    }

    private String getExtraExtPaymentId() {
        return getIntent().getStringExtra("extPaymentId");
    }

    private String getExtraPaymentDate() {
        return getIntent().getStringExtra("paymentDate");
    }

    private String getExtraUserName() {
        return new SPUser().getKeyUserName(this);
    }

    private String getExtraCustomerName() {
        return getIntent().getStringExtra("customerName");
    }

    private int getExtraTotalPayment() {
        return getIntent().getIntExtra("totalPayment", 0);
    }

    private String getExtraPaymentMethod() {
        return getIntent().getStringExtra("paymentMethod");
    }

    private String getExtraNotes() {
        return getIntent().getStringExtra("notes");
    }

    private String getExtraInvoiceId() {
        return getIntent().getStringExtra("invoiceId");
    }

    private String getExtraExtInvoiceId() {
        return getIntent().getStringExtra("extInvoiceId");
    }

    private String getExtraDocPathUrl() {
        return getIntent().getStringExtra("docPathUrl");
    }

    private void setupViews() {
        Glide.with(this)
                .load(new SPUser().getKeyUserStoreLogo(this))
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(userStoreLogo);

        userStoreName.setText(new SPUser().getKeyUserStore(this));
        paymentId.setText(getExtraExtPaymentId());
        paymentDate.setText(getExtraPaymentDate());
        userName.setText(new SPUser().getKeyUserFullName(this));
        customerName.setText(getExtraCustomerName());
        invoiceNumber.setText(getExtraInvoiceId());
        totalPayment.setText(FormatNumber.getFormatCurrencyIDR(getExtraTotalPayment()));
        paymentMethod.setText(getPaymentMethod(getExtraPaymentMethod()));
        notes.setText(getExtraNotes());
    }

    private String getPaymentMethod(String paymentMethod) {
        String mPaymentMethod = null;
        if (paymentMethod.equalsIgnoreCase(FORMAT_CASH)) {
            mPaymentMethod = "Cash";
        } else if (paymentMethod.equalsIgnoreCase(FORMAT_TRANSFER)) {
            mPaymentMethod = "Transfer Bank";
        } else if (paymentMethod.equalsIgnoreCase(FORMAT_CC) || paymentMethod.equalsIgnoreCase("CC")) {
            mPaymentMethod = "Kartu Kredit";
        } else if (paymentMethod.equalsIgnoreCase(FORMAT_CEK_OR_GIRO)) {
            mPaymentMethod = "Cek/Giro";
        } else if (paymentMethod.equalsIgnoreCase(FORMAT_ETC)) {
            mPaymentMethod = "Lain-lain";
        }
        return mPaymentMethod;
    }
}
