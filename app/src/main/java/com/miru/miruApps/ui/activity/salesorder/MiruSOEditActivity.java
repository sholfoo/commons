package com.miru.miruApps.ui.activity.salesorder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.so.EventSoCustomer;
import com.miru.miruApps.eventbus.so.EventSoProduct;
import com.miru.miruApps.model.response.SalesOrder;
import com.miru.miruApps.model.retrieve.RetrieveSoInfoData;
import com.miru.miruApps.model.retrieve.RetrieveSoProductData;
import com.miru.miruApps.network.SoApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.salesorder.EditSOCustomerFragment;
import com.miru.miruApps.ui.fragment.salesorder.EditSOProductFragment;
import com.miru.miruApps.ui.fragment.salesorder.EditSOReviewFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruSOEditActivity extends BaseActivity implements EditSOCustomerFragment.OnNextListener, EditSOProductFragment.OnReviewListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;
    @Inject SoApiService mApiService;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        showDiscardDialog();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_so_main_create);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setupActionBar();
        initializeSOToEdit();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_so);

    }

    private void setupViewPager(NonSwipeableViewPager viewPager, SalesOrder salesOrder) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(EditSOCustomerFragment.newInstance(salesOrder),
                getResources().getString(R.string.tab_title_so_customer));
        mViewPagerAdapter.addFragment(EditSOProductFragment.newInstance(salesOrder),
                getResources().getString(R.string.tab_title_so_product));
        mViewPagerAdapter.addFragment(EditSOReviewFragment.newInstance(salesOrder),
                getResources().getString(R.string.tab_title_so_review));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNextInfo(RetrieveSoInfoData retrieveSoInfoData) {
//        htabTabs.getTabAt(1).select();
        htabTabs.setScrollPosition(1, 0f, true);
        htabViewpager.setCurrentItem(1);
        EventBus.getDefault().postSticky(new EventSoCustomer(retrieveSoInfoData));
    }

    @Override
    public void onActionReviewproduct(RetrieveSoProductData retrieveSoProductData) {
//        htabTabs.getTabAt(2).select();
        htabTabs.setScrollPosition(2, 0f, true);
        htabViewpager.setCurrentItem(2);
        EventBus.getDefault().postSticky(new EventSoProduct(retrieveSoProductData));
    }

    private String getExtraExtSoId(){
        return getIntent().getStringExtra("extSoId");
    }

    @Override
    public void onBackPressed() {
        showDiscardDialog();
    }

    public void initializeSOToEdit(){
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<SalesOrder> salesOrderObservable = mApiService.getSalesOrderDetail(
                new SPUser().getKeyUserClientId(this),
                1,
                getExtraExtSoId());

        salesOrderObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SalesOrder>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(SalesOrder salesOrder) {
                        if (salesOrder.getStatusCode() == STATUS_SUCCESS) {
                            setupViewPager(htabViewpager, salesOrder);
                            htabTabs.setupWithViewPager(htabViewpager);
                        }
                    }
                });
    }
}
