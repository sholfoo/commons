package com.miru.miruApps.ui.activity.dashboard;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.dashboard.DashInvoiceAdapter;
import com.miru.miruApps.adapter.dashboard.DashProductAdapter;
import com.miru.miruApps.data.LocalDataSource;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.SummaryDashboard;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.view.BarChartMarker;
import com.miru.miruApps.view.DayAxisValueFormatter;
import com.miru.miruApps.view.YearMonthPickerDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.PERCENT;

public class MiruDashboardActivity extends BaseActivity {

    @Inject InvoiceApiService mApiService;

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.lv_content_invoice) RecyclerView lvContentInvoice;
    @BindView(R.id.lv_content_product) RecyclerView lvContentProduct;
    @BindView(R.id.invoice_total_amount) TextView invoiceTotalAmount;
    @BindView(R.id.invoice_total_amount_statistic_percent) TextView invoiceTotalAmountStatisticPercent;
    @BindView(R.id.invoice_total_quantity) TextView invoiceTotalQuantity;
    @BindView(R.id.invoice_total_qty_statistic_percent) TextView invoiceTotalQtyStatisticPercent;
    @BindView(R.id.total_unpaid_amount) TextView totalUnpaidAmount;
    @BindView(R.id.total_unpaid_invoice) TextView totalUnpaidInvoice;
    @BindView(R.id.total_unpaid_customer) TextView totalUnpaidCustomer;
    @BindView(R.id.invoice_qty_per_year) TextView invoiceQtyPerYear;
    @BindView(R.id.invoice_nominal_per_year) TextView invoiceNominalPerYear;
    @BindView(R.id.chart_invoice_qty) BarChart chartInvoiceQty;
    @BindView(R.id.chart_invoice_nominal) BarChart chartInvoiceNominal;
    @BindView(R.id.summary_total_payment) TextView summaryTotalPayment;
    @BindView(R.id.summary_average_payment) TextView summaryAveragePayment;
    @BindView(R.id.summary_quantity_payment) TextView summaryQuantityPayment;
    @BindView(R.id.summary_total_invoice) TextView summaryTotalInvoice;
    @BindView(R.id.summary_quantity_invoice) TextView summaryQuantityInvoice;
    @BindView(R.id.summary_average_invoice) TextView summaryAverageInvoice;
    @BindView(R.id.summary_total_so) TextView summaryTotalSo;
    @BindView(R.id.summary_quantity_so) TextView summaryQuantitySo;
    @BindView(R.id.summary_average_so) TextView summaryAverageSo;
    @BindView(R.id.selling_percent) TextView sellingPercent;
    @BindView(R.id.bg_statistic_meter) ImageView bgStatisticMeter;
    @BindView(R.id.current_month_final_amount) TextView currentMonthFinalAmount;
    @BindView(R.id.target_amount) TextView targetAmount;
    @BindView(R.id.last_month_target) TextView lastMonthTarget;
    @BindView(R.id.target_amount_two) TextView targetAmountTwo;
    @BindView(R.id.action_change_month) TextView actionChangeMonth;
    @BindView(R.id.average_invoice_chart) LineChart averageInvoiceChart;
    @BindView(R.id.average_so_chart) LineChart averageSoChart;
    @BindView(R.id.average_payment_chart) LineChart averagePaymenteChart;
    @BindView(R.id.ly_refresh) SwipeRefreshLayout lyRefresh;


    private DashInvoiceAdapter mDashInvoiceAdapter;
    private DashProductAdapter mDashProductAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_change_month)
    void onActionChangeMonth(){
        createDatePickerDialog();
    }

    @OnClick(R.id.action_set_target)
    void onActionSetTarget(){
        showDialogSetAndEditTarget();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_dashboard);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setupActionBar();
        setupAdapters();
        actionChangeMonth.setText(DateTimeUtil.getCurrentFormat("MMM yyyy"));
        lyRefresh.setColorSchemeResources(R.color.colorAccent);
        lyRefresh.setOnRefreshListener(() -> initializeDashboardSummaries(DateTimeUtil.getCurrentFormat("MM-yyyy")));
        initializeDashboardSummaries(DateTimeUtil.getCurrentFormat("MM-yyyy"));
    }

    private void setupActionBar() {
        toolbarLogo.setText(getResources().getString(R.string.action_bar_dashboard));
        Drawable img = getResources().getDrawable(R.drawable.ic_actionbar_dashboard);
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);

    }

    protected void createDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2010,01,01);

        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(this, calendar, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
                SimpleDateFormat dateFormatApi = new SimpleDateFormat("MM-yyyy");

                actionChangeMonth.setText(dateFormat.format(calendar.getTime()));
                initializeDashboardSummaries(dateFormatApi.format(calendar.getTime()));
            }
        });

        yearMonthPickerDialog.show();

//        MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
//        pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
//                String monthStr = month < 10 ? "0" + month : "" + month;
//                String dateStr = monthStr + "-" + year;
//                actionChangeMonth.setText(DateTimeUtil.getConvertedFormat(dateStr,
//                        "MM-yyyy",
//                        "MMM yyyy"));
//
//                initializeDashboardSummaries(dateStr);
//
//            }
//        });
//        pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");

//        DatePickerDialog mDatePickerDialog = new DatePickerDialog(
//                this,
//                R.style.MiruApp_DatePicker,
//                this,
//                now.get(Calendar.YEAR),
//                now.get(Calendar.MONTH),
//                now.get(Calendar.DAY_OF_MONTH));
//        mDatePickerDialog.show();
    }

    private void setupAdapters() {
        mDashInvoiceAdapter = new DashInvoiceAdapter(this);
        mDashProductAdapter = new DashProductAdapter(this);

        lvContentInvoice.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContentProduct.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        lvContentInvoice.setAdapter(mDashInvoiceAdapter);
        lvContentProduct.setAdapter(mDashProductAdapter);
    }

    private void setupViews(SummaryDashboard summaryDashboard) {
        /* for invoice */
        int currentMonthAmount = Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary().getCurrentMonthFinalAmount());
        int lastMonthAmount = Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary().getLastMonthFinalAmount());
        int currentMonthQuantity = Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary().getCurrentMonthNumOfInvoices());
        int lastMonthQuantity = Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary().getLastMonthNumOfInvoices());

        int up = R.drawable.ic_arrow_up_green;
        int down = R.drawable.ic_arrow_down_red;
        int backgroundUp = R.drawable.shape_rounded_green_light;
        int backgroundDown = R.drawable.shape_rounded_red_light;

        invoiceTotalAmount.setText(FormatNumber.getFormatCurrencyIDR(currentMonthAmount));
        invoiceTotalQuantity.setText(FormatNumber.getFormatHashTag(String.valueOf(currentMonthQuantity)));
        invoiceTotalAmountStatisticPercent.setText(String.format("%s%s", summaryDashboard.getCurrentMonthInvoiceSummary().getPercentIncreaseSumOfInvoice(), PERCENT));
        invoiceTotalQtyStatisticPercent.setText(String.format("%s%s", summaryDashboard.getCurrentMonthInvoiceSummary().getPercentIncreaseNumOfInvoice(), PERCENT));

        if (currentMonthAmount < lastMonthAmount) {
            invoiceTotalAmountStatisticPercent.setBackgroundResource(backgroundDown);
            invoiceTotalAmountStatisticPercent.setTextColor(Color.parseColor("#EB5757"));
            invoiceTotalAmountStatisticPercent.setCompoundDrawablesWithIntrinsicBounds(down, 0, 0, 0);
        } else {
            invoiceTotalAmountStatisticPercent.setBackgroundResource(backgroundUp);
            invoiceTotalAmountStatisticPercent.setTextColor(Color.parseColor("#27AE60"));
            invoiceTotalAmountStatisticPercent.setCompoundDrawablesWithIntrinsicBounds(up, 0, 0, 0);
        }

        if (currentMonthQuantity < lastMonthQuantity) {
            invoiceTotalQtyStatisticPercent.setBackgroundResource(backgroundDown);
            invoiceTotalQtyStatisticPercent.setTextColor(Color.parseColor("#EB5757"));
            invoiceTotalQtyStatisticPercent.setCompoundDrawablesWithIntrinsicBounds(down, 0, 0, 0);
        } else {
            invoiceTotalQtyStatisticPercent.setBackgroundResource(backgroundUp);
            invoiceTotalQtyStatisticPercent.setTextColor(Color.parseColor("#27AE60"));
            invoiceTotalQtyStatisticPercent.setCompoundDrawablesWithIntrinsicBounds(up, 0, 0, 0);
        }

        /* for unpaid invoices costumer*/
        totalUnpaidAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(summaryDashboard.getUnpaidInvoiceSummary().getSumOfUnpaidAmount())));
        totalUnpaidInvoice.setText(summaryDashboard.getUnpaidInvoiceSummary().getNumOfUnpaidInvoices());
        totalUnpaidCustomer.setText(summaryDashboard.getUnpaidInvoiceSummary().getNumOfUnpaidCustomer());

        /* for payment */
        summaryTotalPayment.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(summaryDashboard.getPaymentSummary().getSumOfPayments())));
        summaryAveragePayment.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(summaryDashboard.getPaymentSummary().getAvgOfPayments())));
        summaryQuantityPayment.setText(summaryDashboard.getPaymentSummary().getNumOfPayments());

        /* for invoice */
        summaryTotalInvoice.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(
                summaryDashboard.getCurrentMonthInvoiceSummary().getCurrentMonthFinalAmount())));
        summaryAverageInvoice.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(
                summaryDashboard.getCurrentMonthInvoiceSummary().getCurrentMonthAvgAmount())));
        summaryQuantityInvoice.setText(summaryDashboard.getCurrentMonthInvoiceSummary().getCurrentMonthNumOfInvoices());

        /* for SO */
        summaryTotalSo.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(summaryDashboard.getOrderSummary().getSumOfOrders())));
        summaryAverageSo.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(summaryDashboard.getOrderSummary().getAvgOfOrders())));
        summaryQuantitySo.setText(summaryDashboard.getOrderSummary().getNumOfOrders());

        /* for selling invoice target */
        sellingPercent.setText(getInvoiceSellingTargetPercent(
                Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary()
                        .getCurrentMonthFinalAmount()),
                Integer.parseInt(new LocalDataSource().getKeyTargetAmount(this))
        ));

        float percentage = ((float) Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary()
                .getCurrentMonthFinalAmount()) / Integer.parseInt(new LocalDataSource().getKeyTargetAmount(this))) * 100;

        Log.e("Percentage", String.valueOf(percentage));
        if (percentage > 0 && percentage <= 20) {
            Glide.with(this)
                    .load(R.drawable.statistic_meter_20)
                    .into(bgStatisticMeter);
        } else if (percentage > 20 && percentage <= 40) {
            Glide.with(this)
                    .load(R.drawable.statistic_meter_40)
                    .into(bgStatisticMeter);
        } else if (percentage > 40 && percentage <= 60) {
            Glide.with(this)
                    .load(R.drawable.statistic_meter_60)
                    .into(bgStatisticMeter);
        } else if (percentage > 60 && percentage <= 80) {
            Glide.with(this)
                    .load(R.drawable.statistic_meter_80)
                    .into(bgStatisticMeter);
        } else if (percentage > 80 && percentage <= 100) {
            Glide.with(this)
                    .load(R.drawable.statistic_meter_100)
                    .into(bgStatisticMeter);
        } else {
            Glide.with(this)
                    .load(R.drawable.statistic_meter)
                    .into(bgStatisticMeter);
        }

        currentMonthFinalAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary()
                        .getCurrentMonthFinalAmount())));
        targetAmount.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(new LocalDataSource().getKeyTargetAmount(this))));
        lastMonthTarget.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(summaryDashboard.getCurrentMonthInvoiceSummary()
                        .getLastMonthFinalAmount())));
        targetAmountTwo.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(new LocalDataSource().getKeyTargetAmount(this))));

        invoiceQtyPerYear.setText(String.format("Total Jumlah Invoice Tahun %s",
                summaryDashboard.getMonthlyInvoiceSummary().get(DEFAULT_KEYS_DISABLE).getMonth().substring(3, 7)));

        invoiceNominalPerYear.setText(String.format("Total Nominal Invoice Tahun %s",
                summaryDashboard.getMonthlyInvoiceSummary().get(DEFAULT_KEYS_DISABLE).getMonth().substring(3, 7)));
    }

    private void setupNumInvoiceChartGraphic(SummaryDashboard summaryDashboard) {
        chartInvoiceQty.setDrawBarShadow(false);
        chartInvoiceQty.setDrawValueAboveBar(true);
        chartInvoiceQty.getDescription().setEnabled(false);
        chartInvoiceQty.setMaxVisibleValueCount(14);
        chartInvoiceQty.setPinchZoom(false);
        chartInvoiceQty.setDrawGridBackground(false);

        IMarker marker = new BarChartMarker(this, R.layout.marker_bar_chart, "Invoice");

        // set the marker to the chart
        chartInvoiceQty.setMarker(marker);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chartInvoiceQty);

        XAxis xAxis = chartInvoiceQty.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(12);
        xAxis.setValueFormatter(xAxisFormatter);

        chartInvoiceQty.getAxisLeft().setDrawGridLines(false);
        chartInvoiceQty.animateY(1500);
        chartInvoiceQty.getLegend().setEnabled(false);
        chartInvoiceQty.getAxisLeft().setEnabled(false);
        chartInvoiceQty.getAxisRight().setEnabled(false);

        ArrayList<BarEntry> values = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < summaryDashboard.getMonthlyInvoiceSummary().size(); j++){
                int month = getDetermineMonth(summaryDashboard.getMonthlyInvoiceSummary().get(j).getMonth().substring(0, 2));
                Log.e("Month is", String.valueOf(month) + "");
                float quantity = (float) Integer.parseInt(summaryDashboard.getMonthlyInvoiceSummary().get(j).getNumOfInvoices());
                values.add(new BarEntry(
                        month, quantity
                ));
            }
            values.add(new BarEntry(
                    i, 0L
            ));
        }

        Log.e("Values", values.size() + "");

        BarDataSet set1;
        if (chartInvoiceQty.getData() != null &&
                chartInvoiceQty.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chartInvoiceQty.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chartInvoiceQty.getData().notifyDataChanged();
            chartInvoiceQty.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Data Set");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            set1.setDrawValues(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            chartInvoiceQty.setData(data);
            chartInvoiceQty.setFitBars(true);
        }
    }

    private void setupSumInvoiceChartGraphic(SummaryDashboard summaryDashboard) {
        chartInvoiceNominal.setDrawBarShadow(false);
        chartInvoiceNominal.setDrawValueAboveBar(true);
        chartInvoiceNominal.getDescription().setEnabled(false);
        chartInvoiceNominal.setMaxVisibleValueCount(14);
        chartInvoiceNominal.setPinchZoom(false);
        chartInvoiceNominal.setDrawGridBackground(false);

        IMarker marker = new BarChartMarker(this, R.layout.marker_bar_chart, "Rupiah");

        // set the marker to the chart
        chartInvoiceNominal.setMarker(marker);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chartInvoiceNominal);

        XAxis xAxis = chartInvoiceNominal.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(12);
        xAxis.setValueFormatter(xAxisFormatter);

        chartInvoiceNominal.getAxisLeft().setDrawGridLines(false);
        chartInvoiceNominal.animateY(1500);
        chartInvoiceNominal.getLegend().setEnabled(false);
        chartInvoiceNominal.getAxisLeft().setEnabled(false);
        chartInvoiceNominal.getAxisRight().setEnabled(false);

        ArrayList<BarEntry> values = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < summaryDashboard.getMonthlyInvoiceSummary().size(); j++){
                int month = getDetermineMonth(summaryDashboard.getMonthlyInvoiceSummary().get(j).getMonth().substring(0, 2));
                Log.e("Month is", String.valueOf(month) + "");
                float quantity = (float) Integer.parseInt(summaryDashboard.getMonthlyInvoiceSummary().get(j).getSumOfFinalAmount());
                values.add(new BarEntry(
                        month, quantity
                ));
            }
            values.add(new BarEntry(
                    i, 0L
            ));
        }

        Log.e("Values", values.size() + "");

        BarDataSet set1;
        if (chartInvoiceNominal.getData() != null &&
                chartInvoiceNominal.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chartInvoiceNominal.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chartInvoiceNominal.getData().notifyDataChanged();
            chartInvoiceNominal.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Data Set");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            set1.setDrawValues(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            chartInvoiceNominal.setData(data);
            chartInvoiceNominal.setFitBars(true);
        }
    }

    private int getDetermineMonth(String mMonth){
        Log.e("Month", mMonth);
        if (mMonth.equalsIgnoreCase("01"))
            return 0;
        if (mMonth.equalsIgnoreCase("02"))
            return 1;
        if (mMonth.equalsIgnoreCase("03"))
            return 2;
        if (mMonth.equalsIgnoreCase("04"))
            return 3;
        if (mMonth.equalsIgnoreCase("05"))
            return 4;
        if (mMonth.equalsIgnoreCase("06"))
            return 5;
        if (mMonth.equalsIgnoreCase("07"))
            return 6;
        if (mMonth.equalsIgnoreCase("08"))
            return 7;
        if (mMonth.equalsIgnoreCase("09"))
            return 8;

        return Integer.parseInt(mMonth) - 1;
    }

    private void initializeDashboardSummaries(String month) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<SummaryDashboard> summaryDashboardObservable = mApiService.getDashboardSummary(
                new SPUser().getKeyUserClientId(this),
                month
        );

        summaryDashboardObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SummaryDashboard>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof Exception) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(SummaryDashboard summaryDashboard) {
                        mDashInvoiceAdapter.pushData(summaryDashboard.getCustomerInvoiceSummary());
                        mDashProductAdapter.pushData(summaryDashboard.getProductSummary());
                        setupViews(summaryDashboard);
                        setupNumInvoiceChartGraphic(summaryDashboard);
                        setupSumInvoiceChartGraphic(summaryDashboard);
                        setupAverageChart(averageInvoiceChart, Integer.parseInt(
                                summaryDashboard.getCurrentMonthInvoiceSummary().getCurrentMonthNumOfInvoices()));
                        setupAverageChart(averageSoChart, Integer.parseInt(
                                summaryDashboard.getOrderSummary().getNumOfOrders()));
                        setupAverageChart(averagePaymenteChart, Integer.parseInt(
                                summaryDashboard.getPaymentSummary().getNumOfPayments()));
                        lyRefresh.setRefreshing(false);
                    }
                });
    }

    private String getInvoiceSellingTargetPercent(int current, int target){
        float percentage = ((float) current / target) * 100;
        return String.format("%s%%", String.format("%2.02f", percentage));
    }

//    @Override
//    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
//        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
//        String monthStr = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
//        String yearStr = String.valueOf(year);
//
//        String dateStr = dayStr + "-" + monthStr + "-" + yearStr;
//        actionChangeMonth.setText(DateTimeUtil.getConvertedFormat(dateStr,
//                FORMAT_DATE,
//                "MMM yyyy"));
//
//        initializeDashboardSummaries(DateTimeUtil.getConvertedFormat(dateStr,
//                FORMAT_DATE,
//                "MM-yyyy"));
//    }

    private void showDialogSetAndEditTarget(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sellout_target_invoice);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        EditText inputNominal = dialog.findViewById(R.id.input_nominal);
        Button actionSave = dialog.findViewById(R.id.action_save);

        actionSave.setOnClickListener(view -> {
            dialog.dismiss();
            new LocalDataSource().setKeyTargetAmount(this,
                    inputNominal.getText().toString());
        });
        dialog.show();
    }

    private LineData generateDataLine(int quantity) {

        ArrayList<Entry> values1 = new ArrayList<>();

        values1.add(new Entry(0, (int) 0));
        values1.add(new Entry(0, (int) 1));
        values1.add(new Entry(1, (int) quantity));

        LineDataSet d1 = new LineDataSet(values1, "");
        d1.setLineWidth(2.5f);
        d1.setCircleRadius(4.5f);
        d1.setHighLightColor(Color.rgb(244, 117, 117));
        d1.setColor(ColorTemplate.getHoloBlue());
        d1.setCircleColor(ColorTemplate.getHoloBlue());
        d1.setDrawValues(false);

        ArrayList<ILineDataSet> sets = new ArrayList<>();
        sets.add(d1);

        return new LineData(sets);
    }

//    private void setupAverageInvoiceChart(int quantity){
//        averageInvoiceChart.getDescription().setEnabled(false);
//        averageInvoiceChart.setDrawGridBackground(false);
//
//        XAxis xAxis = averageInvoiceChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setDrawGridLines(false);
//        xAxis.setDrawAxisLine(true);
//
//        YAxis leftAxis = averageInvoiceChart.getAxisLeft();
//        int plus = quantity < 10 ? 2 : 10;
//        leftAxis.setLabelCount(quantity + plus, false);
//        leftAxis.setAxisMaximum(quantity + plus);
//        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        YAxis rightAxis = averageInvoiceChart.getAxisRight();
//        rightAxis.setLabelCount(5, false);
//        rightAxis.setDrawGridLines(false);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        averageInvoiceChart.getAxisRight().setEnabled(false);
//        averageInvoiceChart.getXAxis().setDrawLabels(false);
//        averageInvoiceChart.getLegend().setEnabled(false);
//
//        ChartData<?> mChartData = generateDataLine(quantity);
//        // set data
//        averageInvoiceChart.setData((LineData) mChartData);
//
//        // do not forget to refresh the chart
//        // holder.chart.invalidate();
//        averageInvoiceChart.animateX(750);
//    }

    private void setupAverageChart(LineChart lineChart, int quantity){
        lineChart.getDescription().setEnabled(false);
        lineChart.setDrawGridBackground(false);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = lineChart.getAxisLeft();
        int plus = quantity < 10 ? 2 : 10;
        leftAxis.setLabelCount(quantity + plus, false);
        leftAxis.setAxisMaximum(quantity + plus);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        lineChart.getAxisRight().setEnabled(false);
        lineChart.getXAxis().setDrawLabels(false);
        lineChart.getLegend().setEnabled(false);

        ChartData<?> mChartData = generateDataLine(quantity);
        // set data
        lineChart.setData((LineData) mChartData);

        // do not forget to refresh the chart
        // holder.chart.invalidate();
        lineChart.animateX(750);
    }

}
