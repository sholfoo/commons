package com.miru.miruApps.ui.fragment.invoice;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ReviewSoProductAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.SoProduct;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.FAILED;
import static com.miru.miruApps.config.Constants.FULL_PAYMENT;
import static com.miru.miruApps.config.Constants.OVERDUE;
import static com.miru.miruApps.config.Constants.PARTIAL_PAYMENT;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;
import static com.miru.miruApps.config.Constants.UNPAID;

public class DetailInvoiceFragment extends BaseFragment {

    @Inject InvoiceApiService mApiService;

    @BindView(R.id.invoice_status) TextView invoiceStatus;
    @BindView(R.id.invoice_date) TextView invoiceDate;
    @BindView(R.id.ext_invoice_id) TextView extInvoiceId;
    @BindView(R.id.user_store_logo) ImageView userStoreLogo;
    @BindView(R.id.user_store_name) TextView userStoreName;
    @BindView(R.id.user_full_name) TextView userFullName;
    @BindView(R.id.customer_name) TextView customerName;
    @BindView(R.id.invoice_due_date) TextView invoiceDueDate;
    @BindView(R.id.invoice_total) TextView invoiceTotal;
    @BindView(R.id.invoice_remaining) TextView invoiceRemaining;
    @BindView(R.id.lv_content_review) RecyclerView lvContentReview;
    @BindView(R.id.invoice_subtotal) TextView invoiceSubtotal;
    @BindView(R.id.invoice_discount_percent) TextView invoiceDiscountPercent;
    @BindView(R.id.invoice_discount_price) TextView invoiceDiscountPrice;
    @BindView(R.id.invoice_tax_percent) TextView invoiceTaxPercent;
    @BindView(R.id.invoice_tax_price) TextView invoiceTaxPrice;
    @BindView(R.id.invoice_total_price) TextView invoiceTotalPrice;
    @BindView(R.id.invoice_paid_amount) TextView invoicePaidAmount;
    @BindView(R.id.invoice_remaining_amount) TextView invoiceRemainingAmount;
    @BindView(R.id.invoice_note) TextView invoiceNote;
    @BindView(R.id.current_date) TextView currentDate;
    @BindView(R.id.signature_image) ImageView signatureImage;
    @BindView(R.id.customer_name_bottom) EditText customerNameBottom;

    private ReviewSoProductAdapter mAdapter;

    public static DetailInvoiceFragment newInstance(String invoiceId, String extInvoiceId) {
        Bundle bundle = new Bundle();
        bundle.putString("invoiceId", invoiceId);
        bundle.putString("extInvoiceId", extInvoiceId);
        DetailInvoiceFragment detailInvoiceFragment = new DetailInvoiceFragment();
        detailInvoiceFragment.setArguments(bundle);
        return detailInvoiceFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice_detail_invoice, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    private String getArgumentExtraExtInvoiceId() {
        return getArguments() != null ? getArguments().getString("extInvoiceId") : null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new ReviewSoProductAdapter(mContext);
        lvContentReview.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContentReview.setAdapter(mAdapter);
        initDetailSalesOrder();
    }

    private void initDetailSalesOrder() {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<Invoice> invoiceObservable = mApiService.getInvoiceDetail(
                new SPUser().getKeyUserClientId(mContext),
                1,
                getArgumentExtraExtInvoiceId());

        invoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Invoice>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Invoice invoice) {
                        if (invoice.getStatusCode() == STATUS_SUCCESS) {
                            setupReview(invoice);
                        }
                    }
                });
    }

    private void setupReview(Invoice invoice) {
        List<SoProduct> soProductList = new ArrayList<>();
        for (int i = 0; i < invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().size(); i++) {
            int mQuantity = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getQuantity();
            int mPricePerUnit = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getPricePerUnit();
            int totalAmountProduct = mQuantity * mPricePerUnit;

            soProductList.add(new SoProduct(
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getIdentifierId().getItemSKU(),
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getItemName(),
                    String.valueOf(mQuantity),
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getMeasurementUnit(),
                    String.valueOf(mPricePerUnit),
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getDiscount(),
                    String.valueOf(mPricePerUnit),
                    String.valueOf(totalAmountProduct)
            ));
        }

        mAdapter.pushData(soProductList);

        Glide.with(mContext)
                .load(new SPUser().getKeyUserStoreLogo(mContext))
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(userStoreLogo);

        Glide.with(mContext)
                .load(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getSignatureUrl())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(signatureImage);

        userStoreName.setText(new SPUser().getKeyUserStore(mContext));
        invoiceStatus.setText(getInvoiceStatus(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getPaymentStatus()));
        invoiceDate.setText(DateTimeUtil.getConvertedFormat(
                invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getInvoiceDate(), "dd-MM-yyyy",
                "dd MMM yyyy"));
        extInvoiceId.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getExtInvoiceId());
        userFullName.setText(new SPUser().getKeyUserFullName(mContext));
        customerName.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getCustomerName());
        invoiceDueDate.setText(DateTimeUtil.getConvertedFormat(
                invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getDueDate(), "dd-MM-yyyy",
                "dd MMM yyyy"));
        invoiceTotal.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getFinalAmount()));
        invoiceRemaining.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getRemainingAmount()));
        invoiceSubtotal.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalAmount()));
        invoiceDiscountPercent.setText(FormatNumber.getFormatPercent(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalDiscount()));
        invoiceDiscountPrice.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalDiscountInNum()));

        invoiceTaxPercent.setText(FormatNumber.getFormatPercent(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTax()));
        invoiceTaxPrice.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTaxInNum()));

        int finalAmount = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getFinalAmount();
        int remainingAmount = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getRemainingAmount();
        int paidAmount = finalAmount - remainingAmount;
        invoiceTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(finalAmount));
        invoicePaidAmount.setText(FormatNumber.getFormatCurrencyIDR(paidAmount));
        invoiceRemainingAmount.setText(FormatNumber.getFormatCurrencyIDR(remainingAmount));
        invoiceNote.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getNotes());

        currentDate.setText(DateTimeUtil.getCurrentFormat("dd MMM yyyy"));
        customerNameBottom.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getCustomerName());
    }

    private String getInvoiceStatus(String mStatus){
        if (mStatus.equalsIgnoreCase(UNPAID)){
            return "Belum Lunas";
        }if (mStatus.equalsIgnoreCase(FULL_PAYMENT)){
            return "Lunas";
        }if (mStatus.equalsIgnoreCase(PARTIAL_PAYMENT)){
            return "Bayar Sebagian";
        }if (mStatus.equalsIgnoreCase(OVERDUE)){
            return "Terlambat";
        }if (mStatus.equalsIgnoreCase(FAILED)){
            return "Gagal Bayar";
        }

        return mStatus;
    }
}
