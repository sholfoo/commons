package com.miru.miruApps.ui.fragment.salesorder;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ReviewSoProductAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.so.EventSoProduct;
import com.miru.miruApps.model.response.CreateSo;
import com.miru.miruApps.model.retrieve.RetrieveSoProductData;
import com.miru.miruApps.network.SoApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.utils.ImageUtilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class CreateSOReviewFragment extends BaseFragment {

    private static final String TAG = CreateSOReviewFragment.class.getCanonicalName();

    @BindView(R.id.so_date) TextView soDate;
    @BindView(R.id.user_store_logo) ImageView userStoreLogo;
    @BindView(R.id.user_store_name) TextView userStoreName;
    @BindView(R.id.user_full_name) TextView userFullName;
    @BindView(R.id.sales_name) TextView salesName;
    @BindView(R.id.lv_content_review) RecyclerView lvContentReview;
    @BindView(R.id.so_subtotal) TextView soSubtotal;
    @BindView(R.id.so_subtotal_after_discount) TextView soSubtotalAfterDiscount;
    @BindView(R.id.so_discount) TextView soDiscount;
    @BindView(R.id.so_discount_price) TextView soDiscountPrice;
    @BindView(R.id.input_so_tax) TextView inputSoTax;
    @BindView(R.id.so_tax_price) TextView soTaxPrice;
    @BindView(R.id.so_total_price) TextView soTotalPrice;
    @BindView(R.id.so_note) TextView soNote;
    @BindView(R.id.current_date) TextView currentDate;
    @BindView(R.id.signature_pad_description) TextView signaturePadDescription;
    @BindView(R.id.signature_pad) SignaturePad signaturePad;
    @BindView(R.id.signature_pad_container) RelativeLayout signaturePadContainer;
    @BindView(R.id.customer_name) EditText customerName;
    @BindView(R.id.action_save_so_data) Button actionSaveSoData;

    private RetrieveSoProductData retrieveSoProductData;
    private ReviewSoProductAdapter mAdapter;
    private File compressedSignatureFile;

    @Inject SoApiService mApiService;

    @OnClick(R.id.action_save_so_data)
    void onActionSaveSoData(){
        createNewProduct(signaturePad.getSignatureBitmap());
    }

    public static CreateSOReviewFragment newInstance() {
        return new CreateSOReviewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(EventSoProduct eventSoProduct) {
        this.retrieveSoProductData = eventSoProduct.getRetrieveSoProductData();
        Log.e(TAG, "Implementation Running...");
        Log.e(TAG, retrieveSoProductData.toString());
        setupReview(eventSoProduct.getRetrieveSoProductData());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_so_create_review, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ReviewSoProductAdapter(mContext);
        lvContentReview.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContentReview.setAdapter(mAdapter);
    }

    private void setupReview(RetrieveSoProductData retrieveSoProductData) {
        mAdapter.pushData(retrieveSoProductData.getSoProductList());

        Glide.with(mContext)
                .load(new SPUser().getKeyUserStoreLogo(mContext))
                .into(userStoreLogo);

        userStoreName.setText(new SPUser().getKeyUserStore(mContext));
        soDate.setText(retrieveSoProductData.getRetrieveSoInfoData().getmSoDate());
        userFullName.setText(new SPUser().getKeyUserFullName(mContext));
        salesName.setText(retrieveSoProductData.getRetrieveSoInfoData().getmSalesName());
        soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(retrieveSoProductData.getmSubtotal()));
        soSubtotalAfterDiscount.setText(FormatNumber.getFormatCurrencyIDR(retrieveSoProductData.getmSubtotalAftterDiscount()));
        soDiscount.setText(FormatNumber.getFormatPercent(retrieveSoProductData.getmDiscountPercent()));
        soDiscountPrice.setText(FormatNumber.getFormatCurrencyIDR(retrieveSoProductData.getmDiscountTotal()));
        inputSoTax.setText(FormatNumber.getFormatPercent(retrieveSoProductData.getmTaxPercent()));
        soTaxPrice.setText(FormatNumber.getFormatCurrencyIDR(retrieveSoProductData.getmTaxTotal()));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(retrieveSoProductData.getmTotalAmount()));
        soNote.setText(retrieveSoProductData.getRetrieveSoInfoData().getmDescription());
        currentDate.setText(DateTimeUtil.getCurrentFormat("dd MMM yyyy"));
        customerName.setText(retrieveSoProductData.getRetrieveSoInfoData().getmSalesName());
    }

    private void createNewProduct(Bitmap mBitmap) {
        MaterialDialog submitDialog = new MaterialDialog.Builder(mContext)
                .content("Submitting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        List<String> itemSKUList = new ArrayList<>();
        List<String> itemNameList = new ArrayList<>();
        List<String> quantityList = new ArrayList<>();
        List<String> measurementUnitList = new ArrayList<>();
        List<String> pricePerUnitList = new ArrayList<>();
        List<String> discountList = new ArrayList<>();

        String itemSKU = null;
        String itemName = null;
        String quantity = null;
        String measurementUnit = null;
        String pricePerUnit = null;
        String discount = null;

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(mContext));
        RequestBody requestCustomerId = createRequestBody(retrieveSoProductData.getRetrieveSoInfoData().getCustomerId());
        RequestBody requestSalesName = createRequestBody(retrieveSoProductData.getRetrieveSoInfoData().getmSalesName());
        RequestBody requestNotes = createRequestBody(retrieveSoProductData.getRetrieveSoInfoData().getmDescription());
        RequestBody requestSoDate = createRequestBody(retrieveSoProductData.getRetrieveSoInfoData().getmSoDate());

        RequestBody requestTotalAmount = createRequestBody(String.valueOf(retrieveSoProductData.getmSubtotal()));
        RequestBody requestTaxPercent = createRequestBody(String.valueOf(retrieveSoProductData.getmTaxPercent()));
        RequestBody requestDiscountPercent = createRequestBody(String.valueOf(retrieveSoProductData.getmDiscountPercent()));
        RequestBody requestFinalAmount = createRequestBody(String.valueOf(retrieveSoProductData.getmTotalAmount()));

        for (int i = 0; i < retrieveSoProductData.getSoProductList().size(); i++){
            itemSKUList.add(retrieveSoProductData.getSoProductList().get(i).getProductSku());
            itemNameList.add(retrieveSoProductData.getSoProductList().get(i).getProductName());
            quantityList.add(retrieveSoProductData.getSoProductList().get(i).getProductQty());
            measurementUnitList.add(retrieveSoProductData.getSoProductList().get(i).getProductUnit());
            pricePerUnitList.add(retrieveSoProductData.getSoProductList().get(i).getProductAfterDiscount());
            discountList.add(retrieveSoProductData.getSoProductList().get(i).getProductDiscount());
        }

        try {
            itemSKU = getJoinedString(itemSKUList);
            itemName = getJoinedString(itemNameList);
            quantity = getJoinedString(quantityList);
            measurementUnit = getJoinedString(measurementUnitList);
            pricePerUnit = getJoinedString(pricePerUnitList);
            discount = getJoinedString(discountList);
            Log.e("Item SKU", itemSKU);
            Log.e("Quantity", quantity);
            Log.e("Measurement Unit", measurementUnit);
            Log.e("Price Unit", pricePerUnit);
            Log.e("Discount", discount);
        } catch (Exception e){

        }

        compressedSignatureFile = ImageUtilities.signatureImage(mBitmap);
        MultipartBody.Part signaturePart = null;
        signaturePart = prepareFilePart("signature", compressedSignatureFile);

        RequestBody requestItemSKU = createRequestBody(itemSKU);
        RequestBody requestItemName = createRequestBody(itemName);
        RequestBody requestQuantity = createRequestBody(quantity);
        RequestBody requestMeasurementUnit = createRequestBody(measurementUnit);
        RequestBody requestPricePerUnit = createRequestBody(pricePerUnit);
        RequestBody requestDiscount = createRequestBody(discount);

        Observable<CreateSo> createSoObservable = mApiService.createSalesOrder(
                requestClientId,
                requestCustomerId,
                requestSalesName,
                requestTaxPercent,
                requestTotalAmount,
                requestNotes,
                requestItemSKU,
                requestItemName,
                requestQuantity,
                requestMeasurementUnit,
                requestPricePerUnit,
                requestDiscount,
                requestDiscountPercent,
                requestFinalAmount,
                requestSoDate,
                signaturePart
        );
        createSoObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CreateSo>() {
                    @Override
                    public void onCompleted() {
                        submitDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        submitDialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CreateSo createSo) {
                        if (createSo.getStatusCode() == STATUS_SUCCESS) {
                            showDialogAlert();
                            Log.e("JSON", new Gson().toJson(createSo));
                        }
                    }
                });
    }

    private RequestBody createRequestBody(String mValue) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), mValue);
    }

    private MultipartBody.Part prepareFilePart(String partName, File documentFile) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(
                MediaType.parse("multipart/form-data"), documentFile);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, documentFile.getName(), requestFile);
    }

    private void showDialogAlert() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText("Data Sales Order Berhasil Disimpan");

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            getActivity().finish();
        });
        dialog.show();
    }
}
