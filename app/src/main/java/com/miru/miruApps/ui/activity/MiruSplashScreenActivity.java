package com.miru.miruApps.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.miru.miruApps.R;
import com.miru.miruApps.data.preference.SPLogin;
import com.miru.miruApps.ui.activity.login.MiruLoginActivity;
import com.miru.miruApps.ui.base.BaseActivity;

import static com.miru.miruApps.config.Constants.TIME_OUT;

public class MiruSplashScreenActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_miru_splash);
        init();
    }

    private void init(){
        if (new SPLogin().isLoggedIn(this)){
            startMainActivity();
        } else {
            startLoginActivity();
        }
    }

    private void startLoginActivity() {
        new Handler().postDelayed(() -> {
            // This method will be executed once the timer is over
            // Start your app main activity
            startActivity(new Intent(MiruSplashScreenActivity.this, MiruLoginActivity.class));
            // close this activity
            finish();
        }, TIME_OUT);
    }

    private void startMainActivity(){
        /*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         */
        new Handler().postDelayed(() -> {
            // This method will be executed once the timer is over
            // Start your app main activity
            startActivity(new Intent(MiruSplashScreenActivity.this, MiruMainActivity.class));

            // close this activity
            finish();
        }, 0);
    }
}
