package com.miru.miruApps.ui.activity.payment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.so.AutoCompleteCustomerAdapter;
import com.miru.miruApps.data.db.Customer;
import com.miru.miruApps.data.db.CustomerDao;
import com.miru.miruApps.data.db.Invoice;
import com.miru.miruApps.data.db.InvoiceDao;
import com.miru.miruApps.data.preference.SPExtraCustomer;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.CreatePayment;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.view.NumberTextWatcher;
import com.miru.miruApps.view.OnItemSelectedListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_FROM_CUSTOMER;
import static com.miru.miruApps.config.Constants.EXTRA_FROM_INVOICE;
import static com.miru.miruApps.config.Constants.FORMAT_CASH;
import static com.miru.miruApps.config.Constants.FORMAT_CC;
import static com.miru.miruApps.config.Constants.FORMAT_CEK_OR_GIRO;
import static com.miru.miruApps.config.Constants.FORMAT_DATE;
import static com.miru.miruApps.config.Constants.FORMAT_ETC;
import static com.miru.miruApps.config.Constants.FORMAT_TRANSFER;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruPaymentCreateActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private static final String DATE_PICKER = "date_picker_dialog";

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.input_payment_date) EditText inputPaymentDate;
    @BindView(R.id.input_layout_payment_date) TextInputLayout inputLayoutPaymentDate;
    @BindView(R.id.input_payment_customer_name) AutoCompleteTextView inputPaymentCustomerName;
    @BindView(R.id.input_layout_payment_customer_name) TextInputLayout inputLayoutPaymentCustomerName;
    @BindView(R.id.input_invoice_number) MaterialBetterSpinner inputInvoiceNumber;
    @BindView(R.id.payment_customer_name) TextView paymentCustomerName;
    @BindView(R.id.payment_invoice_no) TextView paymentInvoiceNo;
    @BindView(R.id.payment_invoice_date) TextView paymentInvoiceDate;
    @BindView(R.id.payment_amount) TextView paymentAmount;
    @BindView(R.id.payment_kredit) TextView paymentKredit;
    @BindView(R.id.ly_invoice_detail) CardView lyInvoiceDetail;
    @BindView(R.id.input_total_payment) EditText inputTotalPayment;
    @BindView(R.id.input_layout_total_payment) TextInputLayout inputLayoutTotalPayment;
    @BindView(R.id.spn_payment_method) MaterialBetterSpinner spnPaymentMethod;
    @BindView(R.id.input_payment_notes) EditText inputPaymentNotes;
    @BindView(R.id.input_layout_payment_notes) TextInputLayout inputLayoutPaymentNotes;

    @Inject InvoiceApiService mApiService;

    private CustomerDao customerDao;
    private InvoiceDao invoiceDao;
    private Query<Customer> customerQuery;
    private Query<Invoice> invoiceQuery;
    private List<Customer> customerList;
    private List<Invoice> invoiceList;
    private AutoCompleteCustomerAdapter mCustomerAdapter;

    private String mCustomerId;
    private String mCustomerName;
    private int mInvoiceTotal;
    private String mPaymentMethod;

    @OnClick(R.id.action_home)
    void onActionHome() {
        showDiscardDialog();
    }

    @OnClick(R.id.action_save_payment)
    void onActionSavePayment(){
        if (!validateInput(inputLayoutPaymentDate, inputPaymentDate))
            return;

        if (!validateInput(inputLayoutPaymentCustomerName, inputPaymentCustomerName))
            return;

        if (!validateInput(inputLayoutTotalPayment, inputTotalPayment))
            return;

        if (inputInvoiceNumber.getText().toString().trim().isEmpty()){
            Toast.makeText(this,
                    "Silahkan pilih invoice terlebih dahulu.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if (spnPaymentMethod.getText().toString().trim().isEmpty()){
            Toast.makeText(this,
                    "Silahkan pilih metode pembayaran terlebih dahulu.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        createNewPayment();

    }

    @OnClick(R.id.input_payment_date)
    void onActionInputDate() {
        createDatePickerDialog(DATE_PICKER, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag(DATE_PICKER);
        if (dpd != null) {
            dpd.setOnDateSetListener(this);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_payment_create);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        inputPaymentDate.setKeyListener(null);
        inputPaymentDate.setText(DateTimeUtil.getCurrentFormat(FORMAT_DATE));
        setupActionBar();
        setupSpinnerPaymentMethod();

        List<String> invoiceNumbers = new ArrayList<>();
        ArrayAdapter<String> mInvoiceNumberAdapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, invoiceNumbers);
        inputInvoiceNumber.setThreshold(1);
        inputInvoiceNumber.setAdapter(mInvoiceNumberAdapter);

        setupAutoCompleteCustomer();

        if (getExtraFromId() == EXTRA_FROM_CUSTOMER){
            setupViews();
        } else if (getExtraFromId() == EXTRA_FROM_INVOICE){
            setupViewsInvoice(getExtraInvoiceId());
        }

        inputTotalPayment.addTextChangedListener(new NumberTextWatcher(inputTotalPayment, "#,###"));
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_payment);
    }

    protected void createDatePickerDialog(String tag, DatePickerDialog.OnDateSetListener callback) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                callback,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.dismissOnPause(true);
        dpd.setAccentColor(Color.parseColor("#86C2EF"));
        dpd.setOnCancelListener(dialogInterface -> Log.d("DatePicker", "Dialog was cancelled"));
        dpd.setVersion(DatePickerDialog.Version.VERSION_1);
        dpd.show(getFragmentManager(), tag);
    }

    private void setupAutoCompleteCustomer() {
        customerDao = MiruApp.getDaoSession().getCustomerDao();
        customerQuery = customerDao
                .queryBuilder()
                .orderAsc(CustomerDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        customerList = customerQuery.list();
        mCustomerAdapter = new AutoCompleteCustomerAdapter(this, customerList);
        inputPaymentCustomerName.setThreshold(1);
        inputPaymentCustomerName.setAdapter(mCustomerAdapter);
        inputPaymentCustomerName.setOnItemClickListener((adapterView, view, i, l) -> {
            mCustomerId = customerList.get(i).getCustomerId();
            mCustomerName = customerList.get(i).getFullName();
            setupAutoCompleteInvoice(mCustomerId);
        });

    }

    private void setupAutoCompleteInvoice(String customerId) {
//        invoiceDao = MiruApp.getDaoSession().getInvoiceDao();
//        invoiceQuery = invoiceDao
//                .queryBuilder()
//                .orderAsc(InvoiceDao.Properties.Id)
//                .build();
//
//        QueryBuilder.LOG_SQL = true;
//        QueryBuilder.LOG_VALUES = true;
//
//        invoiceList = invoiceQuery.list();
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        Observable<com.miru.miruApps.model.response.Invoice> invoiceObservable = mApiService.getCustomerInvoice(
                new SPUser().getKeyUserClientId(getApplicationContext()),
                10,
                customerId);

        invoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<com.miru.miruApps.model.response.Invoice>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(com.miru.miruApps.model.response.Invoice invoice) {
                        if (invoice.getStatusCode() == STATUS_SUCCESS) {
                            if (invoice.getStatusCode() == STATUS_SUCCESS) {
                                List<String> invoiceNumbers = new ArrayList<>();
                                for (int i = 0; i < invoice.getInvoiceContainer().size(); i++) {
                                    invoiceNumbers.add(invoice.getInvoiceContainer().get(i).getInvoice().getExtInvoiceId());
                                }

                                ArrayAdapter<String> mInvoiceNumberAdapter = new ArrayAdapter<>(getApplicationContext(),
                                        android.R.layout.simple_dropdown_item_1line, invoiceNumbers);

                                inputInvoiceNumber.setThreshold(1);
                                inputInvoiceNumber.setAdapter(mInvoiceNumberAdapter);
                                inputInvoiceNumber.setOnItemClickListener((adapterView, view, i, l) -> {
                                    mInvoiceTotal = invoice.getInvoiceContainer().get(i).getInvoice().getFinalAmount();
                                    paymentCustomerName.setText(invoice.getInvoiceContainer().get(i).getInvoice().getCustomerStore());
                                    paymentInvoiceNo.setText(invoice.getInvoiceContainer().get(i).getInvoice().getExtInvoiceId());
                                    paymentInvoiceDate.setText(DateTimeUtil.getConvertedFormat(
                                            invoice.getInvoiceContainer().get(i).getInvoice().getInvoiceDate(),
                                            "dd-MM-yyyy",
                                            "dd MMM yyyy"));
                                    paymentAmount.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(i).getInvoice().getFinalAmount()));
                                    paymentKredit.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(i).getInvoice().getRemainingAmount()));
                                    lyInvoiceDetail.setVisibility(View.VISIBLE);
                                });
                            }
                        }
                    }
                });
    }

    private void setupSpinnerPaymentMethod() {
        String[] timePeriod = {
                "Cash",
                "Transfer Bank",
                "Cek/Giro",
                "Kartu Kredit",
                "Lain-lain"
        };

        ArrayAdapter<String> paymentMethodAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, timePeriod);
        spnPaymentMethod.setAdapter(paymentMethodAdapter);
        spnPaymentMethod.addTextChangedListener(new OnItemSelectedListener() {
            @Override
            protected void onItemSelected(String string) {
                if (string.equalsIgnoreCase(timePeriod[0])) {
                    mPaymentMethod = FORMAT_CASH;
                    Log.e("FORMAT", mPaymentMethod);
                } else if (string.equalsIgnoreCase(timePeriod[1])) {
                    mPaymentMethod = FORMAT_TRANSFER;
                    Log.e("FORMAT", mPaymentMethod);
                } else if (string.equalsIgnoreCase(timePeriod[2])) {
                    mPaymentMethod = FORMAT_CEK_OR_GIRO;
                    Log.e("FORMAT", mPaymentMethod);
                } else if (string.equalsIgnoreCase(timePeriod[3])) {
                    mPaymentMethod = FORMAT_CC;
                    Log.e("FORMAT", mPaymentMethod);
                } else if (string.equalsIgnoreCase(timePeriod[4])) {
                    mPaymentMethod = FORMAT_ETC;
                    Log.e("FORMAT", mPaymentMethod);
                }
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthStr = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
        String yearStr = String.valueOf(year);

        String dateStr = dayStr + "-" + monthStr + "-" + yearStr;
        inputPaymentDate.setText(dateStr);
    }

    private void createNewPayment() {
        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(this));
        RequestBody requestUserId = createRequestBody(new SPUser().getKeyUserId(this));
        RequestBody requestStoreName = createRequestBody(new SPUser().getKeyUserStore(this));
        RequestBody requestAddress = createRequestBody("null");
        Log.e("UserPhone", new SPUser().getKeyUserPhone(this) + "");
        RequestBody requestPhoneNumber = createRequestBody(new SPUser().getKeyUserPhone(this) + "");

        RequestBody requestCustomerId = createRequestBody(mCustomerId);
        RequestBody requestCustomerName = createRequestBody(mCustomerName);

        RequestBody requestInvoiceId = createRequestBody(inputInvoiceNumber.getText().toString());
        String clearPaymentAmount = FormatNumber.removeFromatCurrencyIDR(inputTotalPayment.getText().toString());
        RequestBody requestPaymentAmount = createRequestBody(clearPaymentAmount);

        int paymentAmount = Integer.parseInt(clearPaymentAmount);
        int remainingAmount = mInvoiceTotal - paymentAmount;

        RequestBody requestRemainingAmount = createRequestBody(String.valueOf(remainingAmount));
        RequestBody requestPaymentMethod = createRequestBody(mPaymentMethod);
        RequestBody requestPaymentDate = createRequestBody(inputPaymentDate.getText().toString());
        RequestBody requestNote = createRequestBody(inputPaymentNotes.getText().toString());

        MaterialDialog submitDialog = new MaterialDialog.Builder(this)
                .content("Submitting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        Observable<CreatePayment> createPaymentObservable = mApiService.createPayment(
                requestClientId,
                requestUserId,
                requestStoreName,
                requestAddress,
                requestPhoneNumber,
                requestCustomerId,
                requestCustomerName,
                requestInvoiceId,
                requestPaymentAmount,
                requestRemainingAmount,
                requestPaymentMethod,
                requestPaymentDate,
                requestNote
        );

        createPaymentObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CreatePayment>() {
                    @Override
                    public void onCompleted() {
                        submitDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        submitDialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(CreatePayment createPayment) {
                        if (createPayment.getStatusCode() == STATUS_SUCCESS) {
                            showDialogAlert();
                            Log.e("JSON", new Gson().toJson(createPayment));
                        }
                    }
                });

    }

    private void showDialogAlert() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText("Data Pembayaran Berhasil Disimpan");

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            finish();
        });
        dialog.show();
    }

    private int getExtraFromId(){
        return getIntent().getIntExtra("extra_from_id", 0);
    }

    private String getExtraInvoiceId(){
        return getIntent().getStringExtra("invoiceId");
    }

    private String getExtraExtInvoiceId(){
        return getIntent().getStringExtra("extInvoiceId");
    }

    private void setupViews(){
        mCustomerId = new SPExtraCustomer().getKeyCustId(this);
        inputPaymentCustomerName.setText(new SPExtraCustomer().getKeyCustName(this));
    }

    private void setupViewsInvoice(String invoiceId){
        invoiceDao = MiruApp.getDaoSession().getInvoiceDao();
        invoiceQuery = invoiceDao
                .queryBuilder()
                .where(InvoiceDao.Properties.InvoiceId.eq(invoiceId))
                .orderAsc(InvoiceDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        invoiceList = invoiceQuery.list();
        for (Invoice invoice : invoiceList){
            mInvoiceTotal = invoice.getFinalAmount();
            paymentCustomerName.setText(invoice.getSalesName());
            paymentInvoiceNo.setText(invoice.getExtInvoiceId());
            paymentInvoiceDate.setText(DateTimeUtil.getConvertedFormat(
                    invoice.getInvoiceDate(),
                    "dd-MM-yyyy",
                    "dd MMM yyyy"));
            paymentAmount.setText(FormatNumber.getFormatCurrencyIDR(invoice.getFinalAmount()));
            paymentKredit.setText(FormatNumber.getFormatCurrencyIDR(invoice.getRemainingAmount()));
            lyInvoiceDetail.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        showDiscardDialog();
    }
}
