package com.miru.miruApps.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabWidget;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.data.preference.SPNotification;
import com.miru.miruApps.eventbus.EventRemoveNotification;
import com.miru.miruApps.eventbus.EventShowNotification;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.main.HomeFragment;
import com.miru.miruApps.ui.fragment.main.NotificationFragment;
import com.miru.miruApps.ui.fragment.main.ProfileFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.miru.miruApps.config.Constants.TAB_HOME;
import static com.miru.miruApps.config.Constants.TAB_NOTIF;
import static com.miru.miruApps.config.Constants.TAB_PROFILE;

public class MiruMainActivity extends BaseActivity {
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @BindView(android.R.id.tabs) TabWidget tabs;
    @BindView(android.R.id.tabcontent) FrameLayout tabcontent;
    @BindView(android.R.id.tabhost) FragmentTabHost tabhost;

    View notificationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_main);
        ButterKnife.bind(this);

        initContent();

        EventBus.getDefault().post(new EventShowNotification(new SPNotification().getKeyCounter(this)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAndRequestPermissions(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(EventShowNotification eventNotifCounter) {
        notificationView.setVisibility(View.VISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(EventRemoveNotification eventClearNotifCounter) {
        if (eventClearNotifCounter.isRead()) {
            notificationView.setVisibility(View.GONE);
        }
    }

    private void initContent() {
        tabhost.setup(this, getBaseFragmentManager(), android.R.id.tabcontent);
        tabhost.getTabWidget().setDividerDrawable(null);
        tabhost.addTab(setIndicator(this, tabhost.newTabSpec(TAB_HOME),
                getResources().getString(R.string.tab_title_home), R.drawable.ic_action_selector_home),
                HomeFragment.class, null);
        tabhost.addTab(setIndicator(this, tabhost.newTabSpec(TAB_NOTIF),
                getResources().getString(R.string.tab_title_notification), R.drawable.ic_action_selector_notif),
                NotificationFragment.class, null);
        tabhost.addTab(setIndicator(this, tabhost.newTabSpec(TAB_PROFILE),
                getResources().getString(R.string.tab_title_profile), R.drawable.ic_action_selector_profile),
                ProfileFragment.class, null);

        tabhost.setOnTabChangedListener(tabId -> {
            if (tabId.equalsIgnoreCase(TAB_NOTIF)) {
                new SPNotification().clearSession(getApplicationContext());
                EventBus.getDefault().post(new EventRemoveNotification(true));
            }
        });
    }

    private FragmentTabHost.TabSpec setIndicator(Context context,
                                                 FragmentTabHost.TabSpec tabSpec, String tabMenuTitle, int tabMenuIcon) {
        View v = LayoutInflater.from(context).inflate(R.layout.tab_item, null);

        TextView titleTabItem = v.findViewById(R.id.title_tabitem);
        ImageView imageTabItem = v.findViewById(R.id.img_tabitem);
        notificationView = v.findViewById(R.id.notification_counter);

        titleTabItem.setText(tabMenuTitle);
        imageTabItem.setImageResource(tabMenuIcon);

        if (tabSpec.getTag().equalsIgnoreCase(TAB_NOTIF)) {
            if (new SPNotification().getKeyCounter(this) == 0){
                notificationView.setVisibility(View.GONE);
            } else {
                notificationView.setVisibility(View.VISIBLE);
            }
        } else {
            notificationView.setVisibility(View.GONE);
        }

        return tabSpec.setIndicator(v);
    }

    @Override
    public void onBackPressed() {
        //get current tab index.
        int index = tabhost.getCurrentTab();
        //decide what to do
        if (index != 0)
            tabhost.setCurrentTab(0);
        else
            super.onBackPressed();
    }

    private static void checkAndRequestPermissions(Activity activity) {
        int camera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }

}
