package com.miru.miruApps.ui.fragment.product;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.AddProductAdapter;
import com.miru.miruApps.data.db.Product;
import com.miru.miruApps.data.db.ProductDao;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.EventProduct;
import com.miru.miruApps.model.ProductPrice;
import com.miru.miruApps.model.response.AddProduct;
import com.miru.miruApps.model.response.DetailProduct;
import com.miru.miruApps.model.response.EditProduct;
import com.miru.miruApps.model.retrieve.RetrieveProductData;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.ui.fragment.customer.EditCustInfoFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.view.NumberTextWatcher;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class EditProductPriceFragment extends BaseFragment {

    static final String TAG = EditProductPriceFragment.class.getCanonicalName();

    @Inject MasterApiService mMasterApiService;
    @BindView(R.id.lv_content) RecyclerView lvContent;
    private RetrieveProductData mRetrieveProductData;
    private AddProductAdapter mAddProductAdapter;
    private List<ProductPrice> productPrices = new ArrayList<>();

    List<String> measurementUnits = new ArrayList<>();
    List<String> pricePerUnits = new ArrayList<>();
    List<String> sellingPrices = new ArrayList<>();
    List<String> discounts = new ArrayList<>();
    String measurementUnit;
    String pricePerUnit;
    String sellingPrice;
    String discount;

    private ProductDao productDao;

    public static EditProductPriceFragment newInstance(DetailProduct detailProduct) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("detail_product", detailProduct);
        EditProductPriceFragment editProductPriceFragment = new EditProductPriceFragment();
        editProductPriceFragment.setArguments(bundle);
        return editProductPriceFragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        productDao = MiruApp.getDaoSession().getProductDao();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.action_save)
    void onActionSave(){
        if (measurementUnits.size() == 0 || pricePerUnits.size() == 0 || sellingPrices.size() == 0){
            Toast.makeText(mContext, "Tidak ada harga produk yang ditambahkan, silahkan tambahkan harga produk",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            measurementUnit = getJoinedString(measurementUnits);
            pricePerUnit = getJoinedString(pricePerUnits);
            sellingPrice = getJoinedString(sellingPrices);
            discount = getJoinedString(discounts);
            Log.e("Measurement Unit", measurementUnit);
            Log.e("Price Unit", pricePerUnit);
            Log.e("Selling Price", sellingPrice);
            Log.e("Discount", discount);
        } catch (Exception e){

        }

        updateProduct(
                getJoinedString(measurementUnits),
                getJoinedString(pricePerUnits),
                getJoinedString(sellingPrices),
                getJoinedString(discounts),
                getArguments().getParcelable("detail_product"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_create_price, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAdapter();
        setupViewsListProduct(getArguments().getParcelable("detail_product"));
    }

    private void setupAdapter() {
        mAddProductAdapter = new AddProductAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_solid));
        lvContent.addItemDecoration(itemDecorator);
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAddProductAdapter);

        mAddProductAdapter.setOnAddProductListener(new AddProductAdapter.OnAddProductListener() {
            @Override
            public void onAddProduct() {
                showDialogAddProduct();
            }

            @Override
            public void onDeleteProduct(int position) {
                removeCollection(position);
            }
        });

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(EventProduct eventProduct) {
        this.mRetrieveProductData = eventProduct.getRetrieveProductData();
        Log.e(TAG, "Implementation CreateProductPriceFragment Running...");
        Log.e("Images", mRetrieveProductData.getImages().size() + "");
    }

    private RequestBody createRequestBody(String mValue) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), mValue);
    }

    private MultipartBody.Part prepareFilePart(String partName, File documentFile) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(
                MediaType.parse("multipart/form-data"), documentFile);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, documentFile.getName(), requestFile);
    }

    private void updateProduct(String measurementUnit, String pricePerUnit, String sellingPrice, String discount, DetailProduct detailProduct) {
        MaterialDialog submitDialog = new MaterialDialog.Builder(mContext)
                .content("Submitting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        MultipartBody.Part[] imagePart = null;

        if (mRetrieveProductData.getImages().size() != 0) {
            imagePart = new MultipartBody.Part[mRetrieveProductData.getImages().size()];
            for (int index = 0; index < mRetrieveProductData.getImages().size(); index++) {
                Log.e("Part", "requestUploadImage: imageLinks " + index + "  " + mRetrieveProductData.getImages().get(index).toString());
                imagePart[index] = prepareFilePart("imageLinks", mRetrieveProductData.getImages().get(index));
            }
        }

        RequestBody requestProductId = createRequestBody(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductId());
        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(mContext));
        RequestBody requestItemSku = createRequestBody(mRetrieveProductData.getProductSku());
        RequestBody requestProductName = createRequestBody(mRetrieveProductData.getProductName());
        RequestBody requestCategory = createRequestBody(mRetrieveProductData.getProductKategory());
        RequestBody requestMeasurementUnit = createRequestBody(measurementUnit);
        RequestBody requestPricePerUnit = createRequestBody(pricePerUnit);
        RequestBody requestSellingPrice = createRequestBody(sellingPrice);
        RequestBody requestDiscount = createRequestBody(discount);
        RequestBody requestBrand = createRequestBody(mRetrieveProductData.getProductBrand());
        RequestBody requestSize = createRequestBody(mRetrieveProductData.getProductSize());
        RequestBody requestDescription = createRequestBody(mRetrieveProductData.getProductDescription());

        Observable<EditProduct> createCustomerObservable = mMasterApiService.updateProduct(
                requestProductId,
                requestClientId,
                requestItemSku,
                requestProductName,
                requestCategory,
                requestMeasurementUnit,
                requestPricePerUnit,
                requestSellingPrice,
                requestDiscount,
                requestBrand,
                requestSize,
                requestDescription,
                imagePart
        );
        createCustomerObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<EditProduct>() {
                    @Override
                    public void onCompleted() {
                        submitDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        submitDialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(EditProduct editProduct) {
                        if (editProduct.getStatusCode() == STATUS_SUCCESS) {
                            showDialogAlert();
                        }
                    }
                });
    }

    private void showDialogAlert() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText("Data Produk Berhasil Diperbaharui");

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            getActivity().finish();
        });
        dialog.show();
    }

    private void showDialogAddProduct() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_product_price);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        EditText inputProductUnit = dialog.findViewById(R.id.input_product_unit);
        EditText inputProductBasicPrice = dialog.findViewById(R.id.input_product_basic_price);
        EditText inputProductSellingPrice = dialog.findViewById(R.id.input_product_selling_price);
        EditText inputProductDiscount = dialog.findViewById(R.id.input_product_discount);
        Button actionAddProduct = dialog.findViewById(R.id.action_add_product);
        ImageButton actionDelete = dialog.findViewById(R.id.action_delete);

        inputProductBasicPrice.addTextChangedListener(new NumberTextWatcher(inputProductBasicPrice, "#,###"));
        inputProductSellingPrice.addTextChangedListener(new NumberTextWatcher(inputProductSellingPrice, "#,###"));

        actionDelete.setOnClickListener(view -> dialog.dismiss());
        actionAddProduct.setOnClickListener(view -> {
            String basicPrice = FormatNumber.removeFromatCurrencyIDR(inputProductBasicPrice.getText().toString());
            String sellingPrice = FormatNumber.removeFromatCurrencyIDR(inputProductSellingPrice.getText().toString());

            measurementUnits.add(inputProductUnit.getText().toString());
            pricePerUnits.add(basicPrice);
            sellingPrices.add(sellingPrice);
            discounts.add(inputProductDiscount.getText().toString());

            productPrices.add(new ProductPrice(
                    inputProductUnit.getText().toString(),
                    basicPrice,
                    sellingPrice,
                    inputProductDiscount.getText().toString()
            ));
            Log.e("Product", productPrices.toString());
            mAddProductAdapter.pushData(productPrices);
        });
        dialog.show();
    }

    private void removeCollection(int position){
        productPrices.remove(position);
        measurementUnits.remove(position);
        pricePerUnits.remove(position);
        sellingPrices.remove(position);
        discounts.remove(position);
    }

    private void setupViewsListProduct(DetailProduct detailProduct){
        for (int i = 0; i < detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().size(); i++){
            String productUnit = detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getIdentifierId().getMeasurementUnit();
            String pricePerUnit = String.valueOf(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getPricePerUnit());
            String sellingPrice = String.valueOf(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getSellingPrice());
            String productDiscount = String.valueOf(detailProduct.getProductContainer().get(DEFAULT_SIZE).getProductPrices().get(i).getDiscount());

            measurementUnits.add(productUnit);
            pricePerUnits.add(pricePerUnit);
            sellingPrices.add(sellingPrice);
            discounts.add(productDiscount);

            productPrices.add(new ProductPrice(
                    productUnit,
                    pricePerUnit,
                    sellingPrice,
                    productDiscount
            ));
            Log.e("Product", productPrices.toString());
            mAddProductAdapter.pushData(productPrices);
        }

    }
}
