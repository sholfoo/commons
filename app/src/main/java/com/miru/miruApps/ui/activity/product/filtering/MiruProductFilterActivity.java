package com.miru.miruApps.ui.activity.product.filtering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.guilhe.views.SeekBarRangedView;
import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.FormatNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_BRAND;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_DISCOUNT;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_MAX;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_MIN;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_QTY;

public class MiruProductFilterActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.action_apply) Button actionApply;
    @BindView(R.id.range_price) SeekBarRangedView rangePrice;
    @BindView(R.id.min_price) EditText minPrice;
    @BindView(R.id.max_price) EditText maxPrice;

    @OnClick(R.id.action_home)
    void onActionHome(){
        finish();
    }

    @OnClick(R.id.action_apply)
    void onActionApply(){
        Intent data = new Intent();

        // Add the required data to be returned to the MainActivity
        data.putExtra(EXTRA_PRODUCT_BRAND, "EXTRA_PRODUCT_BRAND");
        data.putExtra(EXTRA_PRODUCT_DISCOUNT, "EXTRA_PRODUCT_DISCOUNT");
        data.putExtra(EXTRA_PRODUCT_MIN, "EXTRA_PRODUCT_MIN");
        data.putExtra(EXTRA_PRODUCT_MAX, "EXTRA_PRODUCT_MAX");
        data.putExtra(EXTRA_PRODUCT_QTY, "EXTRA_PRODUCT_QTY");

        // Set the resultCode to Activity.RESULT_OK to
        // indicate a success and attach the Intent
        // which contains our result data
        setResult(Activity.RESULT_OK, data);
        finish();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_filter);
        ButterKnife.bind(this);

        setupActionBar();
        setupViews();
    }

    private void setupActionBar(){
        actionHome.setImageResource(R.drawable.ic_action_bar_close);
        toolbarLogo.setText(getResources().getString(R.string.action_bar_filter));
    }

    private void setupViews(){
        minPrice.setText(FormatNumber.getFormatCurrencyIDR(Math.round(rangePrice.getSelectedMinValue())));
        maxPrice.setText(FormatNumber.getFormatCurrencyIDR(Math.round(rangePrice.getSelectedMaxValue())));

        rangePrice.setOnSeekBarRangedChangeListener(new SeekBarRangedView.OnSeekBarRangedChangeListener() {
            @Override
            public void onChanged(SeekBarRangedView view, float minValue, float maxValue) {
                minPrice.setText(FormatNumber.getFormatCurrencyIDR(Math.round(minValue)));
                maxPrice.setText(FormatNumber.getFormatCurrencyIDR(Math.round(maxValue)));
            }

            @Override
            public void onChanging(SeekBarRangedView view, float minValue, float maxValue) {
                minPrice.setText(FormatNumber.getFormatCurrencyIDR(Math.round(minValue)));
                maxPrice.setText(FormatNumber.getFormatCurrencyIDR(Math.round(maxValue)));
            }
        });
    }
}
