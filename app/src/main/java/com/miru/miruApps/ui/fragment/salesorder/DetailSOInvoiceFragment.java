package com.miru.miruApps.ui.fragment.salesorder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.InvoiceAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.activity.invoice.MiruMainCreateInvoiceActivity;
import com.miru.miruApps.ui.base.BaseFragment;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class DetailSOInvoiceFragment extends BaseFragment {

    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoDataTitle) TextView txtNoDataTitle;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.ly_error) LinearLayout lyError;

    @Inject InvoiceApiService mApiService;
    private InvoiceAdapter mAdapter;

    public static DetailSOInvoiceFragment newInstance(String salesOrderId, String extSalesOrderId) {
        Bundle bundle = new Bundle();
        bundle.putString("salesOrderId", salesOrderId);
        bundle.putString("extSalesOrderId", extSalesOrderId);
        DetailSOInvoiceFragment detailSOInvoiceFragment = new DetailSOInvoiceFragment();
        detailSOInvoiceFragment.setArguments(bundle);
        return detailSOInvoiceFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_so_detail_invoice, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new InvoiceAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);

        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(this::initializeInvoices);

        initializeInvoices();
    }

    private String getArgumentExtraSalesOrderId() {
        return getArguments() != null ? getArguments().getString("salesOrderId") : null;
    }

    private String getArgumentExtraExtSalesOrderId() {
        return getArguments() != null ? getArguments().getString("extSalesOrderId") : null;
    }

    private void initializeInvoices() {
        pbLoad.setVisibility(View.VISIBLE);
        Observable<Invoice> invoiceObservable = mApiService.getInvoiceSalesOrder(
                new SPUser().getKeyUserClientId(mContext),
                9,
                getArgumentExtraExtSalesOrderId());

        invoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Invoice>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Invoice invoice) {
                        if (invoice.getStatusCode() == STATUS_SUCCESS) {
                            Log.e("Size", String.valueOf(invoice.getInvoiceContainer().size()));
                            mAdapter.pushData(invoice.getInvoiceContainer());
                            resetUILoading();
                        } else {
                            showErrorView();
                        }
                    }
                });
    }

    protected void resetUILoading() {
        hideErrorView();
        if (isAdded()){
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
        }
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        if (isAdded()){
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
            lyError.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(v -> {
                hideErrorView();
                Intent intentCreateInvoice = new Intent(getActivity(), MiruMainCreateInvoiceActivity.class);
                intentCreateInvoice.putExtra("salesOrderId", getArgumentExtraSalesOrderId());
                intentCreateInvoice.putExtra("extSalesOrderId", getArgumentExtraExtSalesOrderId());
                startActivity(intentCreateInvoice);
            });

            if (!TextUtils.isEmpty(text))
                txtNoDataTitle.setText(getString(R.string.error_no_invoice_data));
        }
    }

    protected void hideErrorView() {
        if (isAdded()){
            lyError.setVisibility(View.GONE);
        }
    }
}
