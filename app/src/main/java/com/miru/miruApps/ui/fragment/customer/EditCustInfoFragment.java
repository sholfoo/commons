package com.miru.miruApps.ui.fragment.customer;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.AddImageAdapter;
import com.miru.miruApps.model.response.DetailCustomer;
import com.miru.miruApps.model.retrieve.RetrieveCustomerData;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.view.NumberTextWatcher;
import com.miru.miruApps.view.TagEditText;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;

public class EditCustInfoFragment extends BaseFragment {

    @BindView(R.id.input_layout_first_name) TextInputLayout inputLayoutFirstName;
    @BindView(R.id.input_layout_last_name) TextInputLayout inputLayoutLastName;
    @BindView(R.id.input_layout_store_name) TextInputLayout inputLayoutStoreName;
    @BindView(R.id.input_layout_phone_number) TextInputLayout inputLayoutPhoneNumber;
    @BindView(R.id.input_layout_mobile_number) TextInputLayout inputLayoutMobileNumber;
    @BindView(R.id.input_layout_email) TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_layout_customer_category) TextInputLayout inputLayoutCustomerCategory;
    @BindView(R.id.input_layout_description) TextInputLayout inputLayoutDescription;
    @BindView(R.id.input_layout_credit_limit) TextInputLayout inputLayoutCreditLimit;
    @BindView(R.id.input_first_name) EditText inputFirstName;
    @BindView(R.id.input_last_name) EditText inputLastName;
    @BindView(R.id.input_store_name) EditText inputStoreName;
    @BindView(R.id.input_phone_number) EditText inputPhoneNumber;
    @BindView(R.id.input_mobile_number) EditText inputMobileNumber;
    @BindView(R.id.input_email) EditText inputEmail;
    @BindView(R.id.input_category) EditText inputCategory;
    @BindView(R.id.input_credit_limit) EditText inputCreditLimit;
    @BindView(R.id.input_tags) TagEditText inputTags;
    @BindView(R.id.input_description) EditText inputDescription;
    @BindView(R.id.input_status) Switch inputStatus;
    @BindView(R.id.status_info) TextView statusInfo;
    @BindView(R.id.action_next) Button actionNext;
    @BindView(R.id.lv_content) RecyclerView lvContent;

    protected OnNextListener mOnNextListener;
    private AddImageAdapter mAdapter;

    private List<String> imageString = new ArrayList<>();
    private ArrayList<File> images = new ArrayList<>();
    private String documentPath;
    private File documentFile = null;
    private File compressedDocumentFile = null;

    @OnClick(R.id.action_next)
    void onActionNext() {
        if (!validateInput(inputLayoutFirstName, inputFirstName))
            return;

        if (!validateInput(inputLayoutStoreName, inputStoreName))
            return;

        attemptNext();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnNextListener = (OnNextListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public static EditCustInfoFragment newInstance(DetailCustomer detailCustomer) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("detail_customer", detailCustomer);
        EditCustInfoFragment editCustInfoFragment = new EditCustInfoFragment();
        editCustInfoFragment.setArguments(bundle);
        return editCustInfoFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_create_information, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAdapter();
        setupViews(getArguments().getParcelable("detail_customer"));
    }

    private void setupAdapter() {
        mAdapter = new AddImageAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_transparent));
        lvContent.addItemDecoration(itemDecorator);
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);

        mAdapter.setOnInsertListener(() -> {
            Log.e("OnClicked", "running....");
            showDialogInsertImage();
        });

    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    private void pickImage() {
        ImagePicker.create(this) // Activity or Fragment
                .folderMode(true)
                .showCamera(false)
                .single()
                .enableLog(false)
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            com.esafirm.imagepicker.model.Image image = ImagePicker.getFirstImageOrNull(data);
            documentPath = String.valueOf(image.getPath());
            documentFile = new File(documentPath);
            compressedDocumentFile = scaleImage(documentPath);
            Log.d("PHOTO PATH", documentPath);

            imageString.add(documentPath);
            images.add(compressedDocumentFile);

            Log.e("ImgString", imageString.toString());
            Log.e("Images", images.toString());
            mAdapter.pushData(imageString);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void attemptNext() {
        String ownerName = inputFirstName.getText().toString() + " " + inputLastName.getText().toString();
        String storeName = inputStoreName.getText().toString();
        String phoneNumber = inputPhoneNumber.getText().toString();
        String mobileNumber = inputMobileNumber.getText().toString();
        String email = inputEmail.getText().toString();
        String customerType = inputCategory.getText().toString();
        String status = inputStatus.isChecked() ? "active" : "deactive";
        String description = inputDescription.getText().toString();
        String creditLimit = FormatNumber.removeFromatCurrencyIDR(inputCreditLimit.getText().toString());

        Log.e("Credit Limit", creditLimit);
        Log.e("Status", status);
        String tags = inputTags.getText().toString();
        try {
            tags = tags.replace(" ", ",");
            Log.e("Tags", tags);
        }catch (Exception e){
            tags = null;
        }

        mOnNextListener.onActionNext(new RetrieveCustomerData(
                ownerName,
                storeName,
                phoneNumber,
                mobileNumber,
                email,
                customerType,
                status,
                description,
                tags,
                creditLimit,
                images
        ));
    }

    public interface OnNextListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onActionNext(RetrieveCustomerData retrieveCustomerData);
    }

    private void showDialogInsertImage() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_image);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        LinearLayout takePicture = dialog.findViewById(R.id.action_take_picture);
        LinearLayout pickImage = dialog.findViewById(R.id.action_pick_image);

        takePicture.setOnClickListener(view -> {
            captureImage();
            dialog.dismiss();
        });

        pickImage.setOnClickListener(view -> {
            pickImage();
            dialog.dismiss();
        });
        dialog.show();
    }

    private void setupViews(DetailCustomer detailCustomer){
        for (String imageStr : detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getImageLink()){
            imageString.add(imageStr);
        }

        try {
            String mFullName = detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getOwnerName();
            String[] splitStr = StringUtils.split(mFullName);
            String mFirstName = splitStr[0];
            String mLastName = splitStr[1];
            inputFirstName.setText(mFirstName);
            inputLastName.setText(mLastName);
        }catch (Exception e){
            inputFirstName.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getOwnerName());
        }
        inputStoreName.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getStoreName());
        inputMobileNumber.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getMobilePhone());
        inputEmail.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getEmailAddress());
        inputCategory.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getCustomerType());
        if (detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getCreditLimit().contains("Rp ")){
            String creditLimit = FormatNumber.removeFromatCurrencyIDR(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getCreditLimit());
            inputCreditLimit.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(creditLimit)));
        } else {
            inputCreditLimit.setText(FormatNumber.getFormatCurrencyIDR(Integer.parseInt(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getCreditLimit())));
        }

        String tags = "";
        for (String s : detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getCustomerTag()){
            tags += s + " ";
        }
        inputTags.setText(tags);
        inputDescription.setText(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getDescription());

        inputStatus.setChecked(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getStatus().equals("active"));
        inputStatus.setOnCheckedChangeListener((compoundButton, b) -> statusInfo.setText(b ? getResources().getString(R.string.action_active) : getResources().getString(R.string.action_deactive)));
        inputCreditLimit.addTextChangedListener(new NumberTextWatcher(inputCreditLimit, "#,###"));
        mAdapter.pushData(detailCustomer.getCustomerContainer().get(DEFAULT_SIZE).getImageLink());
    }
}
