package com.miru.miruApps.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.config.Constants;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.request.UpdateUserBody;
import com.miru.miruApps.model.response.ChangePassword;
import com.miru.miruApps.model.response.UserInfo;
import com.miru.miruApps.model.response.UserInfoEdit;
import com.miru.miruApps.network.ApiService;
import com.miru.miruApps.ui.base.BaseActivity;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MiruUserInfoActivity extends BaseActivity {

    private static final int STORE_LOGO_REQUEST = 200;
    private static final int PROFILE_PIC_REQUEST = 300;
    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.input_first_name) EditText inputFirstName;
    @BindView(R.id.input_layout_first_name) TextInputLayout inputLayoutFirstName;
    @BindView(R.id.input_last_name) EditText inputLastName;
    @BindView(R.id.input_layout_last_name) TextInputLayout inputLayoutLastName;
    @BindView(R.id.input_store_name) EditText inputStoreName;
    @BindView(R.id.input_layout_store_name) TextInputLayout inputLayoutStoreName;
    @BindView(R.id.input_email) EditText inputEmail;
    @BindView(R.id.input_layout_email) TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_area) EditText inputArea;
    @BindView(R.id.input_phone_number) EditText inputPhoneNumber;
    @BindView(R.id.action_add_logo) ImageView actionAddLogo;
    @BindView(R.id.action_add_profile_pic) ImageView actionAddProfilePict;
    @BindView(R.id.action_save) Button actionSave;

    @Inject ApiService mApiService;

    private String storeLogoPath;
    private String profilePicPath;
    private File storeLogoFile = null;
    private File profilePicFile = null;
    private File compressedStoreLogo = null;
    private File compressedProfilePic = null;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_add_logo)
    void onActionAddLogo(){
        showDialogInsertImage(STORE_LOGO_REQUEST);
    }

    @OnClick(R.id.action_add_profile_pic)
    void onActionAddProfilePicture(){
        showDialogInsertImage(PROFILE_PIC_REQUEST);
    }

    @OnClick(R.id.action_save)
    void onActionSave(){
        inputFirstName.setError(null);
        inputLastName.setError(null);
        inputStoreName.setError(null);
        inputEmail.setError(null);
        inputPhoneNumber.setError(null);

        // Store values at the time of the login attempt.
        String fullName = inputFirstName.getText().toString() + " " + inputLastName.getText().toString();
        String storeName = inputStoreName.getText().toString();
        String email = inputEmail.getText().toString();
        String phoneNumber = "0" + inputPhoneNumber.getText().toString();

        if (!validateInput(inputLayoutFirstName, inputFirstName))
            return;
        if (!validateInput(inputLayoutLastName, inputLastName))
            return;
        if (!validateInput(inputLayoutStoreName, inputStoreName))
            return;
        if (!validateInput(inputLayoutEmail, inputEmail))
            return;
        if (!validateInput(inputPhoneNumber))
            return;

        if (isNetworkAvailable()) {
            updateUserProfile(new UpdateUserBody(
                    new SPUser().getKeyUserClientId(this),
                    new SPUser().getKeyUserId(this),
                    storeName,
                    new SPUser().getKeyUserName(this),
                    fullName,
                    email,
                    phoneNumber
            ));
        } else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.error_no_internet_connection),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_user_information);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setSupportActionBar(lyToolbar);
        resetFieldView(false);
        setupActionBar();
        initUserInformation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_user_info_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_user_info:
                resetFieldView(true);
                return true;
            case R.id.action_change_password:
                showDialogChangePassword();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_user_information);
    }

    private void initUserInformation() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        Observable<UserInfo> userInfoObservable = mApiService.getUserInformation(
                new SPUser().getKeyUserClientId(getApplicationContext()),
                new SPUser().getKeyUserId(getApplicationContext())
        );

        userInfoObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<UserInfo>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(UserInfo userInfo) {
                        setupViews(userInfo);
                    }
                });
    }

    private void changePassword(String password) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(this));
        RequestBody requestUserId = createRequestBody(new SPUser().getKeyUserId(this));
        RequestBody requestPassword = createRequestBody(password);

        Observable<ChangePassword> changePasswordObservable = mApiService.changePassword(
                requestClientId,
                requestUserId,
                requestPassword
        );

        changePasswordObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ChangePassword>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(ChangePassword changePassword) {
                        dialog.dismiss();
                        showPasswordChangedDialog(MiruUserInfoActivity.this);
                    }
                });
    }

    private void updateUserProfile(UpdateUserBody updateUserBody) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Updating data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestClientId = createRequestBody(updateUserBody.getClientId());
        RequestBody requestUserId = createRequestBody(updateUserBody.getUserId());
        RequestBody requestClientName = createRequestBody(updateUserBody.getClientName());
        RequestBody requestUserName = createRequestBody(updateUserBody.getUserName());
        RequestBody requestFullName = createRequestBody(updateUserBody.getFullName());
        RequestBody requestEmailAddress = createRequestBody(updateUserBody.getEmailAddress());
        RequestBody requestMobileNumber = createRequestBody(updateUserBody.getMobileNumber());

        MultipartBody.Part storeLogoPart = null;
        MultipartBody.Part profilePicPart = null;
        if (compressedStoreLogo != null) {
            storeLogoPart = prepareFilePart("storeLogo", compressedStoreLogo);
        }

        if (compressedProfilePic != null) {
            profilePicPart = prepareFilePart("profilePic", compressedProfilePic);
        }

        Observable<UserInfoEdit> userInfoEditObservable = mApiService.editProfile(
                requestClientId,
                requestUserId,
                requestClientName,
                requestUserName,
                requestFullName,
                requestEmailAddress,
                requestMobileNumber,
                storeLogoPart,
                profilePicPart
        );

        userInfoEditObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<UserInfoEdit>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(UserInfoEdit userInfoEdit) {
                        dialog.dismiss();
                        if (userInfoEdit.getStatusCode() == Constants.STATUS_SUCCESS){
                            showDialogAlert(userInfoEdit);
                        }
                    }
                });
    }

    private void updateViewAndUserSharedPreference(UserInfoEdit userInfoEdit){
        String mFullName, mFirstName, mLastName, mPhoneNumber;
        mFullName = userInfoEdit.getFullName();
        String[] splitStr = StringUtils.split(mFullName);
        mFirstName = splitStr[0];
        mLastName = splitStr[1];

        mPhoneNumber = userInfoEdit.getMobileNumber().substring(1, userInfoEdit.getMobileNumber().trim().length());

        new SPUser().setKeyUserFullName(this, userInfoEdit.getFullName());
        new SPUser().setKeyUserStore(this, userInfoEdit.getClientName());
        new SPUser().setKeyUserPhone(this, userInfoEdit.getMobileNumber());
        new SPUser().setKeyUserProfile(this, userInfoEdit.getProfilePicture());
        new SPUser().setKeyUserStoreLogo(this, userInfoEdit.getClientLogo());

        inputFirstName.setText(mFirstName);
        inputLastName.setText(mLastName);
        inputStoreName.setText(userInfoEdit.getClientName());
        inputArea.setText("+62");
        inputPhoneNumber.setText(mPhoneNumber);

        Glide.with(this)
                .load(userInfoEdit.getClientLogo())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(actionAddLogo);

        Glide.with(this)
                .load(userInfoEdit.getProfilePicture())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(actionAddProfilePict);
    }

    private void setupViews(UserInfo userInfo) {
        new SPUser().setKeyUserId(this, userInfo.getUserId());
        new SPUser().setKeyUserClientId(this, userInfo.getClientId());
        new SPUser().setKeyUserFullName(this, userInfo.getFullName());
        new SPUser().setKeyUserStore(this, userInfo.getStoreName());
        new SPUser().setKeyUserPhone(this, userInfo.getMobilePhone());
        new SPUser().setKeyUserPhone(this, userInfo.getMobilePhone());
        new SPUser().setKeyUserFullName(this, userInfo.getFullName());
        new SPUser().setKeyUserStoreLogo(this, userInfo.getClientLogoUrl());
        new SPUser().setKeyUserProfile(this, userInfo.getUserProfileImage());

        String mFullName, mFirstName, mLastName, mPhoneNumber;
        mFullName = userInfo.getFullName();
        String[] splitStr = StringUtils.split(mFullName);
        mFirstName = splitStr[0];
        mLastName = splitStr[1];

        mPhoneNumber = userInfo.getMobilePhone().substring(1, userInfo.getMobilePhone().trim().length());

        inputFirstName.setText(mFirstName);
        inputLastName.setText(mLastName);
        inputStoreName.setText(userInfo.getStoreName());
        inputEmail.setText(userInfo.getEmailAddress());
        inputArea.setText("+62");
        inputPhoneNumber.setText(mPhoneNumber);

        Glide.with(this)
                .load(userInfo.getClientLogoUrl())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(actionAddLogo);

        Glide.with(this)
                .load(userInfo.getUserProfileImage())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(actionAddProfilePict);
    }

    private void captureImage(int requestCode) {
        ImagePicker.cameraOnly().start(this, requestCode);
    }

    private void pickImage(int requestCode) {
        ImagePicker.create(this) // Activity or Fragment
                .folderMode(true)
                .showCamera(false)
                .single()
                .enableLog(false)
                .start(requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == STORE_LOGO_REQUEST){
            Log.e("REQUEST CODE", String.valueOf(requestCode));
            com.esafirm.imagepicker.model.Image image = ImagePicker.getFirstImageOrNull(data);
            if (image != null) {
                storeLogoPath = String.valueOf(image.getPath());
                storeLogoFile = new File(storeLogoPath);
                compressedStoreLogo = scaleImage(storeLogoPath);
                Log.e("PHOTO PATH", storeLogoPath);

                Glide.with(this)
                        .load(storeLogoFile)
                        .into(actionAddLogo);
            }
        } else {
            com.esafirm.imagepicker.model.Image image = ImagePicker.getFirstImageOrNull(data);
            if (image != null) {
                profilePicPath = String.valueOf(image.getPath());
                profilePicFile = new File(profilePicPath);
                compressedProfilePic = scaleImage(profilePicPath);
                Log.e("PHOTO PATH", profilePicPath);

                Glide.with(this)
                        .load(profilePicFile)
                        .into(actionAddProfilePict);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialogInsertImage(int requestCode) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_image);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        LinearLayout takePicture = dialog.findViewById(R.id.action_take_picture);
        LinearLayout pickImage = dialog.findViewById(R.id.action_pick_image);

        takePicture.setOnClickListener(view -> {
            captureImage(requestCode);
            dialog.dismiss();
        });

        pickImage.setOnClickListener(view -> {
            pickImage(requestCode);
            dialog.dismiss();
        });
        dialog.show();
    }

    private void showDialogChangePassword() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_password);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextInputLayout inputLayoutPasswordConfirmation = dialog.findViewById(R.id.input_layout_password_confirmation);
        EditText inputPassword = dialog.findViewById(R.id.input_password);
        EditText inputPasswordConfirmation = dialog.findViewById(R.id.input_password_confirmation);
        Button actionUpdate = dialog.findViewById(R.id.action_update);
        ImageButton actionClose = dialog.findViewById(R.id.action_close_dialog);

        actionClose.setOnClickListener(view -> dialog.dismiss());

        actionUpdate.setOnClickListener(view -> {
            if (!inputPassword.getText().toString().equalsIgnoreCase(inputPasswordConfirmation.getText().toString())) {
                inputLayoutPasswordConfirmation.setError("Password tidak sesuai. Silahkan cek kembali.");
                requestFocus(inputPasswordConfirmation);
                return;
            } else {
                inputLayoutPasswordConfirmation.setError(null);
                Log.e("CHANGE PASSWORD", "Password validate");
                dialog.dismiss();
                changePassword(inputPassword.getText().toString());
            }
        });

        dialog.show();
    }

    private void showDialogAlert(UserInfoEdit userInfoEdit) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText("Data pengguna telah disimpan dan berhasil diubah");

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            updateViewAndUserSharedPreference(userInfoEdit);
            resetFieldView(false);
        });
        dialog.show();
    }

    private void resetFieldView(boolean enable){
        inputFirstName.setEnabled(enable);
        inputFirstName.setEnabled(enable);
        inputLastName.setEnabled(enable);
        inputStoreName.setEnabled(enable);
        inputArea.setEnabled(enable);
        inputPhoneNumber.setEnabled(enable);

        if (enable)
            actionSave.setVisibility(View.VISIBLE);
        else
            actionSave.setVisibility(View.GONE);
    }
}
