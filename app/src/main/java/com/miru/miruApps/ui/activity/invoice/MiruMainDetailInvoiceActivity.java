package com.miru.miruApps.ui.activity.invoice;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.DeleteInvoice;
import com.miru.miruApps.network.FileDownloadService;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.invoice.DetailInvoiceFragment;
import com.miru.miruApps.ui.fragment.invoice.DetailPaymentFragment;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruMainDetailInvoiceActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) ViewPager htabViewpager;

    @Inject InvoiceApiService mApiService;

    private ViewPagerAdapter mViewPagerAdapter;
    private ShareActionProvider mShareActionProvider;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_invoice_main_detail);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_detail_invoice);

    }

    private void setupViewPager(ViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(DetailInvoiceFragment.newInstance(getExtraInvoiceId(), getExtraExtInvoiceId()),
                getResources().getString(R.string.tab_title_invoice_detail_invoice));
        mViewPagerAdapter.addFragment(DetailPaymentFragment.newInstance(getExtraInvoiceId(), getExtraExtInvoiceId()),
                getResources().getString(R.string.tab_title_invoice_detail_payment));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_invoice_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                Intent intentEdit = new Intent(this, MiruMainEditInvoiceActivity.class);
                intentEdit.putExtra("invoiceId", getExtraExtInvoiceId());
                startActivity(intentEdit);
                return true;
            case R.id.action_download:
                Intent intentDownload = new Intent(Intent.ACTION_VIEW);
                intentDownload.setData(Uri.parse(getExtraDocPathUrl()));
                startActivity(intentDownload);
                return true;
            case R.id.action_share:
                if (checkPermission()){
                    downloadDocumentFile(getExtraInvoiceId(), getExtraDocPathUrl());
                } else {
                    requestPermission();
                }
                return true;
            case R.id.action_delete:
                initializeDeleteInvoice();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private String getExtraClientId() {
        return getIntent().getStringExtra("clientId");
    }

    private String getExtraInvoiceId() {
        Log.e("Invoice ID", getIntent().getStringExtra("invoiceId"));
        return getIntent().getStringExtra("invoiceId");
    }

    private String getExtraExtInvoiceId() {
        Log.e("Ext Invoice ID", getIntent().getStringExtra("extInvoiceId"));
        return getIntent().getStringExtra("extInvoiceId");
    }

    private String getExtraDocPathUrl() {
        return getIntent().getStringExtra("docPathUrl");
    }

    private void initializeDeleteInvoice(){
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Menghapus Data, Mohon menunggu...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(getApplicationContext()));
        RequestBody requestInvoiceId = createRequestBody(getExtraInvoiceId());

        Observable<DeleteInvoice> deleteInvoiceObservable = mApiService.deleteInvoice(
                requestClientId,
                requestInvoiceId);

        deleteInvoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DeleteInvoice>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(DeleteInvoice invoice) {
                        if (invoice.getStatusCode() == STATUS_SUCCESS) {
                            showDialogAlert(R.drawable.ic_success,
                                    "Data Invoice Berhasil Dihapus");
                        } else if (invoice.getStatusCode() == 210){
                            showDialogAlert(R.drawable.ic_highlight_off_black_24dp,
                                    "Invoice ini tidak bisa dihapus karena telah ada Pembayaran yang tercatat");
                        } else {
                            showDialogAlert(R.drawable.ic_highlight_off_black_24dp,
                                    invoice.getErrorMessage().toString());
                        }
                    }
                });
    }

    private void showDialogAlert(int drawable, String stringMessage) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        ImageView alertImage = dialog.findViewById(R.id.alert_image);
        TextView txtMessage = dialog.findViewById(R.id.dialog_message);
        Button actionOk = dialog.findViewById(R.id.action_ok);

        txtMessage.setText(stringMessage);

        Glide.with(getApplicationContext()).load(drawable).into(alertImage);

        actionOk.setOnClickListener(view -> {
            dialog.dismiss();
            finish();
        });
        dialog.show();
    }

}
