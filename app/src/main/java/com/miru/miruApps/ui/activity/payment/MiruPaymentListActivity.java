package com.miru.miruApps.ui.activity.payment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.PaymentAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Payment;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseActivity;

import java.io.IOException;
import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_ERROR;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruPaymentListActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.action_filter) TextView actionFilter;
    @BindView(R.id.action_shorting) TextView actionShorting;

    @Inject InvoiceApiService mApiService;

    private PaymentAdapter mAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_payment_list);
        MiruApp.getmComponent().inject(this);
        ButterKnife.bind(this);

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupAdapter();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_payment);
        Drawable img = getResources().getDrawable(R.drawable.ic_actionbar_payment);
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);
    }

    private void setupAdapter() {
        mAdapter = new PaymentAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mAdapter);
        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> {
            initListPayments();
        });
        initListPayments();
    }

    private void initListPayments() {
        pbLoad.setVisibility(View.VISIBLE);
        Observable<Payment> paymentObservable = mApiService.getPaymentList(
                new SPUser().getKeyUserClientId(this));

        paymentObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Payment>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Payment payment) {
                        if (payment.getStatusCode() == STATUS_SUCCESS) {
                            Collections.reverse(payment.getPayment());
                            mAdapter.pushData(payment.getPayment());
                            resetUILoading();
                        } else if (payment.getStatusCode() == STATUS_ERROR){
                            showErrorView("Belum ada Pembayaran");
                        } else
                            showErrorView();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu_with_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setQueryHint("Cari");
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().isEmpty()){
                    initListPayments();
                }

                mAdapter.getFilter().filter(query);
                searchViewAndroidActionBar.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().isEmpty()){
                    initListPayments();
                }
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_new:
                startActivity(new Intent(this, MiruPaymentCreateActivity.class));
                return true;
            case R.id.action_search:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initListPayments();
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }
}
