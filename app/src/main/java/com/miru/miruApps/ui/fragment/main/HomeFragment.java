package com.miru.miruApps.ui.fragment.main;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.Weather;
import com.miru.miruApps.model.response.SummaryHome;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.network.WeatherApiService;
import com.miru.miruApps.ui.activity.customer.MiruCustomerListActivity;
import com.miru.miruApps.ui.activity.dashboard.MiruDashboardActivity;
import com.miru.miruApps.ui.activity.invoice.MiruInvoiceListActivity;
import com.miru.miruApps.ui.activity.payment.MiruPaymentListActivity;
import com.miru.miruApps.ui.activity.product.MiruProductListActivity;
import com.miru.miruApps.ui.activity.salesorder.MiruSOListActivity;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.utils.TimeMode;

import java.io.IOException;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.user_image) CircleImageView userImage;
    @BindView(R.id.user_greetings) TextView userGreetings;
    @BindView(R.id.ly_home_theme) LinearLayout lyHomeTheme;
    @BindView(R.id.total_invoice) TextView totalInvoice;
    @BindView(R.id.total_so) TextView totalSo;
    @BindView(R.id.total_amount) TextView totalAmount;
    @BindView(R.id.img_time_mode) ImageView imgTimeMode;
    @BindView(R.id.time_date) TextView timeDate;
    @BindView(R.id.time_day) TextView timeDay;
    @BindView(R.id.time_month) TextView timeMonth;
    @BindView(R.id.user_store_name) TextView userStoreName;
    @BindView(R.id.action_menu_dashboard) LinearLayout actionMenuDashboard;
    @BindView(R.id.action_menu_invoice) LinearLayout actionMenuInvoice;
    @BindView(R.id.action_menu_so) LinearLayout actionMenuSo;
    @BindView(R.id.action_menu_payment) LinearLayout actionMenuPayment;
    @BindView(R.id.action_menu_product) LinearLayout actionMenuProduct;
    @BindView(R.id.action_menu_customer) LinearLayout actionMenuCustomer;
    @BindView(R.id.ly_content_menu) LinearLayout lyContentMenu;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;

    @Inject InvoiceApiService mApiService;
    @Inject WeatherApiService mWeatherApiService;

    @OnClick(R.id.action_menu_dashboard)
    void onActionDashboardMenu() {
        startActivity(new Intent(getActivity(), MiruDashboardActivity.class));
    }

    @OnClick(R.id.action_menu_invoice)
    void onActionInvoiceMenu() {
        startActivity(new Intent(getActivity(), MiruInvoiceListActivity.class));
    }

    @OnClick(R.id.action_menu_so)
    void onActionSoMenu() {
        startActivity(new Intent(getActivity(), MiruSOListActivity.class));
    }

    @OnClick(R.id.action_menu_payment)
    void onActionPaymentMenu() {
        startActivity(new Intent(getActivity(), MiruPaymentListActivity.class));
    }

    @OnClick(R.id.action_menu_product)
    void onActionProductMenu() {
        startActivity(new Intent(getActivity(), MiruProductListActivity.class));
    }

    @OnClick(R.id.action_menu_customer)
    void onActionCustomerMenu() {
        startActivity(new Intent(getActivity(), MiruCustomerListActivity.class));
    }

    @OnClick({R.id.action_total_invoice_overdue, R.id.action_nominal_invoice_overdue})
    void onActionTotalInvoiceOverdue() {
        startActivity(new Intent(getActivity(), MiruInvoiceListActivity.class));
    }

    @OnClick(R.id.action_total_so_pending)
    void onActionTotalSoPending() {
        startActivity(new Intent(getActivity(), MiruSOListActivity.class));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_miru_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> {
            initializeSummaries();
        });

        initializeSummaries();
        setupThemeMode();
        setupViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViews();
    }

    private void setupViews() {
        Glide.with(mContext)
                .load(new SPUser().getKeyUserProfile(mContext))
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(userImage);
        userStoreName.setText(new SPUser().getKeyUserStore(mContext));

        timeDate.setText(DateTimeUtil.getCurrentFormat("dd"));
        timeDay.setText(DateTimeUtil.getCurrentFormat("EEE"));
        timeMonth.setText(DateTimeUtil.getCurrentFormat("MMM"));
    }

    private void setupThemeMode() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        int timeModeDrawable = 0;
        if (timeOfDay >= 0 && timeOfDay < 12) {
            userGreetings.setText(String.format("%s%s", TimeMode.MORNING.greeting(),
                    new SPUser().getKeyUserFullName(mContext)));
            lyHomeTheme.setBackgroundResource(
                    R.drawable.bg_theme_mode_day);
            timeModeDrawable = R.drawable.ic_mode_rain;
        } else if (timeOfDay >= 12 && timeOfDay < 15) {
            userGreetings.setText(String.format("%s%s", TimeMode.NOON.greeting(),
                    new SPUser().getKeyUserFullName(mContext)));
            lyHomeTheme.setBackgroundResource(
                    R.drawable.bg_theme_mode_day);
            timeModeDrawable = R.drawable.ic_mode_rain;
        } else if (timeOfDay >= 15 && timeOfDay < 19) {
            userGreetings.setText(String.format("%s%s", TimeMode.AFTERNOON.greeting(),
                    new SPUser().getKeyUserFullName(mContext)));
            lyHomeTheme.setBackgroundResource(
                    R.drawable.bg_theme_mode_night);
            timeModeDrawable = R.drawable.ic_mode_night;
        } else if (timeOfDay >= 19 && timeOfDay < 24) {
            userGreetings.setText(String.format("%s%s", TimeMode.NIGHT.greeting(),
                    new SPUser().getKeyUserFullName(mContext)));
            lyHomeTheme.setBackgroundResource(
                    R.drawable.bg_theme_mode_night);
            timeModeDrawable = R.drawable.ic_mode_night;
        }

        Glide.with(mContext)
                .load(timeModeDrawable)
                .into(imgTimeMode);
    }

    private void initializeSummaries() {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<SummaryHome> summaryHomeObservable = mApiService.getHomeSummary(
                new SPUser().getKeyUserClientId(mContext)
        );

        summaryHomeObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SummaryHome>() {
                    @Override
                    public void onCompleted() {
                        getCurrentWeather(dialog);
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof Exception) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(SummaryHome summaryHome) {
                        totalInvoice.setText(String.format("%s Invoice",
                                String.valueOf(summaryHome.getOverDueInvoices().getNumOfInvoices())));
                        totalSo.setText(String.format("%s Pemesanan",
                                String.valueOf(summaryHome.getPendingOrders().getNumOfOrders())));
                        totalAmount.setText(FormatNumber.getFormatCurrencyIDR(
                                Integer.parseInt(summaryHome.getOverDueInvoices().getSumOfInvoices())));
                    }
                });
    }

    private void getCurrentWeather(Dialog dialog) {
        Observable<Weather> weatherObservable = mWeatherApiService.getCurrentWeather(
                "-6.3412345",
                "106.841618",
                getString(R.string.open_weather_maps_app_id)
        );

        weatherObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Weather>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                        lyRefresh.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof Exception) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(Weather weather) {
                        Log.e("Weather", weather.toString());
                        setWeatherIcon(weather.getWeather().get(DEFAULT_SIZE).getId());
                    }
                });
    }

    private void setWeatherIcon(int actualId) {
        int id = actualId / 100;
        int timeModeDrawable = 0;
        if (actualId == 800) {
            Calendar c = Calendar.getInstance();
            int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

            if (timeOfDay >= 0 && timeOfDay < 6) {
                timeModeDrawable = R.drawable.ic_mode_night;
            } else if (timeOfDay >= 6 && timeOfDay < 18) {
                timeModeDrawable = R.drawable.ic_mode_sunny;
            } else if (timeOfDay >= 18 && timeOfDay < 24) {
                timeModeDrawable = R.drawable.ic_mode_night;
            }

        } else {
            switch (id) {
                case 2:
                    // geledek
                    timeModeDrawable = R.drawable.ic_mode_thunder;
                    break;
                case 3:
                    // gerimis
                    timeModeDrawable = R.drawable.ic_mode_rain;
                    break;
                case 7:
                    // berkabut
                    timeModeDrawable = R.drawable.ic_mode_cloudy;
                    break;
                case 8:
                    //berawan
                    timeModeDrawable = R.drawable.ic_mode_cloudy;
                    break;
                case 5:
                    // hujan
                    timeModeDrawable = R.drawable.ic_mode_rain;
                    break;
            }
        }

        Glide.with(mContext)
                .load(timeModeDrawable)
                .into(imgTimeMode);

    }
}
