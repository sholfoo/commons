package com.miru.miruApps.ui.base;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.data.db.CategoryDao;
import com.miru.miruApps.data.db.CustomerDao;
import com.miru.miruApps.data.db.InvoiceDao;
import com.miru.miruApps.data.db.ProductDao;
import com.miru.miruApps.data.preference.SPLogin;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.model.response.Login;
import com.miru.miruApps.network.ApiService;
import com.miru.miruApps.ui.activity.MiruMainActivity;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;
public class BaseLoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    @Inject ApiService mApiService;

    protected @BindView(R.id.action_register) LinearLayout actionRegister;
    protected @BindView(R.id.ly_footer) RelativeLayout lyFooter;
    protected @BindView(R.id.input_id) EditText inputId;
    protected @BindView(R.id.input_layout_user_id) TextInputLayout inputLayoutUserId;
    protected @BindView(R.id.input_password) EditText inputPassword;
    protected @BindView(R.id.input_layout_user_password) TextInputLayout inputLayoutUserPassword;
    protected @BindView(R.id.action_login_step) TextView actionLoginStep;
    protected @BindView(R.id.action_forget_password) TextView actionForgetPassword;
    protected @BindView(R.id.action_btn_login) Button actionBtnLogin;
    protected @BindView(R.id.login_progress) ProgressBar loginProgress;
    protected @BindView(R.id.action_connect_gmail) LinearLayout actionConnectGmail;
    protected @BindView(R.id.ly_content) LinearLayout lyContent;
    protected @BindView(R.id.action_connect_facebook) LinearLayout actionConnectFacebook;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    private String mVerificationId;

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "PhoneAuth";
    protected boolean isPhoneAuth = false;

    private CategoryDao categoryDao;
    private ProductDao productDao;
    private CustomerDao customerDao;
    private InvoiceDao invoiceDao;

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startMainActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (new SPLogin().isRemember(this)) {
            inputId.setText(new SPLogin().getKeyUserName(this));
            inputPassword.setText(new SPLogin().getKeyUserPassword(this));
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_login);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        productDao = MiruApp.getDaoSession().getProductDao();
        categoryDao = MiruApp.getDaoSession().getCategoryDao();
        invoiceDao = MiruApp.getDaoSession().getInvoiceDao();
        customerDao = MiruApp.getDaoSession().getCustomerDao();

        if (new SPLogin().isLoggedIn(this)) {
            startMainActivity();
        }

        configureFirebaseAuth();
        configureGoogleSignIn();
        configureCallback();
    }

    private void configureFirebaseAuth() {
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("en");
    }

    private void configureCallback() {
        mCallbackManager = CallbackManager.Factory.create();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                    e.printStackTrace();
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                    e.printStackTrace();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // ...
            }
        };
    }

    private void configureGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestProfile()
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleApiClient.connect();
    }

    protected void signInGoogle() {
        // Configure Google Sign In
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            }catch (Exception e){
                Log.e(TAG, "Google sign in failed", e);
            }

//            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi
//                    .getSignInResultFromIntent(data);
//            handleGoogleSignInResult(googleSignInResult);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.e(TAG, "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        user.getDisplayName();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.e(TAG, "signInWithCredential:failure", task.getException());
                        Toast.makeText(BaseLoginActivity.this, "Authentication Failed.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // success login
            GoogleSignInAccount account = result.getSignInAccount();
            String googleIdToken = account.getIdToken();
            String googleUserId = account.getId();
            String googleName = account.getDisplayName();
            String googleEmail = account.getEmail();
            Uri googlePhotoUri = account.getPhotoUrl();
            String googlePhoto = googlePhotoUri != null ? googlePhotoUri.toString() : "";

            Log.e(TAG, "Token : " + googleIdToken);
            Log.e(TAG, "User Id : " + googleUserId);
            Log.e(TAG, "Display Name : " + googleName);
            Log.e(TAG, "Display Email : " + googleEmail);
            Log.e(TAG, "Display PhotoUrl : " + googlePhoto);

//            Intent intentFillData = new Intent(BaseLoginActivity.this, )
        } else {
            Log.e(TAG, "Login Unsuccessful. ");
        }
    }

    private void signOutGoogle() {
        Auth.GoogleSignInApi
                .signOut(mGoogleApiClient)
                .setResultCallback(status -> Log.e(TAG, "Sign out. "));

        Auth.GoogleSignInApi
                .revokeAccess(mGoogleApiClient)
                .setResultCallback(
                status -> {
                    Log.e(TAG, "Sign out. ");
                    Log.e(TAG, "Sign out. ");
                });
    }

    protected void facebookLogin() {
        LoginManager.getInstance()
                .logInWithReadPermissions(BaseLoginActivity.this,
                        Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "facebook:onError", error);
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.e(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.e(TAG, "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.e(TAG, "signInWithCredential:failure", task.getException());
                        Toast.makeText(BaseLoginActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    protected void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]
    }

    protected void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");

                        FirebaseUser user = task.getResult().getUser();
                        // ...
                    } else {
                        // Sign in failed, display a message and update the UI
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                        }
                    }
                });
    }

    protected void attempLogin() {
        // Reset errors.
        inputId.setError(null);
        inputPassword.setError(null);

        // Store values at the time of the login attempt.
        String userName = inputId.getText().toString();
        String password = inputPassword.getText().toString();

        if (!validateInput())
            return;

        if (!validatePassword())
            return;

        if (isNetworkAvailable()) {
            initializeLoginApiCall(userName, password);
        } else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.error_no_internet_connection),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeLoginApiCall(String userName, String password) {
        showLoginProgress();

        new SPLogin().setKeyUserName(getApplicationContext(), userName);
        new SPLogin().setKeyUserPassword(getApplicationContext(), password);

        RequestBody requestUserName = createRequestBody(userName);
        RequestBody requestPassword = createRequestBody(password);

        Observable<Login> loginObservable = mApiService.login(requestUserName, requestPassword);
        loginObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Login>() {
                    @Override
                    public void onCompleted() {
                        hideLoginProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideLoginProgress();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(Login login) {
                        if (login.getStatusCode() == STATUS_SUCCESS){
                            if (!login.getClientId().equalsIgnoreCase(new SPUser().getKeyUserClientId(getApplicationContext()))){
                                productDao.deleteAll();
                                categoryDao.deleteAll();
                                invoiceDao.deleteAll();
                                customerDao.deleteAll();

                                new SPLogin().setKeyIsLoggedIn(getApplicationContext(), true);
                                new SPUser().setKeyIsStored(getApplicationContext(), true);
                                new SPUser().setKeyUserClientId(getApplicationContext(), login.getClientId());
                                new SPUser().setKeyUserSessionId(getApplicationContext(), login.getSessionId());
                                new SPUser().setKeyUserId(getApplicationContext(), login.getUserId());
                                new SPUser().setKeyUserName(getApplicationContext(), login.getUserName());
                                new SPUser().setKeyUserStore(getApplicationContext(), login.getClientName());
                                new SPUser().setKeyUserFullName(getApplicationContext(), login.getFullName());

                                if (!TextUtils.isEmpty(login.getUserProfileImage())){
                                    new SPUser().setKeyUserProfile(getApplicationContext(), login.getUserProfileImage());
                                }

                                if (!TextUtils.isEmpty(login.getClientLogo())){
                                    new SPUser().setKeyUserStoreLogo(getApplicationContext(), login.getClientLogo());
                                }
                            }

                            FirebaseMessaging.getInstance().subscribeToTopic("broadcast");
                            startActivity(new Intent(
                                    BaseLoginActivity.this,
                                    MiruMainActivity.class));
                            finish();
                        } else if (login.getStatusCode() == 222){
                            inputLayoutUserPassword.setError(login.getErrorMessage().toString());
                            requestFocus(inputPassword);
                        } else {
                            Toast.makeText(BaseLoginActivity.this,
                                    login.getErrorMessage().toString(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    private boolean validateInput() {
        String input = inputId.getText().toString().trim();
        if (input.isEmpty()) {
            inputLayoutUserId.setError(getString(R.string.error_field_required));
            requestFocus(inputId);
            return false;
        } else {
            inputLayoutUserId.setError(null);
        }

        return true;
    }

    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty() ||
                inputPassword.getText().toString().trim().length() < 3) {
            inputLayoutUserPassword.setError(getString(R.string.error_incorrect_password));
            requestFocus(inputPassword);
            return false;
        } else {
            inputLayoutUserPassword.setError(null);
        }

        return true;
    }

    private void showLoginProgress() {
        loginProgress.setVisibility(View.VISIBLE);
        actionBtnLogin.setVisibility(View.GONE);
    }

    private void hideLoginProgress() {
        loginProgress.setVisibility(View.GONE);
        actionBtnLogin.setVisibility(View.VISIBLE);
    }

    protected void configureLoginAuthStep() {
        if (isPhoneAuth) {
            actionLoginStep.setText(getResources().getString(R.string.action_login_with_email));
            inputLayoutUserId.setHint(getResources().getString(R.string.text_email));
            inputLayoutUserPassword.setVisibility(View.VISIBLE);
            actionForgetPassword.setVisibility(View.VISIBLE);
            isPhoneAuth = false;
        } else {
            actionLoginStep.setText(getResources().getString(R.string.action_login_with_phone));
            inputLayoutUserId.setHint(getResources().getString(R.string.text_phone_number));
            inputLayoutUserPassword.setVisibility(View.GONE);
            actionForgetPassword.setVisibility(View.GONE);
            isPhoneAuth = true;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed" + connectionResult);
    }

    private void startMainActivity() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, MiruMainActivity.class));
            finish();
        }, 0);
    }
}
