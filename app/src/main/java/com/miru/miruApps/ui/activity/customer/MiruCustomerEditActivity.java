package com.miru.miruApps.ui.activity.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.EventCustomer;
import com.miru.miruApps.model.response.DetailCustomer;
import com.miru.miruApps.model.retrieve.RetrieveCustomerData;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.customer.EditCustInfoFragment;
import com.miru.miruApps.ui.fragment.customer.EditCustLocationFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruCustomerEditActivity extends BaseActivity implements EditCustInfoFragment.OnNextListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;

    @Inject MasterApiService mMasterApiService;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome(){
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_customer_main_create);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        setupActionBar();
        initDetailCustomer(getExtraCustomerId());
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_customer);

    }

    private void setupViewPager(NonSwipeableViewPager viewPager, DetailCustomer detailCustomer) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(EditCustInfoFragment.newInstance(
                detailCustomer), getResources().getString(R.string.tab_title_cust_info));
        mViewPagerAdapter.addFragment(EditCustLocationFragment.newInstance(detailCustomer), getResources().getString(R.string.tab_title_cust_location));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNext(RetrieveCustomerData retrieveCustomerData) {
        htabTabs.getTabAt(1).select();
        EventBus.getDefault().postSticky(new EventCustomer(retrieveCustomerData));
    }

    private String getExtraCustomerId(){
        return getIntent().getStringExtra("customerId");
    }

    private void initDetailCustomer(String customerId) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<DetailCustomer> detailCustomerObservable = mMasterApiService.getCustomerDetailInfo(
                new SPUser().getKeyUserClientId(this), customerId
        );

        detailCustomerObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DetailCustomer>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                        Log.e("CustomerDetail", "onComplete...");
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof Exception) {
                            Log.e("CustomerDetail", "onError..." + e.getMessage());
                            Log.e("CustomerDetail", "onError..." + e.getLocalizedMessage());
                            Log.e("CustomerDetail", "onError..." + e.getCause());
                            Log.e("CustomerDetail", "onError..." + e.getStackTrace().toString());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(DetailCustomer detailCustomer) {
                        dialog.dismiss();
                        if (detailCustomer.getStatusCode() == STATUS_SUCCESS){
                            setupViewPager(htabViewpager, detailCustomer);
                            htabTabs.setupWithViewPager(htabViewpager);
                        }
                    }
                });
    }
}
