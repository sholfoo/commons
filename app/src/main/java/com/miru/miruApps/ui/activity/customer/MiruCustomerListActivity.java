package com.miru.miruApps.ui.activity.customer;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.customer.CustomerAdapter;
import com.miru.miruApps.data.db.CustomerDao;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Customer;
import com.miru.miruApps.model.section.CustomerContent;
import com.miru.miruApps.model.section.SectionCustomer;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.NULL;
import static com.miru.miruApps.config.Constants.STATUS_ERROR;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruCustomerListActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.ly_search) LinearLayout lySearch;
    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.action_filter) TextView actionFilter;
    @BindView(R.id.action_shorting) TextView actionShorting;
    @BindView(R.id.edit_text_search) EditText editTextSearch;

    @Inject MasterApiService mMasterApiService;

    private CustomerAdapter mCustomerAdapter;

    private CustomerDao customerDao;
    private Customer mCustomerMaster;
    private List<SectionCustomer> sectionCustomerList = new ArrayList<>();

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_filter)
    void onActionFilter() {

    }

    @OnClick(R.id.action_shorting)
    void onActionShorting() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_customer_list);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);

        customerDao = MiruApp.getDaoSession().getCustomerDao();

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        customerDao.deleteAll();
        sectionCustomerList.clear();
        initListCostumer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_customer:
                showDialogAlert();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void setupActionBar() {
        toolbarLogo.setText(getResources().getString(R.string.action_bar_customer));
        Drawable img = getResources().getDrawable(R.drawable.ic_action_profile_menu_icon);
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);

    }

    private void setupViews() {
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setHasFixedSize(true);
        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> {
            mCustomerAdapter.getParentItemList().clear();
            customerDao.deleteAll();
            initListCostumer();
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                performFilter(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                performFilter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                performFilter(editable.toString());
            }
        });
    }

    private void setupAdapter(Customer customers) {
        for (int i = 0; i < customers.getCustomerContainer().size(); i++) {
            String mCustomerId = customers.getCustomerContainer().get(i).getCustomerId();
            String mStoreName = customers.getCustomerContainer().get(i).getStoreName();
            String mStoreType = customers.getCustomerContainer().get(i).getCustomerType();
            String mStoreImage = customers.getCustomerContainer().get(i).getCustomerImage();
            String mAlphabet = mStoreName.substring(DEFAULT_SIZE, 1);
            String mCustomerStatus = customers.getCustomerContainer().get(i).getStatus();

            List<CustomerContent> customerContentList = new ArrayList<>();
            customerContentList.add(new CustomerContent(mCustomerId, mStoreImage, mStoreName, mStoreType, mCustomerStatus));
            sectionCustomerList.add(new SectionCustomer(
                    mAlphabet, customerContentList
            ));
        }

        mCustomerAdapter = new CustomerAdapter(this, sectionCustomerList);
        mCustomerAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                SectionCustomer sectionCustomer = sectionCustomerList.get(position);
            }

            @Override
            public void onListItemCollapsed(int position) {
                SectionCustomer sectionCustomer = sectionCustomerList.get(position);
            }
        });
        lvContent.setAdapter(mCustomerAdapter);
    }

    private void initListCostumer() {
        Observable<Customer> customerObservable = mMasterApiService.getCustomers(new SPUser().getKeyUserClientId(this));
        customerObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Customer>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(Customer customer) {
                        if (customer.getStatusCode() == STATUS_SUCCESS){
                            mCustomerMaster = customer;
                            setupAdapter(customer);
                            saveToSQLiteDatabase(customer);
                            resetUILoading();
                        } else if (customer.getStatusCode() == STATUS_ERROR){
                            showErrorView("Belum ada pelanggan");
                        } else
                            showErrorView();
                    }
                });
    }

    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initListCostumer();
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }

    private void showDialogAlert() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_customer);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        LinearLayout addNew = dialog.findViewById(R.id.action_add_new);
        LinearLayout addFromContact = dialog.findViewById(R.id.action_add_from_contact);

        addNew.setOnClickListener(view -> {
            dialog.dismiss();
            Intent intent = new Intent(this, MiruCustomerCreateActivity.class);
            intent.putExtra("contact_name", NULL);
            intent.putExtra("contact_number", NULL);
            startActivity(intent);
        });

        addFromContact.setOnClickListener(view -> {
            dialog.dismiss();
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, 1);
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            cursor.moveToFirst();
            String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            Intent intent = new Intent(this, MiruCustomerCreateActivity.class);
            intent.putExtra("contact_name", contactName);
            intent.putExtra("contact_number", contactNumber);
            startActivity(intent);
        }
    }

    private void saveToSQLiteDatabase(Customer customer){
        for (int i = 0; i < customer.getCustomerContainer().size(); i++){
            com.miru.miruApps.data.db.Customer customerDb = new com.miru.miruApps.data.db.Customer();
            customerDb.setCustomerId(customer.getCustomerContainer().get(i).getCustomerId());
            customerDb.setFullName(customer.getCustomerContainer().get(i).getOwnerName());
            customerDb.setStoreName(customer.getCustomerContainer().get(i).getStoreName());
            customerDb.setPhoneNumber(customer.getCustomerContainer().get(i).getAddress().get(DEFAULT_SIZE).getPhoneNumber());
            customerDb.setMobileNumber(customer.getCustomerContainer().get(i).getMobilePhone());
            customerDb.setEmail(customer.getCustomerContainer().get(i).getEmailAddress());
            customerDb.setCategory(customer.getCustomerContainer().get(i).getCustomerType());
            customerDb.setStatus(customer.getCustomerContainer().get(i).getStatus());
            customerDb.setDescription(customer.getCustomerContainer().get(i).getDescription());
            customerDb.setDescription(customer.getCustomerContainer().get(i).getCustomerTag().toString());
            customerDb.setCreatedAt(DateTimeUtil.getCurrentFormat("dd-mm-yyyy"));
            customerDb.setUpdatedAt(DateTimeUtil.getCurrentFormat("dd-mm-yyyy"));
            customerDb.setFullAddress(customer.getCustomerContainer().get(i).getAddress().get(DEFAULT_SIZE).getAddress());
            customerDb.setZipCode(customer.getCustomerContainer().get(i).getAddress().get(DEFAULT_SIZE).getPostalCode());
            customerDb.setCity(customer.getCustomerContainer().get(i).getAddress().get(DEFAULT_SIZE).getCity());
            customerDao.insert(customerDb);
        }
    }

    private void performFilter(CharSequence charSequence){
        List<SectionCustomer> filteredSectionCustomerList;
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
            filteredSectionCustomerList = new ArrayList<>(sectionCustomerList);
        } else {
            List<SectionCustomer> filteredList = new ArrayList<>();
            for (Customer.CustomerContainerEntity row : mCustomerMaster.getCustomerContainer()) {
                // name match condition. this might differ depending on your requirement
                // here we are looking for name or phone number match
                if (row.getStoreName().toLowerCase().contains(charString.toLowerCase())) {
                    String mCustomerId = row.getCustomerId();
                    String mStoreName = row.getStoreName();
                    String mCustomerType = row.getCustomerType();
                    String mStoreImage = row.getCustomerImage();
                    String mAlphabet = mStoreName.substring(DEFAULT_SIZE, 1);
                    String mCustomerStatus = row.getStatus();

                    List<CustomerContent> customerContentList = new ArrayList<>();
                    customerContentList.add(new CustomerContent(mCustomerId, mStoreImage, mStoreName, mCustomerType, mCustomerStatus));
                    filteredList.add(new SectionCustomer(
                            mAlphabet, customerContentList
                    ));
                }
            }
            mCustomerAdapter.getParentItemList().clear();
            filteredSectionCustomerList = new ArrayList<>(filteredList);
        }
        mCustomerAdapter = new CustomerAdapter(this, filteredSectionCustomerList);
        lvContent.setAdapter(mCustomerAdapter);
    }
}
