package com.miru.miruApps.ui.activity.invoice.filtering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.EXTRA_INVOCE_STATUS;
import static com.miru.miruApps.config.Constants.EXTRA_INVOICE_PERIODIC_DATE;
import static com.miru.miruApps.config.Constants.EXTRA_INVOICE_PERIODIC_TIME;

public class MiruInvoiceFilterActivity extends BaseActivity {

    @BindView(R.id.action_home)
    ImageView actionHome;
    @BindView(R.id.toolbar_logo)
    TextView toolbarLogo;
    @BindView(R.id.ly_toolbar)
    Toolbar lyToolbar;
    @BindView(R.id.check_pnd)
    CheckBox checkPnd;
    @BindView(R.id.check_due)
    CheckBox checkDue;
    @BindView(R.id.check_ovd)
    CheckBox checkOvd;
    @BindView(R.id.check_paid)
    CheckBox checkPaid;
    @BindView(R.id.spn_time_period)
    MaterialBetterSpinner spnTimePeriod;
    @BindView(R.id.edit_text_time_period)
    EditText editTextTimePeriod;
    @BindView(R.id.action_apply)
    Button actionApply;

    @OnClick(R.id.action_home)
    void onActionHome(){
        finish();
    }

    @OnClick(R.id.action_apply)
    void onActionApply(){
        Intent data = new Intent();

        // Add the required data to be returned to the MainActivity
        data.putExtra(EXTRA_INVOCE_STATUS, "EXTRA_INVOCE_STATUS");
        data.putExtra(EXTRA_INVOICE_PERIODIC_TIME, "EXTRA_INVOICE_PERIODIC_TIME");
        data.putExtra(EXTRA_INVOICE_PERIODIC_DATE, "EXTRA_INVOICE_PERIODIC_DATE");

        // Set the resultCode to Activity.RESULT_OK to
        // indicate a success and attach the Intent
        // which contains our result data
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_invoice_filter);
        ButterKnife.bind(this);

        setupActionBar();
    }

    private void setupActionBar() {
        actionHome.setImageResource(R.drawable.ic_action_bar_close);
        toolbarLogo.setText(getResources().getString(R.string.action_bar_filter));
    }
}
