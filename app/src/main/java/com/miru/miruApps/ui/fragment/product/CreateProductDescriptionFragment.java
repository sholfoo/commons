package com.miru.miruApps.ui.fragment.product;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.AddImageAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.ProductCategory;
import com.miru.miruApps.model.retrieve.RetrieveProductData;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.base.BaseFragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class CreateProductDescriptionFragment extends BaseFragment {


    @Inject
    MasterApiService mMasterApiService;

    protected OnNextListener mOnNextListener;
    @BindView(R.id.lv_content)
    RecyclerView lvContent;
    @BindView(R.id.input_product_sku)
    EditText inputProductSku;
    @BindView(R.id.input_layout_product_sku)
    TextInputLayout inputLayoutProductSku;
    @BindView(R.id.input_product_brand)
    EditText inputProductBrand;
    @BindView(R.id.input_layout_product_brand)
    TextInputLayout inputLayoutProductBrand;
    @BindView(R.id.input_product_name)
    EditText inputProductName;
    @BindView(R.id.input_layout_product_name)
    TextInputLayout inputLayoutProductName;
    @BindView(R.id.input_product_category)
    AutoCompleteTextView inputProductCategory;
    @BindView(R.id.input_layout_product_category)
    TextInputLayout inputLayoutProductCategory;
    @BindView(R.id.input_product_description)
    EditText inputProductDescription;
    @BindView(R.id.input_layout_product_description)
    TextInputLayout inputLayoutProductDescription;
    @BindView(R.id.input_product_size)
    EditText inputProductSize;
    @BindView(R.id.input_layout_product_size)
    TextInputLayout inputLayoutProductSize;
    @BindView(R.id.action_next)
    Button actionNext;

    private List<String> imageString = new ArrayList<>();
    private ArrayList<File> images = new ArrayList<>();

    private String documentPath;
    private File compressedDocumentFile = null;

    private AddImageAdapter mAdapter;

    public static CreateProductDescriptionFragment newInstance() {
        return new CreateProductDescriptionFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnNextListener = (OnNextListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @OnClick(R.id.action_next)
    void onActionNext() {
        if (!validateInput(inputLayoutProductSku, inputProductSku))
            return;

        if (!validateInput(inputLayoutProductBrand, inputProductBrand))
            return;

        if (!validateInput(inputLayoutProductName, inputProductName))
            return;

        if (!validateInput(inputLayoutProductCategory, inputProductCategory))
            return;

        if (!validateInput(inputLayoutProductDescription, inputProductDescription))
            return;

        attemptNext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_create_desc, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAdapter();
        getProductCategories();
    }

    private void setupAdapter() {
        mAdapter = new AddImageAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_transparent));
        lvContent.addItemDecoration(itemDecorator);
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);

        mAdapter.setOnInsertListener(() -> {
            Log.e("OnClicked", "running....");
            showDialogInsertImage();
        });

    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    private void pickImage() {
        ImagePicker.create(this) // Activity or Fragment
                .folderMode(true)
                .showCamera(false)
                .single()
                .enableLog(false)
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            Image image = ImagePicker.getFirstImageOrNull(data);
            documentPath = String.valueOf(image.getPath());
            compressedDocumentFile = scaleImage(documentPath);
            Log.d("PHOTO PATH", documentPath);

            imageString.add(documentPath);
            images.add(compressedDocumentFile);

            Log.e("ImgString", imageString.toString());
            Log.e("Images", images.toString());
            mAdapter.pushData(imageString);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void attemptNext() {
        String productSku = inputProductSku.getText().toString();
        String productBrand = inputProductBrand.getText().toString();
        String productName = inputProductName.getText().toString();
        String productKategory = inputProductCategory.getText().toString();
        String productDescription = inputProductDescription.getText().toString();
        String productSize = inputProductSize.getText().toString();

        mOnNextListener.onActionNext(new RetrieveProductData(
                images,
                productSku,
                productBrand,
                productName,
                productKategory,
                productDescription,
                productSize
        ));
    }

    public interface OnNextListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onActionNext(RetrieveProductData retrieveProductData);
    }

    private void getProductCategories() {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<ProductCategory> productCategoryObservable = mMasterApiService.getProductCategory(
                new SPUser().getKeyUserClientId(mContext));
        productCategoryObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ProductCategory>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(ProductCategory productCategory) {
                        if (productCategory.getStatusCode() == STATUS_SUCCESS)
                            setupAdapterCategory(productCategory);
                    }
                });
    }

    private void setupAdapterCategory(ProductCategory productCategory) {
        List<String> categoryList = new ArrayList<>();
        for (int i = 0; i < productCategory.getCatalog().size(); i++) {
            categoryList.add(productCategory.getCatalog().get(i).getCategory());
        }

        String[] categories = categoryList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_list_item_1, categories);
        inputProductCategory.setThreshold(1);
        inputProductCategory.setAdapter(adapter);
        inputProductCategory.setOnItemClickListener((parent, view, position, id) -> {
            inputProductCategory.showDropDown();
            String selection = (String) parent.getItemAtPosition(position);
            inputProductCategory.setText(selection);
        });
    }

    private void showDialogInsertImage() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_image);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        LinearLayout takePicture = dialog.findViewById(R.id.action_take_picture);
        LinearLayout pickImage = dialog.findViewById(R.id.action_pick_image);

        takePicture.setOnClickListener(view -> {
            captureImage();
            dialog.dismiss();
        });

        pickImage.setOnClickListener(view -> {
            pickImage();
            dialog.dismiss();
        });
        dialog.show();
    }

}
