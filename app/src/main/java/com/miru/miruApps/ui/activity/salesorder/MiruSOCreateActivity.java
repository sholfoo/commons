package com.miru.miruApps.ui.activity.salesorder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.eventbus.so.EventSoCustomer;
import com.miru.miruApps.eventbus.so.EventSoProduct;
import com.miru.miruApps.model.retrieve.RetrieveSoInfoData;
import com.miru.miruApps.model.retrieve.RetrieveSoProductData;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.salesorder.CreateSOCustomerFragment;
import com.miru.miruApps.ui.fragment.salesorder.CreateSOProductFragment;
import com.miru.miruApps.ui.fragment.salesorder.CreateSOReviewFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiruSOCreateActivity extends BaseActivity implements CreateSOCustomerFragment.OnNextListener, CreateSOProductFragment.OnReviewListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;
    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        showDiscardDialog();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_so_main_create);
        ButterKnife.bind(this);

        setupActionBar();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_so);

    }

    private void setupViewPager(NonSwipeableViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(CreateSOCustomerFragment.newInstance(getExtraFromId()),
                getResources().getString(R.string.tab_title_so_customer));
        mViewPagerAdapter.addFragment(CreateSOProductFragment.newInstance(),
                getResources().getString(R.string.tab_title_so_product));
        mViewPagerAdapter.addFragment(CreateSOReviewFragment.newInstance(),
                getResources().getString(R.string.tab_title_so_review));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNextInfo(RetrieveSoInfoData retrieveSoInfoData) {
//        htabTabs.getTabAt(1).select();
        htabTabs.setScrollPosition(1, 0f, true);
        htabViewpager.setCurrentItem(1);
        EventBus.getDefault().postSticky(new EventSoCustomer(retrieveSoInfoData));
    }

    @Override
    public void onActionReviewproduct(RetrieveSoProductData retrieveSoProductData) {
//        htabTabs.getTabAt(2).select();
        htabTabs.setScrollPosition(2, 0f, true);
        htabViewpager.setCurrentItem(2);
        EventBus.getDefault().postSticky(new EventSoProduct(retrieveSoProductData));
    }

    private int getExtraFromId(){
        return getIntent().getIntExtra("extra_from_id", 0);
    }

    @Override
    public void onBackPressed() {
        showDiscardDialog();
    }
}
