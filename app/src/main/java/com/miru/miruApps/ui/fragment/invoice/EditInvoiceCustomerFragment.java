package com.miru.miruApps.ui.fragment.invoice;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.so.AutoCompleteCustomerAdapter;
import com.miru.miruApps.data.db.Customer;
import com.miru.miruApps.data.db.CustomerDao;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.model.retrieve.RetrieveInvoiceInfoData;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.view.OnItemSelectedListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;

public class EditInvoiceCustomerFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    static final String FORMAT_DATE = "dd-MM-yyyy";
    protected OnNextListener mOnNextListener;

    @BindView(R.id.action_next) Button actionNext;
    @BindView(R.id.input_sales_name) AutoCompleteTextView inputSalesName;
    @BindView(R.id.input_layout_sales_name) TextInputLayout inputLayoutSalesName;
    @BindView(R.id.input_invoice_date) EditText inputInvoiceDate;
    @BindView(R.id.input_layout_invoice_date) TextInputLayout inputLayoutInvoiceDate;
    @BindView(R.id.spn_term_of_payment) MaterialBetterSpinner spnTermOfPayment;
    @BindView(R.id.spn_time_period) MaterialBetterSpinner spnTimePeriod;
    @BindView(R.id.input_invoice_due_date) EditText inputInvoiceDueDate;
    @BindView(R.id.input_layout_invoice_due_date) TextInputLayout inputLayoutInvoiceDueDate;
    @BindView(R.id.input_description) EditText inputDescription;
    @BindView(R.id.input_layout_description) TextInputLayout inputLayoutDescription;
    @BindView(R.id.input_store_name) EditText inputStoreName;
    @BindView(R.id.input_layout_store_name) TextInputLayout inputLayoutStoreName;
    @BindView(R.id.input_phone_number) EditText inputPhoneNumber;
    @BindView(R.id.input_layout_phone_number) TextInputLayout inputLayoutPhoneNumber;
    @BindView(R.id.location_city) EditText locationCity;
    @BindView(R.id.input_layout_city) TextInputLayout inputLayoutCity;
    @BindView(R.id.location_postal_code) EditText locationPostalCode;
    @BindView(R.id.input_layout_postal_code) TextInputLayout inputLayoutPostalCode;
    @BindView(R.id.location_address) EditText locationAddress;
    @BindView(R.id.input_layout_complete_address) TextInputLayout inputLayoutCompleteAddress;

    private CustomerDao customerDao;
    private Query<Customer> customerQuery;
    private List<Customer> customerList;
    private AutoCompleteCustomerAdapter adapter;

    private CustomerDao customerEditDao;
    private Query<Customer> customerEditQuery;
    private List<Customer> customerEditList;

    private String mCustomerId = null;
    private String mTermOfPayment = null;

    public static EditInvoiceCustomerFragment newInstance(Invoice invoice) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("invoice", invoice);
        EditInvoiceCustomerFragment editInvoiceCustomerFragment = new EditInvoiceCustomerFragment();
        editInvoiceCustomerFragment.setArguments(bundle);
        return editInvoiceCustomerFragment;
    }

    @OnClick(R.id.input_invoice_date)
    void onActionInputDate() {
        createDatePickerDialog();
    }


    @OnClick(R.id.action_next)
    void onActionNext() {
//        if (!validateInput(inputLayoutSalesName, inputSalesName))
//            return;
//
//        if (!validateInput(inputLayoutInvoiceDate, inputInvoiceDate))
//            return;
//
//        if (!validateInput(inputLayoutInvoiceDueDate, inputInvoiceDueDate))
//            return;
//
//        if (mTermOfPayment == null){
//            spnTermOfPayment.setError(getString(R.string.error_field_required));
//            requestFocus(spnTermOfPayment);
//            return;
//        }

        attemptNext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice_create_customer, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAutoCompleteCustomer();
        inputInvoiceDate.setKeyListener(null);
        inputInvoiceDueDate.setKeyListener(null);

        String dateStr = DateTimeUtil.getCurrentFormat(FORMAT_DATE);
        inputInvoiceDate.setText(dateStr);
        inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                dateStr,
                FORMAT_DATE,
                FORMAT_DATE,
                0
        ));

        setupViews(getArguments().getParcelable("invoice"));

        setupAutoCompleteCustomer();
        setupSpinnerTermOfPayment();
        setupSpinnerTimePeriod();
        hideTimePeriod();
    }

    protected void createDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(
                mContext,
                R.style.MiruApp_DatePicker,
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.show();
    }

    private void attemptNext() {
        String mSalesName = inputSalesName.getText().toString();
        String mStoreName = inputStoreName.getText().toString();
        String mAddress = locationAddress.getText().toString();
        String mCity = locationCity.getText().toString();
        String mZipCode = locationPostalCode.getText().toString();
        String mPhoneNumber = inputPhoneNumber.getText().toString();

        String mInvoiceDate = inputInvoiceDate.getText().toString();
        String mInvoiceDueDate = inputInvoiceDueDate.getText().toString();
        String mDescription = inputDescription.getText().toString();

        mOnNextListener.onActionNextInfo(new RetrieveInvoiceInfoData(
                mCustomerId,
                mSalesName,
                mStoreName,
                mAddress,
                mCity,
                mZipCode,
                mPhoneNumber,
                mTermOfPayment,
                mInvoiceDueDate,
                mInvoiceDate,
                mDescription
        ));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnNextListener = (OnNextListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthStr = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
        String yearStr = String.valueOf(year);

        String dateStr = dayStr + "-" + monthStr + "-" + yearStr;
        inputInvoiceDate.setText(dateStr);
        inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                dateStr,
                FORMAT_DATE,
                FORMAT_DATE,
                0
        ));

    }

    public interface OnNextListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onActionNextInfo(RetrieveInvoiceInfoData retrieveInvoiceInfoData);
    }

    private void setupAutoCompleteCustomer() {
        customerDao = MiruApp.getDaoSession().getCustomerDao();
        customerQuery = customerDao
                .queryBuilder()
                .orderAsc(CustomerDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        customerList = customerQuery.list();
        adapter = new AutoCompleteCustomerAdapter(mContext, customerList);
        inputSalesName.setThreshold(1);
        inputSalesName.setAdapter(adapter);
        inputSalesName.setOnItemClickListener((adapterView, view, i, l) -> {
            mCustomerId = customerList.get(i).getCustomerId();
            inputStoreName.setText(customerList.get(i).getStoreName());
            inputPhoneNumber.setText(customerList.get(i).getPhoneNumber());
            locationCity.setText(customerList.get(i).getCity());
            locationPostalCode.setText(customerList.get(i).getZipCode());
            locationAddress.setText(customerList.get(i).getFullAddress());
        });

    }


    private void setupSpinnerTermOfPayment() {
        String[] termOfPayment = {
                "Cash",
                "Jangka Waktu"
        };

        ArrayAdapter<String> termOfPaymentAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_dropdown_item_1line, termOfPayment);
        spnTermOfPayment.setAdapter(termOfPaymentAdapter);
        spnTermOfPayment.addTextChangedListener(new OnItemSelectedListener() {
            @Override
            protected void onItemSelected(String string) {
                if (!validateInput(inputLayoutInvoiceDate, inputInvoiceDate))
                    return;

                if (string.equalsIgnoreCase(termOfPayment[0])) {
                    mTermOfPayment = string;
                    hideTimePeriod();
                } else {
                    showTimePeriod();
                }
            }
        });
    }

    private void setupSpinnerTimePeriod() {
        String[] timePeriod = {
                "7 Hari",
                "14 Hari",
                "30 Hari",
                "60 Hari",
                "90 Hari",
        };

        ArrayAdapter<String> timePeriodAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_dropdown_item_1line, timePeriod);
        spnTimePeriod.setAdapter(timePeriodAdapter);
        spnTimePeriod.addTextChangedListener(new OnItemSelectedListener() {
            @Override
            protected void onItemSelected(String string) {
                if (!validateInput(inputLayoutInvoiceDate, inputInvoiceDate))
                    return;

                if (string.equalsIgnoreCase(timePeriod[0])) {
                    mTermOfPayment = "top7";
                    inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                            inputInvoiceDate.getText().toString(),
                            FORMAT_DATE,
                            FORMAT_DATE,
                            7
                    ));
                } else if (string.equalsIgnoreCase(timePeriod[1])) {
                    mTermOfPayment = "top14";
                    inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                            inputInvoiceDate.getText().toString(),
                            FORMAT_DATE,
                            FORMAT_DATE,
                            14
                    ));
                } else if (string.equalsIgnoreCase(timePeriod[2])) {
                    mTermOfPayment = "top30";
                    inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                            inputInvoiceDate.getText().toString(),
                            FORMAT_DATE,
                            FORMAT_DATE,
                            30
                    ));
                } else if (string.equalsIgnoreCase(timePeriod[3])) {
                    mTermOfPayment = "top60";
                    inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                            inputInvoiceDate.getText().toString(),
                            FORMAT_DATE,
                            FORMAT_DATE,
                            60
                    ));
                } else if (string.equalsIgnoreCase(timePeriod[4])) {
                    mTermOfPayment = "top90";
                    inputInvoiceDueDate.setText(DateTimeUtil.getFormatDateWithDaysAdded(
                            inputInvoiceDate.getText().toString(),
                            FORMAT_DATE,
                            FORMAT_DATE,
                            90
                    ));
                }
            }
        });
    }

    private void showTimePeriod() {
        spnTimePeriod.setVisibility(View.VISIBLE);
    }

    private void hideTimePeriod() {
        spnTimePeriod.setVisibility(View.GONE);
    }

    private void setupViews(Invoice invoice){
        mCustomerId = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getCustomerId();
        mTermOfPayment = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTermOfPayment();
        customerEditDao = MiruApp.getDaoSession().getCustomerDao();
        customerEditQuery = customerEditDao
                .queryBuilder()
                .where(CustomerDao.Properties.CustomerId.eq(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getCustomerId()))
                .orderAsc(CustomerDao.Properties.Id)
                .build();

        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;

        customerEditList = customerEditQuery.list();
        for (Customer customer : customerEditList){
            inputSalesName.setText(customer.getFullName());
            inputStoreName.setText(customer.getStoreName());
            inputPhoneNumber.setText(customer.getMobileNumber());
            locationCity.setText(customer.getCity());
            locationPostalCode.setText(customer.getZipCode());
            locationAddress.setText(customer.getFullAddress());
        }

        inputInvoiceDate.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getInvoiceDate());
        spnTermOfPayment.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTermOfPayment());
        inputInvoiceDate.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getDueDate());
        inputDescription.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getNotes());
    }
}
