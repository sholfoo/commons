package com.miru.miruApps.ui.fragment.salesorder;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ReviewSoProductAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.SoProduct;
import com.miru.miruApps.model.response.SalesOrder;
import com.miru.miruApps.network.SoApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.FLD;
import static com.miru.miruApps.config.Constants.PND;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class DetailSOOrderFragment extends BaseFragment {

    @BindView(R.id.so_status) TextView soStatus;
    @BindView(R.id.ext_sales_order_id) TextView extSalesOrderId;
    @BindView(R.id.user_store_logo) ImageView userStoreLogo;
    @BindView(R.id.user_store_name) TextView userStoreName;
    @BindView(R.id.so_date) TextView soDate;
    @BindView(R.id.user_full_name) TextView userFullName;
    @BindView(R.id.customer_name) TextView customerName;
    @BindView(R.id.so_total_amount) TextView soTotalAmount;
    @BindView(R.id.lv_content_review) RecyclerView lvContentReview;
    @BindView(R.id.so_subtotal) TextView soSubtotal;
    @BindView(R.id.so_discount) TextView soDiscount;
    @BindView(R.id.so_discount_price) TextView soDiscountPrice;
    @BindView(R.id.input_so_tax) TextView inputSoTax;
    @BindView(R.id.so_tax_price) TextView soTaxPrice;
    @BindView(R.id.so_total_price) TextView soTotalPrice;
    @BindView(R.id.so_note) TextView soNote;
    @BindView(R.id.current_date) TextView currentDate;
    @BindView(R.id.customer_name_bottom) EditText customerNameBottom;
    @BindView(R.id.signature_image) ImageView signatureImage;

    @Inject SoApiService mApiService;

    private ReviewSoProductAdapter mAdapter;

    public static DetailSOOrderFragment newInstance(String salesOrderId, String extSalesOrderId) {
        Bundle bundle = new Bundle();
        bundle.putString("salesOrderId", salesOrderId);
        bundle.putString("extSalesOrderId", extSalesOrderId);
        DetailSOOrderFragment detailSOOrderFragment = new DetailSOOrderFragment();
        detailSOOrderFragment.setArguments(bundle);
        return detailSOOrderFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_so_detail_so, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    private String getArgumentExtraExtSalesOrderId() {
        return getArguments() != null ? getArguments().getString("extSalesOrderId") : null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new ReviewSoProductAdapter(mContext);
        lvContentReview.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContentReview.setAdapter(mAdapter);
        initDetailSalesOrder();
    }

    private void initDetailSalesOrder() {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<SalesOrder> salesOrderObservable = mApiService.getSalesOrderDetail(
                new SPUser().getKeyUserClientId(mContext),
                1,
                getArgumentExtraExtSalesOrderId());

        salesOrderObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SalesOrder>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(SalesOrder salesOrder) {
                        if (salesOrder.getStatusCode() == STATUS_SUCCESS) {
                            setupReview(salesOrder);
                        }
                    }
                });
    }

    private void setupReview(SalesOrder salesOrder) {
        List<SoProduct> soProductList = new ArrayList<>();
        for (int i = 0; i < salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().size(); i++) {
            int mQuantity = salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().get(i).getQuantity();
            int mPricePerUnit = salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().get(i).getPricePerUnit();
            int totalAmountProduct = mQuantity * mPricePerUnit;

            soProductList.add(new SoProduct(
                    salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().get(i).getItemSKU(),
                    salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().get(i).getItemName(),
                    String.valueOf(mQuantity),
                    salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().get(i).getMeasurementUnit(),
                    String.valueOf(mPricePerUnit),
                    salesOrder.getOrders().get(DEFAULT_SIZE).getItems().getItems().get(i).getDiscount(),
                    String.valueOf(mPricePerUnit),
                    String.valueOf(totalAmountProduct)
            ));
        }

        Glide.with(mContext)
                .load(new SPUser().getKeyUserStoreLogo(mContext))
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(userStoreLogo);

        Glide.with(mContext)
                .load(salesOrder.getOrders().get(DEFAULT_SIZE).getSignaturePath())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(signatureImage);

        userStoreName.setText(new SPUser().getKeyUserStore(mContext));
        extSalesOrderId.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getExternalSoId());
        if (salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getStatus().equals(PND)){
            soStatus.setText("Menunggu Diproses");
        } else if (salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getStatus().equals(FLD)){
            soStatus.setText("Sudah Diproses");
        } else {
            soStatus.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getStatus());
        }

        mAdapter.pushData(soProductList);
        userFullName.setText(new SPUser().getKeyUserFullName(mContext));
        soTotalAmount.setText(FormatNumber.getFormatCurrencyIDR(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getFinalAmount()));
        soDate.setText(DateTimeUtil.getConvertedFormat(
                salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getSalesOrderDate(), "dd-MM-yyyy",
                "dd MMM yyyy"));
        customerName.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getSalesName());
        soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getTotalAmount()));
        soDiscount.setText(FormatNumber.getFormatPercent(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getFinalDiscount()));
        soDiscountPrice.setText(FormatNumber.getFormatCurrencyIDR(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getFinalDiscountInNum()));
        inputSoTax.setText(FormatNumber.getFormatPercent(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getTax()));
        soTaxPrice.setText(FormatNumber.getFormatCurrencyIDR((int) salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getTaxInNum()));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getFinalAmount()));
        soNote.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getNotes());
        currentDate.setText(DateTimeUtil.getCurrentFormat("dd MMM yyyy"));
        customerNameBottom.setText(salesOrder.getOrders().get(DEFAULT_SIZE).getSalesOrder().getSalesName());
    }

//    private String getStatusSo(String status){
//        if (status.equalsIgnoreCase(UNPAID)){
//            return "Belum Jatuh Tempo";
//        } else if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(PARTIAL_PAYMENT)){
//            holder.lyShapeCircleSolid.setBackgroundResource(
//                    R.drawable.shape_circle_orange
//            );
//            holder.paidStatus.setText("Bayar Sebagian");
//        } else if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(OVERDUE)){
//            holder.lyShapeCircleSolid.setBackgroundResource(
//                    R.drawable.shape_circle_yellow
//            );
//            holder.paidStatus.setText("Jatuh Tempo");
//        } else if (invoiceDetailsEntity.getStatus().equalsIgnoreCase(FAILED)){
//            holder.lyShapeCircleSolid.setBackgroundResource(
//                    R.drawable.shape_circle_red
//            );
//            holder.paidStatus.setText("Gagal Bayar");
//        }
//    }
}
