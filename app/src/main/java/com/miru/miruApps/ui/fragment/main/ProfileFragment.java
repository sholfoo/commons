package com.miru.miruApps.ui.fragment.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.messaging.FirebaseMessaging;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.data.preference.SPCreateCustomer;
import com.miru.miruApps.data.preference.SPLogin;
import com.miru.miruApps.data.preference.SPNotification;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Logout;
import com.miru.miruApps.network.ApiService;
import com.miru.miruApps.ui.activity.MiruContactUsActivity;
import com.miru.miruApps.ui.activity.MiruPrivacyPolicyActivity;
import com.miru.miruApps.ui.activity.MiruTermConditionActivity;
import com.miru.miruApps.ui.activity.MiruUserInfoActivity;
import com.miru.miruApps.ui.activity.login.MiruLoginActivity;
import com.miru.miruApps.ui.base.BaseFragment;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProfileFragment extends BaseFragment {

    private static final String TAG = ProfileFragment.class.getCanonicalName();
    @BindView(R.id.img_profile) CircleImageView imgProfile;
    @BindView(R.id.profile_name) TextView profileName;
    @BindView(R.id.profile_store_name) TextView profileStoreName;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;

    @Inject ApiService mApiService;
    private GoogleApiClient mGoogleApiClient;

    @OnClick(R.id.action_profile_logout)
    void onActionLogout() {
        showLogoutDialog();
    }

    @OnClick(R.id.action_profile_info)
    void onActionProfileInfo() {
        startActivity(new Intent(getActivity(), MiruUserInfoActivity.class));
    }

    @OnClick(R.id.action_profile_contact_us)
    void onActionContactUs() {
        startActivity(new Intent(getActivity(), MiruContactUsActivity.class));
    }

    @OnClick(R.id.action_profile_faq)
    void onActionFaq() {
        startActivity(new Intent(getActivity(), MiruTermConditionActivity.class));
    }

    @OnClick(R.id.action_profile_privacy)
    void onActionPrivacy() {
        startActivity(new Intent(getActivity(), MiruPrivacyPolicyActivity.class));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_miru_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lyRefresh.setColorSchemeResources(R.color.colorPrimary);
        lyRefresh.setOnRefreshListener(() -> {
            setupViews();
        });

        setupViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViews();
    }

    private void setupViews() {
        Glide.with(mContext)
                .load(new SPUser().getKeyUserProfile(mContext))
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(imgProfile);
        profileName.setText(new SPUser().getKeyUserFullName(mContext));
        profileStoreName.setText(new SPUser().getKeyUserStore(mContext));
        lyRefresh.setRefreshing(false);
    }

    private RequestBody createRequestBody(String mValue) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), mValue);
    }

    private void initLogout() {
        MaterialDialog logoutDialog = new MaterialDialog.Builder(mContext)
                .content("Signing out...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestSessionId = createRequestBody(new SPUser().getKeyUserSessionId(mContext));

        Observable<Logout> logoutObservable = mApiService.logout(requestSessionId);
        logoutObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Logout>() {
                    @Override
                    public void onCompleted() {
                        logoutDialog.dismiss();
                        Intent finishIntent = new Intent(getActivity(), MiruLoginActivity.class);
                        finishIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        finishIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finishIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(finishIntent);
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        logoutDialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(Logout logout) {
                        new SPLogin().clearSession(mContext);
                        new SPCreateCustomer().clearSession(mContext);
                        new SPNotification().clearSession(mContext);

                        FirebaseMessaging.getInstance().unsubscribeFromTopic("broadcast");
                    }
                });
    }

    private void showLogoutDialog() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        Button actionCancel = dialog.findViewById(R.id.action_cancel);
        Button actionLogout = dialog.findViewById(R.id.action_logout);

        actionCancel.setOnClickListener(view -> {
            dialog.dismiss();
        });

        actionLogout.setOnClickListener(view -> {
            dialog.dismiss();
//            mGoogleApiClient = GoogleSignIn.getClient();
//            Auth.GoogleSignInApi
//                    .signOut(mGoogleApiClient)
//                    .setResultCallback(status -> Log.e(TAG, "Sign out. "));
            initLogout();
        });
        dialog.show();
    }

}
