package com.miru.miruApps.ui.base;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miru.miruApps.BuildConfig;
import com.miru.miruApps.R;
import com.miru.miruApps.data.preference.SPLogin;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.network.FileDownloadService;
import com.miru.miruApps.ui.activity.login.MiruLoginActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FileUtilities;
import com.miru.miruApps.utils.ScalingUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class    BaseActivity extends AppCompatActivity {

    static final String TAG = "downloadFileService";
    protected static File downloadedFilePDF;
    protected FragmentManager getBaseFragmentManager() {
        return getSupportFragmentManager();
    }

    protected boolean validateInput(TextInputLayout mTextInputLayout, EditText mInputEditText) {
        String input = mInputEditText.getText().toString().trim();
        if (input.isEmpty()) {
            mTextInputLayout.setError(getString(R.string.error_field_required));
            requestFocus(mInputEditText);
            return false;
        } else {
            mTextInputLayout.setError(null);
        }

        return true;
    }

    protected boolean validateInput(TextInputLayout mTextInputLayout, AutoCompleteTextView mInputEditText) {
        String input = mInputEditText.getText().toString().trim();
        if (input.isEmpty()) {
            mTextInputLayout.setError(getString(R.string.error_field_required));
            requestFocus(mInputEditText);
            return false;
        } else {
            mTextInputLayout.setError(null);
        }

        return true;
    }

    protected boolean validateInput(EditText mInputEditText) {
        String input = mInputEditText.getText().toString().trim();
        if (input.isEmpty()) {
            mInputEditText.setError(getString(R.string.error_field_required));
            requestFocus(mInputEditText);
            return false;
        } else {
            mInputEditText.setError(null);
        }

        return true;
    }

    protected void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            );
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    protected File scaleImage(String path) {
        String strMyImagePath = null;
        Bitmap scaledBitmap;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, 240, 320, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= 240 && unscaledBitmap.getHeight() <= 320)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 240, 320, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return new File(path);
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/Miru");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = DateTimeUtil.getCurrentFormat("ddmmyyyyHHmmss") + ".png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {}

        if (strMyImagePath == null) {
            return new File(path);
        }

        return new File(strMyImagePath);

    }

    public RequestBody createRequestBody(String mValue) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), mValue);
    }

    public MultipartBody.Part prepareFilePart(String partName, File documentFile) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(
                MediaType.parse("multipart/form-data"), documentFile);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, documentFile.getName(), requestFile);
    }


    public void showDiscardDialog(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView dialogMessage = dialog.findViewById(R.id.dialog_message);
        Button actionCancel = dialog.findViewById(R.id.action_cancel);
        Button actionLogout = dialog.findViewById(R.id.action_logout);

        actionLogout.setText(getString(R.string.action_ok));
        dialogMessage.setText(getResources().getString(R.string.dialog_title_discard));

        actionCancel.setOnClickListener(view -> {
            dialog.dismiss();
        });

        actionLogout.setOnClickListener(view -> {
            dialog.dismiss();
            finish();
        });
        dialog.show();
    }

    public void showPasswordChangedDialog(Activity activity){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        TextView dialogMessage = dialog.findViewById(R.id.dialog_message);
        Button actionCancel = dialog.findViewById(R.id.action_cancel);
        Button actionLogout = dialog.findViewById(R.id.action_logout);

        actionLogout.setText(getString(R.string.action_ok));
        dialogMessage.setText(getResources().getString(R.string.process_update_password_success));

        actionCancel.setVisibility(View.GONE);
        actionLogout.setOnClickListener(view -> {
            dialog.dismiss();
//            new SPUser().clearSession(getApplicationContext());
//            new SPLogin().clearSession(getApplicationContext());
//
//            Intent finishIntent = new Intent(activity, MiruLoginActivity.class);
//            finishIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            finishIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            finishIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(finishIntent);
//            finish();
        });
        dialog.show();
    }

    protected void downloadDocumentFile(String invoiceId, String url){
        Log.e("InvoiceID Str", invoiceId);
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Mendownload file, Mohon tunggu...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl("http://35.240.148.132/");
        Retrofit retrofit = builder.client(httpClient.build()).build();

        FileDownloadService downloadService = retrofit.create(FileDownloadService.class);
        Call<ResponseBody> call = downloadService.downloadFileFromUrl(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "server contacted and has file");

                    if (writeResponseBodyToDisk(invoiceId, response.body())){
                        dialog.dismiss();
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        Log.e(TAG, "file download was a success? " + downloadedFilePDF.getAbsolutePath());
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(downloadedFilePDF));
                        startActivity(Intent.createChooser(intent, "Bagikan"));
                    }
                } else {
                    Log.e(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "error");
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Tidak dapat mendownload file", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static boolean writeResponseBodyToDisk(String invoiceId, ResponseBody body) {
        try {
            // todo change the file location/name according to your needs
            String extension = ".pdf";
            String extr =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
            File mFolder = new File(extr + "/Miru");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            Log.e("PATH", mFolder.getAbsolutePath());
            Log.e("FileName", invoiceId + extension);
            downloadedFilePDF = new File(mFolder.getAbsolutePath(),
                    invoiceId + extension
            );
            Log.e("PATH DOWNLOADED", downloadedFilePDF.getAbsolutePath());
            InputStream inputStream = null;
            FileOutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(downloadedFilePDF);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    Log.e("Write Response", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();
                return true;
            } catch (IOException e) {
                Log.e("Exception1", e.toString());
                e.printStackTrace();
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            Log.e("Exception2", e.toString());
            e.printStackTrace();
            return false;
        }
    }

    protected boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;
        }
    }

    protected void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

    }


}
