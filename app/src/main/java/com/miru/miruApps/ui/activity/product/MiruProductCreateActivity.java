package com.miru.miruApps.ui.activity.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.eventbus.EventProduct;
import com.miru.miruApps.model.retrieve.RetrieveProductData;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.product.CreateProductDescriptionFragment;
import com.miru.miruApps.ui.fragment.product.CreateProductPriceFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiruProductCreateActivity extends BaseActivity implements CreateProductDescriptionFragment.OnNextListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_main_create);
        ButterKnife.bind(this);

        setupActionBar();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_product);

    }

    private void setupViewPager(NonSwipeableViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(CreateProductDescriptionFragment.newInstance(), getResources().getString(R.string.tab_title_product_desc));
        mViewPagerAdapter.addFragment(CreateProductPriceFragment.newInstance(), getResources().getString(R.string.tab_title_product_price));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNext(RetrieveProductData retrieveProductData) {
        htabTabs.getTabAt(1).select();
        EventBus.getDefault().postSticky(new EventProduct(retrieveProductData));
    }
}
