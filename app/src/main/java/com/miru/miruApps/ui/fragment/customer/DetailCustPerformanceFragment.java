package com.miru.miruApps.ui.fragment.customer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ProductPerformanceAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.ProductPerformance;
import com.miru.miruApps.model.response.CustomerPerformance;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.FormatNumber;
import com.miru.miruApps.view.BarChartMarker;
import com.miru.miruApps.view.DayAxisValueFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;

public class DetailCustPerformanceFragment extends BaseFragment {

    @Inject InvoiceApiService mApiService;
    @BindView(R.id.payment_status) TextView paymentStatus;
    @BindView(R.id.ongoing_credit) TextView ongoingCredit;
    @BindView(R.id.pb_countdown) ProgressBar pbCountdown;
    @BindView(R.id.limit_credit) TextView limitCredit;
    @BindView(R.id.avrg_invoice_amount) TextView avrgInvoiceAmount;
    @BindView(R.id.invoice_quantity) TextView invoiceQuantity;
    @BindView(R.id.total_unpaid_amount) TextView totalUnpaidAmount;
    @BindView(R.id.total_unpaid_quantity) TextView totalUnpaidQuantity;
    @BindView(R.id.product_quantity) TextView productQuantity;
    @BindView(R.id.lv_content_product) RecyclerView lvContentProduct;
    @BindView(R.id.invoice_qty_per_year) TextView invoiceQtyPerYear;
    @BindView(R.id.chart_invoice_qty) BarChart chartInvoiceQty;
    @BindView(R.id.invoice_nominal_per_year) TextView invoiceNominalPerYear;
    @BindView(R.id.chart_invoice_nominal) BarChart chartInvoiceNominal;
    @BindView(R.id.indicator) View indicator;

    private ProductPerformanceAdapter mAdapter;

    public static DetailCustPerformanceFragment newInstance(String customerId) {
        Bundle bundle = new Bundle();
        bundle.putString("customerId", customerId);
        DetailCustPerformanceFragment detailCustPerformanceFragment = new DetailCustPerformanceFragment();
        detailCustPerformanceFragment.setArguments(bundle);
        return detailCustPerformanceFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_detail_performance, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new ProductPerformanceAdapter(mContext);
        lvContentProduct.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContentProduct.setAdapter(mAdapter);

        initDetailCustomerPerformance(getArgumentExtraCustomerId());
    }

    private String getArgumentExtraCustomerId() {
        return getArguments() != null ? getArguments().getString("customerId") : null;
    }

    private void initDetailCustomerPerformance(String argumentExtraCustomerId) {
        MaterialDialog dialog = new MaterialDialog.Builder(mContext)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        Observable<CustomerPerformance> customerPerformance = mApiService.getCustomerPerformance(
                new SPUser().getKeyUserClientId(mContext),
                argumentExtraCustomerId
        );

        customerPerformance.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<CustomerPerformance>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof Exception) {
                            Log.e("CustomerDetail", "onError..." + e.getMessage());
                            Log.e("CustomerDetail", "onError..." + e.getLocalizedMessage());
                            Log.e("CustomerDetail", "onError..." + e.getCause());
                            Log.e("CustomerDetail", "onError..." + e.getStackTrace().toString());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(CustomerPerformance customerPerformance) {
                        dialog.dismiss();
                        setupViews(customerPerformance);
                        setupInvoiceQuantityChartGraphic(customerPerformance);
                        setupInvoiceNominalChartGraphic(customerPerformance);
                    }
                });
    }

    private void setupViews(CustomerPerformance customerPerformance) {
        List<ProductPerformance> productPerformances = new ArrayList<>();
        for (int i = 0; i < customerPerformance.getProducts().size(); i++) {
            productPerformances.add(new ProductPerformance(
                    customerPerformance.getProducts().get(i).getItemSKU(),
                    customerPerformance.getProducts().get(i).getItemName(),
                    customerPerformance.getProducts().get(i).getTotalAmount()
            ));
        }
        mAdapter.pushData(productPerformances);

        int mOngoingCredit = 0;
        int mLimitedCredit = 0;
        if (customerPerformance.getSummary().getCreditLimit().contains("Rp ")){
            String mCreditLimit = FormatNumber.removeFromatCurrencyIDR(customerPerformance.getSummary().getCreditLimit());
            mOngoingCredit = Integer.parseInt(customerPerformance.getSummary().getSumOfUnpaidAmount());
            mLimitedCredit = Integer.parseInt(mCreditLimit);
        }

        float progress = (mOngoingCredit / mLimitedCredit) * 100;
        Log.e("OnGoingCredit", String.valueOf(mOngoingCredit));
        Log.e("LimitedCredit", String.valueOf(mLimitedCredit));
        Log.e("Progress", Math.round(progress) + "");

        pbCountdown.setProgress(Math.round(progress));
        Log.e("OnGoingCredit", String.valueOf(mOngoingCredit));
        Log.e("LimitedCredit", String.valueOf(mLimitedCredit));
        Log.e("Progress", Math.round(progress) + "");
        paymentStatus.setText(customerPerformance.getSummary().getPaymentStatus());
        if (customerPerformance.getSummary().getPaymentStatus().equalsIgnoreCase("Tidak Lancar")) {
            paymentStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorTextRed));
            indicator.setBackgroundResource(R.drawable.shape_circle_red);
        }

        limitCredit.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(customerPerformance.getSummary().getCreditLimit())));
        ongoingCredit.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(customerPerformance.getSummary().getSumOfUnpaidAmount())
        ));
        avrgInvoiceAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(customerPerformance.getSummary().getAvgOfInvoices())));
        invoiceQuantity.setText(FormatNumber.getFormatHashTag(
                customerPerformance.getSummary().getNumOfInvoices()));
        totalUnpaidAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(customerPerformance.getSummary().getSumOfUnpaidAmount())));
        totalUnpaidQuantity.setText(FormatNumber.getFormatHashTag(
                customerPerformance.getSummary().getNumOfUnpaidInvoices()));
        productQuantity.setText(String.valueOf(customerPerformance.getProducts().size()));
        invoiceQtyPerYear.setText(String.format("Total Jumlah Invoice Tahun %s",
                customerPerformance.getMonthly().get(DEFAULT_SIZE).getMonth().substring(3, 7)));
        invoiceNominalPerYear.setText(String.format("Total Nominal Invoice Tahun %s",
                customerPerformance.getMonthly().get(DEFAULT_SIZE).getMonth().substring(3, 7)));
    }

    private void setupInvoiceQuantityChartGraphic(CustomerPerformance customerPerformance) {
        chartInvoiceQty.setDrawBarShadow(false);
        chartInvoiceQty.setDrawValueAboveBar(true);
        chartInvoiceQty.getDescription().setEnabled(true);
        chartInvoiceQty.setMaxVisibleValueCount(14);
        chartInvoiceQty.setPinchZoom(false);
        chartInvoiceQty.setDrawGridBackground(false);

        IMarker marker = new BarChartMarker(mContext, R.layout.marker_bar_chart, "Invoice");

        // set the marker to the chart
        chartInvoiceQty.setMarker(marker);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chartInvoiceQty);

        XAxis xAxis = chartInvoiceQty.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(12);
        xAxis.setValueFormatter(xAxisFormatter);

        chartInvoiceQty.getAxisLeft().setDrawGridLines(false);
        chartInvoiceQty.animateY(1500);
        chartInvoiceQty.getLegend().setEnabled(false);
        chartInvoiceQty.getAxisLeft().setEnabled(false);
        chartInvoiceQty.getAxisRight().setEnabled(false);

        ArrayList<BarEntry> values = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < customerPerformance.getMonthly().size(); j++) {
                int month = getDetermineMonth(customerPerformance.getMonthly().get(j).getMonth().substring(0, 2));
                Log.e("Month is", String.valueOf(month) + "");
                float quantity = (float) Integer.parseInt(customerPerformance.getMonthly().get(j).getNumOfInvoices());
                values.add(new BarEntry(
                        month, quantity
                ));
            }
            values.add(new BarEntry(
                    i, 0L
            ));
        }

        Log.e("Values", values.size() + "");

        BarDataSet set1;
        if (chartInvoiceQty.getData() != null &&
                chartInvoiceQty.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chartInvoiceQty.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chartInvoiceQty.getData().notifyDataChanged();
            chartInvoiceQty.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Data Set");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            set1.setDrawValues(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            chartInvoiceQty.setData(data);
            chartInvoiceQty.setFitBars(true);
        }
    }

    private void setupInvoiceNominalChartGraphic(CustomerPerformance customerPerformance) {
        chartInvoiceNominal.setDrawBarShadow(false);
        chartInvoiceNominal.setDrawValueAboveBar(true);
        chartInvoiceNominal.getDescription().setEnabled(true);
        chartInvoiceNominal.setMaxVisibleValueCount(14);
        chartInvoiceNominal.setPinchZoom(false);
        chartInvoiceNominal.setDrawGridBackground(false);

        IMarker marker = new BarChartMarker(mContext, R.layout.marker_bar_chart, "Rupiah");

        // set the marker to the chart
        chartInvoiceNominal.setMarker(marker);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chartInvoiceNominal);

        XAxis xAxis = chartInvoiceNominal.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(12);
        xAxis.setValueFormatter(xAxisFormatter);

        chartInvoiceNominal.getAxisLeft().setDrawGridLines(false);
        chartInvoiceNominal.animateY(1500);
        chartInvoiceNominal.getLegend().setEnabled(false);
        chartInvoiceNominal.getAxisLeft().setEnabled(false);
        chartInvoiceNominal.getAxisRight().setEnabled(false);

        ArrayList<BarEntry> values = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < customerPerformance.getMonthly().size(); j++) {
                int month = getDetermineMonth(customerPerformance.getMonthly().get(j).getMonth().substring(0, 2));
                Log.e("Month is", String.valueOf(month) + "");
                float quantity = (float) Integer.parseInt(customerPerformance.getMonthly().get(j).getSumOfFinalAmount());
                values.add(new BarEntry(
                        month, quantity
                ));
            }
            values.add(new BarEntry(
                    i, 0L
            ));
        }

        Log.e("Values", values.size() + "");

        BarDataSet set1;
        if (chartInvoiceNominal.getData() != null &&
                chartInvoiceNominal.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chartInvoiceNominal.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chartInvoiceNominal.getData().notifyDataChanged();
            chartInvoiceNominal.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Data Set");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            set1.setDrawValues(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            chartInvoiceNominal.setData(data);
            chartInvoiceNominal.setFitBars(true);
        }
    }

    private int getDetermineMonth(String mMonth) {
        Log.e("Month", mMonth);
        if (mMonth.equalsIgnoreCase("01"))
            return 0;
        if (mMonth.equalsIgnoreCase("02"))
            return 1;
        if (mMonth.equalsIgnoreCase("03"))
            return 2;
        if (mMonth.equalsIgnoreCase("04"))
            return 3;
        if (mMonth.equalsIgnoreCase("05"))
            return 4;
        if (mMonth.equalsIgnoreCase("06"))
            return 5;
        if (mMonth.equalsIgnoreCase("07"))
            return 6;
        if (mMonth.equalsIgnoreCase("08"))
            return 7;
        if (mMonth.equalsIgnoreCase("09"))
            return 8;

        return Integer.parseInt(mMonth) - 1;
    }
}
