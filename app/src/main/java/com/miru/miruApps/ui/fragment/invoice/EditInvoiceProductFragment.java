package com.miru.miruApps.ui.fragment.invoice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.AddSoProductAdapter;
import com.miru.miruApps.data.db.Product;
import com.miru.miruApps.eventbus.invoice.EventInvoiceCustomer;
import com.miru.miruApps.model.CataloguePick;
import com.miru.miruApps.model.SoProduct;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.model.retrieve.RetrieveInvoiceInfoData;
import com.miru.miruApps.model.retrieve.RetrieveInvoiceProductData;
import com.miru.miruApps.ui.activity.product.MiruProductListDialogActivity;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.FormatNumber;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
public class EditInvoiceProductFragment extends BaseFragment {
    @BindView(R.id.so_subtotal) TextView soSubtotal;
    @BindView(R.id.so_subtotal_after_discount) TextView soSubtotalAfterDiscount;
    @BindView(R.id.input_so_discount) EditText inputSoDiscount;
    @BindView(R.id.so_discount_price) TextView soDiscountPrice;
    @BindView(R.id.input_so_tax) EditText inputSoTax;
    @BindView(R.id.so_tax_price) TextView soTaxPrice;
    @BindView(R.id.so_total_price) TextView soTotalPrice;
    @BindView(R.id.lv_content)
    RecyclerView lvContent;

    final String TAG = CreateInvoiceProductFragment.class.getCanonicalName();
    protected OnReviewListener mOnReviewListener;

    private RetrieveInvoiceInfoData retrieveInvoiceInfoData;
    private AddSoProductAdapter mSoAddProductAdapter;

    private List<SoProduct> soProductList = new ArrayList<>();

    private int finalDiscountPrice;
    private int finalTaxPrice;
    private int mSubtotalAmount = 0;
    private int mSubtotalAfterDiscountAmount = 0;
    private int finalPriceAfterDiscount;
    private int finalPriceAfterTax = 0;

    @OnClick(R.id.action_review)
    void onActionReview() {
        attempReview();
    }
    public static EditInvoiceProductFragment newInstance(Invoice invoice) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("invoice", invoice);
        EditInvoiceProductFragment editInvoiceProductFragment = new EditInvoiceProductFragment();
        editInvoiceProductFragment.setArguments(bundle);
        return editInvoiceProductFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_so_create_product, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAdapter();
        inputSoDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                calculateDiscount(editable.length() != 0 ? Integer.parseInt(editable.toString()) : 0);
            }
        });

        inputSoTax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                calculateTax(editable.length() != 0 ? Integer.parseInt(editable.toString()) : 0);

            }
        });

        setupViewsToEdit(getArguments().getParcelable("invoice"));
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(EventInvoiceCustomer eventInvoiceCustomer) {
        this.retrieveInvoiceInfoData = eventInvoiceCustomer.getRetrieveInvoiceInfoData();
        Log.e(TAG, "Implementation Running...");
        Log.e("Info", retrieveInvoiceInfoData.toString());
    }

    private void setupAdapter() {
        mSoAddProductAdapter = new AddSoProductAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_solid));
        lvContent.addItemDecoration(itemDecorator);
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mSoAddProductAdapter);

        mSoAddProductAdapter.setOnActionProductListener(new AddSoProductAdapter.OnActionProductListener() {
            @Override
            public void onAddProduct(Product product) {
                showDialogAddSoProduct(product);
            }

            @Override
            public void onQuantityChanged(int quantity) {
                mSubtotalAmount = 0;
                for (SoProduct soProduct : mSoAddProductAdapter.getListData()){
                    mSubtotalAmount += Integer.parseInt(soProduct.getTotalAmount());
                }
                soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
                soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
                finalPriceAfterDiscount = mSubtotalAmount;
                Log.e("Total Amount", String.valueOf(finalPriceAfterDiscount));
                Log.e("SubTotal Amount", String.valueOf(mSubtotalAmount));
            }

            @Override
            public void onDeleteSoProduct(int position) {
                removeCollection(position);
            }

            @Override
            public void onSearchCatlogue() {
                Intent intent = new Intent(mContext, MiruProductListDialogActivity.class);
                startActivityForResult(intent, 99);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if(resultCode == Activity.RESULT_OK){
                Log.e("onActivityResult", "Running...");
                CataloguePick result = data.getParcelableExtra("result");
                for (SoProduct soProduct : result.getSoProductList()){
                    soProductList.add(soProduct);
                }

                mSoAddProductAdapter.pushData(soProductList);
                lvContent.scrollToPosition(0);
                mSubtotalAmount = 0;
                for (SoProduct soProduct : mSoAddProductAdapter.getListData()){
                    mSubtotalAmount += Integer.parseInt(soProduct.getTotalAmount());
                }

                soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
                soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
                finalPriceAfterDiscount = mSubtotalAmount;
            }
        }
    }

    private void attempReview() {
        Log.e("productList", soProductList.toString());
        Log.e("subtotalPrice", String.valueOf(mSubtotalAmount));
        Log.e("discountPercent", inputSoDiscount.getText().toString());
        Log.e("finalDiscountPrice", String.valueOf(finalDiscountPrice));
        Log.e("finalPriceAfterDiscount", String.valueOf(finalPriceAfterDiscount));
        Log.e("taxPercent", inputSoTax.getText().toString());
        Log.e("finalTaxPrice", String.valueOf(finalTaxPrice));
        Log.e("finalPriceAfterTax", String.valueOf(finalPriceAfterTax));
        Toast.makeText(mContext, String.valueOf(finalPriceAfterTax), Toast.LENGTH_SHORT).show();
        Log.e("Customer", retrieveInvoiceInfoData.getmSalesName());
        Log.e("Date", retrieveInvoiceInfoData.getmInvoiceDate());
        Log.e("Note", retrieveInvoiceInfoData.getmDescription());

        soProductList.clear();
        soProductList = mSoAddProductAdapter.getListData();

        mOnReviewListener.onActionReviewproduct(new RetrieveInvoiceProductData(
                retrieveInvoiceInfoData,
                soProductList,
                mSubtotalAmount,
                mSubtotalAfterDiscountAmount,
                inputSoTax.getText().toString(),
                finalTaxPrice,
                inputSoDiscount.getText().toString(),
                finalDiscountPrice,
                finalPriceAfterTax
        ));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mOnReviewListener = (OnReviewListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public interface OnReviewListener {
        /**
         * Called by MenuFragment when a button item is selected
         *
         * @param "v"
         */
        void onActionReviewproduct(RetrieveInvoiceProductData retrieveInvoiceProductData);
    }

    private void calculateDiscount(int discount){
        finalDiscountPrice = discount * mSubtotalAmount / 100;
        finalPriceAfterDiscount = mSubtotalAmount - finalDiscountPrice;
        mSubtotalAfterDiscountAmount = finalPriceAfterDiscount;
        soDiscountPrice.setText(FormatNumber.getFormatCurrencyIDR(finalDiscountPrice));
        soSubtotalAfterDiscount.setText(FormatNumber.getFormatCurrencyIDR(finalPriceAfterDiscount));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(finalPriceAfterDiscount));
    }

    private void calculateTax(int tax){
        finalTaxPrice = tax * mSubtotalAfterDiscountAmount / 100;
        finalPriceAfterTax = mSubtotalAfterDiscountAmount + finalTaxPrice;
        soTaxPrice.setText(FormatNumber.getFormatCurrencyIDR(finalTaxPrice));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(finalPriceAfterTax));
    }

    private void showDialogAddSoProduct(Product product) {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_so_product_quantity);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        EditText inputProductQty = dialog.findViewById(R.id.input_product_quantity);
        Button actionAddProduct = dialog.findViewById(R.id.action_add_product);
        ImageButton actionDelete = dialog.findViewById(R.id.action_delete);

        actionDelete.setOnClickListener(view -> dialog.dismiss());
        actionAddProduct.setOnClickListener(view -> {
            dialog.dismiss();
            int discountPrice = Integer.parseInt(product.getProductDiscount()) * Integer.parseInt(product.getProductSellingPrice()) / 100;
            int finalPrice = Integer.parseInt(product.getProductSellingPrice()) - discountPrice;
            int totalAmountGlobal = Integer.parseInt(inputProductQty.getText().toString()) * finalPrice;
            mSubtotalAmount += totalAmountGlobal;

            soProductList.add(new SoProduct(
                    product.getProductSku(),
                    product.getProductName(),
                    inputProductQty.getText().toString(),
                    product.getProductUnit(),
                    product.getProductSellingPrice(),
                    product.getProductDiscount(),
                    String.valueOf(finalPrice),
                    String.valueOf(totalAmountGlobal)));

            mSoAddProductAdapter.pushData(soProductList);
            if (mSoAddProductAdapter.getItemCount() == 2) {
                lvContent.scrollToPosition(0);
            }
            soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
            soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
            finalPriceAfterDiscount = mSubtotalAmount;
            Log.e("Total Amount", String.valueOf(finalPriceAfterDiscount));
            Log.e("SubTotal Amount", String.valueOf(mSubtotalAmount));
        });
        dialog.show();
    }

    private void removeCollection(int position){
        soProductList.remove(position);

        mSubtotalAmount = 0;
        for (SoProduct soProduct : mSoAddProductAdapter.getListData()){
            mSubtotalAmount += Integer.parseInt(soProduct.getTotalAmount());
        }

        soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(mSubtotalAmount));
        finalPriceAfterDiscount = mSubtotalAmount;
        Log.e("Total Amount", String.valueOf(finalPriceAfterDiscount));
        Log.e("SubTotal Amount", String.valueOf(mSubtotalAmount));
    }

    private void setupViewsToEdit(Invoice invoice){

        for (int i = 0; i < invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().size(); i++){
            int discountPrice = Integer.parseInt(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getDiscount()) * invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getPricePerUnit() / 100;
            int finalPrice = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getPricePerUnit() - discountPrice;
            int totalAmountGlobal = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getQuantity() * finalPrice;

            soProductList.add(new SoProduct(
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getIdentifierId().getItemSKU(),
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getItemName(),
                    String.valueOf(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getQuantity()),
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getMeasurementUnit(),
                    String.valueOf(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getPricePerUnit()),
                    invoice.getInvoiceContainer().get(DEFAULT_SIZE).getItems().get(i).getDiscount(),
                    String.valueOf(finalPrice),
                    String.valueOf(totalAmountGlobal)));
        }

        mSoAddProductAdapter.pushData(soProductList);

        finalDiscountPrice = Integer.parseInt(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalDiscount());
        finalTaxPrice = Integer.parseInt(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTax());
        mSubtotalAmount = invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalAmount();

        calculateDiscount(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalDiscountInNum());
        calculateTax(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTaxInNum());
        soSubtotal.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalAmount()));
        soTotalPrice.setText(FormatNumber.getFormatCurrencyIDR(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getFinalAmount()));

        inputSoDiscount.setText(String.valueOf(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalDiscount()));
        inputSoTax.setText(String.valueOf(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTax()));
//        soDiscountPrice.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTotalDiscount());
//        soTaxPrice.setText(invoice.getInvoiceContainer().get(DEFAULT_SIZE).getInvoice().getTax());
    }
}
