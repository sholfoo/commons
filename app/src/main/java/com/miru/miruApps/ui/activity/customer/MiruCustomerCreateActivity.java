package com.miru.miruApps.ui.activity.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.eventbus.EventCustomer;
import com.miru.miruApps.model.retrieve.RetrieveCustomerData;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.customer.CreateCustInformationFragment;
import com.miru.miruApps.ui.fragment.customer.CreateCustLocationFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiruCustomerCreateActivity extends BaseActivity implements CreateCustInformationFragment.OnNextListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome(){
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_customer_main_create);
        ButterKnife.bind(this);

        setupActionBar();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_customer);

    }

    private void setupViewPager(NonSwipeableViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(CreateCustInformationFragment.newInstance(
                getExtraContactName(), getExtraContactNumber()), getResources().getString(R.string.tab_title_cust_info));
        mViewPagerAdapter.addFragment(CreateCustLocationFragment.newInstance(), getResources().getString(R.string.tab_title_cust_location));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNext(RetrieveCustomerData retrieveCustomerData) {
        htabTabs.getTabAt(1).select();
        EventBus.getDefault().postSticky(new EventCustomer(retrieveCustomerData));
    }

    private String getExtraContactName(){
        return getIntent().getStringExtra("contact_name");
    }

    private String getExtraContactNumber(){
        return getIntent().getStringExtra("contact_number");
    }
}
