package com.miru.miruApps.ui.activity.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.ui.activity.invoice.MiruMainEditInvoiceActivity;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.customer.DetailCustInvoiceFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustPaymentFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustPerformanceFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustProfileFragment;
import com.miru.miruApps.ui.fragment.customer.DetailCustSalesOrderFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MiruCustomerDetailActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.customer_image) CircleImageView customerImage;
    @BindView(R.id.customer_store_name) TextView customerStoreName;
    @BindView(R.id.customer_status) TextView customerStatus;
    @BindView(R.id.customer_type) TextView customerType;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) ViewPager htabViewpager;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome(){
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_customer_main_detail);
        ButterKnife.bind(this);

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViews();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupViews();
        setupViewPager(htabViewpager);
        htabTabs.setupWithViewPager(htabViewpager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_customer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_customer:
                Intent intentEdit = new Intent(this, MiruCustomerEditActivity.class);
                intentEdit.putExtra("customerId", getExtraCustomerId());
                startActivity(intentEdit);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void setupActionBar(){
        toolbarLogo.setText(getResources().getString(R.string.action_bar_detail_customer));
    }

    private void setupViews(){
        if (getExtraCustomerStatus().equalsIgnoreCase("deactive")){
            customerStatus.setBackgroundResource(R.drawable.shape_rounded_red);
        }
        customerStatus.setText(getExtraCustomerStatus());
        customerStoreName.setText(getExtraCustomerStoreName());
        customerType.setText(getExtraCustomerType());
        Glide.with(this)
                .load(getExtraCustomerStoreImage())
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .dontAnimate())
                .into(customerImage);
    }

    private void setupViewPager(ViewPager viewPager) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(DetailCustProfileFragment.newInstance(getExtraCustomerId()), getResources().getString(R.string.tab_title_cust_detail_profile));
        mViewPagerAdapter.addFragment(DetailCustPerformanceFragment.newInstance(getExtraCustomerId()), getResources().getString(R.string.tab_title_cust_detail_performance));
        mViewPagerAdapter.addFragment(DetailCustInvoiceFragment.newInstance(getExtraCustomerId()), getResources().getString(R.string.tab_title_cust_detail_invoice));
        mViewPagerAdapter.addFragment(DetailCustSalesOrderFragment.newInstance(getExtraCustomerId()), getResources().getString(R.string.tab_title_cust_detail_so));
        mViewPagerAdapter.addFragment(DetailCustPaymentFragment.newInstance(getExtraCustomerId()), getResources().getString(R.string.tab_title_cust_detail_payment));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    private String getExtraCustomerStoreName(){
        return getIntent().getStringExtra("customerStoreName");
    }

    private String getExtraCustomerType(){
        return getIntent().getStringExtra("customerType");
    }

    private String getExtraCustomerStoreImage(){
        return getIntent().getStringExtra("customerStoreImage");
    }

    private String getExtraCustomerId(){
        return getIntent().getStringExtra("customerId");
    }

    private String getExtraCustomerStatus(){
        return getIntent().getStringExtra("customerStatus");
    }
}
