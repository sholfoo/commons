package com.miru.miruApps.ui.activity.product.filtering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_PRICE;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_SHORT;

public class MiruProductShortingActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.spn_alphabet) MaterialBetterSpinner spnAlphabet;
    @BindView(R.id.spn_price) MaterialBetterSpinner spnPrice;
    @BindView(R.id.action_apply) Button actionApply;

    private static String[] alphabetShort = {"Alfabet A-Z", "Alfabet Z-A"};
    private static String[] priceShort = {"Termulah", "Termahal"};
    private String alphabetShortStr = "";
    private String priceShortStr = "";


    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.action_apply)
    void onActionApply(){
        if (spnAlphabet.getText().toString().equalsIgnoreCase(alphabetShort[0])){
            alphabetShortStr = "ASC";
        } else {
            alphabetShortStr = "DESC";
        }

        if (spnPrice.getText().toString().equalsIgnoreCase(priceShort[0])){
            priceShortStr = "ASC";
        } else {
            priceShortStr = "DESC";
        }

        Intent data = new Intent();

        // Add the required data to be returned to the MainActivity
        data.putExtra(EXTRA_PRODUCT_SHORT, alphabetShortStr);
        data.putExtra(EXTRA_PRODUCT_PRICE, priceShortStr);
        // Set the resultCode to Activity.RESULT_OK to
        // indicate a success and attach the Intent
        // which contains our result data
        setResult(Activity.RESULT_OK, data);
        finish();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_shorting);
        ButterKnife.bind(this);

        setupActionBar();
        setupSpinner();
    }

    private void setupActionBar() {
        actionHome.setImageResource(R.drawable.ic_action_bar_close);
        toolbarLogo.setText(getResources().getString(R.string.action_bar_shorting));
    }

    private void setupSpinner(){
        ArrayAdapter<String> alphabetAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, alphabetShort);
        ArrayAdapter<String> priceAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, priceShort);
        spnAlphabet.setAdapter(alphabetAdapter);
        spnPrice.setAdapter(priceAdapter);
    }
}
