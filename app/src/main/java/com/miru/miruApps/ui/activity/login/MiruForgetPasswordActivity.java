package com.miru.miruApps.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiruForgetPasswordActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.action_btn_send_email) Button actionBtnSendEmail;
    @BindView(R.id.action_back_to_login) Button actionBackToLogin;

    private FirebaseAuth mAuth;

    @OnClick(R.id.action_home)
    void onActionHome(){
        finish();
    }

    @OnClick(R.id.action_back_to_login)
    void onActionBackToLogin() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_forget_password);
        ButterKnife.bind(this);

        setupActionBar();
        configureFirebaseAuth();
    }

    private void setupActionBar() {
        toolbarLogo.setText(getResources().getString(R.string.action_forgot_password));
    }

    private void configureFirebaseAuth() {
        mAuth = FirebaseAuth.getInstance();
    }

    private void sendResetPasswordAuth(String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        startActivity(new Intent(MiruForgetPasswordActivity.this,
                                MiruForgetPasswordConfirmationActivity.class));
                    } else {
                        Toast.makeText(MiruForgetPasswordActivity.this,
                                "Fail to send reset password email!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
