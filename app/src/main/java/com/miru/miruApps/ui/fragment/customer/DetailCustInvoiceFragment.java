package com.miru.miruApps.ui.fragment.customer;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.InvoiceAdapter;
import com.miru.miruApps.adapter.UnpaidInvoiceAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.activity.invoice.MiruMainCreateInvoiceActivity;
import com.miru.miruApps.ui.base.BaseFragment;
import com.miru.miruApps.utils.FormatNumber;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.EXTRA_FROM_CUSTOMER;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class DetailCustInvoiceFragment extends BaseFragment {

    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.ly_error) LinearLayout lyError;
    @BindView(R.id.action_filter) TextView actionFilter;
    @BindView(R.id.action_shorting) TextView actionShorting;
    @BindView(R.id.ly_shape_circle_blue) View lyShapeCircleBlue;
    @BindView(R.id.unpaid_quantity) TextView unpaidQuantity;
    @BindView(R.id.ly_shape_circle_unpaid_transparent) View lyShapeCircleUnpaidTransparent;
    @BindView(R.id.unpaid_amount) TextView unpaidAmount;
    @BindView(R.id.ly_action_unpaid) LinearLayout lyActionUnpaid;
    @BindView(R.id.ly_shape_circle_green) View lyShapeCircleGreen;
    @BindView(R.id.paid_quantity) TextView paidQuantity;
    @BindView(R.id.ly_shape_circle_paid_transparent) View lyShapeCirclePaidTransparent;
    @BindView(R.id.paid_amount) TextView paidAmount;
    @BindView(R.id.ly_action_paid) LinearLayout lyActionPaid;
    @BindView(R.id.header) RecyclerViewHeader header;
    @BindView(R.id.ly_list) FrameLayout lyList;

    @Inject InvoiceApiService mApiService;

    private InvoiceAdapter mAdapter;
    private UnpaidInvoiceAdapter unpaidInvoiceAdapter;
    private Invoice mInvoice;

    @OnClick(R.id.ly_action_unpaid)
    void onActionLayoutUnpaid() {
        showDialogUnpaidInvoices();
    }

    @OnClick(R.id.action_create_invoice)
    void onActionCreateInvoice(){
        startActivity(new Intent(getActivity(),
                MiruMainCreateInvoiceActivity.class)
                .putExtra("extra_from_id", EXTRA_FROM_CUSTOMER));
    }

    public static DetailCustInvoiceFragment newInstance(String customerId) {
        Bundle bundle = new Bundle();
        bundle.putString("customerId", customerId);
        DetailCustInvoiceFragment detailCustInvoiceFragment = new DetailCustInvoiceFragment();
        detailCustInvoiceFragment.setArguments(bundle);
        return detailCustInvoiceFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MiruApp.getmComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cust_detail_invoice, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new InvoiceAdapter(mContext);
        lvContent.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);
        header.attachTo(lvContent);

        lyRefresh.setColorSchemeResources(R.color.colorAccent);
        lyRefresh.setOnRefreshListener(this::initializeCustomerInvoices);

        initializeCustomerInvoices();
    }

    private void setupViews(Invoice invoice){
        unpaidQuantity.setText(String.valueOf(invoice.getSummary().getNumOfUnpaidInvoice()));
        unpaidAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(invoice.getSummary().getSumOfUnpaidInvoice())));
        paidQuantity.setText(String.valueOf(invoice.getSummary().getNumOfPaidInvoice()));
        paidAmount.setText(FormatNumber.getFormatCurrencyIDR(
                Integer.parseInt(invoice.getSummary().getSumOfPaidInvoice())));

    }

    private String getArgumentExtraCustomerId() {
        return getArguments() != null ? getArguments().getString("customerId") : null;
    }

    private void initializeCustomerInvoices() {
        lyList.setVisibility(View.GONE);
        pbLoad.setVisibility(View.VISIBLE);
        Observable<Invoice> invoiceObservable = mApiService.getCustomerInvoice(
                new SPUser().getKeyUserClientId(mContext),
                10,
                getArgumentExtraCustomerId());

        invoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Invoice>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(mContext,
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(mContext,
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Invoice invoice) {
                        if (invoice.getStatusCode() == STATUS_SUCCESS) {
                            mInvoice = invoice;
                            setupViews(invoice);
                            mAdapter.pushData(invoice.getInvoiceContainer());
                            resetUILoading();
                        } else {
                            showErrorView();
                        }
                    }
                });
    }

    protected void resetUILoading() {
        hideErrorView();
        if (isAdded()) {
            pbLoad.setVisibility(View.GONE);
            lyList.setVisibility(View.VISIBLE);
            lyRefresh.setRefreshing(false);
        }
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        if (isAdded()) {
            lyRefresh.setRefreshing(false);
            pbLoad.setVisibility(View.GONE);
            lyList.setVisibility(View.GONE);
            lyError.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(v -> {
                hideErrorView();
                initializeCustomerInvoices();
            });

            if (!TextUtils.isEmpty(text))
                txtNoData.setText(getString(R.string.error_no_data));
        }
    }

    protected void hideErrorView() {
        if (isAdded()) {
            lyError.setVisibility(View.GONE);
        }
    }

    private void showDialogUnpaidInvoices() {
        Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invoice_unpaid);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        RecyclerView lvContentUnpaid = dialog.findViewById(R.id.lv_content_unpaid);
        ImageButton closeDialog = dialog.findViewById(R.id.action_close_dialog);

        unpaidInvoiceAdapter = new UnpaidInvoiceAdapter(mContext);
        lvContentUnpaid.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_solid));
        lvContentUnpaid.addItemDecoration(itemDecorator);
        lvContentUnpaid.setAdapter(unpaidInvoiceAdapter);
        unpaidInvoiceAdapter.pushData(mInvoice.getSummary().getInvoiceDetails());

        closeDialog.setOnClickListener((View view) -> dialog.dismiss());
        dialog.show();
    }
}
