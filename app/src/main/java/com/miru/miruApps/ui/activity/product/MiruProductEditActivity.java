package com.miru.miruApps.ui.activity.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.ViewPagerAdapter;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.eventbus.EventProduct;
import com.miru.miruApps.model.response.DetailProduct;
import com.miru.miruApps.model.retrieve.RetrieveProductData;
import com.miru.miruApps.network.MasterApiService;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.ui.fragment.product.CreateProductDescriptionFragment;
import com.miru.miruApps.ui.fragment.product.CreateProductPriceFragment;
import com.miru.miruApps.ui.fragment.product.EditProductDescFragment;
import com.miru.miruApps.ui.fragment.product.EditProductPriceFragment;
import com.miru.miruApps.view.NonSwipeableViewPager;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruProductEditActivity extends BaseActivity implements EditProductDescFragment.OnNextListener{

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.htab_tabs) TabLayout htabTabs;
    @BindView(R.id.htab_viewpager) NonSwipeableViewPager htabViewpager;

    @Inject MasterApiService mMasterApiService;

    private ViewPagerAdapter mViewPagerAdapter;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_product_main_create);
        ButterKnife.bind(this);
        MiruApp.getmComponent().inject(this);


        setupActionBar();
        initDetailProduct();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_create_product);

    }

    private void setupViewPager(NonSwipeableViewPager viewPager, DetailProduct detailProduct) {
        mViewPagerAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        mViewPagerAdapter.addFragment(EditProductDescFragment.newInstance(detailProduct), getResources().getString(R.string.tab_title_product_desc));
        mViewPagerAdapter.addFragment(EditProductPriceFragment.newInstance(detailProduct), getResources().getString(R.string.tab_title_product_price));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onActionNext(RetrieveProductData retrieveProductData) {
        htabTabs.getTabAt(1).select();
        EventBus.getDefault().postSticky(new EventProduct(retrieveProductData));
    }

    private String getExtraItemSKU() {
        return getIntent().getStringExtra("itemSKU");
    }

    private void initDetailProduct() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Collecting data, Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();

        RequestBody requestClientId = createRequestBody(new SPUser().getKeyUserClientId(this));
        RequestBody requestItemSku = createRequestBody(getExtraItemSKU());
        Observable<DetailProduct> detailProductObservable = mMasterApiService.getProductDetail(
                requestClientId,
                requestItemSku);
        detailProductObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DetailProduct>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNext(DetailProduct detailProduct) {
                        if (detailProduct.getStatusCode() == STATUS_SUCCESS) {
                            Log.e("Response", detailProduct.toString());
                            setupViewPager(htabViewpager, detailProduct);
                            htabTabs.setupWithViewPager(htabViewpager);
                        }
                    }
                });
    }

}
