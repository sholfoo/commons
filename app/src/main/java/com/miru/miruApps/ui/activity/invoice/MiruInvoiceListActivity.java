package com.miru.miruApps.ui.activity.invoice;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.miru.miruApps.MiruApp;
import com.miru.miruApps.R;
import com.miru.miruApps.adapter.InvoiceAdapter;
import com.miru.miruApps.adapter.InvoiceShortingAdapter;
import com.miru.miruApps.adapter.ProductShortingAdapter;
import com.miru.miruApps.adapter.UnpaidInvoiceAdapter;
import com.miru.miruApps.data.db.InvoiceDao;
import com.miru.miruApps.data.db.Product;
import com.miru.miruApps.data.db.ProductDao;
import com.miru.miruApps.data.preference.SPUser;
import com.miru.miruApps.model.ProductShort;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.network.InvoiceApiService;
import com.miru.miruApps.ui.activity.invoice.filtering.MiruInvoiceFilterActivity;
import com.miru.miruApps.ui.activity.invoice.filtering.MiruInvoiceSortingActivity;
import com.miru.miruApps.ui.base.BaseActivity;
import com.miru.miruApps.utils.DateTimeUtil;
import com.miru.miruApps.utils.FormatNumber;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.miru.miruApps.config.Constants.DEFAULT_SIZE;
import static com.miru.miruApps.config.Constants.EXTRA_INVOCE_TIME_CREATE;
import static com.miru.miruApps.config.Constants.EXTRA_INVOICE_DATE;
import static com.miru.miruApps.config.Constants.EXTRA_INVOICE_SHORT;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_PRICE;
import static com.miru.miruApps.config.Constants.EXTRA_PRODUCT_SHORT;
import static com.miru.miruApps.config.Constants.FORMAT_DATE;
import static com.miru.miruApps.config.Constants.ORDER_ASC;
import static com.miru.miruApps.config.Constants.ORDER_DESC;
import static com.miru.miruApps.config.Constants.STATUS_ERROR;
import static com.miru.miruApps.config.Constants.STATUS_SUCCESS;

public class MiruInvoiceListActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener{

    private static final int FILTER_REQ = 1001;
    private static final int SHORTING_REQ = 1002;

    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.lvContent) RecyclerView lvContent;
    @BindView(R.id.lyRefresh) SwipeRefreshLayout lyRefresh;
    @BindView(R.id.date_day) TextView dateDay;
    @BindView(R.id.date_days_of_month) TextView dateDaysOfMonth;
    @BindView(R.id.date_month_year) TextView dateMonthYear;
    @BindView(R.id.unpaid_quantity) TextView unpaidQuantity;
    @BindView(R.id.unpaid_amount) TextView unpaidAmount;
    @BindView(R.id.ly_action_unpaid) LinearLayout lyActionUnpaid;
    @BindView(R.id.paid_quantity) TextView paidQuantity;
    @BindView(R.id.paid_amount) TextView paidAmount;
    @BindView(R.id.header) RecyclerViewHeader header;
    @BindView(R.id.ly_list) FrameLayout lyList;
    @BindView(R.id.pbLoad) ProgressBar pbLoad;
    @BindView(R.id.imgContent) ImageView imgContent;
    @BindView(R.id.txtNoData) TextView txtNoData;
    @BindView(R.id.btn_retry) Button btnRetry;
    @BindView(R.id.action_change_date) LinearLayout actionChooseDate;

    @Inject InvoiceApiService mApiService;

    private InvoiceAdapter mAdapter;
    private InvoiceShortingAdapter mInvoiceShortingAdapter;
    private InvoiceDao invoiceDao;
    private UnpaidInvoiceAdapter unpaidInvoiceAdapter;
    private Invoice mInvoice;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @OnClick(R.id.ly_action_unpaid)
    void onActionLayoutUnpaid() {
        showDialogUnpaidInvoices();
    }

    @OnClick(R.id.action_change_date)
    void onActionChangeDate(){
        createDatePickerDialog();
    }

    @OnClick(R.id.action_filter)
    void onActionFilter() {
        Intent intentFilter = new Intent(this, MiruInvoiceFilterActivity.class);
        startActivityForResult(intentFilter, FILTER_REQ);
    }

    @OnClick(R.id.action_shorting)
    void onActionShorting() {
        Intent intentFilter = new Intent(this, MiruInvoiceSortingActivity.class);
        startActivityForResult(intentFilter, SHORTING_REQ);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invoiceDao.deleteAll();
        initializeInvoices();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_invoice_list);
        ButterKnife.bind(this);

        MiruApp.getmComponent().inject(this);
        invoiceDao = MiruApp.getDaoSession().getInvoiceDao();

        setSupportActionBar(lyToolbar);
        setupActionBar();
        setupViewsAndAdapter();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_invoice);
        Drawable img = getResources().getDrawable(R.drawable.ic_action_profile_menu_icon);
        toolbarLogo.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        toolbarLogo.setCompoundDrawablePadding(16);
    }

    private void setupViewsAndAdapter() {
//        dateDay.setText(DateTimeUtil.getCurrentFormat("EEE"));
//        dateDaysOfMonth.setText(DateTimeUtil.getCurrentFormat("dd"));
//        dateMonthYear.setText(
//                String.format("%s %s",
//                        DateTimeUtil.getCurrentFormat("MMM"),
//                        DateTimeUtil.getCurrentFormat("yyyy"))
//        );

        mAdapter = new InvoiceAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setHasFixedSize(true);
        lvContent.setAdapter(mAdapter);
        header.attachTo(lvContent);

        lyRefresh.setColorSchemeResources(R.color.colorAccent);
        lyRefresh.setOnRefreshListener(() -> initializeInvoices());

        initializeInvoices();
    }

    protected void createDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(
                this,
                R.style.MiruApp_DatePicker,
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.show();
    }

    private void initializeInvoices() {
        pbLoad.setVisibility(View.VISIBLE);
        lyList.setVisibility(View.GONE);
        Observable<Invoice> invoiceObservable = mApiService.getInvoiceList(new SPUser().getKeyUserClientId(this),
                DEFAULT_SIZE);

        invoiceObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Invoice>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        resetUILoading();
                        if (e instanceof HttpException) {
                            Toast.makeText(getApplicationContext(),
                                    ((HttpException) e).response().message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        if (e instanceof IOException) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.error_network_failure),
                                    Toast.LENGTH_SHORT).show();
                        }

                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Invoice invoice) {
                        if (invoice.getStatusCode() == STATUS_SUCCESS) {
                            mInvoice = invoice;
                            saveToSQLiteDatabase(invoice);
                            Collections.reverse(invoice.getInvoiceContainer());
                            mAdapter.pushData(invoice.getInvoiceContainer());
                            unpaidQuantity.setText(
                                    String.valueOf(invoice.getSummary().getNumOfUnpaidInvoice()));
                            unpaidAmount.setText(
                                    FormatNumber.getFormatCurrencyIDR(
                                            Integer.parseInt(invoice.getSummary()
                                                    .getSumOfUnpaidInvoice())));
                            paidQuantity.setText(
                                    String.valueOf(invoice.getSummary().getNumOfPaidInvoice()));
                            paidAmount.setText(
                                    FormatNumber.getFormatCurrencyIDR(
                                            Integer.parseInt(invoice.getSummary()
                                                    .getSumOfPaidInvoice())));
                            resetUILoading();
                        } else if (invoice.getStatusCode() == STATUS_ERROR){
                            showErrorView("Belum ada Invoice");
                        }else
                            showErrorView();
                    }
                });
    }

    private void saveToSQLiteDatabase(Invoice invoice) {
        for (int i = 0; i < invoice.getInvoiceContainer().size(); i++) {
            com.miru.miruApps.data.db.Invoice invoiceDb = new com.miru.miruApps.data.db.Invoice();
            invoiceDb.setInvoiceId(invoice.getInvoiceContainer().get(i).getInvoice().getInvoiceId());
            invoiceDb.setExtInvoiceId(invoice.getInvoiceContainer().get(i).getInvoice().getExtInvoiceId());
            invoiceDb.setSalesName(invoice.getInvoiceContainer().get(i).getInvoice().getSalesName());
            invoiceDb.setInvoiceDate(invoice.getInvoiceContainer().get(i).getInvoice().getInvoiceDate());
            invoiceDb.setFinalAmount(invoice.getInvoiceContainer().get(i).getInvoice().getFinalAmount());
            invoiceDb.setRemainingAmount(invoice.getInvoiceContainer().get(i).getInvoice().getRemainingAmount());
            invoiceDao.insert(invoiceDb);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_menu_with_search, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setQueryHint("Cari");
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.trim().isEmpty()) {
                    initializeInvoices();
                }

                mAdapter.getFilter().filter(query);
                searchViewAndroidActionBar.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().isEmpty()) {
                    initializeInvoices();
                }
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_new:
                startActivity(new Intent(this, MiruMainCreateInvoiceActivity.class));
                return true;
            case R.id.action_search:
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void showDialogUnpaidInvoices() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invoice_unpaid);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        RecyclerView lvContentUnpaid = dialog.findViewById(R.id.lv_content_unpaid);
        ImageButton closeDialog = dialog.findViewById(R.id.action_close_dialog);

        unpaidInvoiceAdapter = new UnpaidInvoiceAdapter(this);
        lvContentUnpaid.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(getResources().getDrawable(R.drawable.line_divider_solid));
        lvContentUnpaid.addItemDecoration(itemDecorator);
        lvContentUnpaid.setAdapter(unpaidInvoiceAdapter);
        unpaidInvoiceAdapter.pushData(mInvoice.getSummary().getInvoiceDetails());

        closeDialog.setOnClickListener((View view) -> dialog.dismiss());
        dialog.show();
    }


    protected void resetUILoading() {
        hideErrorView();
        pbLoad.setVisibility(View.GONE);
        lyList.setVisibility(View.VISIBLE);
        lyRefresh.setRefreshing(false);
    }

    protected void showErrorView() {
        showErrorView(null);
    }

    protected void showErrorView(String text) {
        pbLoad.setVisibility(View.GONE);
        lyRefresh.setRefreshing(false);
        txtNoData.setVisibility(View.VISIBLE);
        imgContent.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(v -> {
            hideErrorView();
            initializeInvoices();
        });

        if (!TextUtils.isEmpty(text))
            txtNoData.setText(text);
    }

    protected void hideErrorView() {
        txtNoData.setVisibility(View.GONE);
        imgContent.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTER_REQ) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
            }
        } else if (requestCode == SHORTING_REQ) {
            if (resultCode == Activity.RESULT_OK){
                final String timeCreateShort = data.getStringExtra(EXTRA_INVOCE_TIME_CREATE);
                final String invoiceDateShort = data.getStringExtra(EXTRA_INVOICE_DATE);
                final String alphabetShort = data.getStringExtra(EXTRA_INVOICE_SHORT);

                Log.e("Result", timeCreateShort + "|" + invoiceDateShort + "|" + alphabetShort);

                setupShortingAdapter(alphabetShort, timeCreateShort, invoiceDateShort);
            }
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthStr = (monthOfYear + 1) < 10 ? "0" + (monthOfYear + 1) : "" + (monthOfYear + 1);
        String yearStr = String.valueOf(year);

        String dateStr = dayStr + "-" + monthStr + "-" + yearStr;
        dateDay.setText(DateTimeUtil.getConvertedFormat(dateStr, FORMAT_DATE, "EEE"));
        dateDaysOfMonth.setText(DateTimeUtil.getConvertedFormat(dateStr, FORMAT_DATE, "dd"));
        dateMonthYear.setText(DateTimeUtil.getConvertedFormat(dateStr, FORMAT_DATE, "MMM yyyy"));
//        initializeDashboardSummaries(DateTimeUtil.getConvertedFormat(dateStr,
//                FORMAT_DATE,
//                "MM-yyyy"));
    }

    private void setupShortingAdapter(String alphabetShort, String timeCreateShort, String invoiceDateShort) {
        mInvoiceShortingAdapter = new InvoiceShortingAdapter(this);
        lvContent.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        lvContent.setAdapter(mInvoiceShortingAdapter);

        invoiceDao = MiruApp.getDaoSession().getInvoiceDao();
//        if (alphabetShort.equalsIgnoreCase(ORDER_ASC) && timeCreateShort.equalsIgnoreCase(ORDER_ASC) && invoiceDateShort.equalsIgnoreCase(ORDER_ASC)){
//            productQuery = invoiceDao
//                    .queryBuilder()
//                    .orderAsc(InvoiceDao.Properties.SalesName, ProductDao.Properties.ProductBasicPrice)
//                    .build();
//        }
//
//        if (alphabetShort.equalsIgnoreCase(ORDER_DESC) && priceShort.equalsIgnoreCase(ORDER_DESC)){
//            productQuery = productDao
//                    .queryBuilder()
//                    .orderDesc(ProductDao.Properties.ProductName, ProductDao.Properties.ProductBasicPrice)
//                    .build();
//        }
//
//        if (alphabetShort.equalsIgnoreCase(ORDER_ASC) && priceShort.equalsIgnoreCase(ORDER_DESC)){
//            productQuery = productDao
//                    .queryBuilder()
//                    .orderAsc(ProductDao.Properties.ProductName)
//                    .orderDesc(ProductDao.Properties.ProductBasicPrice)
//                    .build();
//        }
//
//        if (alphabetShort.equalsIgnoreCase(ORDER_DESC) && priceShort.equalsIgnoreCase(ORDER_ASC)){
//            productQuery = productDao
//                    .queryBuilder()
//                    .orderDesc(ProductDao.Properties.ProductName)
//                    .orderAsc(ProductDao.Properties.ProductBasicPrice)
//                    .build();
//        }
//
//        QueryBuilder.LOG_SQL = true;
//        QueryBuilder.LOG_VALUES = true;
//
//        List<Product> productList = productQuery.list();
//        List<ProductShort> productShortList = new ArrayList<>();
//        for (com.miru.miruApps.data.db.Product product : productList){
//            productShortList.add(new ProductShort(
//                    String.valueOf(product.getId()),
//                    product.getProductSku(),
//                    product.getProductName(),
//                    product.getProductKategory(),
//                    product.getProductDescription(),
//                    product.getProductImages(),
//                    product.getProductBrand(),
//                    product.getProductSize(),
//                    product.getProductBasicPrice(),
//                    product.getProductSellingPrice(),
//                    product.getProductDiscount()
//            ));
//        }
//
//        mProductShortingAdapter.pushData(productShortList);
    }


}
