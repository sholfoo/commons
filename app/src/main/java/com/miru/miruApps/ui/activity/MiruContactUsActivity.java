package com.miru.miruApps.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.miru.miruApps.R;
import com.miru.miruApps.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MiruContactUsActivity extends BaseActivity {

    @BindView(R.id.action_home) ImageView actionHome;
    @BindView(R.id.toolbar_logo) TextView toolbarLogo;
    @BindView(R.id.ly_toolbar) Toolbar lyToolbar;
    @BindView(R.id.imageView) ImageView imageView;

    @OnClick(R.id.action_home)
    void onActionHome() {
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miru_contact_us);
        ButterKnife.bind(this);

        setupActionBar();
    }

    private void setupActionBar() {
        toolbarLogo.setText(R.string.action_bar_contact_us);
    }
}
