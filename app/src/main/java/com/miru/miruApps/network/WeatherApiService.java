package com.miru.miruApps.network;

import com.miru.miruApps.model.Weather;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface WeatherApiService {

    @GET("data/2.5/weather")
    Observable<Weather> getCurrentWeather(
            @Query("lat") String latitude,
            @Query("lon") String longitude,
            @Query("appid") String appId
    );

}
