package com.miru.miruApps.network;

import com.miru.miruApps.model.response.AddProduct;
import com.miru.miruApps.model.response.CreateCategory;
import com.miru.miruApps.model.response.CreateCustomer;
import com.miru.miruApps.model.response.Customer;
import com.miru.miruApps.model.response.DetailCustomer;
import com.miru.miruApps.model.response.DetailProduct;
import com.miru.miruApps.model.response.EditCustomer;
import com.miru.miruApps.model.response.EditProduct;
import com.miru.miruApps.model.response.Product;
import com.miru.miruApps.model.response.ProductCategory;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface MasterApiService {

    @Multipart
    @POST("customer/addNewCustomer")
    Observable<CreateCustomer> createCustomer(
            @Part("clientId") RequestBody clientId,
            @Part("storeName") RequestBody storeName,
            @Part("ownerName") RequestBody ownerName,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("province") RequestBody province,
            @Part("postalCode") RequestBody postalCode,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("mobilePhone") RequestBody mobileNumber,
            @Part("emailAddress") RequestBody emailAddress,
            @Part("status") RequestBody status,
            @Part("customerType") RequestBody customerType,
            @Part("customerTag") RequestBody tags,
            @Part("description") RequestBody description,
            @Part("creditLimit") RequestBody creditLimit,
            @Part MultipartBody.Part[] docImage
    );

    @Multipart
    @POST("customer/editCustomer")
    Observable<EditCustomer> updateCustomer(
            @Part("customerId") RequestBody customerId,
            @Part("clientId") RequestBody clientId,
            @Part("storeName") RequestBody storeName,
            @Part("ownerName") RequestBody ownerName,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("province") RequestBody province,
            @Part("postalCode") RequestBody postalCode,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("mobilePhone") RequestBody mobileNumber,
            @Part("emailAddress") RequestBody emailAddress,
            @Part("status") RequestBody status,
            @Part("customerType") RequestBody customerType,
            @Part("customerTag") RequestBody tags,
            @Part("description") RequestBody description,
            @Part("creditLimit") RequestBody creditLimit,
            @Part MultipartBody.Part[] docImage
    );

    @GET("customer/getCustomerMasterData")
    Observable<Customer> getCustomers(
            @Query("clientId") String clientId
    );

    @GET("customer/getCustomerMasterDataByCustId")
    Observable<DetailCustomer> getCustomerDetailInfo(
            @Query("clientId") String clientId,
            @Query("customerId") String customerId
    );

    @Multipart
    @POST("product/addNewProduct")
    Observable<AddProduct> createProduct(
            @Part("clientId") RequestBody clientId,
            @Part("itemSKU") RequestBody productSku,
            @Part("productName") RequestBody productName,
            @Part("category") RequestBody productCategory,
            @Part("measurementUnit") RequestBody productUnit,
            @Part("pricePerUnit") RequestBody productPrice,
            @Part("sellingPrice") RequestBody sellingPrice,
            @Part("discount") RequestBody productDiscount,
            @Part("brand") RequestBody productBrand,
            @Part("size") RequestBody productSize,
            @Part("description") RequestBody productDescription,
            @Part MultipartBody.Part[] docImage
    );

    @Multipart
    @POST("product/updateProductDetail")
    Observable<EditProduct> updateProduct(
            @Part("productId") RequestBody productId,
            @Part("clientId") RequestBody clientId,
            @Part("itemSKU") RequestBody productSku,
            @Part("productName") RequestBody productName,
            @Part("category") RequestBody productCategory,
            @Part("measurementUnit") RequestBody productUnit,
            @Part("pricePerUnit") RequestBody productPrice,
            @Part("sellingPrice") RequestBody sellingPrice,
            @Part("discount") RequestBody productDiscount,
            @Part("brand") RequestBody productBrand,
            @Part("size") RequestBody productSize,
            @Part("description") RequestBody productDescription,
            @Part MultipartBody.Part[] docImage
    );

    @Multipart
    @POST("product/getAllClientProduct")
    Observable<Product> getProducts(
            @Part("clientId") RequestBody clientId
    );

    @Multipart
    @POST("product/getProductDetail")
    Observable<DetailProduct> getProductDetail(
            @Part("clientId") RequestBody clientId,
            @Part("itemSKU") RequestBody itemSku
    );

    @GET("product/getProductCategory")
    Observable<ProductCategory> getProductCategory(
            @Query("clientId") String clientId
    );

    @Multipart
    @POST("product/addProductCategory")
    Observable<CreateCategory> addProductCategory(
            @Part("clientId") RequestBody clientId,
            @Part("category") RequestBody category
    );

}
