package com.miru.miruApps.network;

import com.miru.miruApps.model.response.CreateSo;
import com.miru.miruApps.model.response.DeleteSO;
import com.miru.miruApps.model.response.Notification;
import com.miru.miruApps.model.response.SalesOrder;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface SoApiService {

    @Multipart
    @POST("salesOrder/newSalesOrder")
    Observable<CreateSo> createSalesOrder(
            @Part("clientId") RequestBody clientId,
            @Part("customerId") RequestBody customerId,
            @Part("salesName") RequestBody salesName,
            @Part("tax") RequestBody tax,
            @Part("totalAmount") RequestBody totalAmount,
            @Part("notes") RequestBody notes,
            @Part("itemSKU") RequestBody itemSku,
            @Part("itemName") RequestBody itemName,
            @Part("quantity") RequestBody quantity,
            @Part("measurementUnit") RequestBody measurementUnit,
            @Part("pricePerUnit") RequestBody pricePerUnit,
            @Part("discount") RequestBody discount,
            @Part("finalDiscount") RequestBody finalDiscount,
            @Part("finalAmount") RequestBody finalAmount,
            @Part("salesOrderDate") RequestBody salesOrderDate,
            @Part MultipartBody.Part signature
    );

    @Multipart
    @POST("salesOrder/newSalesOrder")
    Observable<CreateSo> updateSalesOrder(
            @Part("salesOrderId") RequestBody salesOrderId,
            @Part("clientId") RequestBody clientId,
            @Part("customerId") RequestBody customerId,
            @Part("salesName") RequestBody salesName,
            @Part("tax") RequestBody tax,
            @Part("totalAmount") RequestBody totalAmount,
            @Part("notes") RequestBody notes,
            @Part("itemSKU") RequestBody itemSku,
            @Part("itemName") RequestBody itemName,
            @Part("quantity") RequestBody quantity,
            @Part("measurementUnit") RequestBody measurementUnit,
            @Part("pricePerUnit") RequestBody pricePerUnit,
            @Part("discount") RequestBody discount,
            @Part("finalDiscount") RequestBody finalDiscount,
            @Part("finalAmount") RequestBody finalAmount,
            @Part("salesOrderDate") RequestBody salesOrderDate,
            @Part MultipartBody.Part signature
    );

    @Multipart
    @POST("salesOrder/delete")
    Observable<DeleteSO> deleteSalesOrder(
            @Part("clientId") RequestBody clientId,
            @Part("extSalesOrderId") RequestBody extSalesOrderId
    );


    /**
     *
     * @param clientId
     * @param parameter = 0
     * @return
     */
    @GET("salesOrder/getSalesOrder")
    Observable<SalesOrder> getSalesOrderList(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter,
            @Query("creationDate") String creationDate
    );

    /**
     *
     * @param clientId
     * @param parameter = 1
     * @param extSalesOrderId
     * @return
     */
    @GET("salesOrder/getSalesOrder")
    Observable<SalesOrder> getSalesOrderDetail(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter,
            @Query("extSalesOrderId") String extSalesOrderId
    );

    /**
     *
     * @param clientId
     * @param parameter = 8
     * @param customerId
     * @return
     */
    @GET("salesOrder/getSalesOrder")
    Observable<SalesOrder> getCustomerSo(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter,
            @Query("customerId") String customerId
    );

    @GET("notifications/getMessage")
    Observable<Notification> getNotifications(
            @Query("clientId") String clientId,
            @Query("date") String date
    );

}
