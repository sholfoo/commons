package com.miru.miruApps.network;

import com.miru.miruApps.model.response.ChangePassword;
import com.miru.miruApps.model.response.Login;
import com.miru.miruApps.model.response.Logout;
import com.miru.miruApps.model.response.Register;
import com.miru.miruApps.model.response.UserInfo;
import com.miru.miruApps.model.response.UserInfoEdit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by @sholfoo on 26/11/2018, with awesomeness.
 */
public interface ApiService {

    @Multipart
    @POST("login")
    Observable<Login> login(
            @Part("userName") RequestBody userName,
            @Part("password") RequestBody password
    );

    @Multipart
    @POST("usermgt/logout")
    Observable<Logout> logout(
            @Part("sessionId") RequestBody sessionId
    );

    @Multipart
    @POST("register")
    Observable<Register> register(
            @Part("clientName") RequestBody clientName,
            @Part("fullName") RequestBody fullName,
            @Part("emailAddress") RequestBody emailAddress,
            @Part("userName") RequestBody userName,
            @Part("password") RequestBody password,
            @Part("mobileNumber") RequestBody mobileNumber

    );

    @GET("usermgt/getUserInfo")
    Observable<UserInfo> getUserInformation(
            @Query("clientId") String clientId,
            @Query("userId") String userId
    );

    @Multipart
    @POST("usermgt/editProfile")
    Observable<UserInfoEdit> editProfile(
            @Part("clientId") RequestBody clientId,
            @Part("userId") RequestBody userId,
            @Part("clientName") RequestBody clientName,
            @Part("userName") RequestBody userName,
            @Part("fullName") RequestBody fullName,
            @Part("emailAddress") RequestBody emailAddress,
            @Part("mobileNumber") RequestBody mobileNumber,
            @Part MultipartBody.Part storeLogo,
            @Part MultipartBody.Part profilePic

    );

    @Multipart
    @POST("usermgt/changePassword")
    Observable<ChangePassword> changePassword(
            @Part("clientId") RequestBody clientId,
            @Part("userId") RequestBody userId,
            @Part("password") RequestBody password
    );
}

