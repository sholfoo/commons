package com.miru.miruApps.network;

import com.miru.miruApps.model.response.CreateInvoice;
import com.miru.miruApps.model.response.CreatePayment;
import com.miru.miruApps.model.response.CustomerPerformance;
import com.miru.miruApps.model.response.DeleteInvoice;
import com.miru.miruApps.model.response.Invoice;
import com.miru.miruApps.model.response.Payment;
import com.miru.miruApps.model.response.SummaryDashboard;
import com.miru.miruApps.model.response.SummaryHome;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface InvoiceApiService {

    @Multipart
    @POST("invoice/newInvoice")
    Observable<CreateInvoice> createInvoice(
            @Part("clientId") RequestBody clientId,
            @Part("userId") RequestBody userId,
            @Part("fullName") RequestBody fullName,
            @Part("storeName") RequestBody storeName,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("zipCode") RequestBody zipCode,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("notes") RequestBody notes,
            @Part("customerId") RequestBody customerId,
            @Part("salesName") RequestBody fullNameAgain,
            @Part("customerName") RequestBody customerName,
            @Part("customerAddress") RequestBody customerAddress,
            @Part("customerCity") RequestBody customerCity,
            @Part("customerZipCode") RequestBody customerZipCode,
            @Part("customerPhoneNumber") RequestBody customerPhoneNumber,
            @Part("invoiceDate") RequestBody invoiceDate,
            @Part("termOfPayment") RequestBody termOfPayment,
            @Part("dueDate") RequestBody dueDate,
            @Part("itemSKU") RequestBody itemSku,
            @Part("itemName") RequestBody itemName,
            @Part("pricePerUnit") RequestBody pricePerUnit,
            @Part("measurementUnit") RequestBody measurementUnit,
            @Part("discount") RequestBody discount,
            @Part("quantity") RequestBody quantity,
            @Part("totalDiscount") RequestBody totalDiscount,
            @Part("tax") RequestBody tax,
            @Part("totalAmount") RequestBody totalAmount,
            @Part("finalAmount") RequestBody finalAmount,
            @Part MultipartBody.Part signature
    );

    @Multipart
    @POST("invoice/updatingInvoice")
    Observable<CreateInvoice> updateInvoice(
            @Part("extInvoiceId") RequestBody extInvoiceId,
            @Part("clientId") RequestBody clientId,
            @Part("userId") RequestBody userId,
            @Part("fullName") RequestBody fullName,
            @Part("storeName") RequestBody storeName,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("zipCode") RequestBody zipCode,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("notes") RequestBody notes,
            @Part("customerId") RequestBody customerId,
            @Part("salesName") RequestBody fullNameAgain,
            @Part("customerName") RequestBody customerName,
            @Part("customerAddress") RequestBody customerAddress,
            @Part("customerCity") RequestBody customerCity,
            @Part("customerZipCode") RequestBody customerZipCode,
            @Part("customerPhoneNumber") RequestBody customerPhoneNumber,
            @Part("invoiceDate") RequestBody invoiceDate,
            @Part("termOfPayment") RequestBody termOfPayment,
            @Part("dueDate") RequestBody dueDate,
            @Part("itemSKU") RequestBody itemSku,
            @Part("itemName") RequestBody itemName,
            @Part("pricePerUnit") RequestBody pricePerUnit,
            @Part("measurementUnit") RequestBody measurementUnit,
            @Part("discount") RequestBody discount,
            @Part("quantity") RequestBody quantity,
            @Part("totalDiscount") RequestBody totalDiscount,
            @Part("tax") RequestBody tax,
            @Part("totalAmount") RequestBody totalAmount,
            @Part("finalAmount") RequestBody finalAmount,
            @Part MultipartBody.Part signature
    );

    /**
     *
     * @param clientId
     * @param parameter = 0
     * @return
     */
    @GET("invoice/getInvoice")
    Observable<Invoice> getInvoiceList(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter
    );

    /**
     *
     * @param clientId
     * @param parameter = 1
     * @param extInvoiceId
     * @return
     */
    @GET("invoice/getInvoice")
    Observable<Invoice> getInvoiceDetail(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter,
            @Query("extInvoiceId") String extInvoiceId
    );

    @Multipart
    @POST("invoice/delete")
    Observable<DeleteInvoice> deleteInvoice(
            @Part("clientId") RequestBody clientId,
            @Part("invoiceId") RequestBody storeName
    );

    /**
     *
     * @param clientId
     * @param parameter = 9
     * @param extSalesOrderId
     * @return
     */
    @GET("invoice/getInvoice")
    Observable<Invoice> getInvoiceSalesOrder(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter,
            @Query("extSalesOrderId") String extSalesOrderId
    );

    @GET("payment/getPaymentByClientId")
    Observable<Payment> getPaymentList(
            @Query("clientId") String clientId
    );

    @GET("payment/getPaymentByInvoiceId")
    Observable<Payment> getPaymentByInvoiceList(
            @Query("clientId") String clientId,
            @Query("extInvoiceId") String invoiceId
    );

    @Multipart
    @POST("payment/newPayment")
    Observable<CreatePayment> createPayment(
            @Part("clientId") RequestBody clientId,
            @Part("userId") RequestBody userId,
            @Part("storeName") RequestBody storeName,
            @Part("address") RequestBody address,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("customerId") RequestBody customerId,
            @Part("customerName") RequestBody customerName,
            @Part("invoiceId") RequestBody invoiceId,
            @Part("paymentAmount") RequestBody paymentAmount,
            @Part("remainingAmount") RequestBody remainingAmount,
            @Part("paymentMethod") RequestBody paymentMethod,
            @Part("paymentDate") RequestBody paymentDate,
            @Part("notes") RequestBody notes
    );

    /* for detail customer */
    @GET("customerPerformance")
    Observable<CustomerPerformance> getCustomerPerformance(
            @Query("clientId") String clientId,
            @Query("customerId") String customerId
    );

    /**
     *
     * @param clientId
     * @param parameter = 10
     * @param extSalesOrderId
     * @return
     */
    @GET("invoice/getInvoice")
    Observable<Invoice> getCustomerInvoice(
            @Query("clientId") String clientId,
            @Query("parameter") int parameter,
            @Query("customerId") String extSalesOrderId
    );

    @GET("payment/getPaymentByClientIdAndCustomerId")
    Observable<Payment> getCustomerPayment(
            @Query("clientId") String clientId,
            @Query("customerId") String extSalesOrderId
    );


    /* for home summary */
    @GET("homeDashboard")
    Observable<SummaryHome> getHomeSummary(
            @Query("clientId") String clientId
    );

    @GET("clientDashboard")
    Observable<SummaryDashboard> getDashboardSummary(
            @Query("clientId") String clientId,
            @Query("invoiceMonth") String invoiceMonth
    );
}
