package com.miru.miruApps.config;

/**
 * Created by sholfoo on 8/2/16.
 */
public class Constants {

    /* constant for tab */
    public static final String TAB_HOME = "tab_home";
    public static final String TAB_NOTIF = "tab_notif";
    public static final String TAB_PROFILE = "tab_profile";

    public static final String NULL = "";
    public static final String STATUS = "status";
    public static final String IDR = "Rp ";
    public static final String PERCENT = "%";
    public static final String FORMAT_DATE = "dd-MM-yyyy";

    public static final String BASE_URL_PORT_8899 = "http://35.240.148.132:8899/";
    public static final String BASE_URL_PORT_8201 = "http://35.240.148.132:8201/";
    public static final String BASE_URL_PORT_8200 = "http://35.240.148.132:8200/";
    public static final String BASE_URL_PORT_8202 = "http://35.240.148.132:8202/";
    public static final int TIME_OUT = 500;
    public static final int DEFAULT_SIZE = 0;

    public static final int STATUS_SUCCESS = 201;
    public static final int STATUS_ERROR = 404;

    public static final String FORMAT_CASH = "CSH";
    public static final String FORMAT_CEK_OR_GIRO = "CEK";
    public static final String FORMAT_TRANSFER = "TRF";
    public static final String FORMAT_CC = "CRC";
    public static final String FORMAT_ETC = "ETC";

    public static final String UNPAID = "UNP";
    public static final String DUE_DATE = "DUE";
    public static final String FULL_PAYMENT = "FUP";
    public static final String PARTIAL_PAYMENT = "PAP";
    public static final String OVERDUE = "OVD";
    public static final String FAILED = "FAI";

    public static final String PND = "PND";
    public static final String FLD = "FLD";

    public static final String EXTRA_PRODUCT_BRAND = "product_brand";
    public static final String EXTRA_PRODUCT_DISCOUNT = "product_discount";
    public static final String EXTRA_PRODUCT_MIN = "product_min_price";
    public static final String EXTRA_PRODUCT_MAX = "product_max_price";
    public static final String EXTRA_PRODUCT_QTY = "product_qty";

    public static final String EXTRA_PRODUCT_SHORT = "product_short";
    public static final String EXTRA_PRODUCT_PRICE = "product_price";


    public static final String EXTRA_INVOCE_STATUS = "invoice_status";
    public static final String EXTRA_INVOICE_PERIODIC_TIME = "invoice_periodic_time";
    public static final String EXTRA_INVOICE_PERIODIC_DATE = "invoice_periodic_date";

    public static final String EXTRA_INVOCE_TIME_CREATE = "invoice_time_create";
    public static final String EXTRA_INVOICE_DATE = "invoice_date";
    public static final String EXTRA_INVOICE_SHORT = "invoice_short";

    public static final int EXTRA_FROM_CUSTOMER = 1;
    public static final int EXTRA_FROM_INVOICE = 2;
    public static final int EXTRA_FROM_OTHER = 0;

    public static final int SO_PARAMETER_ALL = 0;
    public static final int SO_PARAMETER_DATE = 3;

    public static final String ORDER_ASC = "ASC";
    public static final String ORDER_DESC = "DESC";

}