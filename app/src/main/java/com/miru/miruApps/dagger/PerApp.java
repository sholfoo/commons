package com.miru.miruApps.dagger;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by sholfoo on 19/3/19 with awesomeness.
 */
@Scope
@Retention(RUNTIME)
public @interface PerApp {
}
