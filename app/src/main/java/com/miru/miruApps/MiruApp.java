package com.miru.miruApps;

import com.miru.miruApps.data.LocalDataSource;
import com.miru.miruApps.data.db.DaoMaster;
import com.miru.miruApps.data.db.DaoSession;
import com.yayandroid.locationmanager.LocationManager;

import org.greenrobot.greendao.database.Database;

public class MiruApp extends BaseApplication{

    private static MiruComponent mComponent;
    private static DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        LocationManager.enableLog(true);
        mComponent = MiruComponent.initializer.init(this);

        // regular SQLite database
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "miru-db");
        Database db = helper.getWritableDb();

        // encrypted SQLCipher database
        // note: you need to add SQLCipher to your dependencies, check the build.gradle file
        // DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db-encrypted");
        // Database db = helper.getEncryptedWritableDb("encryption-key");

        daoSession = new DaoMaster(db).newSession();
        new LocalDataSource().setKeyIsStored(this, true);
        new LocalDataSource().setKeyTargetAmount(this, "100000000");
    }

    public static MiruComponent getmComponent() {
        return mComponent;
    }

    public static DaoSession getDaoSession() {
        return daoSession;
    }

}
