package com.miru.miruApps;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

/**
 * Created by sholfoo on 11/3/19.
 */

public class BaseApplication extends MultiDexApplication {

    public static Context context;
    private static BaseApplication enableMultiDex;

    public BaseApplication() {
        enableMultiDex = this;
    }

    public static BaseApplication getEnableMultiDexApp() {
        return enableMultiDex;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

    }
}
