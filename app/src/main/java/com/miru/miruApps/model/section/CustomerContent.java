package com.miru.miruApps.model.section;

public class CustomerContent {

    String customerId;
    String customerImage;
    String customerStoreName;
    String customerType;
    String customerStatus;

    public CustomerContent(String customerId, String customerImage, String customerStoreName, String customerType, String customerStatus) {
        this.customerId = customerId;
        this.customerImage = customerImage;
        this.customerStoreName = customerStoreName;
        this.customerType = customerType;
        this.customerStatus = customerStatus;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }

    public String getCustomerStoreName() {
        return customerStoreName;
    }

    public void setCustomerStoreName(String customerStoreName) {
        this.customerStoreName = customerStoreName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }
}
