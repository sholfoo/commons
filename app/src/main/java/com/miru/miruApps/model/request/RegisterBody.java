package com.miru.miruApps.model.request;

public class RegisterBody {

    String clientName;
    String fullName;
    String emailAddress;
    String userName;
    String password;
    String mobileNumber;

    public RegisterBody() {
    }

    public RegisterBody(String clientName, String fullName, String emailAddress, String userName, String password, String mobileNumber) {
        this.clientName = clientName;
        this.fullName = fullName;
        this.emailAddress = emailAddress;
        this.userName = userName;
        this.password = password;
        this.mobileNumber = mobileNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
