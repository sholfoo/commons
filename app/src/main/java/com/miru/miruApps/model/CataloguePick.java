package com.miru.miruApps.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class CataloguePick implements Parcelable {
    private List<SoProduct> soProductList;

    public CataloguePick(List<SoProduct> soProductList) {
        this.soProductList = soProductList;
    }

    protected CataloguePick(Parcel in) {
        soProductList = in.createTypedArrayList(SoProduct.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(soProductList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CataloguePick> CREATOR = new Creator<CataloguePick>() {
        @Override
        public CataloguePick createFromParcel(Parcel in) {
            return new CataloguePick(in);
        }

        @Override
        public CataloguePick[] newArray(int size) {
            return new CataloguePick[size];
        }
    };

    public List<SoProduct> getSoProductList() {
        return soProductList;
    }

    public void setSoProductList(List<SoProduct> soProductList) {
        this.soProductList = soProductList;
    }
}
