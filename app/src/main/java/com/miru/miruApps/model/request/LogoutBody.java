package com.miru.miruApps.model.request;

public class LogoutBody {
    String sessionId;

    public LogoutBody(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
