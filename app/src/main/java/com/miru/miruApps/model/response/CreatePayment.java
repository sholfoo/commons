package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreatePayment implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * paymentId : PAY201905BC-003002-002
     * clientId : BC-003
     * customerId : BC-003-CST-TM-001
     * invoiceId : INV/2019/05/BC-003/002
     * paymentAmount : 1000000
     * remainingAmount : 0
     * paymentMethod : CC
     * paymentDate : 30-04-2019
     * notes : dicicil sampe bulan depan
     * docUrlPath : http://35.240.148.132/pdf/BC-003/PAY/PAY201905BC-003002-002.pdf
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("paymentId")
    private String paymentId;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("invoiceId")
    private String invoiceId;
    @SerializedName("paymentAmount")
    private int paymentAmount;
    @SerializedName("remainingAmount")
    private int remainingAmount;
    @SerializedName("paymentMethod")
    private String paymentMethod;
    @SerializedName("paymentDate")
    private String paymentDate;
    @SerializedName("notes")
    private String notes;
    @SerializedName("docUrlPath")
    private String docUrlPath;
    @SerializedName("errorMessage")
    private List<?> errorMessage;

    protected CreatePayment(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        paymentId = in.readString();
        clientId = in.readString();
        customerId = in.readString();
        invoiceId = in.readString();
        paymentAmount = in.readInt();
        remainingAmount = in.readInt();
        paymentMethod = in.readString();
        paymentDate = in.readString();
        notes = in.readString();
        docUrlPath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(paymentId);
        dest.writeString(clientId);
        dest.writeString(customerId);
        dest.writeString(invoiceId);
        dest.writeInt(paymentAmount);
        dest.writeInt(remainingAmount);
        dest.writeString(paymentMethod);
        dest.writeString(paymentDate);
        dest.writeString(notes);
        dest.writeString(docUrlPath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreatePayment> CREATOR = new Creator<CreatePayment>() {
        @Override
        public CreatePayment createFromParcel(Parcel in) {
            return new CreatePayment(in);
        }

        @Override
        public CreatePayment[] newArray(int size) {
            return new CreatePayment[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(int paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public int getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(int remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDocUrlPath() {
        return docUrlPath;
    }

    public void setDocUrlPath(String docUrlPath) {
        this.docUrlPath = docUrlPath;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }
}
