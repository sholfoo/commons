package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateSo implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * clientId : SS-001
     * customerId : SS-001-CST-TB-002
     * salesOrderId :
     * invoiceId : INV/2019/05/SS-001/001
     * salesName : BUDI
     * tax : 10
     * totalAmount : 500000
     * notes : test invoice
     * items : [{"itemSKU":"ABC123","itemName":"Baju","quantity":10,"measurementUnit":"PCS","pricePerUnit":10000,"discount":"10"},{"itemSKU":"BAC234","itemName":"Celana","quantity":2,"measurementUnit":"PCS","pricePerUnit":2000,"discount":"0"},{"itemSKU":"RET222","itemName":"Kemeja","quantity":4,"measurementUnit":"PCS","pricePerUnit":3000,"discount":"0"}]
     * paymentStatus : This invoice hasn't been paid
     * dueDate : 02-05-2019
     * invoiceDate : 20-04-2019
     * termOfPayment : CASH
     * totalDiscount : 5
     * finalAmount : 500000
     * userId : BB-001-AD-001
     * docUrlPath : http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-001.pdf
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("salesOrderId")
    private String salesOrderId;
    @SerializedName("invoiceId")
    private String invoiceId;
    @SerializedName("salesName")
    private String salesName;
    @SerializedName("tax")
    private String tax;
    @SerializedName("totalAmount")
    private int totalAmount;
    @SerializedName("notes")
    private String notes;
    @SerializedName("paymentStatus")
    private String paymentStatus;
    @SerializedName("dueDate")
    private String dueDate;
    @SerializedName("invoiceDate")
    private String invoiceDate;
    @SerializedName("termOfPayment")
    private String termOfPayment;
    @SerializedName("totalDiscount")
    private String totalDiscount;
    @SerializedName("finalAmount")
    private int finalAmount;
    @SerializedName("userId")
    private String userId;
    @SerializedName("docUrlPath")
    private String docUrlPath;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("items")
    private List<ItemsEntity> items;

    protected CreateSo(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        clientId = in.readString();
        customerId = in.readString();
        salesOrderId = in.readString();
        invoiceId = in.readString();
        salesName = in.readString();
        tax = in.readString();
        totalAmount = in.readInt();
        notes = in.readString();
        paymentStatus = in.readString();
        dueDate = in.readString();
        invoiceDate = in.readString();
        termOfPayment = in.readString();
        totalDiscount = in.readString();
        finalAmount = in.readInt();
        userId = in.readString();
        docUrlPath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(clientId);
        dest.writeString(customerId);
        dest.writeString(salesOrderId);
        dest.writeString(invoiceId);
        dest.writeString(salesName);
        dest.writeString(tax);
        dest.writeInt(totalAmount);
        dest.writeString(notes);
        dest.writeString(paymentStatus);
        dest.writeString(dueDate);
        dest.writeString(invoiceDate);
        dest.writeString(termOfPayment);
        dest.writeString(totalDiscount);
        dest.writeInt(finalAmount);
        dest.writeString(userId);
        dest.writeString(docUrlPath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreateSo> CREATOR = new Creator<CreateSo>() {
        @Override
        public CreateSo createFromParcel(Parcel in) {
            return new CreateSo(in);
        }

        @Override
        public CreateSo[] newArray(int size) {
            return new CreateSo[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(String salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTermOfPayment() {
        return termOfPayment;
    }

    public void setTermOfPayment(String termOfPayment) {
        this.termOfPayment = termOfPayment;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public int getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(int finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocUrlPath() {
        return docUrlPath;
    }

    public void setDocUrlPath(String docUrlPath) {
        this.docUrlPath = docUrlPath;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<ItemsEntity> getItems() {
        return items;
    }

    public void setItems(List<ItemsEntity> items) {
        this.items = items;
    }

    public static class ItemsEntity {
        /**
         * itemSKU : ABC123
         * itemName : Baju
         * quantity : 10
         * measurementUnit : PCS
         * pricePerUnit : 10000
         * discount : 10
         */

        @SerializedName("itemSKU")
        private String itemSKU;
        @SerializedName("itemName")
        private String itemName;
        @SerializedName("quantity")
        private int quantity;
        @SerializedName("measurementUnit")
        private String measurementUnit;
        @SerializedName("pricePerUnit")
        private int pricePerUnit;
        @SerializedName("discount")
        private String discount;

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getMeasurementUnit() {
            return measurementUnit;
        }

        public void setMeasurementUnit(String measurementUnit) {
            this.measurementUnit = measurementUnit;
        }

        public int getPricePerUnit() {
            return pricePerUnit;
        }

        public void setPricePerUnit(int pricePerUnit) {
            this.pricePerUnit = pricePerUnit;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }
    }
}
