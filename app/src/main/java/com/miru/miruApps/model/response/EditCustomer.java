package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditCustomer implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * customerId : BC-001-CST-TB-001
     * clientId : BC-001
     * ownerName : Budi Sujatniko
     * storeName : Toko Sembako Jaya
     * address : [{"address":"Jalan Timur 1, Depok","city":"Jakarta Selatan","province":"DKI Jakarta","postalCode":"12345","phoneNumber":"081123"}]
     * emailAddress : budi@gmail.com
     * imageLink : []
     * customerImage : http://35.240.148.132/images/customerImages/BC-001/BC-001-CST-TB-001/BC-001-CST-TB-001-2.png
     * status : active
     * customerType : retail
     * customerTag : ["retail","beras"]
     * mobilePhone : 123123123
     * longitudes :
     * latitudes :
     * description : toko baru pintu biru
     * creditLimit : 10000000
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("customerId")
    private String customerId;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("ownerName")
    private String ownerName;
    @SerializedName("storeName")
    private String storeName;
    @SerializedName("emailAddress")
    private String emailAddress;
    @SerializedName("customerImage")
    private String customerImage;
    @SerializedName("status")
    private String status;
    @SerializedName("customerType")
    private String customerType;
    @SerializedName("mobilePhone")
    private String mobilePhone;
    @SerializedName("longitudes")
    private String longitudes;
    @SerializedName("latitudes")
    private String latitudes;
    @SerializedName("description")
    private String description;
    @SerializedName("creditLimit")
    private String creditLimit;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("address")
    private List<AddressEntity> address;
    @SerializedName("imageLink")
    private List<?> imageLink;
    @SerializedName("customerTag")
    private List<String> customerTag;

    protected EditCustomer(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        customerId = in.readString();
        clientId = in.readString();
        ownerName = in.readString();
        storeName = in.readString();
        emailAddress = in.readString();
        customerImage = in.readString();
        status = in.readString();
        customerType = in.readString();
        mobilePhone = in.readString();
        longitudes = in.readString();
        latitudes = in.readString();
        description = in.readString();
        creditLimit = in.readString();
        customerTag = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(customerId);
        dest.writeString(clientId);
        dest.writeString(ownerName);
        dest.writeString(storeName);
        dest.writeString(emailAddress);
        dest.writeString(customerImage);
        dest.writeString(status);
        dest.writeString(customerType);
        dest.writeString(mobilePhone);
        dest.writeString(longitudes);
        dest.writeString(latitudes);
        dest.writeString(description);
        dest.writeString(creditLimit);
        dest.writeStringList(customerTag);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EditCustomer> CREATOR = new Creator<EditCustomer>() {
        @Override
        public EditCustomer createFromParcel(Parcel in) {
            return new EditCustomer(in);
        }

        @Override
        public EditCustomer[] newArray(int size) {
            return new EditCustomer[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getLongitudes() {
        return longitudes;
    }

    public void setLongitudes(String longitudes) {
        this.longitudes = longitudes;
    }

    public String getLatitudes() {
        return latitudes;
    }

    public void setLatitudes(String latitudes) {
        this.latitudes = latitudes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<AddressEntity> getAddress() {
        return address;
    }

    public void setAddress(List<AddressEntity> address) {
        this.address = address;
    }

    public List<?> getImageLink() {
        return imageLink;
    }

    public void setImageLink(List<?> imageLink) {
        this.imageLink = imageLink;
    }

    public List<String> getCustomerTag() {
        return customerTag;
    }

    public void setCustomerTag(List<String> customerTag) {
        this.customerTag = customerTag;
    }

    public static class AddressEntity {
        /**
         * address : Jalan Timur 1, Depok
         * city : Jakarta Selatan
         * province : DKI Jakarta
         * postalCode : 12345
         * phoneNumber : 081123
         */

        @SerializedName("address")
        private String address;
        @SerializedName("city")
        private String city;
        @SerializedName("province")
        private String province;
        @SerializedName("postalCode")
        private String postalCode;
        @SerializedName("phoneNumber")
        private String phoneNumber;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }
}
