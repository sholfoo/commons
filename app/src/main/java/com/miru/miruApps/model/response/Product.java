package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * productContainer : [{"productId":"SS-001/11133","clientId":"SS-001","itemSKU":"11133","productName":"Tissue Paseo","category":"TISSUE","description":"","imageLinks":["image.png","image.png"],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg","brand":"Paseo","size":"200 lembar","productPrices":[{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"LUSIN"},"pricePerUnit":300000,"sellingPrice":350000,"discount":10000},{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"PCS"},"pricePerUnit":15000,"sellingPrice":20000,"discount":1000}],"lastUpdatedTime":"26-04-2019 07:39:10.000"},{"productId":"SS-001/CHRG001","clientId":"SS-001","itemSKU":"CHRG001","productName":"Xiaomi Charger Adapter","category":"ADAPTER","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/CHRG001/SS-001-CHRG001-1.png","brand":"xiaomi","size":"0","productPrices":[{"identifierId":{"productId":"SS-001/CHRG001","itemSKU":"CHRG001","measurementUnit":"box"},"pricePerUnit":545000,"sellingPrice":750000,"discount":25},{"identifierId":{"productId":"SS-001/CHRG001","itemSKU":"CHRG001","measurementUnit":"pcs"},"pricePerUnit":200000,"sellingPrice":250000,"discount":10}],"lastUpdatedTime":"29-04-2019 03:51:59.000"},{"productId":"SS-001/EMNY92929290","clientId":"SS-001","itemSKU":"EMNY92929290","productName":"emoney edition","category":"EMONEY","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/EMNY92929290/SS-001-EMNY92929290-1.png","brand":"mandiri","size":"0","productPrices":[{"identifierId":{"productId":"SS-001/EMNY92929290","itemSKU":"EMNY92929290","measurementUnit":"pcs"},"pricePerUnit":35000,"sellingPrice":50000,"discount":0}],"lastUpdatedTime":"29-04-2019 03:28:23.000"},{"productId":"SS-001/EXPD000019","clientId":"SS-001","itemSKU":"EXPD000019","productName":"Expedition","category":"JAM TANGAN","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/EXPD000019/SS-001-EXPD000019-1.png","brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/EXPD000019","itemSKU":"EXPD000019","measurementUnit":"pcs"},"pricePerUnit":945000,"sellingPrice":1345000,"discount":15}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/EXPDT09282828","clientId":"SS-001","itemSKU":"EXPDT09282828","productName":"Expedition E6318M","category":"JAM TANGAN","description":null,"imageLinks":[],"imageProfile":null,"brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/EXPDT09282828","itemSKU":"EXPDT09282828","measurementUnit":"pcs"},"pricePerUnit":950000,"sellingPrice":1300000,"discount":10}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/KTLS00192090","clientId":"SS-001","itemSKU":"KTLS00192090","productName":"Thiskid.id Series Monster Tee","category":"Clothing","description":null,"imageLinks":["/Midgard/env/MasterDataService/images/productImages//SS-001/KTLS00192090/SS-001-KTLS00192090-2.png","/Midgard/env/MasterDataService/images/productImages//SS-001/KTLS00192090/SS-001-KTLS00192090-3.png"],"imageProfile":"/Midgard/env/MasterDataService/images/productImages//SS-001/KTLS00192090/SS-001-KTLS00192090-1.png","brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/KTLS00192090","itemSKU":"KTLS00192090","measurementUnit":"LUSIN"},"pricePerUnit":348000,"sellingPrice":0,"discount":0},{"identifierId":{"productId":"SS-001/KTLS00192090","itemSKU":"KTLS00192090","measurementUnit":"PCS"},"pricePerUnit":35000,"sellingPrice":0,"discount":0}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/KTLS00192091","clientId":"SS-001","itemSKU":"KTLS00192091","productName":"Thiskid.id Series Monster Tee","category":"Clothing","description":null,"imageLinks":["http://35.240.148.132/images/productImages/SS-001/KTLS00192091/SS-001-KTLS00192091-2.png","http://35.240.148.132/images/productImages/SS-001/KTLS00192091/SS-001-KTLS00192091-3.png"],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/KTLS00192091/SS-001-KTLS00192091-1.png","brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/KTLS00192091","itemSKU":"KTLS00192091","measurementUnit":"LUSIN"},"pricePerUnit":348000,"sellingPrice":0,"discount":0},{"identifierId":{"productId":"SS-001/KTLS00192091","itemSKU":"KTLS00192091","measurementUnit":"PCS"},"pricePerUnit":35000,"sellingPrice":0,"discount":0}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/LQHR70008","clientId":"SS-001","itemSKU":"LQHR70008","productName":"NYX Mango 60ml ","category":"LIQUID","description":null,"imageLinks":[],"imageProfile":null,"brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/LQHR70008","itemSKU":"LQHR70008","measurementUnit":"botol"},"pricePerUnit":59000,"sellingPrice":73000,"discount":5}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/RHT0019","clientId":"SS-001","itemSKU":"RHT0019","productName":"Rohto Cool","category":"OBAT MATA","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/RHT0019/SS-001-RHT0019-1.png","brand":"Rohto","size":"7ml","productPrices":[{"identifierId":{"productId":"SS-001/RHT0019","itemSKU":"RHT0019","measurementUnit":"box"},"pricePerUnit":125000,"sellingPrice":150000,"discount":5},{"identifierId":{"productId":"SS-001/RHT0019","itemSKU":"RHT0019","measurementUnit":"pcs"},"pricePerUnit":5000,"sellingPrice":7500,"discount":0}],"lastUpdatedTime":"29-04-2019 03:55:21.000"},{"productId":"SS-001/RNV000019","clientId":"SS-001","itemSKU":"RNV000019","productName":"Renova Catridge 2Ml","category":"LIQUID","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/RNV000019/SS-001-RNV000019-1.png","brand":"Renova","size":"2ml","productPrices":[{"identifierId":{"productId":"SS-001/RNV000019","itemSKU":"RNV000019","measurementUnit":"box"},"pricePerUnit":80000,"sellingPrice":95000,"discount":0},{"identifierId":{"productId":"SS-001/RNV000019","itemSKU":"RNV000019","measurementUnit":"pcs"},"pricePerUnit":40000,"sellingPrice":55000,"discount":0}],"lastUpdatedTime":"29-04-2019 03:36:13.000"},{"productId":"SS-001/RNVA00982","clientId":"SS-001","itemSKU":"RNVA00982","productName":"Renova Zero Pod","category":"VAPE","description":null,"imageLinks":[],"imageProfile":null,"brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/RNVA00982","itemSKU":"RNVA00982","measurementUnit":"pcs"},"pricePerUnit":215000,"sellingPrice":265000,"discount":10}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/SNDK92929","clientId":"SS-001","itemSKU":"SNDK92929","productName":"Sandisk 16Gb","category":"USB","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/SNDK92929/SS-001-SNDK92929-1.png","brand":"sandisk","size":"0","productPrices":[{"identifierId":{"productId":"SS-001/SNDK92929","itemSKU":"SNDK92929","measurementUnit":"lusin"},"pricePerUnit":950000,"sellingPrice":1000000,"discount":20},{"identifierId":{"productId":"SS-001/SNDK92929","itemSKU":"SNDK92929","measurementUnit":"pcs"},"pricePerUnit":75000,"sellingPrice":80000,"discount":0}],"lastUpdatedTime":"29-04-2019 03:58:10.000"},{"productId":"SS-001/SNMBS000090","clientId":"SS-001","itemSKU":"SNMBS000090","productName":"Classic Snowmobaby Infant","category":"Clothing","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/SNMBS000090/SS-001-SNMBS000090-1.png","brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/SNMBS000090","itemSKU":"SNMBS000090","measurementUnit":"LUSIN"},"pricePerUnit":348000,"sellingPrice":0,"discount":0},{"identifierId":{"productId":"SS-001/SNMBS000090","itemSKU":"SNMBS000090","measurementUnit":"PCS"},"pricePerUnit":35000,"sellingPrice":0,"discount":0}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/SNMBS000091","clientId":"SS-001","itemSKU":"SNMBS000091","productName":"Classic Snowmobaby Infant","category":"CLOTHING","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/SNMBS000091/SS-001-SNMBS000091-1.png","brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/SNMBS000091","itemSKU":"SNMBS000091","measurementUnit":"LUSIN"},"pricePerUnit":385000,"sellingPrice":0,"discount":0},{"identifierId":{"productId":"SS-001/SNMBS000091","itemSKU":"SNMBS000091","measurementUnit":"PCS"},"pricePerUnit":35000,"sellingPrice":0,"discount":0}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/SNMBS000092","clientId":"SS-001","itemSKU":"SNMBS000092","productName":"Classic Snowmobaby Infant","category":"TSHIRT","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/SNMBS000092/SS-001-SNMBS000092-1.png","brand":null,"size":null,"productPrices":[{"identifierId":{"productId":"SS-001/SNMBS000092","itemSKU":"SNMBS000092","measurementUnit":"LUSIN"},"pricePerUnit":348000,"sellingPrice":0,"discount":0},{"identifierId":{"productId":"SS-001/SNMBS000092","itemSKU":"SNMBS000092","measurementUnit":"PCS"},"pricePerUnit":35000,"sellingPrice":0,"discount":0}],"lastUpdatedTime":"14-06-2019 08:39:15.000"},{"productId":"SS-001/TPTRCK000092","clientId":"SS-001","itemSKU":"TPTRCK000092","productName":"Topi Trucker Red","category":"TOPI","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/TPTRCK000092/SS-001-TPTRCK000092-1.png","brand":"Bigreds","size":"0","productPrices":[{"identifierId":{"productId":"SS-001/TPTRCK000092","itemSKU":"TPTRCK000092","measurementUnit":"lusin"},"pricePerUnit":300000,"sellingPrice":275000,"discount":5},{"identifierId":{"productId":"SS-001/TPTRCK000092","itemSKU":"TPTRCK000092","measurementUnit":"pcs"},"pricePerUnit":25000,"sellingPrice":35000,"discount":0}],"lastUpdatedTime":"29-04-2019 03:23:25.000"},{"productId":"SS-001/ZR001970","clientId":"SS-001","itemSKU":"ZR001970","productName":"Renova Zero Catridge","category":"VAPE","description":null,"imageLinks":[],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png","brand":"zero","size":"2ml","productPrices":[{"identifierId":{"productId":"SS-001/ZR001970","itemSKU":"ZR001970","measurementUnit":"Box"},"pricePerUnit":75000,"sellingPrice":85000,"discount":0},{"identifierId":{"productId":"SS-001/ZR001970","itemSKU":"ZR001970","measurementUnit":"pcs"},"pricePerUnit":35000,"sellingPrice":40000,"discount":0}],"lastUpdatedTime":"29-04-2019 03:32:25.000"}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("productContainer")
    private List<ProductContainerEntity> productContainer;

    protected Product(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<ProductContainerEntity> getProductContainer() {
        return productContainer;
    }

    public void setProductContainer(List<ProductContainerEntity> productContainer) {
        this.productContainer = productContainer;
    }

    public static class ProductContainerEntity {
        /**
         * productId : SS-001/11133
         * clientId : SS-001
         * itemSKU : 11133
         * productName : Tissue Paseo
         * category : TISSUE
         * description :
         * imageLinks : ["image.png","image.png"]
         * imageProfile : http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg
         * brand : Paseo
         * size : 200 lembar
         * productPrices : [{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"LUSIN"},"pricePerUnit":300000,"sellingPrice":350000,"discount":10000},{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"PCS"},"pricePerUnit":15000,"sellingPrice":20000,"discount":1000}]
         * lastUpdatedTime : 26-04-2019 07:39:10.000
         */

        @SerializedName("productId")
        private String productId;
        @SerializedName("clientId")
        private String clientId;
        @SerializedName("itemSKU")
        private String itemSKU;
        @SerializedName("productName")
        private String productName;
        @SerializedName("category")
        private String category;
        @SerializedName("description")
        private String description;
        @SerializedName("imageProfile")
        private String imageProfile;
        @SerializedName("brand")
        private String brand;
        @SerializedName("size")
        private String size;
        @SerializedName("lastUpdatedTime")
        private String lastUpdatedTime;
        @SerializedName("imageLinks")
        private List<String> imageLinks;
        @SerializedName("productPrices")
        private List<ProductPricesEntity> productPrices;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageProfile() {
            return imageProfile;
        }

        public void setImageProfile(String imageProfile) {
            this.imageProfile = imageProfile;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getLastUpdatedTime() {
            return lastUpdatedTime;
        }

        public void setLastUpdatedTime(String lastUpdatedTime) {
            this.lastUpdatedTime = lastUpdatedTime;
        }

        public List<String> getImageLinks() {
            return imageLinks;
        }

        public void setImageLinks(List<String> imageLinks) {
            this.imageLinks = imageLinks;
        }

        public List<ProductPricesEntity> getProductPrices() {
            return productPrices;
        }

        public void setProductPrices(List<ProductPricesEntity> productPrices) {
            this.productPrices = productPrices;
        }

        public static class ProductPricesEntity {
            /**
             * identifierId : {"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"LUSIN"}
             * pricePerUnit : 300000
             * sellingPrice : 350000
             * discount : 10000
             */

            @SerializedName("identifierId")
            private IdentifierIdEntity identifierId;
            @SerializedName("pricePerUnit")
            private int pricePerUnit;
            @SerializedName("sellingPrice")
            private int sellingPrice;
            @SerializedName("discount")
            private int discount;

            public IdentifierIdEntity getIdentifierId() {
                return identifierId;
            }

            public void setIdentifierId(IdentifierIdEntity identifierId) {
                this.identifierId = identifierId;
            }

            public int getPricePerUnit() {
                return pricePerUnit;
            }

            public void setPricePerUnit(int pricePerUnit) {
                this.pricePerUnit = pricePerUnit;
            }

            public int getSellingPrice() {
                return sellingPrice;
            }

            public void setSellingPrice(int sellingPrice) {
                this.sellingPrice = sellingPrice;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public static class IdentifierIdEntity {
                /**
                 * productId : SS-001/11133
                 * itemSKU : 11133
                 * measurementUnit : LUSIN
                 */

                @SerializedName("productId")
                private String productId;
                @SerializedName("itemSKU")
                private String itemSKU;
                @SerializedName("measurementUnit")
                private String measurementUnit;

                public String getProductId() {
                    return productId;
                }

                public void setProductId(String productId) {
                    this.productId = productId;
                }

                public String getItemSKU() {
                    return itemSKU;
                }

                public void setItemSKU(String itemSKU) {
                    this.itemSKU = itemSKU;
                }

                public String getMeasurementUnit() {
                    return measurementUnit;
                }

                public void setMeasurementUnit(String measurementUnit) {
                    this.measurementUnit = measurementUnit;
                }
            }
        }
    }
}
