package com.miru.miruApps.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Property;

public class ProductShort implements Parcelable {

    String productId;
    String itemSKU;
    String productName;
    String category;
    String description;
    String imageLinks;
    String brand;
    String size;
    String productBasicPrice;
    String productSellingPrice;
    String productDiscount;

    public ProductShort() {
    }

    public ProductShort(String productId,String itemSKU, String productName, String category, String description, String imageLinks,String brand, String size, String productBasicPrice, String productSellingPrice, String productDiscount) {
        this.productId = productId;
        this.itemSKU = itemSKU;
        this.productName = productName;
        this.category = category;
        this.description = description;
        this.imageLinks = imageLinks;
        this.brand = brand;
        this.size = size;
        this.productBasicPrice = productBasicPrice;
        this.productSellingPrice = productSellingPrice;
        this.productDiscount = productDiscount;
    }

    protected ProductShort(Parcel in) {
        productId = in.readString();
        itemSKU = in.readString();
        productName = in.readString();
        category = in.readString();
        description = in.readString();
        imageLinks = in.readString();
        brand = in.readString();
        size = in.readString();
        productBasicPrice = in.readString();
        productSellingPrice = in.readString();
        productDiscount = in.readString();
    }

    public static final Creator<ProductShort> CREATOR = new Creator<ProductShort>() {
        @Override
        public ProductShort createFromParcel(Parcel in) {
            return new ProductShort(in);
        }

        @Override
        public ProductShort[] newArray(int size) {
            return new ProductShort[size];
        }
    };

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLinks() {
        return imageLinks;
    }

    public void setImageLinks(String imageLinks) {
        this.imageLinks = imageLinks;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getProductBasicPrice() {
        return productBasicPrice;
    }

    public void setProductBasicPrice(String productBasicPrice) {
        this.productBasicPrice = productBasicPrice;
    }

    public String getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(String productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(productId);
        parcel.writeString(itemSKU);
        parcel.writeString(productName);
        parcel.writeString(category);
        parcel.writeString(description);
        parcel.writeString(imageLinks);
        parcel.writeString(brand);
        parcel.writeString(size);
        parcel.writeString(productBasicPrice);
        parcel.writeString(productSellingPrice);
        parcel.writeString(productDiscount);
    }
}
