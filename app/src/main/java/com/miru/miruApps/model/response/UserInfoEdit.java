package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserInfoEdit implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * clientName : Toko Maju Jaya Selalu
     * fullName : Rico Saja
     * emailAddress : rico123@gmail.com
     * userName : block
     * mobileNumber : 1231231234
     * clientId : BC-001
     * roleCode : ADM
     * userId : BC-001-AD-001
     * status : Y
     * clientLogo : http://35.240.148.132/images/clientImages/BC-001/clientLogo.png
     * profilePicture : http://35.240.148.132/images/clientImages/BC-001/BC-001-AD-001-profile.png
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("clientName")
    private String clientName;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("emailAddress")
    private String emailAddress;
    @SerializedName("userName")
    private String userName;
    @SerializedName("mobileNumber")
    private String mobileNumber;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("roleCode")
    private String roleCode;
    @SerializedName("userId")
    private String userId;
    @SerializedName("status")
    private String status;
    @SerializedName("clientLogo")
    private String clientLogo;
    @SerializedName("profilePicture")
    private String profilePicture;
    @SerializedName("errorMessage")
    private List<?> errorMessage;

    protected UserInfoEdit(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        clientName = in.readString();
        fullName = in.readString();
        emailAddress = in.readString();
        userName = in.readString();
        mobileNumber = in.readString();
        clientId = in.readString();
        roleCode = in.readString();
        userId = in.readString();
        status = in.readString();
        clientLogo = in.readString();
        profilePicture = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(clientName);
        dest.writeString(fullName);
        dest.writeString(emailAddress);
        dest.writeString(userName);
        dest.writeString(mobileNumber);
        dest.writeString(clientId);
        dest.writeString(roleCode);
        dest.writeString(userId);
        dest.writeString(status);
        dest.writeString(clientLogo);
        dest.writeString(profilePicture);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserInfoEdit> CREATOR = new Creator<UserInfoEdit>() {
        @Override
        public UserInfoEdit createFromParcel(Parcel in) {
            return new UserInfoEdit(in);
        }

        @Override
        public UserInfoEdit[] newArray(int size) {
            return new UserInfoEdit[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClientLogo() {
        return clientLogo;
    }

    public void setClientLogo(String clientLogo) {
        this.clientLogo = clientLogo;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }
}
