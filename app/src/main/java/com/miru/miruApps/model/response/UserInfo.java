package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserInfo implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * clientId : SS-001
     * userId : SS-001-IA-001
     * storeName : Sholfoostore
     * fullName : Irfan Al
     * emailAddress : irfansholfo@sholfoostore.com
     * password : 9b31542e188d0bb99875e95f9b6d425b
     * mobilePhone : 085728373838
     * clientLogoUrl : http://35.240.148.132/images/clientImages/SS-001/clientLogo.png
     * userProfileImage : http://35.240.148.132/images/clientImages/SS-001/SS-001-IA-001-profile.png
     * address : {"address":"Jalan Banteng","province":"DKI Jakarta","zipCode":"13089"}
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("userId")
    private String userId;
    @SerializedName("storeName")
    private String storeName;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("emailAddress")
    private String emailAddress;
    @SerializedName("password")
    private String password;
    @SerializedName("mobilePhone")
    private String mobilePhone;
    @SerializedName("clientLogoUrl")
    private String clientLogoUrl;
    @SerializedName("userProfileImage")
    private String userProfileImage;
    @SerializedName("address")
    private AddressEntity address;
    @SerializedName("errorMessage")
    private List<?> errorMessage;

    protected UserInfo(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        clientId = in.readString();
        userId = in.readString();
        storeName = in.readString();
        fullName = in.readString();
        emailAddress = in.readString();
        password = in.readString();
        mobilePhone = in.readString();
        clientLogoUrl = in.readString();
        userProfileImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(clientId);
        dest.writeString(userId);
        dest.writeString(storeName);
        dest.writeString(fullName);
        dest.writeString(emailAddress);
        dest.writeString(password);
        dest.writeString(mobilePhone);
        dest.writeString(clientLogoUrl);
        dest.writeString(userProfileImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getClientLogoUrl() {
        return clientLogoUrl;
    }

    public void setClientLogoUrl(String clientLogoUrl) {
        this.clientLogoUrl = clientLogoUrl;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static class AddressEntity {
        /**
         * address : Jalan Banteng
         * province : DKI Jakarta
         * zipCode : 13089
         */

        @SerializedName("address")
        private String address;
        @SerializedName("province")
        private String province;
        @SerializedName("zipCode")
        private String zipCode;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }
    }
}
