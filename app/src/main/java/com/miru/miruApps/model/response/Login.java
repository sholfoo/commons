package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Login implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * sessionId : 23cc1d70cd4264f2e61ab8024426fcdf
     * userId : SS-001-IA-001
     * clientId : SS-001
     * userName : sholfoo
     * clientName : SholfooStore
     * userProfileImage : http://35.240.148.132/images/clientImages/SS-001/SS-001-IA-001-profile.jpg
     * fullName : Irfan Sholfoo Ghozaly
     * clientLogo : http://35.240.148.132/images/clientImages/SS-001/clientLogo.png
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("sessionId")
    private String sessionId;
    @SerializedName("userId")
    private String userId;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("userName")
    private String userName;
    @SerializedName("clientName")
    private String clientName;
    @SerializedName("userProfileImage")
    private String userProfileImage;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("clientLogo")
    private String clientLogo;
    @SerializedName("errorMessage")
    private List<?> errorMessage;

    protected Login(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        sessionId = in.readString();
        userId = in.readString();
        clientId = in.readString();
        userName = in.readString();
        clientName = in.readString();
        userProfileImage = in.readString();
        fullName = in.readString();
        clientLogo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(sessionId);
        dest.writeString(userId);
        dest.writeString(clientId);
        dest.writeString(userName);
        dest.writeString(clientName);
        dest.writeString(userProfileImage);
        dest.writeString(fullName);
        dest.writeString(clientLogo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Login> CREATOR = new Creator<Login>() {
        @Override
        public Login createFromParcel(Parcel in) {
            return new Login(in);
        }

        @Override
        public Login[] newArray(int size) {
            return new Login[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getClientLogo() {
        return clientLogo;
    }

    public void setClientLogo(String clientLogo) {
        this.clientLogo = clientLogo;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }
}
