package com.miru.miruApps.model.section;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

public class SectionCustomer implements ParentListItem {

    String section;
    List<CustomerContent> customerContents;

    public SectionCustomer() {
    }

    public SectionCustomer(String section, List<CustomerContent> customerContents) {
        this.section = section;
        this.customerContents = customerContents;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public List<CustomerContent> getCustomerContents() {
        return customerContents;
    }

    public void setCustomerContents(List<CustomerContent> customerContents) {
        this.customerContents = customerContents;
    }

    @Override
    public List<?> getChildItemList() {
        return customerContents;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return true;
    }
}
