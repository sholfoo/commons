package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Invoice implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * invoiceContainer : [{"invoice":{"invoiceId":"INV201905SS-001005","extInvoiceId":"INV/2019/05/SS-001/005","clientId":"SS-001","salesOrderId":"","extSalesOrderId":"","customerId":"SS-001-CST-RP-001","customerName":"Rizal","customerStore":"Rizal","salesName":"Rizali Ahmad Nugraha","dueDate":"07-05-2019","invoiceDate":"07-05-2019","invoiceMonth":"05-2019","invoiceYear":"2019","paymentStatus":"OVD","termOfPayment":"CASH","tax":"78400","totalDiscount":"0","totalAmount":1120000,"finalAmount":1120000,"notes":"test invoice","docUrlPath":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-005.pdf","totalDiscountInNum":0,"taxInNum":878080000,"remainingAmount":1120000},"items":[{"identifierId":{"invoiceId":"INV201905SS-001005","itemSKU":"SNDK92929"},"pricePerUnit":80000,"itemName":"Sandisk 16Gb","measurementUnit":"PCS","quantity":9,"discount":"0","discountInNum":0},{"identifierId":{"invoiceId":"INV201905SS-001005","itemSKU":"ZR001970"},"pricePerUnit":40000,"itemName":"Renova Zero Catridge","measurementUnit":"PCS","quantity":10,"discount":"0","discountInNum":0}],"signatureUrl":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-005-SIGN.png","customerStore":null},{"invoice":{"invoiceId":"INV201905SS-001006","extInvoiceId":"INV/2019/05/SS-001/006","clientId":"SS-001","salesOrderId":"","extSalesOrderId":"","customerId":"SS-001-CST-RP-001","customerName":"Tommy","customerStore":"Tommy","salesName":"Rizali Ahmad Nugraha","dueDate":"07-05-2019","invoiceDate":"07-05-2019","invoiceMonth":"05-2019","invoiceYear":"2019","paymentStatus":"OVD","termOfPayment":"CASH","tax":"78400","totalDiscount":"0","totalAmount":1120000,"finalAmount":1120000,"notes":"ksksk sks ske eiw alam sla slwlma. sks","docUrlPath":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-006.pdf","totalDiscountInNum":0,"taxInNum":878080000,"remainingAmount":1120000},"items":[{"identifierId":{"invoiceId":"INV201905SS-001006","itemSKU":"SNDK92929"},"pricePerUnit":80000,"itemName":"Sandisk 16Gb","measurementUnit":"pcs","quantity":9,"discount":"0","discountInNum":0},{"identifierId":{"invoiceId":"INV201905SS-001006","itemSKU":"ZR001970"},"pricePerUnit":40000,"itemName":"Renova Zero Catridge","measurementUnit":"pcs","quantity":10,"discount":"0","discountInNum":0}],"signatureUrl":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-006-SIGN.png","customerStore":null},{"invoice":{"invoiceId":"INV201906SS-001001","extInvoiceId":"INV/2019/06/SS-001/001","clientId":"SS-001","salesOrderId":"","extSalesOrderId":"","customerId":"SS-001-CST-RP-001","customerName":"Rizali Ahmad Nugraha","customerStore":"Rizali Ahmad Nugraha","salesName":"Irfan Al Ghozaly","dueDate":"12-06-2019","invoiceDate":"12-06-2019","invoiceMonth":"06-2019","invoiceYear":"2019","paymentStatus":"OVD","termOfPayment":"CASH","tax":"5","totalDiscount":"10","totalAmount":2700000,"finalAmount":2700000,"notes":"asdfg","docUrlPath":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-06-SS-001-001.pdf","totalDiscountInNum":270000,"taxInNum":121500,"remainingAmount":2700000},"items":[{"identifierId":{"invoiceId":"INV201906SS-001001","itemSKU":"RNVA00982"},"pricePerUnit":238500,"itemName":"Renova Zero Pod","measurementUnit":"pcs","quantity":10,"discount":"10","discountInNum":0},{"identifierId":{"invoiceId":"INV201906SS-001001","itemSKU":"TPTRCK000092"},"pricePerUnit":35000,"itemName":"Topi Trucker Red","measurementUnit":"pcs","quantity":9,"discount":"0","discountInNum":0}],"signatureUrl":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-06-SS-001-001-SIGN.png","customerStore":null}]
     * summary : {"numOfPaidInvoice":0,"sumOfPaidInvoice":"0","numOfUnpaidInvoice":3,"sumOfUnpaidInvoice":"4940000","invoiceDetails":[{"numOfInvoices":"0","sumOfInvoices":"0","status":"DUE"},{"numOfInvoices":"0","sumOfInvoices":"0","status":"UNP"},{"numOfInvoices":"3","sumOfInvoices":"4940000","status":"OVD"},{"numOfInvoices":"0","sumOfInvoices":"0","status":"PAP"}]}
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("summary")
    private SummaryEntity summary;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("invoiceContainer")
    private List<InvoiceContainerEntity> invoiceContainer;

    protected Invoice(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Invoice> CREATOR = new Creator<Invoice>() {
        @Override
        public Invoice createFromParcel(Parcel in) {
            return new Invoice(in);
        }

        @Override
        public Invoice[] newArray(int size) {
            return new Invoice[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public SummaryEntity getSummary() {
        return summary;
    }

    public void setSummary(SummaryEntity summary) {
        this.summary = summary;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<InvoiceContainerEntity> getInvoiceContainer() {
        return invoiceContainer;
    }

    public void setInvoiceContainer(List<InvoiceContainerEntity> invoiceContainer) {
        this.invoiceContainer = invoiceContainer;
    }

    public static class SummaryEntity {
        /**
         * numOfPaidInvoice : 0
         * sumOfPaidInvoice : 0
         * numOfUnpaidInvoice : 3
         * sumOfUnpaidInvoice : 4940000
         * invoiceDetails : [{"numOfInvoices":"0","sumOfInvoices":"0","status":"DUE"},{"numOfInvoices":"0","sumOfInvoices":"0","status":"UNP"},{"numOfInvoices":"3","sumOfInvoices":"4940000","status":"OVD"},{"numOfInvoices":"0","sumOfInvoices":"0","status":"PAP"}]
         */

        @SerializedName("numOfPaidInvoice")
        private int numOfPaidInvoice;
        @SerializedName("sumOfPaidInvoice")
        private String sumOfPaidInvoice;
        @SerializedName("numOfUnpaidInvoice")
        private int numOfUnpaidInvoice;
        @SerializedName("sumOfUnpaidInvoice")
        private String sumOfUnpaidInvoice;
        @SerializedName("invoiceDetails")
        private List<InvoiceDetailsEntity> invoiceDetails;

        public int getNumOfPaidInvoice() {
            return numOfPaidInvoice;
        }

        public void setNumOfPaidInvoice(int numOfPaidInvoice) {
            this.numOfPaidInvoice = numOfPaidInvoice;
        }

        public String getSumOfPaidInvoice() {
            return sumOfPaidInvoice;
        }

        public void setSumOfPaidInvoice(String sumOfPaidInvoice) {
            this.sumOfPaidInvoice = sumOfPaidInvoice;
        }

        public int getNumOfUnpaidInvoice() {
            return numOfUnpaidInvoice;
        }

        public void setNumOfUnpaidInvoice(int numOfUnpaidInvoice) {
            this.numOfUnpaidInvoice = numOfUnpaidInvoice;
        }

        public String getSumOfUnpaidInvoice() {
            return sumOfUnpaidInvoice;
        }

        public void setSumOfUnpaidInvoice(String sumOfUnpaidInvoice) {
            this.sumOfUnpaidInvoice = sumOfUnpaidInvoice;
        }

        public List<InvoiceDetailsEntity> getInvoiceDetails() {
            return invoiceDetails;
        }

        public void setInvoiceDetails(List<InvoiceDetailsEntity> invoiceDetails) {
            this.invoiceDetails = invoiceDetails;
        }

        public static class InvoiceDetailsEntity {
            /**
             * numOfInvoices : 0
             * sumOfInvoices : 0
             * status : DUE
             */

            @SerializedName("numOfInvoices")
            private String numOfInvoices;
            @SerializedName("sumOfInvoices")
            private String sumOfInvoices;
            @SerializedName("status")
            private String status;

            public String getNumOfInvoices() {
                return numOfInvoices;
            }

            public void setNumOfInvoices(String numOfInvoices) {
                this.numOfInvoices = numOfInvoices;
            }

            public String getSumOfInvoices() {
                return sumOfInvoices;
            }

            public void setSumOfInvoices(String sumOfInvoices) {
                this.sumOfInvoices = sumOfInvoices;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }

    public static class InvoiceContainerEntity {
        /**
         * invoice : {"invoiceId":"INV201905SS-001005","extInvoiceId":"INV/2019/05/SS-001/005","clientId":"SS-001","salesOrderId":"","extSalesOrderId":"","customerId":"SS-001-CST-RP-001","customerName":"Rizal","customerStore":"Rizal","salesName":"Rizali Ahmad Nugraha","dueDate":"07-05-2019","invoiceDate":"07-05-2019","invoiceMonth":"05-2019","invoiceYear":"2019","paymentStatus":"OVD","termOfPayment":"CASH","tax":"78400","totalDiscount":"0","totalAmount":1120000,"finalAmount":1120000,"notes":"test invoice","docUrlPath":"http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-005.pdf","totalDiscountInNum":0,"taxInNum":878080000,"remainingAmount":1120000}
         * items : [{"identifierId":{"invoiceId":"INV201905SS-001005","itemSKU":"SNDK92929"},"pricePerUnit":80000,"itemName":"Sandisk 16Gb","measurementUnit":"PCS","quantity":9,"discount":"0","discountInNum":0},{"identifierId":{"invoiceId":"INV201905SS-001005","itemSKU":"ZR001970"},"pricePerUnit":40000,"itemName":"Renova Zero Catridge","measurementUnit":"PCS","quantity":10,"discount":"0","discountInNum":0}]
         * signatureUrl : http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-005-SIGN.png
         * customerStore : null
         */

        @SerializedName("invoice")
        private InvoiceEntity invoice;
        @SerializedName("signatureUrl")
        private String signatureUrl;
        @SerializedName("customerStore")
        private Object customerStore;
        @SerializedName("items")
        private List<ItemsEntity> items;

        public InvoiceEntity getInvoice() {
            return invoice;
        }

        public void setInvoice(InvoiceEntity invoice) {
            this.invoice = invoice;
        }

        public String getSignatureUrl() {
            return signatureUrl;
        }

        public void setSignatureUrl(String signatureUrl) {
            this.signatureUrl = signatureUrl;
        }

        public Object getCustomerStore() {
            return customerStore;
        }

        public void setCustomerStore(Object customerStore) {
            this.customerStore = customerStore;
        }

        public List<ItemsEntity> getItems() {
            return items;
        }

        public void setItems(List<ItemsEntity> items) {
            this.items = items;
        }

        public static class InvoiceEntity {
            /**
             * invoiceId : INV201905SS-001005
             * extInvoiceId : INV/2019/05/SS-001/005
             * clientId : SS-001
             * salesOrderId :
             * extSalesOrderId :
             * customerId : SS-001-CST-RP-001
             * customerName : Rizal
             * customerStore : Rizal
             * salesName : Rizali Ahmad Nugraha
             * dueDate : 07-05-2019
             * invoiceDate : 07-05-2019
             * invoiceMonth : 05-2019
             * invoiceYear : 2019
             * paymentStatus : OVD
             * termOfPayment : CASH
             * tax : 78400
             * totalDiscount : 0
             * totalAmount : 1120000
             * finalAmount : 1120000
             * notes : test invoice
             * docUrlPath : http://35.240.148.132/pdf/SS-001/INV/INV-2019-05-SS-001-005.pdf
             * totalDiscountInNum : 0
             * taxInNum : 878080000
             * remainingAmount : 1120000
             */

            @SerializedName("invoiceId")
            private String invoiceId;
            @SerializedName("extInvoiceId")
            private String extInvoiceId;
            @SerializedName("clientId")
            private String clientId;
            @SerializedName("salesOrderId")
            private String salesOrderId;
            @SerializedName("extSalesOrderId")
            private String extSalesOrderId;
            @SerializedName("customerId")
            private String customerId;
            @SerializedName("customerName")
            private String customerName;
            @SerializedName("customerStore")
            private String customerStore;
            @SerializedName("salesName")
            private String salesName;
            @SerializedName("dueDate")
            private String dueDate;
            @SerializedName("invoiceDate")
            private String invoiceDate;
            @SerializedName("invoiceMonth")
            private String invoiceMonth;
            @SerializedName("invoiceYear")
            private String invoiceYear;
            @SerializedName("paymentStatus")
            private String paymentStatus;
            @SerializedName("termOfPayment")
            private String termOfPayment;
            @SerializedName("tax")
            private String tax;
            @SerializedName("totalDiscount")
            private String totalDiscount;
            @SerializedName("totalAmount")
            private int totalAmount;
            @SerializedName("finalAmount")
            private int finalAmount;
            @SerializedName("notes")
            private String notes;
            @SerializedName("docUrlPath")
            private String docUrlPath;
            @SerializedName("totalDiscountInNum")
            private int totalDiscountInNum;
            @SerializedName("taxInNum")
            private int taxInNum;
            @SerializedName("remainingAmount")
            private int remainingAmount;

            public String getInvoiceId() {
                return invoiceId;
            }

            public void setInvoiceId(String invoiceId) {
                this.invoiceId = invoiceId;
            }

            public String getExtInvoiceId() {
                return extInvoiceId;
            }

            public void setExtInvoiceId(String extInvoiceId) {
                this.extInvoiceId = extInvoiceId;
            }

            public String getClientId() {
                return clientId;
            }

            public void setClientId(String clientId) {
                this.clientId = clientId;
            }

            public String getSalesOrderId() {
                return salesOrderId;
            }

            public void setSalesOrderId(String salesOrderId) {
                this.salesOrderId = salesOrderId;
            }

            public String getExtSalesOrderId() {
                return extSalesOrderId;
            }

            public void setExtSalesOrderId(String extSalesOrderId) {
                this.extSalesOrderId = extSalesOrderId;
            }

            public String getCustomerId() {
                return customerId;
            }

            public void setCustomerId(String customerId) {
                this.customerId = customerId;
            }

            public String getCustomerName() {
                return customerName;
            }

            public void setCustomerName(String customerName) {
                this.customerName = customerName;
            }

            public String getCustomerStore() {
                return customerStore;
            }

            public void setCustomerStore(String customerStore) {
                this.customerStore = customerStore;
            }

            public String getSalesName() {
                return salesName;
            }

            public void setSalesName(String salesName) {
                this.salesName = salesName;
            }

            public String getDueDate() {
                return dueDate;
            }

            public void setDueDate(String dueDate) {
                this.dueDate = dueDate;
            }

            public String getInvoiceDate() {
                return invoiceDate;
            }

            public void setInvoiceDate(String invoiceDate) {
                this.invoiceDate = invoiceDate;
            }

            public String getInvoiceMonth() {
                return invoiceMonth;
            }

            public void setInvoiceMonth(String invoiceMonth) {
                this.invoiceMonth = invoiceMonth;
            }

            public String getInvoiceYear() {
                return invoiceYear;
            }

            public void setInvoiceYear(String invoiceYear) {
                this.invoiceYear = invoiceYear;
            }

            public String getPaymentStatus() {
                return paymentStatus;
            }

            public void setPaymentStatus(String paymentStatus) {
                this.paymentStatus = paymentStatus;
            }

            public String getTermOfPayment() {
                return termOfPayment;
            }

            public void setTermOfPayment(String termOfPayment) {
                this.termOfPayment = termOfPayment;
            }

            public String getTax() {
                return tax;
            }

            public void setTax(String tax) {
                this.tax = tax;
            }

            public String getTotalDiscount() {
                return totalDiscount;
            }

            public void setTotalDiscount(String totalDiscount) {
                this.totalDiscount = totalDiscount;
            }

            public int getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(int totalAmount) {
                this.totalAmount = totalAmount;
            }

            public int getFinalAmount() {
                return finalAmount;
            }

            public void setFinalAmount(int finalAmount) {
                this.finalAmount = finalAmount;
            }

            public String getNotes() {
                return notes;
            }

            public void setNotes(String notes) {
                this.notes = notes;
            }

            public String getDocUrlPath() {
                return docUrlPath;
            }

            public void setDocUrlPath(String docUrlPath) {
                this.docUrlPath = docUrlPath;
            }

            public int getTotalDiscountInNum() {
                return totalDiscountInNum;
            }

            public void setTotalDiscountInNum(int totalDiscountInNum) {
                this.totalDiscountInNum = totalDiscountInNum;
            }

            public int getTaxInNum() {
                return taxInNum;
            }

            public void setTaxInNum(int taxInNum) {
                this.taxInNum = taxInNum;
            }

            public int getRemainingAmount() {
                return remainingAmount;
            }

            public void setRemainingAmount(int remainingAmount) {
                this.remainingAmount = remainingAmount;
            }
        }

        public static class ItemsEntity {
            /**
             * identifierId : {"invoiceId":"INV201905SS-001005","itemSKU":"SNDK92929"}
             * pricePerUnit : 80000
             * itemName : Sandisk 16Gb
             * measurementUnit : PCS
             * quantity : 9
             * discount : 0
             * discountInNum : 0
             */

            @SerializedName("identifierId")
            private IdentifierIdEntity identifierId;
            @SerializedName("pricePerUnit")
            private int pricePerUnit;
            @SerializedName("itemName")
            private String itemName;
            @SerializedName("measurementUnit")
            private String measurementUnit;
            @SerializedName("quantity")
            private int quantity;
            @SerializedName("discount")
            private String discount;
            @SerializedName("discountInNum")
            private int discountInNum;

            public IdentifierIdEntity getIdentifierId() {
                return identifierId;
            }

            public void setIdentifierId(IdentifierIdEntity identifierId) {
                this.identifierId = identifierId;
            }

            public int getPricePerUnit() {
                return pricePerUnit;
            }

            public void setPricePerUnit(int pricePerUnit) {
                this.pricePerUnit = pricePerUnit;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getMeasurementUnit() {
                return measurementUnit;
            }

            public void setMeasurementUnit(String measurementUnit) {
                this.measurementUnit = measurementUnit;
            }

            public int getQuantity() {
                return quantity;
            }

            public void setQuantity(int quantity) {
                this.quantity = quantity;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public int getDiscountInNum() {
                return discountInNum;
            }

            public void setDiscountInNum(int discountInNum) {
                this.discountInNum = discountInNum;
            }

            public static class IdentifierIdEntity {
                /**
                 * invoiceId : INV201905SS-001005
                 * itemSKU : SNDK92929
                 */

                @SerializedName("invoiceId")
                private String invoiceId;
                @SerializedName("itemSKU")
                private String itemSKU;

                public String getInvoiceId() {
                    return invoiceId;
                }

                public void setInvoiceId(String invoiceId) {
                    this.invoiceId = invoiceId;
                }

                public String getItemSKU() {
                    return itemSKU;
                }

                public void setItemSKU(String itemSKU) {
                    this.itemSKU = itemSKU;
                }
            }
        }
    }
}
