package com.miru.miruApps.model.retrieve;

import java.io.File;
import java.util.ArrayList;

public class RetrieveCustomerData {
    String fullName;
    String storeName;
    String phoneNumber;
    String mobileNumber;
    String email;
    String category;
    String status;
    String description;
    String tags;
    String creditLimit;
    ArrayList<File> images = new ArrayList<>();

    public RetrieveCustomerData() {
    }

    public RetrieveCustomerData(String fullName, String storeName, String phoneNumber, String mobileNumber, String email, String category, String status, String description, String tags, String creditLimit, ArrayList<File> images) {
        this.fullName = fullName;
        this.storeName = storeName;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.category = category;
        this.status = status;
        this.description = description;
        this.tags = tags;
        this.creditLimit = creditLimit;
        this.images = images;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public ArrayList<File> getImages() {
        return images;
    }

    public void setImages(ArrayList<File> images) {
        this.images = images;
    }
}
