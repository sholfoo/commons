package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddProduct implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * productId : SS-001/11133
     * clientId : SS-001
     * itemSKU : 11133
     * productName : Tissue Paseo
     * category : TISSUE
     * description : tisue putih biasa
     * imageLinks : ["http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg"]
     * brand : Paseo
     * size : 200 lembar
     * productPrices : [{"measurementUnit":"PCS","pricePerUnit":"15000","sellingPrice":"20000","discount":"1000"},{"measurementUnit":"LUSIN","pricePerUnit":"300000","sellingPrice":"350000","discount":"10000"}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("productId")
    private String productId;
    @SerializedName("clientId")
    private String clientId;
    @SerializedName("itemSKU")
    private String itemSKU;
    @SerializedName("productName")
    private String productName;
    @SerializedName("category")
    private String category;
    @SerializedName("description")
    private String description;
    @SerializedName("brand")
    private String brand;
    @SerializedName("size")
    private String size;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("imageLinks")
    private List<String> imageLinks;
    @SerializedName("productPrices")
    private List<ProductPricesEntity> productPrices;

    protected AddProduct(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
        productId = in.readString();
        clientId = in.readString();
        itemSKU = in.readString();
        productName = in.readString();
        category = in.readString();
        description = in.readString();
        brand = in.readString();
        size = in.readString();
        imageLinks = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
        dest.writeString(productId);
        dest.writeString(clientId);
        dest.writeString(itemSKU);
        dest.writeString(productName);
        dest.writeString(category);
        dest.writeString(description);
        dest.writeString(brand);
        dest.writeString(size);
        dest.writeStringList(imageLinks);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AddProduct> CREATOR = new Creator<AddProduct>() {
        @Override
        public AddProduct createFromParcel(Parcel in) {
            return new AddProduct(in);
        }

        @Override
        public AddProduct[] newArray(int size) {
            return new AddProduct[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getImageLinks() {
        return imageLinks;
    }

    public void setImageLinks(List<String> imageLinks) {
        this.imageLinks = imageLinks;
    }

    public List<ProductPricesEntity> getProductPrices() {
        return productPrices;
    }

    public void setProductPrices(List<ProductPricesEntity> productPrices) {
        this.productPrices = productPrices;
    }

    public static class ProductPricesEntity {
        /**
         * measurementUnit : PCS
         * pricePerUnit : 15000
         * sellingPrice : 20000
         * discount : 1000
         */

        @SerializedName("measurementUnit")
        private String measurementUnit;
        @SerializedName("pricePerUnit")
        private String pricePerUnit;
        @SerializedName("sellingPrice")
        private String sellingPrice;
        @SerializedName("discount")
        private String discount;

        public String getMeasurementUnit() {
            return measurementUnit;
        }

        public void setMeasurementUnit(String measurementUnit) {
            this.measurementUnit = measurementUnit;
        }

        public String getPricePerUnit() {
            return pricePerUnit;
        }

        public void setPricePerUnit(String pricePerUnit) {
            this.pricePerUnit = pricePerUnit;
        }

        public String getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(String sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }
    }
}
