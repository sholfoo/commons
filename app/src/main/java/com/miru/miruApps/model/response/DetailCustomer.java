package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailCustomer implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * customerContainer : [{"customerId":"SS-001-CST-AS-001","clientId":"SS-001","ownerName":"Amira","storeName":"Amira Store","address":[{"address":"Jl. Nusantara No.12a, RW.2, Pasir Gn. Sel., Cimanggis, Kota Depok, Jawa Barat 16451, Indonesia","city":"\"\"","province":"Jawa Barat","postalCode":"16451","phoneNumber":"+6285728373838"}],"emailAddress":"amira@gmail.com","imageLink":["http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-AS-001/SS-001-SS-001-CST-AS-001-1.png","http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-AS-001/SS-001-SS-001-CST-AS-001-1.png"],"customerImage":"http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-AS-001/SS-001-SS-001-CST-AS-001-1.png","status":"deactive","customerType":"Retail","customerTag":["clothing",""],"mobilePhone":"081111111","longitudes":"106.8396013","latitudes":"-6.3460087","description":"toko baju","creditLimit":"0","lastUpdatedTime":"14-06-2019 07:50:13.000"}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("customerContainer")
    private List<CustomerContainerEntity> customerContainer;

    protected DetailCustomer(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetailCustomer> CREATOR = new Creator<DetailCustomer>() {
        @Override
        public DetailCustomer createFromParcel(Parcel in) {
            return new DetailCustomer(in);
        }

        @Override
        public DetailCustomer[] newArray(int size) {
            return new DetailCustomer[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<CustomerContainerEntity> getCustomerContainer() {
        return customerContainer;
    }

    public void setCustomerContainer(List<CustomerContainerEntity> customerContainer) {
        this.customerContainer = customerContainer;
    }

    public static class CustomerContainerEntity {
        /**
         * customerId : SS-001-CST-AS-001
         * clientId : SS-001
         * ownerName : Amira
         * storeName : Amira Store
         * address : [{"address":"Jl. Nusantara No.12a, RW.2, Pasir Gn. Sel., Cimanggis, Kota Depok, Jawa Barat 16451, Indonesia","city":"\"\"","province":"Jawa Barat","postalCode":"16451","phoneNumber":"+6285728373838"}]
         * emailAddress : amira@gmail.com
         * imageLink : ["http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-AS-001/SS-001-SS-001-CST-AS-001-1.png","http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-AS-001/SS-001-SS-001-CST-AS-001-1.png"]
         * customerImage : http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-AS-001/SS-001-SS-001-CST-AS-001-1.png
         * status : deactive
         * customerType : Retail
         * customerTag : ["clothing",""]
         * mobilePhone : 081111111
         * longitudes : 106.8396013
         * latitudes : -6.3460087
         * description : toko baju
         * creditLimit : 0
         * lastUpdatedTime : 14-06-2019 07:50:13.000
         */

        @SerializedName("customerId")
        private String customerId;
        @SerializedName("clientId")
        private String clientId;
        @SerializedName("ownerName")
        private String ownerName;
        @SerializedName("storeName")
        private String storeName;
        @SerializedName("emailAddress")
        private String emailAddress;
        @SerializedName("customerImage")
        private String customerImage;
        @SerializedName("status")
        private String status;
        @SerializedName("customerType")
        private String customerType;
        @SerializedName("mobilePhone")
        private String mobilePhone;
        @SerializedName("longitudes")
        private String longitudes;
        @SerializedName("latitudes")
        private String latitudes;
        @SerializedName("description")
        private String description;
        @SerializedName("creditLimit")
        private String creditLimit;
        @SerializedName("lastUpdatedTime")
        private String lastUpdatedTime;
        @SerializedName("address")
        private List<AddressEntity> address;
        @SerializedName("imageLink")
        private List<String> imageLink;
        @SerializedName("customerTag")
        private List<String> customerTag;

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getOwnerName() {
            return ownerName;
        }

        public void setOwnerName(String ownerName) {
            this.ownerName = ownerName;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getEmailAddress() {
            return emailAddress;
        }

        public void setEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        public String getCustomerImage() {
            return customerImage;
        }

        public void setCustomerImage(String customerImage) {
            this.customerImage = customerImage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCustomerType() {
            return customerType;
        }

        public void setCustomerType(String customerType) {
            this.customerType = customerType;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

        public String getLongitudes() {
            return longitudes;
        }

        public void setLongitudes(String longitudes) {
            this.longitudes = longitudes;
        }

        public String getLatitudes() {
            return latitudes;
        }

        public void setLatitudes(String latitudes) {
            this.latitudes = latitudes;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreditLimit() {
            return creditLimit;
        }

        public void setCreditLimit(String creditLimit) {
            this.creditLimit = creditLimit;
        }

        public String getLastUpdatedTime() {
            return lastUpdatedTime;
        }

        public void setLastUpdatedTime(String lastUpdatedTime) {
            this.lastUpdatedTime = lastUpdatedTime;
        }

        public List<AddressEntity> getAddress() {
            return address;
        }

        public void setAddress(List<AddressEntity> address) {
            this.address = address;
        }

        public List<String> getImageLink() {
            return imageLink;
        }

        public void setImageLink(List<String> imageLink) {
            this.imageLink = imageLink;
        }

        public List<String> getCustomerTag() {
            return customerTag;
        }

        public void setCustomerTag(List<String> customerTag) {
            this.customerTag = customerTag;
        }

        public static class AddressEntity {
            /**
             * address : Jl. Nusantara No.12a, RW.2, Pasir Gn. Sel., Cimanggis, Kota Depok, Jawa Barat 16451, Indonesia
             * city : ""
             * province : Jawa Barat
             * postalCode : 16451
             * phoneNumber : +6285728373838
             */

            @SerializedName("address")
            private String address;
            @SerializedName("city")
            private String city;
            @SerializedName("province")
            private String province;
            @SerializedName("postalCode")
            private String postalCode;
            @SerializedName("phoneNumber")
            private String phoneNumber;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getPostalCode() {
                return postalCode;
            }

            public void setPostalCode(String postalCode) {
                this.postalCode = postalCode;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }
        }
    }
}
