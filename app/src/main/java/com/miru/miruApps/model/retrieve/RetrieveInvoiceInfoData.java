package com.miru.miruApps.model.retrieve;

public class RetrieveInvoiceInfoData {

    String customerId;
    String mSalesName;
    String mStoreName;
    String mAddress;
    String mCity;
    String mZipCode;
    String mPhoneNumber;
    String mTermOfPayment;
    String mDueDate;
    String mInvoiceDate;
    String mDescription;

    public RetrieveInvoiceInfoData(String customerId, String mSalesName, String mStoreName, String mAddress, String mCity, String mZipCode, String mPhoneNumber, String mTermOfPayment, String mDueDate, String mInvoiceDate, String mDescription) {
        this.customerId = customerId;
        this.mSalesName = mSalesName;
        this.mStoreName = mStoreName;
        this.mAddress = mAddress;
        this.mCity = mCity;
        this.mZipCode = mZipCode;
        this.mPhoneNumber = mPhoneNumber;
        this.mTermOfPayment = mTermOfPayment;
        this.mDueDate = mDueDate;
        this.mInvoiceDate = mInvoiceDate;
        this.mDescription = mDescription;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getmSalesName() {
        return mSalesName;
    }

    public void setmSalesName(String mSalesName) {
        this.mSalesName = mSalesName;
    }

    public String getmStoreName() {
        return mStoreName;
    }

    public void setmStoreName(String mStoreName) {
        this.mStoreName = mStoreName;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmZipCode() {
        return mZipCode;
    }

    public void setmZipCode(String mZipCode) {
        this.mZipCode = mZipCode;
    }

    public String getmPhoneNumber() {
        return mPhoneNumber;
    }

    public void setmPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public String getmTermOfPayment() {
        return mTermOfPayment;
    }

    public void setmTermOfPayment(String mTermOfPayment) {
        this.mTermOfPayment = mTermOfPayment;
    }

    public String getmDueDate() {
        return mDueDate;
    }

    public void setmDueDate(String mDueDate) {
        this.mDueDate = mDueDate;
    }

    public String getmInvoiceDate() {
        return mInvoiceDate;
    }

    public void setmInvoiceDate(String mInvoiceDate) {
        this.mInvoiceDate = mInvoiceDate;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
