package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SummaryDashboard implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * currentMonthInvoiceSummary : {"currentMonthFinalAmount":"2775000","percentIncreaseNumOfInvoice":"-40","lastMonthNumOfInvoices":"5","lastMonthFinalAmount":"2500000","currentMonthAvgAmount":"925000","currentMonthNumOfInvoices":"3","percentIncreaseSumOfInvoice":"11"}
     * unpaidInvoiceSummary : {"numOfUnpaidInvoices":"9","sumOfUnpaidAmount":"7975000","numOfUnpaidCustomer":"3"}
     * customerInvoiceSummary : [{"customerProfile":"http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-TB-002/SS-001-SS-001-CST-TB-002-1.jpeg","customerStoreName":"Tikus Biru","customerId":"SS-001-CST-TB-002","sumOfFinalAmount":"535000","numOfInvoices":"1"},{"customerProfile":"http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-RP-001/SS-001-SS-001-CST-RP-001-1.jpeg","customerStoreName":"Rizali Phone Store","customerId":"SS-001-CST-RP-001","sumOfFinalAmount":"2240000","numOfInvoices":"2"}]
     * paymentSummary : {"sumOfPayments":"2120000","avgOfPayments":"706666","numOfPayments":"3"}
     * monthlyInvoiceSummary : [{"month":"05-2019","sumOfFinalAmount":"2775000","numOfInvoices":"3"},{"month":"04-2019","sumOfFinalAmount":"2500000","numOfInvoices":"5"},{"month":"06-2019","sumOfFinalAmount":"3575000","numOfInvoices":"3"}]
     * productSummary : [{"images":["http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png","http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png"],"itemSKU":"ZR001970","itemName":"Renova Zero Catridge","unit":"PCS","totalQty":10,"totalAmount":400000},{"images":["http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png","http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png"],"itemSKU":"ZR001970","itemName":"Renova Zero Catridge","unit":"pcs","totalQty":19,"totalAmount":760000},{"images":["http://35.240.148.132/images/productImages/SS-001/TPTRCK000092/SS-001-TPTRCK000092-1.png","http://35.240.148.132/images/productImages/SS-001/TPTRCK000092/SS-001-TPTRCK000092-1.png"],"itemSKU":"TPTRCK000092","itemName":"Topi Trucker Red","unit":"pcs","totalQty":5,"totalAmount":175000},{"images":["http://35.240.148.132/images/productImages/SS-001/SNDK92929/SS-001-SNDK92929-1.png","http://35.240.148.132/images/productImages/SS-001/SNDK92929/SS-001-SNDK92929-1.png"],"itemSKU":"SNDK92929","itemName":"Sandisk 16Gb","unit":"PCS","totalQty":9,"totalAmount":720000},{"images":["http://35.240.148.132/images/productImages/SS-001/SNDK92929/SS-001-SNDK92929-1.png","http://35.240.148.132/images/productImages/SS-001/SNDK92929/SS-001-SNDK92929-1.png"],"itemSKU":"SNDK92929","itemName":"Sandisk 16Gb","unit":"pcs","totalQty":9,"totalAmount":720000}]
     * orderSummary : {"numOfOrders":"1","avgOfOrders":"2943165","sumOfOrders":"2943165"}
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("currentMonthInvoiceSummary")
    private CurrentMonthInvoiceSummaryEntity currentMonthInvoiceSummary;
    @SerializedName("unpaidInvoiceSummary")
    private UnpaidInvoiceSummaryEntity unpaidInvoiceSummary;
    @SerializedName("paymentSummary")
    private PaymentSummaryEntity paymentSummary;
    @SerializedName("orderSummary")
    private OrderSummaryEntity orderSummary;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("customerInvoiceSummary")
    private List<CustomerInvoiceSummaryEntity> customerInvoiceSummary;
    @SerializedName("monthlyInvoiceSummary")
    private List<MonthlyInvoiceSummaryEntity> monthlyInvoiceSummary;
    @SerializedName("productSummary")
    private List<ProductSummaryEntity> productSummary;

    protected SummaryDashboard(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SummaryDashboard> CREATOR = new Creator<SummaryDashboard>() {
        @Override
        public SummaryDashboard createFromParcel(Parcel in) {
            return new SummaryDashboard(in);
        }

        @Override
        public SummaryDashboard[] newArray(int size) {
            return new SummaryDashboard[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public CurrentMonthInvoiceSummaryEntity getCurrentMonthInvoiceSummary() {
        return currentMonthInvoiceSummary;
    }

    public void setCurrentMonthInvoiceSummary(CurrentMonthInvoiceSummaryEntity currentMonthInvoiceSummary) {
        this.currentMonthInvoiceSummary = currentMonthInvoiceSummary;
    }

    public UnpaidInvoiceSummaryEntity getUnpaidInvoiceSummary() {
        return unpaidInvoiceSummary;
    }

    public void setUnpaidInvoiceSummary(UnpaidInvoiceSummaryEntity unpaidInvoiceSummary) {
        this.unpaidInvoiceSummary = unpaidInvoiceSummary;
    }

    public PaymentSummaryEntity getPaymentSummary() {
        return paymentSummary;
    }

    public void setPaymentSummary(PaymentSummaryEntity paymentSummary) {
        this.paymentSummary = paymentSummary;
    }

    public OrderSummaryEntity getOrderSummary() {
        return orderSummary;
    }

    public void setOrderSummary(OrderSummaryEntity orderSummary) {
        this.orderSummary = orderSummary;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<CustomerInvoiceSummaryEntity> getCustomerInvoiceSummary() {
        return customerInvoiceSummary;
    }

    public void setCustomerInvoiceSummary(List<CustomerInvoiceSummaryEntity> customerInvoiceSummary) {
        this.customerInvoiceSummary = customerInvoiceSummary;
    }

    public List<MonthlyInvoiceSummaryEntity> getMonthlyInvoiceSummary() {
        return monthlyInvoiceSummary;
    }

    public void setMonthlyInvoiceSummary(List<MonthlyInvoiceSummaryEntity> monthlyInvoiceSummary) {
        this.monthlyInvoiceSummary = monthlyInvoiceSummary;
    }

    public List<ProductSummaryEntity> getProductSummary() {
        return productSummary;
    }

    public void setProductSummary(List<ProductSummaryEntity> productSummary) {
        this.productSummary = productSummary;
    }

    public static class CurrentMonthInvoiceSummaryEntity {
        /**
         * currentMonthFinalAmount : 2775000
         * percentIncreaseNumOfInvoice : -40
         * lastMonthNumOfInvoices : 5
         * lastMonthFinalAmount : 2500000
         * currentMonthAvgAmount : 925000
         * currentMonthNumOfInvoices : 3
         * percentIncreaseSumOfInvoice : 11
         */

        @SerializedName("currentMonthFinalAmount")
        private String currentMonthFinalAmount;
        @SerializedName("percentIncreaseNumOfInvoice")
        private String percentIncreaseNumOfInvoice;
        @SerializedName("lastMonthNumOfInvoices")
        private String lastMonthNumOfInvoices;
        @SerializedName("lastMonthFinalAmount")
        private String lastMonthFinalAmount;
        @SerializedName("currentMonthAvgAmount")
        private String currentMonthAvgAmount;
        @SerializedName("currentMonthNumOfInvoices")
        private String currentMonthNumOfInvoices;
        @SerializedName("percentIncreaseSumOfInvoice")
        private String percentIncreaseSumOfInvoice;

        public String getCurrentMonthFinalAmount() {
            return currentMonthFinalAmount;
        }

        public void setCurrentMonthFinalAmount(String currentMonthFinalAmount) {
            this.currentMonthFinalAmount = currentMonthFinalAmount;
        }

        public String getPercentIncreaseNumOfInvoice() {
            return percentIncreaseNumOfInvoice;
        }

        public void setPercentIncreaseNumOfInvoice(String percentIncreaseNumOfInvoice) {
            this.percentIncreaseNumOfInvoice = percentIncreaseNumOfInvoice;
        }

        public String getLastMonthNumOfInvoices() {
            return lastMonthNumOfInvoices;
        }

        public void setLastMonthNumOfInvoices(String lastMonthNumOfInvoices) {
            this.lastMonthNumOfInvoices = lastMonthNumOfInvoices;
        }

        public String getLastMonthFinalAmount() {
            return lastMonthFinalAmount;
        }

        public void setLastMonthFinalAmount(String lastMonthFinalAmount) {
            this.lastMonthFinalAmount = lastMonthFinalAmount;
        }

        public String getCurrentMonthAvgAmount() {
            return currentMonthAvgAmount;
        }

        public void setCurrentMonthAvgAmount(String currentMonthAvgAmount) {
            this.currentMonthAvgAmount = currentMonthAvgAmount;
        }

        public String getCurrentMonthNumOfInvoices() {
            return currentMonthNumOfInvoices;
        }

        public void setCurrentMonthNumOfInvoices(String currentMonthNumOfInvoices) {
            this.currentMonthNumOfInvoices = currentMonthNumOfInvoices;
        }

        public String getPercentIncreaseSumOfInvoice() {
            return percentIncreaseSumOfInvoice;
        }

        public void setPercentIncreaseSumOfInvoice(String percentIncreaseSumOfInvoice) {
            this.percentIncreaseSumOfInvoice = percentIncreaseSumOfInvoice;
        }
    }

    public static class UnpaidInvoiceSummaryEntity {
        /**
         * numOfUnpaidInvoices : 9
         * sumOfUnpaidAmount : 7975000
         * numOfUnpaidCustomer : 3
         */

        @SerializedName("numOfUnpaidInvoices")
        private String numOfUnpaidInvoices;
        @SerializedName("sumOfUnpaidAmount")
        private String sumOfUnpaidAmount;
        @SerializedName("numOfUnpaidCustomer")
        private String numOfUnpaidCustomer;

        public String getNumOfUnpaidInvoices() {
            return numOfUnpaidInvoices;
        }

        public void setNumOfUnpaidInvoices(String numOfUnpaidInvoices) {
            this.numOfUnpaidInvoices = numOfUnpaidInvoices;
        }

        public String getSumOfUnpaidAmount() {
            return sumOfUnpaidAmount;
        }

        public void setSumOfUnpaidAmount(String sumOfUnpaidAmount) {
            this.sumOfUnpaidAmount = sumOfUnpaidAmount;
        }

        public String getNumOfUnpaidCustomer() {
            return numOfUnpaidCustomer;
        }

        public void setNumOfUnpaidCustomer(String numOfUnpaidCustomer) {
            this.numOfUnpaidCustomer = numOfUnpaidCustomer;
        }
    }

    public static class PaymentSummaryEntity {
        /**
         * sumOfPayments : 2120000
         * avgOfPayments : 706666
         * numOfPayments : 3
         */

        @SerializedName("sumOfPayments")
        private String sumOfPayments;
        @SerializedName("avgOfPayments")
        private String avgOfPayments;
        @SerializedName("numOfPayments")
        private String numOfPayments;

        public String getSumOfPayments() {
            return sumOfPayments;
        }

        public void setSumOfPayments(String sumOfPayments) {
            this.sumOfPayments = sumOfPayments;
        }

        public String getAvgOfPayments() {
            return avgOfPayments;
        }

        public void setAvgOfPayments(String avgOfPayments) {
            this.avgOfPayments = avgOfPayments;
        }

        public String getNumOfPayments() {
            return numOfPayments;
        }

        public void setNumOfPayments(String numOfPayments) {
            this.numOfPayments = numOfPayments;
        }
    }

    public static class OrderSummaryEntity {
        /**
         * numOfOrders : 1
         * avgOfOrders : 2943165
         * sumOfOrders : 2943165
         */

        @SerializedName("numOfOrders")
        private String numOfOrders;
        @SerializedName("avgOfOrders")
        private String avgOfOrders;
        @SerializedName("sumOfOrders")
        private String sumOfOrders;

        public String getNumOfOrders() {
            return numOfOrders;
        }

        public void setNumOfOrders(String numOfOrders) {
            this.numOfOrders = numOfOrders;
        }

        public String getAvgOfOrders() {
            return avgOfOrders;
        }

        public void setAvgOfOrders(String avgOfOrders) {
            this.avgOfOrders = avgOfOrders;
        }

        public String getSumOfOrders() {
            return sumOfOrders;
        }

        public void setSumOfOrders(String sumOfOrders) {
            this.sumOfOrders = sumOfOrders;
        }
    }

    public static class CustomerInvoiceSummaryEntity {
        /**
         * customerProfile : http://35.240.148.132/images/customerImages/SS-001/SS-001-CST-TB-002/SS-001-SS-001-CST-TB-002-1.jpeg
         * customerStoreName : Tikus Biru
         * customerId : SS-001-CST-TB-002
         * sumOfFinalAmount : 535000
         * numOfInvoices : 1
         */

        @SerializedName("customerProfile")
        private String customerProfile;
        @SerializedName("customerStoreName")
        private String customerStoreName;
        @SerializedName("customerId")
        private String customerId;
        @SerializedName("sumOfFinalAmount")
        private String sumOfFinalAmount;
        @SerializedName("numOfInvoices")
        private String numOfInvoices;

        public String getCustomerProfile() {
            return customerProfile;
        }

        public void setCustomerProfile(String customerProfile) {
            this.customerProfile = customerProfile;
        }

        public String getCustomerStoreName() {
            return customerStoreName;
        }

        public void setCustomerStoreName(String customerStoreName) {
            this.customerStoreName = customerStoreName;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getSumOfFinalAmount() {
            return sumOfFinalAmount;
        }

        public void setSumOfFinalAmount(String sumOfFinalAmount) {
            this.sumOfFinalAmount = sumOfFinalAmount;
        }

        public String getNumOfInvoices() {
            return numOfInvoices;
        }

        public void setNumOfInvoices(String numOfInvoices) {
            this.numOfInvoices = numOfInvoices;
        }
    }

    public static class MonthlyInvoiceSummaryEntity {
        /**
         * month : 05-2019
         * sumOfFinalAmount : 2775000
         * numOfInvoices : 3
         */

        @SerializedName("month")
        private String month;
        @SerializedName("sumOfFinalAmount")
        private String sumOfFinalAmount;
        @SerializedName("numOfInvoices")
        private String numOfInvoices;

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getSumOfFinalAmount() {
            return sumOfFinalAmount;
        }

        public void setSumOfFinalAmount(String sumOfFinalAmount) {
            this.sumOfFinalAmount = sumOfFinalAmount;
        }

        public String getNumOfInvoices() {
            return numOfInvoices;
        }

        public void setNumOfInvoices(String numOfInvoices) {
            this.numOfInvoices = numOfInvoices;
        }
    }

    public static class ProductSummaryEntity {
        /**
         * images : ["http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png","http://35.240.148.132/images/productImages/SS-001/ZR001970/SS-001-ZR001970-1.png"]
         * itemSKU : ZR001970
         * itemName : Renova Zero Catridge
         * unit : PCS
         * totalQty : 10
         * totalAmount : 400000
         */

        @SerializedName("itemSKU")
        private String itemSKU;
        @SerializedName("itemName")
        private String itemName;
        @SerializedName("unit")
        private String unit;
        @SerializedName("totalQty")
        private int totalQty;
        @SerializedName("totalAmount")
        private int totalAmount;
        @SerializedName("images")
        private List<String> images;

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public int getTotalQty() {
            return totalQty;
        }

        public void setTotalQty(int totalQty) {
            this.totalQty = totalQty;
        }

        public int getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(int totalAmount) {
            this.totalAmount = totalAmount;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }
    }
}
