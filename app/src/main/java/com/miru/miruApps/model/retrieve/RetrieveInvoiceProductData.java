package com.miru.miruApps.model.retrieve;

import com.miru.miruApps.model.SoProduct;

import java.util.List;

public class RetrieveInvoiceProductData {

    RetrieveInvoiceInfoData retrieveInvoiceInfoData;
    List<SoProduct> soProductList;
    int mSubtotal;
    int mSubtotalAftterDiscount;
    String mTaxPercent;
    int mTaxTotal;
    String mDiscountPercent;
    int mDiscountTotal;
    int mTotalAmount;

    public RetrieveInvoiceProductData(RetrieveInvoiceInfoData retrieveInvoiceInfoData, List<SoProduct> soProductList, int mSubtotal, int mSubtotalAftterDiscount, String mTaxPercent, int mTaxTotal, String mDiscountPercent, int mDiscountTotal, int mTotalAmount) {
        this.retrieveInvoiceInfoData = retrieveInvoiceInfoData;
        this.soProductList = soProductList;
        this.mSubtotal = mSubtotal;
        this.mSubtotalAftterDiscount = mSubtotalAftterDiscount;
        this.mTaxPercent = mTaxPercent;
        this.mTaxTotal = mTaxTotal;
        this.mDiscountPercent = mDiscountPercent;
        this.mDiscountTotal = mDiscountTotal;
        this.mTotalAmount = mTotalAmount;
    }

    public RetrieveInvoiceInfoData getRetrieveInvoiceInfoData() {
        return retrieveInvoiceInfoData;
    }

    public void setRetrieveInvoiceInfoData(RetrieveInvoiceInfoData retrieveInvoiceInfoData) {
        this.retrieveInvoiceInfoData = retrieveInvoiceInfoData;
    }

    public List<SoProduct> getSoProductList() {
        return soProductList;
    }

    public void setSoProductList(List<SoProduct> soProductList) {
        this.soProductList = soProductList;
    }

    public int getmSubtotal() {
        return mSubtotal;
    }

    public void setmSubtotal(int mSubtotal) {
        this.mSubtotal = mSubtotal;
    }

    public int getmSubtotalAftterDiscount() {
        return mSubtotalAftterDiscount;
    }

    public void setmSubtotalAftterDiscount(int mSubtotalAftterDiscount) {
        this.mSubtotalAftterDiscount = mSubtotalAftterDiscount;
    }

    public String getmTaxPercent() {
        return mTaxPercent;
    }

    public void setmTaxPercent(String mTaxPercent) {
        this.mTaxPercent = mTaxPercent;
    }

    public int getmTaxTotal() {
        return mTaxTotal;
    }

    public void setmTaxTotal(int mTaxTotal) {
        this.mTaxTotal = mTaxTotal;
    }

    public String getmDiscountPercent() {
        return mDiscountPercent;
    }

    public void setmDiscountPercent(String mDiscountPercent) {
        this.mDiscountPercent = mDiscountPercent;
    }

    public int getmDiscountTotal() {
        return mDiscountTotal;
    }

    public void setmDiscountTotal(int mDiscountTotal) {
        this.mDiscountTotal = mDiscountTotal;
    }

    public int getmTotalAmount() {
        return mTotalAmount;
    }

    public void setmTotalAmount(int mTotalAmount) {
        this.mTotalAmount = mTotalAmount;
    }
}
