package com.miru.miruApps.model;

public class ProductPrice {
    String productUnit;
    String productBasicPrice;
    String productSellingPrice;
    String productDiscount;

    public ProductPrice() {

    }

    public ProductPrice(String productUnit, String productBasicPrice, String productSellingPrice, String productDiscount) {
        this.productUnit = productUnit;
        this.productBasicPrice = productBasicPrice;
        this.productSellingPrice = productSellingPrice;
        this.productDiscount = productDiscount;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public String getProductBasicPrice() {
        return productBasicPrice;
    }

    public void setProductBasicPrice(String productBasicPrice) {
        this.productBasicPrice = productBasicPrice;
    }

    public String getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(String productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }
}
