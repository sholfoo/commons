package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailSo implements Parcelable{


    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * orders : [{"salesOrder":{"salesOrderId":"SO201904SS-001001","clientId":"SS-001","customerId":"123","salesName":"BUDI","externalSoId":"SO/2019/04/SS-001/001","tax":"230000","taxInNum":4761000000,"totalAmount":2300000,"finalAmount":2530000,"finalDiscount":"10","finalDiscountInNum":230000,"notes":"TEST SALES ORDER","creationDate":"26-04-2019","salesOrderDate":"26-04-2019","fulfilledTotalAmount":2300000,"fulfilledFinalAmount":0,"fulfilledFinalDiscount":"0"},"items":{"items":[{"itemSKU":"1222","quantity":10,"fulfilledQuantity":10,"measurementUnit":"PCS","fulfilledMeasurementUnit":"-","pricePerUnit":2500,"fulfilledPricePerUnit":0,"discount":"0","discountInNum":0},{"itemSKU":"2221","quantity":2,"fulfilledQuantity":2,"measurementUnit":"PCS","fulfilledMeasurementUnit":"-","pricePerUnit":1500,"fulfilledPricePerUnit":0,"discount":"0","discountInNum":0},{"itemSKU":"2314","quantity":5,"fulfilledQuantity":5,"measurementUnit":"PCS","fulfilledMeasurementUnit":"-","pricePerUnit":2000,"fulfilledPricePerUnit":0,"discount":"0","discountInNum":0}]}}]
     * info : {"numOfProcessedOrders":0,"sumOfProcessedOrders":"0","numOfUnprocessedOrders":1,"sumOfUnprocessedOrders":"2530000"}
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("info")
    private InfoEntity info;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("orders")
    private List<OrdersEntity> orders;

    protected DetailSo(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetailSo> CREATOR = new Creator<DetailSo>() {
        @Override
        public DetailSo createFromParcel(Parcel in) {
            return new DetailSo(in);
        }

        @Override
        public DetailSo[] newArray(int size) {
            return new DetailSo[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public InfoEntity getInfo() {
        return info;
    }

    public void setInfo(InfoEntity info) {
        this.info = info;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<OrdersEntity> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersEntity> orders) {
        this.orders = orders;
    }

    public static class InfoEntity {
        /**
         * numOfProcessedOrders : 0
         * sumOfProcessedOrders : 0
         * numOfUnprocessedOrders : 1
         * sumOfUnprocessedOrders : 2530000
         */

        @SerializedName("numOfProcessedOrders")
        private int numOfProcessedOrders;
        @SerializedName("sumOfProcessedOrders")
        private String sumOfProcessedOrders;
        @SerializedName("numOfUnprocessedOrders")
        private int numOfUnprocessedOrders;
        @SerializedName("sumOfUnprocessedOrders")
        private String sumOfUnprocessedOrders;

        public int getNumOfProcessedOrders() {
            return numOfProcessedOrders;
        }

        public void setNumOfProcessedOrders(int numOfProcessedOrders) {
            this.numOfProcessedOrders = numOfProcessedOrders;
        }

        public String getSumOfProcessedOrders() {
            return sumOfProcessedOrders;
        }

        public void setSumOfProcessedOrders(String sumOfProcessedOrders) {
            this.sumOfProcessedOrders = sumOfProcessedOrders;
        }

        public int getNumOfUnprocessedOrders() {
            return numOfUnprocessedOrders;
        }

        public void setNumOfUnprocessedOrders(int numOfUnprocessedOrders) {
            this.numOfUnprocessedOrders = numOfUnprocessedOrders;
        }

        public String getSumOfUnprocessedOrders() {
            return sumOfUnprocessedOrders;
        }

        public void setSumOfUnprocessedOrders(String sumOfUnprocessedOrders) {
            this.sumOfUnprocessedOrders = sumOfUnprocessedOrders;
        }
    }

    public static class OrdersEntity {
        /**
         * salesOrder : {"salesOrderId":"SO201904SS-001001","clientId":"SS-001","customerId":"123","salesName":"BUDI","externalSoId":"SO/2019/04/SS-001/001","tax":"230000","taxInNum":4761000000,"totalAmount":2300000,"finalAmount":2530000,"finalDiscount":"10","finalDiscountInNum":230000,"notes":"TEST SALES ORDER","creationDate":"26-04-2019","salesOrderDate":"26-04-2019","fulfilledTotalAmount":2300000,"fulfilledFinalAmount":0,"fulfilledFinalDiscount":"0"}
         * items : {"items":[{"itemSKU":"1222","quantity":10,"fulfilledQuantity":10,"measurementUnit":"PCS","fulfilledMeasurementUnit":"-","pricePerUnit":2500,"fulfilledPricePerUnit":0,"discount":"0","discountInNum":0},{"itemSKU":"2221","quantity":2,"fulfilledQuantity":2,"measurementUnit":"PCS","fulfilledMeasurementUnit":"-","pricePerUnit":1500,"fulfilledPricePerUnit":0,"discount":"0","discountInNum":0},{"itemSKU":"2314","quantity":5,"fulfilledQuantity":5,"measurementUnit":"PCS","fulfilledMeasurementUnit":"-","pricePerUnit":2000,"fulfilledPricePerUnit":0,"discount":"0","discountInNum":0}]}
         */

        @SerializedName("salesOrder")
        private SalesOrderEntity salesOrder;
        @SerializedName("items")
        private ItemsEntityX items;

        public SalesOrderEntity getSalesOrder() {
            return salesOrder;
        }

        public void setSalesOrder(SalesOrderEntity salesOrder) {
            this.salesOrder = salesOrder;
        }

        public ItemsEntityX getItems() {
            return items;
        }

        public void setItems(ItemsEntityX items) {
            this.items = items;
        }

        public static class SalesOrderEntity {
            /**
             * salesOrderId : SO201904SS-001001
             * clientId : SS-001
             * customerId : 123
             * salesName : BUDI
             * externalSoId : SO/2019/04/SS-001/001
             * tax : 230000
             * taxInNum : 4761000000
             * totalAmount : 2300000
             * finalAmount : 2530000
             * finalDiscount : 10
             * finalDiscountInNum : 230000
             * notes : TEST SALES ORDER
             * creationDate : 26-04-2019
             * salesOrderDate : 26-04-2019
             * fulfilledTotalAmount : 2300000
             * fulfilledFinalAmount : 0
             * fulfilledFinalDiscount : 0
             */

            @SerializedName("salesOrderId")
            private String salesOrderId;
            @SerializedName("clientId")
            private String clientId;
            @SerializedName("customerId")
            private String customerId;
            @SerializedName("salesName")
            private String salesName;
            @SerializedName("externalSoId")
            private String externalSoId;
            @SerializedName("tax")
            private String tax;
            @SerializedName("taxInNum")
            private long taxInNum;
            @SerializedName("totalAmount")
            private int totalAmount;
            @SerializedName("finalAmount")
            private int finalAmount;
            @SerializedName("finalDiscount")
            private String finalDiscount;
            @SerializedName("finalDiscountInNum")
            private int finalDiscountInNum;
            @SerializedName("notes")
            private String notes;
            @SerializedName("creationDate")
            private String creationDate;
            @SerializedName("salesOrderDate")
            private String salesOrderDate;
            @SerializedName("fulfilledTotalAmount")
            private int fulfilledTotalAmount;
            @SerializedName("fulfilledFinalAmount")
            private int fulfilledFinalAmount;
            @SerializedName("fulfilledFinalDiscount")
            private String fulfilledFinalDiscount;

            public String getSalesOrderId() {
                return salesOrderId;
            }

            public void setSalesOrderId(String salesOrderId) {
                this.salesOrderId = salesOrderId;
            }

            public String getClientId() {
                return clientId;
            }

            public void setClientId(String clientId) {
                this.clientId = clientId;
            }

            public String getCustomerId() {
                return customerId;
            }

            public void setCustomerId(String customerId) {
                this.customerId = customerId;
            }

            public String getSalesName() {
                return salesName;
            }

            public void setSalesName(String salesName) {
                this.salesName = salesName;
            }

            public String getExternalSoId() {
                return externalSoId;
            }

            public void setExternalSoId(String externalSoId) {
                this.externalSoId = externalSoId;
            }

            public String getTax() {
                return tax;
            }

            public void setTax(String tax) {
                this.tax = tax;
            }

            public long getTaxInNum() {
                return taxInNum;
            }

            public void setTaxInNum(long taxInNum) {
                this.taxInNum = taxInNum;
            }

            public int getTotalAmount() {
                return totalAmount;
            }

            public void setTotalAmount(int totalAmount) {
                this.totalAmount = totalAmount;
            }

            public int getFinalAmount() {
                return finalAmount;
            }

            public void setFinalAmount(int finalAmount) {
                this.finalAmount = finalAmount;
            }

            public String getFinalDiscount() {
                return finalDiscount;
            }

            public void setFinalDiscount(String finalDiscount) {
                this.finalDiscount = finalDiscount;
            }

            public int getFinalDiscountInNum() {
                return finalDiscountInNum;
            }

            public void setFinalDiscountInNum(int finalDiscountInNum) {
                this.finalDiscountInNum = finalDiscountInNum;
            }

            public String getNotes() {
                return notes;
            }

            public void setNotes(String notes) {
                this.notes = notes;
            }

            public String getCreationDate() {
                return creationDate;
            }

            public void setCreationDate(String creationDate) {
                this.creationDate = creationDate;
            }

            public String getSalesOrderDate() {
                return salesOrderDate;
            }

            public void setSalesOrderDate(String salesOrderDate) {
                this.salesOrderDate = salesOrderDate;
            }

            public int getFulfilledTotalAmount() {
                return fulfilledTotalAmount;
            }

            public void setFulfilledTotalAmount(int fulfilledTotalAmount) {
                this.fulfilledTotalAmount = fulfilledTotalAmount;
            }

            public int getFulfilledFinalAmount() {
                return fulfilledFinalAmount;
            }

            public void setFulfilledFinalAmount(int fulfilledFinalAmount) {
                this.fulfilledFinalAmount = fulfilledFinalAmount;
            }

            public String getFulfilledFinalDiscount() {
                return fulfilledFinalDiscount;
            }

            public void setFulfilledFinalDiscount(String fulfilledFinalDiscount) {
                this.fulfilledFinalDiscount = fulfilledFinalDiscount;
            }
        }

        public static class ItemsEntityX {
            @SerializedName("items")
            private List<ItemsEntity> items;

            public List<ItemsEntity> getItems() {
                return items;
            }

            public void setItems(List<ItemsEntity> items) {
                this.items = items;
            }

            public static class ItemsEntity {
                /**
                 * itemSKU : 1222
                 * quantity : 10
                 * fulfilledQuantity : 10
                 * measurementUnit : PCS
                 * fulfilledMeasurementUnit : -
                 * pricePerUnit : 2500
                 * fulfilledPricePerUnit : 0
                 * discount : 0
                 * discountInNum : 0
                 */

                @SerializedName("itemSKU")
                private String itemSKU;
                @SerializedName("quantity")
                private int quantity;
                @SerializedName("fulfilledQuantity")
                private int fulfilledQuantity;
                @SerializedName("measurementUnit")
                private String measurementUnit;
                @SerializedName("fulfilledMeasurementUnit")
                private String fulfilledMeasurementUnit;
                @SerializedName("pricePerUnit")
                private int pricePerUnit;
                @SerializedName("fulfilledPricePerUnit")
                private int fulfilledPricePerUnit;
                @SerializedName("discount")
                private String discount;
                @SerializedName("discountInNum")
                private int discountInNum;

                public String getItemSKU() {
                    return itemSKU;
                }

                public void setItemSKU(String itemSKU) {
                    this.itemSKU = itemSKU;
                }

                public int getQuantity() {
                    return quantity;
                }

                public void setQuantity(int quantity) {
                    this.quantity = quantity;
                }

                public int getFulfilledQuantity() {
                    return fulfilledQuantity;
                }

                public void setFulfilledQuantity(int fulfilledQuantity) {
                    this.fulfilledQuantity = fulfilledQuantity;
                }

                public String getMeasurementUnit() {
                    return measurementUnit;
                }

                public void setMeasurementUnit(String measurementUnit) {
                    this.measurementUnit = measurementUnit;
                }

                public String getFulfilledMeasurementUnit() {
                    return fulfilledMeasurementUnit;
                }

                public void setFulfilledMeasurementUnit(String fulfilledMeasurementUnit) {
                    this.fulfilledMeasurementUnit = fulfilledMeasurementUnit;
                }

                public int getPricePerUnit() {
                    return pricePerUnit;
                }

                public void setPricePerUnit(int pricePerUnit) {
                    this.pricePerUnit = pricePerUnit;
                }

                public int getFulfilledPricePerUnit() {
                    return fulfilledPricePerUnit;
                }

                public void setFulfilledPricePerUnit(int fulfilledPricePerUnit) {
                    this.fulfilledPricePerUnit = fulfilledPricePerUnit;
                }

                public String getDiscount() {
                    return discount;
                }

                public void setDiscount(String discount) {
                    this.discount = discount;
                }

                public int getDiscountInNum() {
                    return discountInNum;
                }

                public void setDiscountInNum(int discountInNum) {
                    this.discountInNum = discountInNum;
                }
            }
        }
    }
}
