package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notification implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * notification : [{"date":"14-05-2019","data":[{"title":"Berhasil membuat invoice dengan nomor INV/2019/05/SS-001/008","message":"Berhasil membuat invoice dengan nomor INV/2019/05/SS-001/008","instance":"INVOICE","transactionId":"INV/2019/05/SS-001/008"}]}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("notification")
    private List<NotificationEntity> notification;

    protected Notification(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<NotificationEntity> getNotification() {
        return notification;
    }

    public void setNotification(List<NotificationEntity> notification) {
        this.notification = notification;
    }

    public static class NotificationEntity {
        /**
         * date : 14-05-2019
         * data : [{"title":"Berhasil membuat invoice dengan nomor INV/2019/05/SS-001/008","message":"Berhasil membuat invoice dengan nomor INV/2019/05/SS-001/008","instance":"INVOICE","transactionId":"INV/2019/05/SS-001/008"}]
         */

        @SerializedName("date")
        private String date;
        @SerializedName("data")
        private List<DataEntity> data;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<DataEntity> getData() {
            return data;
        }

        public void setData(List<DataEntity> data) {
            this.data = data;
        }

        public static class DataEntity {
            /**
             * title : Berhasil membuat invoice dengan nomor INV/2019/05/SS-001/008
             * message : Berhasil membuat invoice dengan nomor INV/2019/05/SS-001/008
             * instance : INVOICE
             * transactionId : INV/2019/05/SS-001/008
             */

            @SerializedName("title")
            private String title;
            @SerializedName("message")
            private String message;
            @SerializedName("instance")
            private String instance;
            @SerializedName("transactionId")
            private String transactionId;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getInstance() {
                return instance;
            }

            public void setInstance(String instance) {
                this.instance = instance;
            }

            public String getTransactionId() {
                return transactionId;
            }

            public void setTransactionId(String transactionId) {
                this.transactionId = transactionId;
            }
        }
    }
}
