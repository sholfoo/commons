package com.miru.miruApps.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SoProduct implements Parcelable {

    String productSku;
    String productName;
    String productQty;
    String productUnit;
    String productSellingPrice;
    String productDiscount;
    String productAfterDiscount;
    String totalAmount;

    public SoProduct() {
    }

    public SoProduct(String productSku, String productName, String productQty, String productUnit, String productSellingPrice, String productDiscount, String productAfterDiscount, String totalAmount) {
        this.productSku = productSku;
        this.productName = productName;
        this.productQty = productQty;
        this.productUnit = productUnit;
        this.productSellingPrice = productSellingPrice;
        this.productDiscount = productDiscount;
        this.productAfterDiscount = productAfterDiscount;
        this.totalAmount = totalAmount;
    }

    protected SoProduct(Parcel in) {
        productSku = in.readString();
        productName = in.readString();
        productQty = in.readString();
        productUnit = in.readString();
        productSellingPrice = in.readString();
        productDiscount = in.readString();
        productAfterDiscount = in.readString();
        totalAmount = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productSku);
        dest.writeString(productName);
        dest.writeString(productQty);
        dest.writeString(productUnit);
        dest.writeString(productSellingPrice);
        dest.writeString(productDiscount);
        dest.writeString(productAfterDiscount);
        dest.writeString(totalAmount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SoProduct> CREATOR = new Creator<SoProduct>() {
        @Override
        public SoProduct createFromParcel(Parcel in) {
            return new SoProduct(in);
        }

        @Override
        public SoProduct[] newArray(int size) {
            return new SoProduct[size];
        }
    };

    public String getProductSku() {
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQty() {
        return productQty;
    }

    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public String getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(String productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getProductAfterDiscount() {
        return productAfterDiscount;
    }

    public void setProductAfterDiscount(String productAfterDiscount) {
        this.productAfterDiscount = productAfterDiscount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}
