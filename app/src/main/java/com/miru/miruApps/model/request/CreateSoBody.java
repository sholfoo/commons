package com.miru.miruApps.model.request;

public class CreateSoBody {

    String clientId;
    String customerId;
    String salesName;
    int tax;
    int totalAmount;
    String notes;
    String itemSKU;
    String quantity;
    String measurementUnit;
    String pricePerUnit;
    String discount;
    int finalDiscount;
    int finalAmount;
    String salesOrderDate;

    public CreateSoBody() {
    }

    public CreateSoBody(String clientId, String customerId, String salesName, int tax, int totalAmount, String notes, String itemSKU, String quantity, String measurementUnit, String pricePerUnit, String discount, int finalDiscount, int finalAmount, String salesOrderDate) {
        this.clientId = clientId;
        this.customerId = customerId;
        this.salesName = salesName;
        this.tax = tax;
        this.totalAmount = totalAmount;
        this.notes = notes;
        this.itemSKU = itemSKU;
        this.quantity = quantity;
        this.measurementUnit = measurementUnit;
        this.pricePerUnit = pricePerUnit;
        this.discount = discount;
        this.finalDiscount = finalDiscount;
        this.finalAmount = finalAmount;
        this.salesOrderDate = salesOrderDate;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public String getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(String pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public int getFinalDiscount() {
        return finalDiscount;
    }

    public void setFinalDiscount(int finalDiscount) {
        this.finalDiscount = finalDiscount;
    }

    public int getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(int finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getSalesOrderDate() {
        return salesOrderDate;
    }

    public void setSalesOrderDate(String salesOrderDate) {
        this.salesOrderDate = salesOrderDate;
    }
}
