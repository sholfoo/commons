package com.miru.miruApps.model.retrieve;

public class RetrieveSoInfoData {

    String customerId;
    String mSalesName;
    String mSoDate;
    String mDescription;

    public RetrieveSoInfoData(String customerId, String mSalesName, String mSoDate, String mDescription) {
        this.customerId = customerId;
        this.mSalesName = mSalesName;
        this.mSoDate = mSoDate;
        this.mDescription = mDescription;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getmSalesName() {
        return mSalesName;
    }

    public void setmSalesName(String mSalesName) {
        this.mSalesName = mSalesName;
    }

    public String getmSoDate() {
        return mSoDate;
    }

    public void setmSoDate(String mSoDate) {
        this.mSoDate = mSoDate;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
