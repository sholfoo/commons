package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailProduct implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * productContainer : [{"productId":"SS-001/11133","clientId":"SS-001","itemSKU":"11133","productName":"Tissue Paseo","category":"TISSUE","description":"tisue putih biasa","imageLinks":["http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg"],"imageProfile":"http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg","brand":"Paseo","size":"200 lembar","productPrices":[{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"LUSIN"},"pricePerUnit":300000,"sellingPrice":350000,"discount":10000},{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"PCS"},"pricePerUnit":15000,"sellingPrice":20000,"discount":1000}],"lastUpdatedTime":"14-06-2019 10:06:34.000"}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("productContainer")
    private List<ProductContainerEntity> productContainer;

    protected DetailProduct(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetailProduct> CREATOR = new Creator<DetailProduct>() {
        @Override
        public DetailProduct createFromParcel(Parcel in) {
            return new DetailProduct(in);
        }

        @Override
        public DetailProduct[] newArray(int size) {
            return new DetailProduct[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<ProductContainerEntity> getProductContainer() {
        return productContainer;
    }

    public void setProductContainer(List<ProductContainerEntity> productContainer) {
        this.productContainer = productContainer;
    }

    public static class ProductContainerEntity {
        /**
         * productId : SS-001/11133
         * clientId : SS-001
         * itemSKU : 11133
         * productName : Tissue Paseo
         * category : TISSUE
         * description : tisue putih biasa
         * imageLinks : ["http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg"]
         * imageProfile : http://35.240.148.132/images/productImages/SS-001/11133/SS-001-11133-1.jpeg
         * brand : Paseo
         * size : 200 lembar
         * productPrices : [{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"LUSIN"},"pricePerUnit":300000,"sellingPrice":350000,"discount":10000},{"identifierId":{"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"PCS"},"pricePerUnit":15000,"sellingPrice":20000,"discount":1000}]
         * lastUpdatedTime : 14-06-2019 10:06:34.000
         */

        @SerializedName("productId")
        private String productId;
        @SerializedName("clientId")
        private String clientId;
        @SerializedName("itemSKU")
        private String itemSKU;
        @SerializedName("productName")
        private String productName;
        @SerializedName("category")
        private String category;
        @SerializedName("description")
        private String description;
        @SerializedName("imageProfile")
        private String imageProfile;
        @SerializedName("brand")
        private String brand;
        @SerializedName("size")
        private String size;
        @SerializedName("lastUpdatedTime")
        private String lastUpdatedTime;
        @SerializedName("imageLinks")
        private List<String> imageLinks;
        @SerializedName("productPrices")
        private List<ProductPricesEntity> productPrices;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageProfile() {
            return imageProfile;
        }

        public void setImageProfile(String imageProfile) {
            this.imageProfile = imageProfile;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getLastUpdatedTime() {
            return lastUpdatedTime;
        }

        public void setLastUpdatedTime(String lastUpdatedTime) {
            this.lastUpdatedTime = lastUpdatedTime;
        }

        public List<String> getImageLinks() {
            return imageLinks;
        }

        public void setImageLinks(List<String> imageLinks) {
            this.imageLinks = imageLinks;
        }

        public List<ProductPricesEntity> getProductPrices() {
            return productPrices;
        }

        public void setProductPrices(List<ProductPricesEntity> productPrices) {
            this.productPrices = productPrices;
        }

        public static class ProductPricesEntity {
            /**
             * identifierId : {"productId":"SS-001/11133","itemSKU":"11133","measurementUnit":"LUSIN"}
             * pricePerUnit : 300000
             * sellingPrice : 350000
             * discount : 10000
             */

            @SerializedName("identifierId")
            private IdentifierIdEntity identifierId;
            @SerializedName("pricePerUnit")
            private int pricePerUnit;
            @SerializedName("sellingPrice")
            private int sellingPrice;
            @SerializedName("discount")
            private int discount;

            public IdentifierIdEntity getIdentifierId() {
                return identifierId;
            }

            public void setIdentifierId(IdentifierIdEntity identifierId) {
                this.identifierId = identifierId;
            }

            public int getPricePerUnit() {
                return pricePerUnit;
            }

            public void setPricePerUnit(int pricePerUnit) {
                this.pricePerUnit = pricePerUnit;
            }

            public int getSellingPrice() {
                return sellingPrice;
            }

            public void setSellingPrice(int sellingPrice) {
                this.sellingPrice = sellingPrice;
            }

            public int getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public static class IdentifierIdEntity {
                /**
                 * productId : SS-001/11133
                 * itemSKU : 11133
                 * measurementUnit : LUSIN
                 */

                @SerializedName("productId")
                private String productId;
                @SerializedName("itemSKU")
                private String itemSKU;
                @SerializedName("measurementUnit")
                private String measurementUnit;

                public String getProductId() {
                    return productId;
                }

                public void setProductId(String productId) {
                    this.productId = productId;
                }

                public String getItemSKU() {
                    return itemSKU;
                }

                public void setItemSKU(String itemSKU) {
                    this.itemSKU = itemSKU;
                }

                public String getMeasurementUnit() {
                    return measurementUnit;
                }

                public void setMeasurementUnit(String measurementUnit) {
                    this.measurementUnit = measurementUnit;
                }
            }
        }
    }
}
