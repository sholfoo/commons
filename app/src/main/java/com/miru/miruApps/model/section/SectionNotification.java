package com.miru.miruApps.model.section;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.miru.miruApps.model.response.Notification;

import java.util.List;

public class SectionNotification implements ParentListItem {
    String section;
    List<Notification.NotificationEntity.DataEntity> notificationContents;

    public SectionNotification(String section, List<Notification.NotificationEntity.DataEntity> notificationContents) {
        this.section = section;
        this.notificationContents = notificationContents;
    }

    public SectionNotification() {
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public List<Notification.NotificationEntity.DataEntity> getNotificationContents() {
        return notificationContents;
    }

    public void setNotificationContents(List<Notification.NotificationEntity.DataEntity> notificationContents) {
        this.notificationContents = notificationContents;
    }

    @Override
    public List<?> getChildItemList() {
        return notificationContents;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return true;
    }
}
