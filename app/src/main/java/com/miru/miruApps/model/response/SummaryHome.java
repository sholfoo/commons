package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SummaryHome implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * pendingOrders : {"numOfOrders":"4","avgOfOrders":"2633291","sumOfOrders":"10533165"}
     * overDueInvoices : {"avgOfInvoices":"659375","numOfInvoices":"8","sumOfInvoices":"5275000"}
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("pendingOrders")
    private PendingOrdersEntity pendingOrders;
    @SerializedName("overDueInvoices")
    private OverDueInvoicesEntity overDueInvoices;
    @SerializedName("errorMessage")
    private List<?> errorMessage;

    protected SummaryHome(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SummaryHome> CREATOR = new Creator<SummaryHome>() {
        @Override
        public SummaryHome createFromParcel(Parcel in) {
            return new SummaryHome(in);
        }

        @Override
        public SummaryHome[] newArray(int size) {
            return new SummaryHome[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public PendingOrdersEntity getPendingOrders() {
        return pendingOrders;
    }

    public void setPendingOrders(PendingOrdersEntity pendingOrders) {
        this.pendingOrders = pendingOrders;
    }

    public OverDueInvoicesEntity getOverDueInvoices() {
        return overDueInvoices;
    }

    public void setOverDueInvoices(OverDueInvoicesEntity overDueInvoices) {
        this.overDueInvoices = overDueInvoices;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static class PendingOrdersEntity {
        /**
         * numOfOrders : 4
         * avgOfOrders : 2633291
         * sumOfOrders : 10533165
         */

        @SerializedName("numOfOrders")
        private String numOfOrders;
        @SerializedName("avgOfOrders")
        private String avgOfOrders;
        @SerializedName("sumOfOrders")
        private String sumOfOrders;

        public String getNumOfOrders() {
            return numOfOrders;
        }

        public void setNumOfOrders(String numOfOrders) {
            this.numOfOrders = numOfOrders;
        }

        public String getAvgOfOrders() {
            return avgOfOrders;
        }

        public void setAvgOfOrders(String avgOfOrders) {
            this.avgOfOrders = avgOfOrders;
        }

        public String getSumOfOrders() {
            return sumOfOrders;
        }

        public void setSumOfOrders(String sumOfOrders) {
            this.sumOfOrders = sumOfOrders;
        }
    }

    public static class OverDueInvoicesEntity {
        /**
         * avgOfInvoices : 659375
         * numOfInvoices : 8
         * sumOfInvoices : 5275000
         */

        @SerializedName("avgOfInvoices")
        private String avgOfInvoices;
        @SerializedName("numOfInvoices")
        private String numOfInvoices;
        @SerializedName("sumOfInvoices")
        private String sumOfInvoices;

        public String getAvgOfInvoices() {
            return avgOfInvoices;
        }

        public void setAvgOfInvoices(String avgOfInvoices) {
            this.avgOfInvoices = avgOfInvoices;
        }

        public String getNumOfInvoices() {
            return numOfInvoices;
        }

        public void setNumOfInvoices(String numOfInvoices) {
            this.numOfInvoices = numOfInvoices;
        }

        public String getSumOfInvoices() {
            return sumOfInvoices;
        }

        public void setSumOfInvoices(String sumOfInvoices) {
            this.sumOfInvoices = sumOfInvoices;
        }
    }
}
