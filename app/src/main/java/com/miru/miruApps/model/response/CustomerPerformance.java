package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerPerformance implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * summary : {"numOfUnpaidInvoices":"0","avgOfInvoices":"500000","creditLimit":"0","sumOfUnpaidAmount":"500000","numOfInvoices":"1","paymentStatus":"Tidak Lancar"}
     * monthly : [{"month":"05-2019","sumOfFinalAmount":"500000","numOfInvoices":"1"}]
     * products : [{"images":[],"itemSKU":"RET222","itemName":"Kemeja","unit":"PCS","totalQty":4,"totalAmount":12000},{"images":[],"itemSKU":"ABC123","itemName":"Baju","unit":"PCS","totalQty":10,"totalAmount":90000},{"images":[],"itemSKU":"BAC234","itemName":"Celana","unit":"PCS","totalQty":2,"totalAmount":4000}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("summary")
    private SummaryEntity summary;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("monthly")
    private List<MonthlyEntity> monthly;
    @SerializedName("products")
    private List<ProductsEntity> products;

    protected CustomerPerformance(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CustomerPerformance> CREATOR = new Creator<CustomerPerformance>() {
        @Override
        public CustomerPerformance createFromParcel(Parcel in) {
            return new CustomerPerformance(in);
        }

        @Override
        public CustomerPerformance[] newArray(int size) {
            return new CustomerPerformance[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public SummaryEntity getSummary() {
        return summary;
    }

    public void setSummary(SummaryEntity summary) {
        this.summary = summary;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<MonthlyEntity> getMonthly() {
        return monthly;
    }

    public void setMonthly(List<MonthlyEntity> monthly) {
        this.monthly = monthly;
    }

    public List<ProductsEntity> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsEntity> products) {
        this.products = products;
    }

    public static class SummaryEntity {
        /**
         * numOfUnpaidInvoices : 0
         * avgOfInvoices : 500000
         * creditLimit : 0
         * sumOfUnpaidAmount : 500000
         * numOfInvoices : 1
         * paymentStatus : Tidak Lancar
         */

        @SerializedName("numOfUnpaidInvoices")
        private String numOfUnpaidInvoices;
        @SerializedName("avgOfInvoices")
        private String avgOfInvoices;
        @SerializedName("creditLimit")
        private String creditLimit;
        @SerializedName("sumOfUnpaidAmount")
        private String sumOfUnpaidAmount;
        @SerializedName("numOfInvoices")
        private String numOfInvoices;
        @SerializedName("paymentStatus")
        private String paymentStatus;

        public String getNumOfUnpaidInvoices() {
            return numOfUnpaidInvoices;
        }

        public void setNumOfUnpaidInvoices(String numOfUnpaidInvoices) {
            this.numOfUnpaidInvoices = numOfUnpaidInvoices;
        }

        public String getAvgOfInvoices() {
            return avgOfInvoices;
        }

        public void setAvgOfInvoices(String avgOfInvoices) {
            this.avgOfInvoices = avgOfInvoices;
        }

        public String getCreditLimit() {
            return creditLimit;
        }

        public void setCreditLimit(String creditLimit) {
            this.creditLimit = creditLimit;
        }

        public String getSumOfUnpaidAmount() {
            return sumOfUnpaidAmount;
        }

        public void setSumOfUnpaidAmount(String sumOfUnpaidAmount) {
            this.sumOfUnpaidAmount = sumOfUnpaidAmount;
        }

        public String getNumOfInvoices() {
            return numOfInvoices;
        }

        public void setNumOfInvoices(String numOfInvoices) {
            this.numOfInvoices = numOfInvoices;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }
    }

    public static class MonthlyEntity {
        /**
         * month : 05-2019
         * sumOfFinalAmount : 500000
         * numOfInvoices : 1
         */

        @SerializedName("month")
        private String month;
        @SerializedName("sumOfFinalAmount")
        private String sumOfFinalAmount;
        @SerializedName("numOfInvoices")
        private String numOfInvoices;

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getSumOfFinalAmount() {
            return sumOfFinalAmount;
        }

        public void setSumOfFinalAmount(String sumOfFinalAmount) {
            this.sumOfFinalAmount = sumOfFinalAmount;
        }

        public String getNumOfInvoices() {
            return numOfInvoices;
        }

        public void setNumOfInvoices(String numOfInvoices) {
            this.numOfInvoices = numOfInvoices;
        }
    }

    public static class ProductsEntity {
        /**
         * images : []
         * itemSKU : RET222
         * itemName : Kemeja
         * unit : PCS
         * totalQty : 4
         * totalAmount : 12000
         */

        @SerializedName("itemSKU")
        private String itemSKU;
        @SerializedName("itemName")
        private String itemName;
        @SerializedName("unit")
        private String unit;
        @SerializedName("totalQty")
        private int totalQty;
        @SerializedName("totalAmount")
        private int totalAmount;
        @SerializedName("images")
        private List<?> images;

        public String getItemSKU() {
            return itemSKU;
        }

        public void setItemSKU(String itemSKU) {
            this.itemSKU = itemSKU;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public int getTotalQty() {
            return totalQty;
        }

        public void setTotalQty(int totalQty) {
            this.totalQty = totalQty;
        }

        public int getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(int totalAmount) {
            this.totalAmount = totalAmount;
        }

        public List<?> getImages() {
            return images;
        }

        public void setImages(List<?> images) {
            this.images = images;
        }
    }
}
