package com.miru.miruApps.model;

public class ProductPerformance {

    String productImageUrl;
    String productName;
    int productAmount;

    public ProductPerformance(String productImageUrl, String productName, int productAmount) {
        this.productImageUrl = productImageUrl;
        this.productName = productName;
        this.productAmount = productAmount;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }
}
