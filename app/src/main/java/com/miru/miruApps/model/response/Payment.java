package com.miru.miruApps.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Payment implements Parcelable {

    /**
     * statusCode : 201
     * statusMessage : OK
     * errorMessage : []
     * payment : [{"paymentId":"PAY201906TT-001001-001","extPaymentId":"PAY/2019/06/TT-001/001-001","clientId":"TT-001","customerId":"TT-001-CST-OO-001","invoiceId":"INV201906TT-001001","extInvoiceId":"INV/2019/06/TT-001/001","paymentAmount":20000,"remainingAmount":180000,"paymentMethod":"CSH","paymentDate":"26-06-2019","notes":"hhh","docUrlPath":"/TT-001/PAY/PAY201906TT-001001-001.pdf","customerName":"Erlina Linn","customerAddress":"null","customerCity":"","customerZipCode":"","customerPhoneNumber":"null","customerStoreName":"Oriental","invoiceDueDate":"22-07-2019"},{"paymentId":"PAY201906TT-001001-002","extPaymentId":"PAY/2019/06/TT-001/001-002","clientId":"TT-001","customerId":"TT-001-CST-OO-001","invoiceId":"INV201906TT-001001","extInvoiceId":"INV/2019/06/TT-001/001","paymentAmount":50000,"remainingAmount":150000,"paymentMethod":"TRF","paymentDate":"22-07-2019","notes":"yitkg","docUrlPath":"/TT-001/PAY/PAY201906TT-001001-002.pdf","customerName":"Erlina Linn","customerAddress":"null","customerCity":"","customerZipCode":"","customerPhoneNumber":"null","customerStoreName":"Erlina Linn","invoiceDueDate":"22-07-2019"},{"paymentId":"PAY201906TT-001003-001","extPaymentId":"PAY/2019/06/TT-001/003-001","clientId":"TT-001","customerId":"TT-001-CST-CA-001","invoiceId":"INV201906TT-001003","extInvoiceId":"INV/2019/06/TT-001/003","paymentAmount":100000,"remainingAmount":200000,"paymentMethod":"CSH","paymentDate":"23-07-2019","notes":"","docUrlPath":"/TT-001/PAY/PAY201906TT-001003-001.pdf","customerName":"Co Hive","customerAddress":"null","customerCity":"","customerZipCode":"","customerPhoneNumber":"08123456789","customerStoreName":"Cohive Apl","invoiceDueDate":"22-07-2019"}]
     */

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("errorMessage")
    private List<?> errorMessage;
    @SerializedName("payment")
    private List<PaymentEntity> payment;

    protected Payment(Parcel in) {
        statusCode = in.readInt();
        statusMessage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(statusCode);
        dest.writeString(statusMessage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Payment> CREATOR = new Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel in) {
            return new Payment(in);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<?> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(List<?> errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<PaymentEntity> getPayment() {
        return payment;
    }

    public void setPayment(List<PaymentEntity> payment) {
        this.payment = payment;
    }

    public static class PaymentEntity {
        /**
         * paymentId : PAY201906TT-001001-001
         * extPaymentId : PAY/2019/06/TT-001/001-001
         * clientId : TT-001
         * customerId : TT-001-CST-OO-001
         * invoiceId : INV201906TT-001001
         * extInvoiceId : INV/2019/06/TT-001/001
         * paymentAmount : 20000
         * remainingAmount : 180000
         * paymentMethod : CSH
         * paymentDate : 26-06-2019
         * notes : hhh
         * docUrlPath : /TT-001/PAY/PAY201906TT-001001-001.pdf
         * customerName : Erlina Linn
         * customerAddress : null
         * customerCity :
         * customerZipCode :
         * customerPhoneNumber : null
         * customerStoreName : Oriental
         * invoiceDueDate : 22-07-2019
         */

        @SerializedName("paymentId")
        private String paymentId;
        @SerializedName("extPaymentId")
        private String extPaymentId;
        @SerializedName("clientId")
        private String clientId;
        @SerializedName("customerId")
        private String customerId;
        @SerializedName("invoiceId")
        private String invoiceId;
        @SerializedName("extInvoiceId")
        private String extInvoiceId;
        @SerializedName("paymentAmount")
        private int paymentAmount;
        @SerializedName("remainingAmount")
        private int remainingAmount;
        @SerializedName("paymentMethod")
        private String paymentMethod;
        @SerializedName("paymentDate")
        private String paymentDate;
        @SerializedName("notes")
        private String notes;
        @SerializedName("docUrlPath")
        private String docUrlPath;
        @SerializedName("customerName")
        private String customerName;
        @SerializedName("customerAddress")
        private String customerAddress;
        @SerializedName("customerCity")
        private String customerCity;
        @SerializedName("customerZipCode")
        private String customerZipCode;
        @SerializedName("customerPhoneNumber")
        private String customerPhoneNumber;
        @SerializedName("customerStoreName")
        private String customerStoreName;
        @SerializedName("invoiceDueDate")
        private String invoiceDueDate;

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

        public String getExtPaymentId() {
            return extPaymentId;
        }

        public void setExtPaymentId(String extPaymentId) {
            this.extPaymentId = extPaymentId;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getInvoiceId() {
            return invoiceId;
        }

        public void setInvoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
        }

        public String getExtInvoiceId() {
            return extInvoiceId;
        }

        public void setExtInvoiceId(String extInvoiceId) {
            this.extInvoiceId = extInvoiceId;
        }

        public int getPaymentAmount() {
            return paymentAmount;
        }

        public void setPaymentAmount(int paymentAmount) {
            this.paymentAmount = paymentAmount;
        }

        public int getRemainingAmount() {
            return remainingAmount;
        }

        public void setRemainingAmount(int remainingAmount) {
            this.remainingAmount = remainingAmount;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getPaymentDate() {
            return paymentDate;
        }

        public void setPaymentDate(String paymentDate) {
            this.paymentDate = paymentDate;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getDocUrlPath() {
            return docUrlPath;
        }

        public void setDocUrlPath(String docUrlPath) {
            this.docUrlPath = docUrlPath;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerAddress() {
            return customerAddress;
        }

        public void setCustomerAddress(String customerAddress) {
            this.customerAddress = customerAddress;
        }

        public String getCustomerCity() {
            return customerCity;
        }

        public void setCustomerCity(String customerCity) {
            this.customerCity = customerCity;
        }

        public String getCustomerZipCode() {
            return customerZipCode;
        }

        public void setCustomerZipCode(String customerZipCode) {
            this.customerZipCode = customerZipCode;
        }

        public String getCustomerPhoneNumber() {
            return customerPhoneNumber;
        }

        public void setCustomerPhoneNumber(String customerPhoneNumber) {
            this.customerPhoneNumber = customerPhoneNumber;
        }

        public String getCustomerStoreName() {
            return customerStoreName;
        }

        public void setCustomerStoreName(String customerStoreName) {
            this.customerStoreName = customerStoreName;
        }

        public String getInvoiceDueDate() {
            return invoiceDueDate;
        }

        public void setInvoiceDueDate(String invoiceDueDate) {
            this.invoiceDueDate = invoiceDueDate;
        }
    }
}
