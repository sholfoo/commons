package com.miru.miruApps.model.retrieve;

import java.io.File;
import java.util.ArrayList;

public class RetrieveProductData {
    ArrayList<File> images;
    String productSku;
    String productBrand;
    String productName;
    String productKategory;
    String productDescription;
    String productSize;

    public RetrieveProductData(ArrayList<File> images, String productSku, String productBrand, String productName, String productKategory, String productDescription,String productSize) {
        this.images = images;
        this.productSku = productSku;
        this.productBrand = productBrand;
        this.productName = productName;
        this.productKategory = productKategory;
        this.productDescription = productDescription;
        this.productSize = productSize;
    }

    public ArrayList<File> getImages() {
        return images;
    }

    public void setImages(ArrayList<File> images) {
        this.images = images;
    }

    public String getProductSku() {
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductKategory() {
        return productKategory;
    }

    public void setProductKategory(String productKategory) {
        this.productKategory = productKategory;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }
}
